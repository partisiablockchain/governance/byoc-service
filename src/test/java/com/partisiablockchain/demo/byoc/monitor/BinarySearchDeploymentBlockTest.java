package com.partisiablockchain.demo.byoc.monitor;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import java.math.BigInteger;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatcher;
import org.mockito.Mockito;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.core.DefaultBlockParameterNumber;
import org.web3j.protocol.core.Request;
import org.web3j.protocol.core.methods.response.EthGetCode;

/** Test deployment block lookup using binary search. */
public final class BinarySearchDeploymentBlockTest {

  private static final String ethContract = "ethereum contract address";
  private static final BigInteger DEPLOYED_IN_BLOCK = BigInteger.valueOf(14_873_415L);
  private static final BigInteger LATEST_BLOCK = BigInteger.valueOf(18_669_571L);
  private final Web3j web3jMock = Mockito.mock(Web3j.class);

  Ethereum.BinarySearchDeploymentBlock subject =
      new Ethereum.BinarySearchDeploymentBlock(web3jMock, ethContract);

  @BeforeEach
  void setUp() {
    Mockito.when(web3jMock.ethBlockNumber())
        .thenReturn(new EthBlockNumberRequestStub<>(LATEST_BLOCK.longValue()));
    Mockito.when(
            web3jMock.ethGetCode(
                Mockito.eq(ethContract),
                Mockito.argThat(new GreaterOrEqualBlock(DEPLOYED_IN_BLOCK))))
        .thenReturn(new EthCodeRequest<>("001122"));
    Mockito.when(
            web3jMock.ethGetCode(
                Mockito.eq(ethContract), Mockito.argThat(new LessThanBlock(DEPLOYED_IN_BLOCK))))
        .thenReturn(new EthCodeRequest<>(""));
  }

  @Test
  void findDeploymentBlock() {
    Assertions.assertThat(subject.deploymentBlock()).isEqualTo(DEPLOYED_IN_BLOCK);
  }

  @Test
  void deploymentBlockIsCachedWhenFound() {
    subject.deploymentBlock();
    Mockito.when(web3jMock.ethGetCode(Mockito.any(), Mockito.any()))
        .thenThrow(new RuntimeException("Should not be called!"));
    Assertions.assertThat(subject.deploymentBlock()).isEqualTo(DEPLOYED_IN_BLOCK);
  }

  @Test
  void blockSearchRangeIsCachedSoSearchProgressIsSaved() {
    Mockito.when(
            web3jMock.ethGetCode(
                Mockito.eq(ethContract),
                Mockito.argThat(new GreaterOrEqualBlock(BigInteger.valueOf(14_875_036L)))))
        .thenThrow(new RuntimeException("Rate limited by RPC provider!"))
        .thenReturn(new EthCodeRequest<>("001122"));
    Assertions.assertThatThrownBy(subject::deploymentBlock)
        .isInstanceOf(RuntimeException.class)
        .hasMessage(
            "Failed to get contract code [contract='ethereum contract address',"
                + " lowBlock='14002179', middleBlock='16335875', highBlock='18669571']");
    Assertions.assertThat(subject.deploymentBlock()).isEqualTo(DEPLOYED_IN_BLOCK);
  }

  @Test
  void unableTopFindDeploymentOfUnknownContract() {
    String anotherContract = "another ethereum contract address";
    Mockito.when(web3jMock.ethGetCode(Mockito.eq(anotherContract), Mockito.any()))
        .thenReturn(new EthCodeRequest<>(""));
    Ethereum.BinarySearchDeploymentBlock newSubject =
        new Ethereum.BinarySearchDeploymentBlock(web3jMock, anotherContract);
    Assertions.assertThatThrownBy(newSubject::deploymentBlock)
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Failed to find deployment block for 'another ethereum contract address'");
  }

  static final class EthCodeRequest<S> extends Request<S, EthGetCode> {

    private final String code;

    EthCodeRequest(String code) {
      this.code = code;
    }

    @Override
    public EthGetCode send() {
      EthGetCode ethCode = new EthGetCode();
      ethCode.setResult("0x" + code);
      return ethCode;
    }
  }

  static final class GreaterOrEqualBlock implements ArgumentMatcher<DefaultBlockParameterNumber> {

    private final BigInteger value;

    GreaterOrEqualBlock(BigInteger value) {
      this.value = value;
    }

    @Override
    public boolean matches(DefaultBlockParameterNumber argument) {
      return argument.getBlockNumber().compareTo(value) >= 0;
    }
  }

  static final class LessThanBlock implements ArgumentMatcher<DefaultBlockParameterNumber> {

    private final BigInteger value;

    LessThanBlock(BigInteger value) {
      this.value = value;
    }

    @Override
    public boolean matches(DefaultBlockParameterNumber argument) {
      return argument.getBlockNumber().compareTo(value) < 0;
    }
  }
}
