package com.partisiablockchain.demo.byoc.monitor;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.KeyPair;
import com.partisiablockchain.governance.ThresholdSessionId;
import java.math.BigInteger;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test. */
public final class InMemoryMessageStorageTest {

  @Test
  void getUnicastMessages() {
    InMemoryMessageStorage storage = new InMemoryMessageStorage();
    ThresholdSessionId sessionId = new ThresholdSessionId(1, 2);
    BlockchainPublicKey publicKey = new KeyPair(BigInteger.valueOf(123)).getPublic();
    byte[] roundUnicastMessage = storage.getRoundUnicastMessage(sessionId, 42, publicKey);
    Assertions.assertThat(roundUnicastMessage).isNull();
  }

  @Test
  void getMyBroadcastMessages() {
    InMemoryMessageStorage storage = new InMemoryMessageStorage();
    ThresholdSessionId sessionId = new ThresholdSessionId(1, 2);
    Hash roundUnicastMessage = storage.getMyBroadcastMessageHash(sessionId, 42);
    Assertions.assertThat(roundUnicastMessage).isNull();
  }
}
