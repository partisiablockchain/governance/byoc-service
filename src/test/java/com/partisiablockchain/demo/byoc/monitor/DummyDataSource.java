package com.partisiablockchain.demo.byoc.monitor;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.demo.byoc.governance.byocincoming.DepositTransaction;
import com.partisiablockchain.demo.byoc.governance.byocoutgoing.WithdrawalTransaction;
import com.partisiablockchain.demo.byoc.rest.dto.DepositDto;
import com.secata.tools.coverage.ExceptionConverter;
import java.util.Collections;
import java.util.List;
import java.util.NavigableMap;
import java.util.TreeMap;
import java.util.function.LongSupplier;

/** Class mimicking the Ethereum blockchain as a DataSource. */
public final class DummyDataSource implements DataSource {

  private final NavigableMap<Long, DepositDto> deposits;
  private final LongSupplier initialNonceLookup;

  DummyDataSource(LongSupplier initialNonceLookup) {
    this.initialNonceLookup = initialNonceLookup;
    this.deposits = Collections.synchronizedNavigableMap(new TreeMap<>());
  }

  @Override
  public synchronized DepositDto waitForDeposit(long depositNonce) {
    while (deposits.get(depositNonce) == null) {
      ExceptionConverter.run(this::wait, "Wait interrupted");
    }
    return deposits.get(depositNonce);
  }

  @Override
  public DepositTransaction getDeposit(long depositNonce) {
    return null;
  }

  @Override
  public WithdrawalTransaction getWithdrawal(WithdrawalId id) {
    return null;
  }

  @Override
  public List<WithdrawalTransaction> getAllWithdrawalsForOracleNonce(long oracleNonce) {
    return List.of();
  }

  /**
   * Add deposit request.
   *
   * @param depositDto dto containing deposit information
   */
  public synchronized void addDeposit(DepositDto depositDto) {
    long index = deposits.isEmpty() ? initialNonceLookup.getAsLong() : deposits.lastKey() + 1L;
    deposits.put(index, depositDto);
    this.notifyAll();
  }
}
