package com.partisiablockchain.demo.byoc.monitor;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static java.lang.Thread.sleep;
import static org.assertj.core.api.Assertions.assertThat;

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.client.transaction.BlockchainTransactionClient;
import com.partisiablockchain.client.transaction.SenderAuthenticationKeyPair;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.KeyPair;
import com.partisiablockchain.crypto.Signature;
import com.partisiablockchain.demo.byoc.governance.byocoutgoing.ByocOutgoingContractState;
import com.partisiablockchain.demo.byoc.governance.byocoutgoing.Epoch;
import com.partisiablockchain.demo.byoc.governance.byocoutgoing.EthereumAddress;
import com.partisiablockchain.demo.byoc.governance.byocoutgoing.OracleMember;
import com.partisiablockchain.demo.byoc.governance.byocoutgoing.OutgoingHelper;
import com.partisiablockchain.math.Unsigned256;
import com.partisiablockchain.serialization.StateLong;
import com.partisiablockchain.server.tcp.client.Client;
import com.partisiablockchain.tree.AvlTree;
import com.secata.stream.SafeDataInputStream;
import com.secata.tools.immutable.FixedList;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/** Tests. */
public final class WithdrawalProcessTest {
  private final EthereumAddress ethByocContract =
      OutgoingHelper.ethereumAddress("0000000000000000000000000000000000000000");
  private final EthereumAddress ethReceiver =
      OutgoingHelper.ethereumAddress("0000000000000000000000000000000000000001");
  private static final Unsigned256 amount = Unsigned256.create(10_000L);
  private final List<KeyPair> oracleKeys =
      List.of(new KeyPair(BigInteger.ONE), new KeyPair(BigInteger.TWO));
  private final List<BlockchainPublicKey> oraclePubKeys =
      oracleKeys.stream().map(KeyPair::getPublic).toList();
  private final List<KeyPair> pbcAccountKeys =
      List.of(new KeyPair(new BigInteger("DD", 16)), new KeyPair(new BigInteger("EE", 16)));
  private final List<BlockchainAddress> oracleAccounts =
      pbcAccountKeys.stream().map(k -> k.getPublic().createAddress()).collect(Collectors.toList());
  private final List<OracleMember> oracleMembers = createMembers(oracleAccounts);
  private final KeyPair unknownProducerKey = new KeyPair(BigInteger.TEN);
  private final KeyPair unknownPbcKey = new KeyPair(new BigInteger("FF", 16));
  private final BlockchainAddress withdrawalContract =
      BlockchainAddress.fromString("010000000000000000000000000000000000123456");
  private static final String shard1 = "s1";
  private static final String shard2 = "s2";
  private final DummyPbcClient<ByocOutgoingContractState> pbcClient1 =
      DummyPbcClient.forByocOutgoing(ethByocContract, oracleMembers);
  private final DummyPbcClient<ByocOutgoingContractState> pbcClient2 =
      DummyPbcClient.forByocOutgoing(ethByocContract, oracleMembers);
  private static final int timeout = 100;

  private WithdrawalProcess process;
  private WithdrawalProcess withdrawalProcess1;

  private final ByocOutgoingContractState dummyOutgoingState =
      new ByocOutgoingContractState(
          ethByocContract,
          0,
          0,
          0,
          AvlTree.<Long, Epoch>create()
              .set(
                  0L,
                  new Epoch(AvlTree.create(), AvlTree.create(), FixedList.create(oracleMembers))));

  private final ByocTransactionSender senderKey0 =
      (trx, cost) -> {
        BlockchainTransactionClient transactionClient =
            BlockchainTransactionClient.create(
                pbcClient1,
                new SenderAuthenticationKeyPair(pbcAccountKeys.get(0)),
                timeout,
                timeout);
        transactionClient.waitForSpawnedEvents(transactionClient.signAndSend(trx, 100_000));
      };

  private final ByocTransactionSender senderKey1 =
      (trx, cost) -> {
        BlockchainTransactionClient transactionClient =
            BlockchainTransactionClient.create(
                pbcClient2,
                new SenderAuthenticationKeyPair(pbcAccountKeys.get(1)),
                timeout,
                timeout);
        transactionClient.waitForSpawnedEvents(transactionClient.signAndSend(trx, 100_000));
      };

  private final ContractStateHandler contractStateHandler =
      new ContractStateHandler(List.of(withdrawalContract));
  private Client.Listener contractListener;

  /** Create default listeners. */
  @BeforeEach
  public void setup() {
    createListener();
  }

  @Test
  public void singlePendingWithdrawTransaction() {
    addDefaultExecutedTransactions(1);
    process =
        new WithdrawalProcess(
            withdrawalContract,
            oracleKeys.get(0),
            pbcAccountKeys.get(0).getPublic().createAddress(),
            10,
            senderKey0,
            contractStateHandler);
    withdrawalProcess1 =
        new WithdrawalProcess(
            withdrawalContract,
            oracleKeys.get(1),
            pbcAccountKeys.get(1).getPublic().createAddress(),
            10,
            senderKey1,
            contractStateHandler);

    ByocOutgoingContractState state =
        (ByocOutgoingContractState) contractStateHandler.contractStates.get(withdrawalContract);
    state = OutgoingHelper.createPendingWithdrawal(state, ethReceiver, amount);
    contractListener.update(state);

    ByocProcessTestHelper.TransactionRpc result =
        ByocProcessTestHelper.runProcess(
            process, withdrawalProcess1, pbcClient1, pbcClient2, oracleAccounts);
    assertThat(result.rpc1).isNotEqualTo(result.rpc2);
    Hash msg =
        messageDigest(
            0L,
            state
                .epochs()
                .getValue(state.oracleNonce())
                .withdrawals()
                .getValue(0L)
                .amountWithoutTax());

    SafeDataInputStream stream = SafeDataInputStream.createFromBytes(result.rpc1);
    assertThat(stream.readUnsignedByte()).isEqualTo(1);
    assertThat(stream.readLong()).isEqualTo(0L);
    assertThat(stream.readLong()).isEqualTo(0L);
    assertThat(Signature.read(stream).recoverPublicKey(msg)).isEqualTo(oraclePubKeys.get(0));

    stream = SafeDataInputStream.createFromBytes(result.rpc2);
    assertThat(stream.readUnsignedByte()).isEqualTo(1);
    assertThat(stream.readLong()).isEqualTo(0L);
    assertThat(stream.readLong()).isEqualTo(0L);
    assertThat(Signature.read(stream).recoverPublicKey(msg)).isEqualTo(oraclePubKeys.get(1));
  }

  @Test
  public void multiplePendingWithdrawTransaction() {
    addDefaultExecutedTransactions(4);
    process =
        new WithdrawalProcess(
            withdrawalContract,
            oracleKeys.get(0),
            pbcAccountKeys.get(0).getPublic().createAddress(),
            10,
            senderKey0,
            contractStateHandler);
    withdrawalProcess1 =
        new WithdrawalProcess(
            withdrawalContract,
            oracleKeys.get(1),
            pbcAccountKeys.get(1).getPublic().createAddress(),
            10,
            senderKey1,
            contractStateHandler);

    ByocOutgoingContractState state =
        (ByocOutgoingContractState) contractStateHandler.contractStates.get(withdrawalContract);
    long withdrawalNonce = state.withdrawNonce();
    state = OutgoingHelper.createPendingWithdrawal(state, ethReceiver, amount);
    assertThat(withdrawalNonce + 1).isEqualTo(state.withdrawNonce());
    state =
        OutgoingHelper.createPendingWithdrawal(
            state, ethReceiver, amount.add(Unsigned256.create(111)));
    state =
        OutgoingHelper.createPendingWithdrawal(
            state, ethReceiver, amount.add(Unsigned256.create(222)));
    state =
        OutgoingHelper.createPendingWithdrawal(
            state, ethReceiver, amount.add(Unsigned256.create(333)));
    contractListener.update(state);

    ByocProcessTestHelper.TransactionRpc result =
        ByocProcessTestHelper.runProcess(
            process, withdrawalProcess1, pbcClient1, pbcClient2, oracleAccounts);
    assertThat(result.rpc1).isNotEqualTo(result.rpc2);

    int txCount1 = pbcClient1.getTransactionsCount();
    assertThat(txCount1).isEqualTo(4);
    int txCount2 = pbcClient2.getTransactionsCount();
    assertThat(txCount2).isEqualTo(4);

    Hash msg =
        messageDigest(
            3L,
            state
                .epochs()
                .getValue(state.oracleNonce())
                .withdrawals()
                .getValue(3L)
                .amountWithoutTax());

    SafeDataInputStream stream = SafeDataInputStream.createFromBytes(result.rpc1);
    assertThat(stream.readUnsignedByte()).isEqualTo(1);
    assertThat(stream.readLong()).isEqualTo(0L);
    assertThat(stream.readLong()).isEqualTo(3L);
    BlockchainPublicKey sender = Signature.read(stream).recoverPublicKey(msg);
    assertThat(sender).isEqualTo(oraclePubKeys.get(0));

    stream = SafeDataInputStream.createFromBytes(result.rpc2);
    assertThat(stream.readUnsignedByte()).isEqualTo(1);
    assertThat(stream.readLong()).isEqualTo(0L);
    assertThat(stream.readLong()).isEqualTo(3L);
    assertThat(Signature.read(stream).recoverPublicKey(msg)).isEqualTo(oraclePubKeys.get(1));
  }

  @Test
  public void processDoesNotRunWithoutContract() {
    process =
        new WithdrawalProcess(
            withdrawalContract,
            oracleKeys.get(0),
            pbcAccountKeys.get(0).getPublic().createAddress(),
            10,
            senderKey0,
            contractStateHandler);
    contractListener.remove();
    // contractStateHandler has not created a contract yet, so run() should bail out early
    assertThat(process.run()).isFalse();
  }

  @Test
  public void oracleWillNotSignWithdrawalTwice() {
    addDefaultExecutedTransactions(1);
    process =
        new WithdrawalProcess(
            withdrawalContract,
            oracleKeys.get(0),
            pbcAccountKeys.get(0).getPublic().createAddress(),
            10,
            senderKey0,
            contractStateHandler);
    withdrawalProcess1 =
        new WithdrawalProcess(
            withdrawalContract,
            oracleKeys.get(1),
            pbcAccountKeys.get(1).getPublic().createAddress(),
            10,
            senderKey1,
            contractStateHandler);

    ByocOutgoingContractState state =
        (ByocOutgoingContractState) contractStateHandler.contractStates.get(withdrawalContract);
    Hash messageDigest = messageDigest(0L, amount);
    state = OutgoingHelper.createPendingWithdrawal(state, ethReceiver, amount);
    state = OutgoingHelper.signWithdrawal(state, 0, pbcAccountKeys.get(0), messageDigest);
    contractListener.update(state);

    process.run();
    withdrawalProcess1.run();

    assertThat(pbcClient1.getTransactionsCount()).isEqualTo(0);
    assertThat(pbcClient2.getTransactionsCount()).isEqualTo(1);
  }

  @Test
  public void oracleUnknownToContractWillNotSign() {
    addDefaultExecutedTransactions(1);
    process =
        new WithdrawalProcess(
            withdrawalContract,
            unknownProducerKey,
            unknownPbcKey.getPublic().createAddress(),
            10,
            senderKey0,
            contractStateHandler);

    ByocOutgoingContractState state =
        (ByocOutgoingContractState) contractStateHandler.contractStates.get(withdrawalContract);
    state = OutgoingHelper.createPendingWithdrawal(state, ethReceiver, amount);
    contractListener.update(state);

    assertThat(process.run()).isFalse();

    assertThat(pbcClient1.getTransactionsCount()).isEqualTo(0);
  }

  @Test
  public void processWaitsIfNoChange() throws InterruptedException {
    addDefaultExecutedTransactions(1);
    process =
        new WithdrawalProcess(
            withdrawalContract,
            unknownProducerKey,
            unknownPbcKey.getPublic().createAddress(),
            10,
            senderKey0,
            contractStateHandler);
    Thread thread =
        new Thread(
            () -> {
              process.run();
              process.run();
            });
    thread.start();
    sleep(300);
    assertThat(thread.getState().toString()).isEqualTo("WAITING");
  }

  @Test
  void processOnlyRunsOnRelevantContractChanges() throws InterruptedException {
    BlockchainAddress dummyContract =
        BlockchainAddress.fromHash(
            BlockchainAddress.Type.CONTRACT_GOV, Hash.create(h -> h.writeString("outgoing")));
    ContractStateHandler contractStateHandler =
        new ContractStateHandler(List.of(withdrawalContract, dummyContract));
    process =
        new WithdrawalProcess(
            withdrawalContract,
            oracleKeys.get(0),
            oracleKeys.get(0).getPublic().createAddress(),
            10,
            senderKey0,
            contractStateHandler);

    // Contract state is null
    assertThat(process.run()).isFalse();

    AvlTree<Long, Epoch> epochs =
        AvlTree.create(
            Map.of(0L, new Epoch(AvlTree.create(), AvlTree.create(), FixedList.create())));
    ByocOutgoingContractState state =
        new ByocOutgoingContractState(ethByocContract, 0, 0, 0, epochs);
    final Client.Listener listener =
        contractStateHandler.createContract(null, null, withdrawalContract, null, state);
    final Client.Listener dummyListener =
        contractStateHandler.createContract(null, null, dummyContract, null, new StateLong(0));

    Thread thread =
        new Thread(
            () -> {
              // First runs finishes, as it has no contract states
              process.run();
              // Second run should be blocked temporarily
              process.run();
            });
    thread.start();

    // Ensure the process is waiting for contract change
    sleep(300);
    assertThat(thread.getState().toString()).isEqualTo("WAITING");

    // Change the state of the non-relevant contract. This should not notify the thread
    dummyListener.update(new StateLong(99));
    // Check that the thread is still waiting
    assertThat(thread.getState().toString()).isEqualTo("WAITING");

    // Change the state of the relevant contract. Should notify the thread
    listener.update(
        new ByocOutgoingContractState(
            state.byocContract(),
            state.withdrawNonce() + 1,
            state.oracleNonce(),
            state.currentEpoch(),
            state.epochs()));
    assertThat(thread.getState().toString()).isEqualTo("BLOCKED");

    // Let the thread regain the lock, and finish the rest of the run
    sleep(300);
    assertThat(thread.getState().toString()).isEqualTo("TERMINATED");
  }

  private void addDefaultExecutedTransactions(int iterations) {
    for (int i = 0; i < iterations; i++) {
      pbcClient1.addDefaultExecutedTransactions(shard1);
      pbcClient2.addDefaultExecutedTransactions(shard2);
    }
  }

  private Hash messageDigest(long nonce, Unsigned256 amount) {
    return Hash.create(
        stream -> {
          ethByocContract.write(stream);
          stream.writeLong(0L);
          stream.writeLong(nonce);
          ethReceiver.write(stream);
          amount.write(stream);
        });
  }

  private static List<OracleMember> createMembers(List<BlockchainAddress> identities) {
    List<OracleMember> members = new ArrayList<>();
    for (BlockchainAddress identity : identities) {
      members.add(new OracleMember(identity));
    }
    return members;
  }

  private void createListener() {
    contractListener =
        contractStateHandler.createContract(
            null, null, withdrawalContract, null, dummyOutgoingState);
  }
}
