package com.partisiablockchain.demo.byoc.monitor.chainlink;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.math.Unsigned256;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

final class ChainlinkRetryHandlerTest {

  /** Incrementing used attempts reduces the remaining attempts by one. */
  @Test
  void incrementAttempts() {
    String contractAddress = "ContractAddress";
    Unsigned256 roundId = Unsigned256.ONE;

    ChainlinkRetryHandler retryHandler = ChainlinkRetryHandler.create();
    Assertions.assertThat(retryHandler.getRemainingAttempts(contractAddress, roundId))
        .isEqualTo(ChainlinkRetryHandler.MAX_RETRIES);
    retryHandler.incrementAttempts(contractAddress, roundId);
    Assertions.assertThat(retryHandler.getRemainingAttempts(contractAddress, roundId))
        .isEqualTo(ChainlinkRetryHandler.MAX_RETRIES - 1);
  }

  /**
   * Removing attempts for rounds on a contract older than a specific round identifier removes all
   * retry attempts with a round identifier older than or equal to the specified round identifier
   * for a contract. Retry attempts for older rounds on other contracts should not be removed.
   */
  @Test
  void removeAttemptsForRoundsOlderThan() {
    String contractA = "A";
    String contractB = "B";
    Unsigned256 olderRoundId = Unsigned256.create(99);
    Unsigned256 roundId = Unsigned256.create(100);
    Unsigned256 newerRoundId = Unsigned256.create(101);

    ChainlinkRetryHandler retryHandler = ChainlinkRetryHandler.create();
    retryHandler.incrementAttempts(contractA, olderRoundId);
    retryHandler.incrementAttempts(contractA, roundId);
    retryHandler.incrementAttempts(contractA, newerRoundId);
    retryHandler.incrementAttempts(contractB, olderRoundId);
    retryHandler.incrementAttempts(contractB, roundId);
    retryHandler.incrementAttempts(contractB, newerRoundId);

    retryHandler.removeAttemptsForRoundsOlderThan(contractA, roundId);

    Assertions.assertThat(retryHandler.getRetries().get(contractA)).containsOnlyKeys(newerRoundId);
    Assertions.assertThat(retryHandler.getRetries().get(contractB))
        .containsOnlyKeys(olderRoundId, roundId, newerRoundId);
  }

  /** Removing the retry attempts for all rounds for a contract should remove the contract entry. */
  @Test
  void removingAllAttemptsShouldRemoveContract() {
    String contractA = "A";
    Unsigned256 olderRoundId = Unsigned256.create(99);

    ChainlinkRetryHandler retryHandler = ChainlinkRetryHandler.create();
    retryHandler.incrementAttempts(contractA, olderRoundId);

    Unsigned256 roundId = Unsigned256.create(100);
    retryHandler.removeAttemptsForRoundsOlderThan(contractA, roundId);

    Assertions.assertThat(retryHandler.getRetries()).doesNotContainKey(contractA);
  }
}
