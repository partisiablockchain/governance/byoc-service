package com.partisiablockchain.demo.byoc.monitor;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.demo.byoc.governance.bporchestration.BpOrchestrationContractState;
import com.partisiablockchain.demo.byoc.governance.largeoracle.LargeOracleContractState;

/** Wrapper the contract states read by the large oracle. */
final class StateWrapper {

  public BpOrchestrationContractState bpOrchestrationContractState;
  public LargeOracleContractState largeOracleContractState;

  public StateWrapper(
      BpOrchestrationContractState bpOrchestrationContractState,
      LargeOracleContractState largeOracleContractState) {
    this.bpOrchestrationContractState = bpOrchestrationContractState;
    this.largeOracleContractState = largeOracleContractState;
  }
}
