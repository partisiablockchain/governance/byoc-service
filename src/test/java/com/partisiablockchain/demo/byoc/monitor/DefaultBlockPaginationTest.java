package com.partisiablockchain.demo.byoc.monitor;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import java.math.BigInteger;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.web3j.protocol.Web3j;

/** Test default block pagination strategy. */
public final class DefaultBlockPaginationTest {
  private final BigInteger blockValidityDepth = BigInteger.TEN;
  private final BigInteger maxBlockRange = BigInteger.valueOf(100);

  private final Web3j web3jMock = Mockito.mock(Web3j.class);

  private final Ethereum.DefaultBlockPagination subject =
      new Ethereum.DefaultBlockPagination(web3jMock, blockValidityDepth, maxBlockRange);

  @Test
  void blockPaginationReturnsTrueUntilThereAreNoMoreBlocksAvailable() {
    long currentBlockNumber = 5_010L;
    Mockito.when(web3jMock.ethBlockNumber())
        .thenReturn(new EthBlockNumberRequestStub<>(currentBlockNumber));

    Ethereum.BlockRange range = subject.nextRange(BigInteger.ZERO);
    Assertions.assertThat(range.fromBlock()).isEqualTo(BigInteger.ZERO);
    Assertions.assertThat(range.toBlock()).isEqualTo(maxBlockRange);
    Assertions.assertThat(range.moreBlocksAvailable()).isTrue();

    long expectedIterations = currentBlockNumber / maxBlockRange.longValue();
    BigInteger latestCheckedBlock = range.toBlock();
    for (int i = 1; i < expectedIterations - 1; i++) {
      range = subject.nextRange(latestCheckedBlock);
      BigInteger expectedToBlock = latestCheckedBlock.add(maxBlockRange);
      Assertions.assertThat(range.fromBlock()).isEqualTo(latestCheckedBlock);
      Assertions.assertThat(range.toBlock()).isEqualTo(expectedToBlock);
      Assertions.assertThat(range.moreBlocksAvailable()).isTrue();
      latestCheckedBlock = range.toBlock();
    }

    range = subject.nextRange(latestCheckedBlock);
    Assertions.assertThat(range.fromBlock()).isEqualTo(latestCheckedBlock);
    Assertions.assertThat(range.toBlock()).isEqualTo(BigInteger.valueOf(5_000L));
    Assertions.assertThat(range.moreBlocksAvailable()).isFalse();
  }

  @Test
  void getBlockCalledWhenLatestFinalIsBeforeLatestChecked() {
    Mockito.when(web3jMock.ethBlockNumber()).thenReturn(new EthBlockNumberRequestStub<>(500L));
    subject.nextRange(BigInteger.ZERO);
    Mockito.verify(web3jMock, Mockito.times(1)).ethBlockNumber();
    subject.nextRange(BigInteger.valueOf(90));
    Mockito.verify(web3jMock, Mockito.times(1)).ethBlockNumber();
    subject.nextRange(BigInteger.valueOf(490));
    Mockito.verify(web3jMock, Mockito.times(2)).ethBlockNumber();
  }
}
