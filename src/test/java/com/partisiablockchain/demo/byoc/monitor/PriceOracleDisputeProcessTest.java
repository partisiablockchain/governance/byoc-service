package com.partisiablockchain.demo.byoc.monitor;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.client.transaction.Transaction;
import com.partisiablockchain.demo.byoc.governance.largeoracle.Dispute;
import com.partisiablockchain.demo.byoc.governance.largeoracle.LargeOracleContractHelper;
import com.partisiablockchain.demo.byoc.governance.largeoracle.LargeOracleContractState;
import com.partisiablockchain.demo.byoc.governance.largeoracle.OracleDisputeId;
import com.partisiablockchain.demo.byoc.governance.largeoracle.OracleMember;
import com.partisiablockchain.demo.byoc.governance.priceoracle.DisputeInfo;
import com.partisiablockchain.demo.byoc.governance.priceoracle.PriceOracleContractState;
import com.partisiablockchain.demo.byoc.governance.priceoracle.PriceOracleDispute;
import com.partisiablockchain.demo.byoc.governance.priceoracle.PriceOracleRoundData;
import com.partisiablockchain.demo.byoc.monitor.chainlink.ChainlinkClient;
import com.partisiablockchain.math.Unsigned256;
import com.partisiablockchain.server.tcp.client.Client;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import com.secata.tools.immutable.FixedList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.web3j.protocol.Web3j;

final class PriceOracleDisputeProcessTest {

  final Web3j web3jMock = Mockito.mock(Web3j.class);

  ContractStateHandler contractStateHandler =
      new ContractStateHandler(
          List.of(
              PriceOracleTestUtility.priceOracleContract1,
              PriceOracleTestUtility.priceOracleContract2,
              PriceOracleTestUtility.largeOracleContract));
  Map<BlockchainAddress, Client.Listener> listeners;
  private Transaction latestSentTransaction;
  private final PriceOracleDisputeProcess disputeProcess =
      new PriceOracleDisputeProcess(
          PriceOracleTestUtility.nodeAddress,
          PriceOracleTestUtility.priceOracleContracts,
          (transaction, cost) -> latestSentTransaction = transaction,
          PriceOracleTestUtility.largeOracleContract,
          contractStateHandler,
          Map.of(
              "ChainId",
              new ChainlinkClient(
                  web3jMock, PriceOracleTestUtility.ethereumBlockValidityThreshold)));
  final RoundData incorrectClaim =
      new RoundData(
          PriceOracleTestUtility.roundData.roundId(),
          PriceOracleTestUtility.roundData.answer().add(Unsigned256.ONE),
          PriceOracleTestUtility.roundData.startedAt(),
          PriceOracleTestUtility.roundData.updatedAt(),
          PriceOracleTestUtility.roundData.answeredInRound());
  private final OracleDisputeId oracleDisputeId =
      new OracleDisputeId(
          PriceOracleTestUtility.priceOracleContract1,
          0,
          PriceOracleTestUtility.roundData.roundId().longValueExact());

  @BeforeEach
  public void setup() {
    listeners = PriceOracleTestUtility.createListeners(contractStateHandler);
  }

  @Test
  void addCounterClaim() {
    Mockito.when(web3jMock.ethBlockNumber()).thenReturn(new EthBlockNumberRequestStub<>(50L));
    Mockito.when(web3jMock.ethCall(Mockito.any(), Mockito.any()))
        .thenReturn(
            new PriceOracleTestUtility.EthCallRoundDataRequest<>(PriceOracleTestUtility.roundData));
    Mockito.when(web3jMock.ethGetBlockByNumber(Mockito.any(), Mockito.anyBoolean()))
        .thenReturn(new EthereumTest.EthBlockRequest<>("1652874975"));
    final RoundData incorrectRoundData =
        new RoundData(
            PriceOracleTestUtility.roundData.roundId(),
            PriceOracleTestUtility.roundData.answer().add(Unsigned256.ONE),
            PriceOracleTestUtility.roundData.startedAt(),
            PriceOracleTestUtility.roundData.updatedAt(),
            PriceOracleTestUtility.roundData.answeredInRound());
    final PriceOracleDispute priceOracleDispute =
        new PriceOracleDispute(
            FixedList.create(List.of(PriceOracleRoundData.convert(incorrectRoundData))));
    final Dispute dispute = new Dispute(FixedList.create(Collections.nCopies(1, null)));

    LargeOracleContractState locState =
        PriceOracleTestUtility.getContractState(
            PriceOracleTestUtility.largeOracleContract,
            LargeOracleContractState.class,
            contractStateHandler);
    locState = LargeOracleContractHelper.setDispute(locState, oracleDisputeId, dispute);
    listeners.get(PriceOracleTestUtility.largeOracleContract).update(locState);

    PriceOracleContractState pocState =
        PriceOracleTestUtility.getContractState(
            PriceOracleTestUtility.priceOracleContract1,
            PriceOracleContractState.class,
            contractStateHandler);
    pocState =
        PriceOracleTestUtility.setFakeActiveChallengePeriod(
            pocState,
            incorrectRoundData,
            1,
            List.of(
                PriceOracleTestUtility.nodeAddress,
                PriceOracleTestUtility.oracleAccounts.get(1),
                PriceOracleTestUtility.oracleAccounts.get(2)));
    pocState = setFakeDispute(pocState, priceOracleDispute, true);
    listeners.get(PriceOracleTestUtility.priceOracleContract1).update(pocState);

    disputeProcess.run();

    byte[] rpc = latestSentTransaction.getRpc();
    SafeDataInputStream stream = SafeDataInputStream.createFromBytes(rpc);
    byte addCounterClaim = 1;
    assertThat(stream.readUnsignedByte()).isEqualTo(addCounterClaim);
    assertThat(BlockchainAddress.read(stream))
        .isEqualTo(PriceOracleTestUtility.priceOracleContract1);
    assertThat(stream.readLong()).isEqualTo(oracleDisputeId.oracleId());
    assertThat(stream.readLong()).isEqualTo(oracleDisputeId.disputeId());
    assertThat(stream.readDynamicBytes())
        .isEqualTo(SafeDataOutputStream.serialize(PriceOracleTestUtility.roundData::write));
  }

  @Test
  void processWaitsOnStateChange() {
    // Setup process so it runs correctly
    PriceOracleTestUtility.setupMockEthReturnValue(web3jMock, PriceOracleTestUtility.roundData);
    Dispute dispute = new Dispute(FixedList.create(Collections.nCopies(1, null)));
    Dispute disputeWithVotes = new Dispute(FixedList.create(Collections.nCopies(1, null)));
    disputeWithVotes = LargeOracleContractHelper.addVote(disputeWithVotes, 0, 0);
    OracleDisputeId idUnused =
        new OracleDisputeId(
            PriceOracleTestUtility.nodeAddress,
            0,
            PriceOracleTestUtility.roundData.roundId().longValueExact());

    LargeOracleContractState locState =
        PriceOracleTestUtility.getContractState(
            PriceOracleTestUtility.largeOracleContract,
            LargeOracleContractState.class,
            contractStateHandler);
    locState = LargeOracleContractHelper.setDispute(locState, oracleDisputeId, dispute);
    locState = LargeOracleContractHelper.setDispute(locState, idUnused, disputeWithVotes);
    listeners.get(PriceOracleTestUtility.largeOracleContract).update(locState);

    PriceOracleContractState pocState =
        PriceOracleTestUtility.getContractState(
            PriceOracleTestUtility.priceOracleContract1,
            PriceOracleContractState.class,
            contractStateHandler);
    listeners
        .get(PriceOracleTestUtility.priceOracleContract1)
        .update(
            setFakeDispute(
                pocState,
                createFakeDispute(PriceOracleTestUtility.roundData, incorrectClaim),
                true));

    // Process runs correctly, so a transaction is created
    disputeProcess.run();
    assertThat(latestSentTransaction).isNotNull();

    // Create a new dispute but without updating the large oracle state
    latestSentTransaction = null;
    Thread thread = new Thread(disputeProcess::run);
    thread.start();
    pocState =
        PriceOracleTestUtility.getContractState(
            PriceOracleTestUtility.priceOracleContract1,
            PriceOracleContractState.class,
            contractStateHandler);
    RoundData roundData1 = new RoundData("2", "0", "0", "0", "0");
    pocState = setFakeDispute(pocState, createFakeDispute(roundData1, incorrectClaim), true);
    listeners.get(PriceOracleTestUtility.priceOracleContract1).update(pocState);
    // The thread handling disputes should still be waiting as no changes were made to the large
    // oracle state
    PriceOracleTestUtility.pauseThread(300);
    assertThat(thread.getState().toString()).isEqualTo("WAITING");
    assertThat(latestSentTransaction).isNull();
  }

  @Test
  void voteNoFraud() {
    castVoteOnPriceDispute(PriceOracleTestUtility.roundData, incorrectClaim, -1);
  }

  @Test
  void voteOnChallengersClaim() {
    castVoteOnPriceDispute(incorrectClaim, PriceOracleTestUtility.roundData, 1);
  }

  @Test
  void nodeVotesOnDisputeEvenIfOptedOut() {
    PriceOracleContractState state = PriceOracleTestUtility.dummyPriceOracleState();
    setupStatesForDispute(PriceOracleTestUtility.roundData, incorrectClaim);
    Client.Listener listener = listeners.get(PriceOracleTestUtility.priceOracleContract1);
    listener.update(
        PriceOracleTestUtility.setNodeAsOptedOut(state, PriceOracleTestUtility.nodeAddress));
    voteNoFraud();
  }

  void setupStatesForDispute(RoundData firstClaim, RoundData secondClaim) {
    Mockito.when(web3jMock.ethCall(Mockito.any(), Mockito.any()))
        .thenReturn(
            new PriceOracleTestUtility.EthCallRoundDataRequest<>(PriceOracleTestUtility.roundData));
    Mockito.when(web3jMock.ethGetBlockByNumber(Mockito.any(), Mockito.anyBoolean()))
        .thenReturn(new EthereumTest.EthBlockRequest<>("1652874975"));
    Mockito.when(web3jMock.ethBlockNumber()).thenReturn(new EthBlockNumberRequestStub<>(50L));
    final Dispute dispute = new Dispute(FixedList.create(Collections.nCopies(1, null)));
    Dispute disputeWithVotes = new Dispute(FixedList.create(Collections.nCopies(1, null)));
    disputeWithVotes = LargeOracleContractHelper.addVote(disputeWithVotes, 0, 0);
    final OracleDisputeId idUnused =
        new OracleDisputeId(
            PriceOracleTestUtility.nodeAddress,
            0,
            PriceOracleTestUtility.roundData.roundId().longValueExact());
    final PriceOracleDispute fakeDispute = createFakeDispute(firstClaim, secondClaim);

    PriceOracleTestUtility.setupStates(
        oracleDisputeId,
        dispute,
        idUnused,
        disputeWithVotes,
        fakeDispute,
        contractStateHandler,
        listeners,
        0L);
  }

  void castVoteOnPriceDispute(RoundData firstClaim, RoundData secondClaim, int expectedVote) {
    setupStatesForDispute(firstClaim, secondClaim);
    disputeProcess.run();

    byte[] rpc = latestSentTransaction.getRpc();
    SafeDataInputStream stream = SafeDataInputStream.createFromBytes(rpc);
    byte voteOnDispute = 2;
    assertThat(stream.readUnsignedByte()).isEqualTo(voteOnDispute);

    assertThat(BlockchainAddress.read(stream))
        .isEqualTo(PriceOracleTestUtility.priceOracleContract1);
    assertThat(stream.readLong()).isEqualTo(oracleDisputeId.oracleId());
    assertThat(stream.readLong()).isEqualTo(oracleDisputeId.disputeId());
    assertThat(stream.readInt()).isEqualTo(expectedVote);
  }

  @Test
  void processDoesNotVoteOnDisputeIfRoundIsInFuture() {
    Mockito.when(web3jMock.ethCall(Mockito.any(), Mockito.any()))
        .thenReturn(
            new PriceOracleTestUtility.EthCallRoundDataRequest<>(PriceOracleTestUtility.roundData));
    Mockito.when(web3jMock.ethGetBlockByNumber(Mockito.any(), Mockito.anyBoolean()))
        .thenReturn(new EthereumTest.EthBlockRequest<>("5"));
    Mockito.when(web3jMock.ethBlockNumber()).thenReturn(new EthBlockNumberRequestStub<>(50L));
    final Dispute dispute = new Dispute(FixedList.create(Collections.singleton(null)));
    final PriceOracleDispute fakeDispute =
        createFakeDispute(incorrectClaim, PriceOracleTestUtility.roundData);

    PriceOracleTestUtility.setupStates(
        oracleDisputeId,
        dispute,
        oracleDisputeId,
        dispute,
        fakeDispute,
        contractStateHandler,
        listeners,
        0);

    disputeProcess.run();
  }

  @Test
  void canNotVoteOnDisputeWithExistingVote() {
    Mockito.when(web3jMock.ethCall(Mockito.any(), Mockito.any()))
        .thenReturn(
            new PriceOracleTestUtility.EthCallRoundDataRequest<>(PriceOracleTestUtility.roundData));
    final PriceOracleDispute priceOracleDispute =
        new PriceOracleDispute(
            FixedList.create(
                List.of(PriceOracleRoundData.convert(PriceOracleTestUtility.roundData))));
    Dispute dispute = new Dispute(FixedList.create(Collections.nCopies(1, null)));
    dispute = LargeOracleContractHelper.addVote(dispute, 0, 1);

    LargeOracleContractState locState =
        PriceOracleTestUtility.getContractState(
            PriceOracleTestUtility.largeOracleContract,
            LargeOracleContractState.class,
            contractStateHandler);
    locState = LargeOracleContractHelper.setDispute(locState, oracleDisputeId, dispute);
    listeners.get(PriceOracleTestUtility.largeOracleContract).update(locState);

    PriceOracleContractState pocState =
        PriceOracleTestUtility.getContractState(
            PriceOracleTestUtility.priceOracleContract1,
            PriceOracleContractState.class,
            contractStateHandler);
    pocState =
        PriceOracleTestUtility.setFakeActiveChallengePeriod(
            pocState,
            PriceOracleTestUtility.roundData,
            1,
            List.of(
                PriceOracleTestUtility.nodeAddress,
                PriceOracleTestUtility.oracleAccounts.get(1),
                PriceOracleTestUtility.oracleAccounts.get(2)));
    pocState = setFakeDispute(pocState, priceOracleDispute, true);
    listeners.get(PriceOracleTestUtility.priceOracleContract1).update(pocState);

    latestSentTransaction = null;
    disputeProcess.run();
    assertThat(latestSentTransaction).isNull();
  }

  @Test
  void nodeIsNotOracle() {
    PriceOracleTestUtility.setupMockEthReturnValue(web3jMock, PriceOracleTestUtility.roundData);

    Client.Listener listener = listeners.get(PriceOracleTestUtility.largeOracleContract);
    LargeOracleContractState state =
        (LargeOracleContractState)
            contractStateHandler.contractStates.get(PriceOracleTestUtility.largeOracleContract);
    listener.update(createLocStateWithOracleMembers(List.of(), state));
    assertThat(disputeProcess.run()).isFalse();
  }

  @Test
  void processDoesNotRunIfContractStatesAreNull() {
    PriceOracleTestUtility.setupMockEthReturnValue(web3jMock, PriceOracleTestUtility.roundData);
    for (Client.Listener listener : listeners.values()) {
      listener.remove();
    }
    assertThat(disputeProcess.run()).isFalse();
    Client.Listener largeListener =
        contractStateHandler.createContract(
            null,
            null,
            PriceOracleTestUtility.largeOracleContract,
            null,
            PriceOracleTestUtility.dummyLargeOracleState());
    assertThat(disputeProcess.run()).isFalse();
    largeListener.remove();
    contractStateHandler.createContract(
        null,
        null,
        PriceOracleTestUtility.priceOracleContract1,
        null,
        PriceOracleTestUtility.dummyPriceOracleState());
    assertThat(disputeProcess.run()).isFalse();
  }

  @Test
  void returnEarlyIfRoundDataIsNull() {
    Mockito.when(web3jMock.ethBlockNumber()).thenReturn(new EthBlockNumberRequestStub<>(50L));
    Mockito.when(web3jMock.ethGetBlockByNumber(Mockito.any(), Mockito.anyBoolean()))
        .thenReturn(new EthereumTest.EthBlockRequest<>("1652874975"));
    Mockito.when(web3jMock.ethCall(Mockito.any(), Mockito.any()))
        .thenReturn(new PriceOracleTestUtility.EthCallErrorRequest<>(3, "execution reverted"));
    final RoundData roundData =
        new RoundData(
            PriceOracleTestUtility.roundData.roundId(),
            PriceOracleTestUtility.roundData.answer().add(Unsigned256.ONE),
            PriceOracleTestUtility.roundData.startedAt(),
            PriceOracleTestUtility.roundData.updatedAt(),
            PriceOracleTestUtility.roundData.answeredInRound());
    final PriceOracleDispute priceOracleDispute =
        new PriceOracleDispute(FixedList.create(List.of(PriceOracleRoundData.convert(roundData))));
    final Dispute dispute = new Dispute(FixedList.create(Collections.nCopies(1, null)));

    LargeOracleContractState locState =
        PriceOracleTestUtility.getContractState(
            PriceOracleTestUtility.largeOracleContract,
            LargeOracleContractState.class,
            contractStateHandler);
    listeners
        .get(PriceOracleTestUtility.largeOracleContract)
        .update(LargeOracleContractHelper.setDispute(locState, oracleDisputeId, dispute));

    PriceOracleContractState pocState =
        PriceOracleTestUtility.getContractState(
            PriceOracleTestUtility.priceOracleContract1,
            PriceOracleContractState.class,
            contractStateHandler);
    pocState =
        PriceOracleTestUtility.setFakeActiveChallengePeriod(
            pocState,
            roundData,
            1,
            List.of(
                PriceOracleTestUtility.nodeAddress,
                PriceOracleTestUtility.oracleAccounts.get(1),
                PriceOracleTestUtility.oracleAccounts.get(2)));
    pocState = setFakeDispute(pocState, priceOracleDispute, true);
    Client.Listener pocListener = listeners.get(PriceOracleTestUtility.priceOracleContract1);
    pocListener.update(
        PriceOracleTestUtility.registerNode(pocState, PriceOracleTestUtility.nodeAddress));
    pocListener.update(setFakeDispute(pocState, priceOracleDispute, true));

    latestSentTransaction = null;
    disputeProcess.run();
    assertThat(latestSentTransaction).isNull();
  }

  @Test
  void name() {
    assertThat(disputeProcess.name()).isEqualTo(PriceOracleDisputeProcess.class.getSimpleName());
  }

  /**
   * Run process without necessary external chain client for the chain identifier. Should do
   * nothing.
   */
  @Test
  void withoutNecessaryExternalChainClient() {
    PriceOracleDisputeProcess processWithoutExternalChainClients =
        new PriceOracleDisputeProcess(
            PriceOracleTestUtility.nodeAddress,
            PriceOracleTestUtility.priceOracleContracts,
            (transaction, cost) -> latestSentTransaction = transaction,
            PriceOracleTestUtility.largeOracleContract,
            contractStateHandler,
            Map.of());

    processWithoutExternalChainClients.run();

    assertThat(latestSentTransaction).isNull();
  }

  private static PriceOracleContractState setFakeDispute(
      PriceOracleContractState state, PriceOracleDispute dispute, boolean pending) {
    return new PriceOracleContractState(
        state.largeOracleContractAddress(),
        state.referenceContractAddress(),
        state.roundPriceUpdates(),
        state.latestPublishedRoundId(),
        state.symbol(),
        state.oracleNodes(),
        state.challengePeriod(),
        new DisputeInfo(dispute, pending, new PriceOracleDispute(dispute.claims())),
        state.chainId());
  }

  private LargeOracleContractState createLocStateWithOracleMembers(
      List<OracleMember> oracleAccounts, LargeOracleContractState state) {
    return new LargeOracleContractState(
        FixedList.create(oracleAccounts),
        state.stakedTokens(),
        state.pendingMessages(),
        state.currentMessageNonce(),
        state.initialMessageNonce(),
        state.signedMessages(),
        state.activeDisputes());
  }

  PriceOracleDispute createFakeDispute(RoundData firstClaim, RoundData secondClaim) {
    return new PriceOracleDispute(
        FixedList.create(
            List.of(
                PriceOracleRoundData.convert(firstClaim),
                PriceOracleRoundData.convert(secondClaim))));
  }
}
