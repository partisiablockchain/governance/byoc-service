package com.partisiablockchain.demo.byoc.monitor;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static java.lang.Thread.sleep;

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.KeyPair;
import com.partisiablockchain.demo.byoc.governance.largeoracle.Dispute;
import com.partisiablockchain.demo.byoc.governance.largeoracle.LargeOracleContractHelper;
import com.partisiablockchain.demo.byoc.governance.largeoracle.LargeOracleContractState;
import com.partisiablockchain.demo.byoc.governance.largeoracle.OracleDisputeId;
import com.partisiablockchain.demo.byoc.governance.largeoracle.OracleMember;
import com.partisiablockchain.demo.byoc.governance.priceoracle.ChallengePeriod;
import com.partisiablockchain.demo.byoc.governance.priceoracle.DisputeInfo;
import com.partisiablockchain.demo.byoc.governance.priceoracle.PriceOracleContractState;
import com.partisiablockchain.demo.byoc.governance.priceoracle.PriceOracleDispute;
import com.partisiablockchain.demo.byoc.governance.priceoracle.PriceOracleNodes;
import com.partisiablockchain.demo.byoc.governance.priceoracle.PriceOracleRoundData;
import com.partisiablockchain.demo.byoc.governance.priceoracle.PriceUpdates;
import com.partisiablockchain.demo.byoc.governance.priceoracle.RegistrationStatus;
import com.partisiablockchain.demo.byoc.governance.priceoracle.RoundPriceUpdates;
import com.partisiablockchain.math.Unsigned256;
import com.partisiablockchain.server.tcp.client.Client;
import com.partisiablockchain.tree.AvlTree;
import com.secata.tools.coverage.ExceptionConverter;
import com.secata.tools.immutable.FixedList;
import java.math.BigInteger;
import java.util.List;
import java.util.Map;
import org.mockito.Mockito;
import org.web3j.abi.DefaultFunctionEncoder;
import org.web3j.abi.datatypes.generated.Uint256;
import org.web3j.abi.datatypes.generated.Uint80;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.core.Request;
import org.web3j.protocol.core.Response;
import org.web3j.protocol.core.methods.response.EthCall;

final class PriceOracleTestUtility {
  static final BlockchainAddress largeOracleContract =
      BlockchainAddress.fromHash(
          BlockchainAddress.Type.CONTRACT_GOV,
          Hash.create(h -> h.writeString("LARGE_CONTRACT_ADDRESS")));
  static final BigInteger ethereumBlockValidityThreshold = BigInteger.TEN;
  static final BlockchainAddress priceOracleContract1 =
      BlockchainAddress.fromHash(
          BlockchainAddress.Type.CONTRACT_GOV,
          Hash.create(h -> h.writeString("PRICE_ORACLE_CONTRACT_1")));
  static final BlockchainAddress priceOracleContract2 =
      BlockchainAddress.fromHash(
          BlockchainAddress.Type.CONTRACT_GOV,
          Hash.create(h -> h.writeString("PRICE_ORACLE_CONTRACT_2")));
  static final List<BlockchainAddress> priceOracleContracts =
      List.of(priceOracleContract1, priceOracleContract2);

  static final Unsigned256 ethAnswer = Unsigned256.create(203083841850L);
  static final Unsigned256 startedAt = Unsigned256.create(1652874975L);
  static final Unsigned256 updatedAt = Unsigned256.create(1652874975L);
  static final RoundData roundData =
      new RoundData(Unsigned256.TEN, ethAnswer, startedAt, updatedAt, Unsigned256.TEN);

  static void pauseThread(int i) {
    ExceptionConverter.run(() -> sleep(i), "Could not sleep");
  }

  static PriceOracleContractState dummyPriceOracleState() {
    AvlTree<BlockchainAddress, RegistrationStatus> nodes = AvlTree.create();
    return new PriceOracleContractState(
        null,
        "REFERENCE_CONTRACT_ADDRESS",
        new RoundPriceUpdates(AvlTree.create()),
        Unsigned256.ZERO,
        "ETH",
        new PriceOracleNodes(nodes),
        null,
        new DisputeInfo(null, false, null),
        "ChainId");
  }

  static final List<KeyPair> oracleKeys =
      List.of(
          new KeyPair(BigInteger.ONE),
          new KeyPair(BigInteger.TWO),
          new KeyPair(BigInteger.valueOf(5)),
          new KeyPair(BigInteger.TEN));
  static final List<BlockchainPublicKey> oraclePubKeys =
      oracleKeys.stream().map(KeyPair::getPublic).toList();
  static final List<BlockchainAddress> oracleAccounts =
      oraclePubKeys.stream().map(BlockchainPublicKey::createAddress).toList();
  static final BlockchainAddress nodeAddress = oracleAccounts.get(0);
  static final List<OracleMember> loOracleMembers =
      oracleAccounts.stream().map(OracleMember::new).toList();

  static LargeOracleContractState dummyLargeOracleState() {
    return new LargeOracleContractState(
        FixedList.create(loOracleMembers),
        AvlTree.create(),
        FixedList.create(),
        0,
        0,
        AvlTree.create(),
        AvlTree.create());
  }

  static Map<BlockchainAddress, Client.Listener> createListeners(
      ContractStateHandler contractStateHandler) {
    return Map.of(
        priceOracleContract1,
        contractStateHandler.createContract(
            null, null, priceOracleContract1, null, dummyPriceOracleState()),
        priceOracleContract2,
        contractStateHandler.createContract(
            null, null, priceOracleContract2, null, dummyPriceOracleState()),
        largeOracleContract,
        contractStateHandler.createContract(
            null, null, largeOracleContract, null, dummyLargeOracleState()));
  }

  static PriceOracleContractState setFakeActiveChallengePeriod(
      PriceOracleContractState state,
      RoundData roundData,
      long challengePeriodEnd,
      List<BlockchainAddress> notifyingNodes) {
    AvlTree<BlockchainAddress, PriceOracleRoundData> oraclePriceUpdates = AvlTree.create();
    for (BlockchainAddress node : notifyingNodes) {
      oraclePriceUpdates = oraclePriceUpdates.set(node, PriceOracleRoundData.convert(roundData));
    }
    PriceUpdates priceUpdates = new PriceUpdates(oraclePriceUpdates);
    AvlTree<Unsigned256, PriceUpdates> roundPriceUpdates =
        AvlTree.create(Map.of(roundData.roundId(), priceUpdates));
    return new PriceOracleContractState(
        state.largeOracleContractAddress(),
        state.referenceContractAddress(),
        new RoundPriceUpdates(roundPriceUpdates),
        state.latestPublishedRoundId(),
        state.symbol(),
        state.oracleNodes(),
        new ChallengePeriod(
            roundData.roundId(),
            challengePeriodEnd - ChallengePeriod.CHALLENGE_PERIOD_DURATION_MILLIS),
        null,
        state.chainId());
  }

  static void setupStates(
      OracleDisputeId id,
      Dispute dispute,
      OracleDisputeId idUnused,
      Dispute disputeWithVotes,
      PriceOracleDispute fakeDispute,
      ContractStateHandler contractStateHandler,
      Map<BlockchainAddress, Client.Listener> listeners,
      long challengePeriodEnd) {
    createListeners(contractStateHandler);

    LargeOracleContractState locState =
        getContractState(largeOracleContract, LargeOracleContractState.class, contractStateHandler);
    locState = LargeOracleContractHelper.setDispute(locState, id, dispute);
    locState = LargeOracleContractHelper.setDispute(locState, idUnused, disputeWithVotes);
    listeners.get(largeOracleContract).update(locState);

    PriceOracleContractState pocState =
        getContractState(
            priceOracleContract1, PriceOracleContractState.class, contractStateHandler);
    pocState =
        setFakeActiveChallengePeriod(
            pocState,
            roundData,
            challengePeriodEnd,
            List.of(nodeAddress, oracleAccounts.get(1), oracleAccounts.get(2)));
    pocState = setFakeDispute(pocState, fakeDispute);

    listeners.get(priceOracleContract1).update(pocState);
  }

  static void setupMockEthReturnValue(Web3j web3jMock, RoundData roundData) {
    // create a new round
    Mockito.when(web3jMock.ethBlockNumber()).thenReturn(new EthBlockNumberRequestStub<>(50L));
    Mockito.when(web3jMock.ethCall(Mockito.any(), Mockito.any()))
        .thenReturn(new EthCallRoundDataRequest<>(roundData));
    Mockito.when(web3jMock.ethGetBlockByNumber(Mockito.any(), Mockito.anyBoolean()))
        .thenReturn(new EthereumTest.EthBlockRequest<>("1652874975"));
  }

  static <T> T getContractState(
      BlockchainAddress contract, Class<T> stateClass, ContractStateHandler contractStateHandler) {
    return stateClass.cast(contractStateHandler.contractStates.get(contract));
  }

  static PriceOracleContractState setNodeAsOptedOut(
      PriceOracleContractState state, BlockchainAddress nodeAddress) {
    AvlTree<BlockchainAddress, RegistrationStatus> nodes = state.oracleNodes().nodes();
    nodes = nodes.set(nodeAddress, RegistrationStatus.OPTED_OUT);
    return new PriceOracleContractState(
        state.largeOracleContractAddress(),
        state.referenceContractAddress(),
        state.roundPriceUpdates(),
        state.latestPublishedRoundId(),
        state.symbol(),
        new PriceOracleNodes(nodes),
        state.challengePeriod(),
        state.disputeInfo(),
        state.chainId());
  }

  static PriceOracleContractState registerNode(
      PriceOracleContractState state, BlockchainAddress nodeAddress) {
    AvlTree<BlockchainAddress, RegistrationStatus> nodes = state.oracleNodes().nodes();
    nodes = nodes.set(nodeAddress, RegistrationStatus.REGISTERED);
    return new PriceOracleContractState(
        state.largeOracleContractAddress(),
        state.referenceContractAddress(),
        state.roundPriceUpdates(),
        state.latestPublishedRoundId(),
        state.symbol(),
        new PriceOracleNodes(nodes),
        state.challengePeriod(),
        state.disputeInfo(),
        state.chainId());
  }

  static void registerNode(
      Map<BlockchainAddress, Client.Listener> listeners,
      ContractStateHandler contractStateHandler,
      BlockchainAddress node) {
    PriceOracleContractState state =
        getContractState(
            priceOracleContract1, PriceOracleContractState.class, contractStateHandler);
    state = registerNode(state, node);
    Client.Listener pocListener = listeners.get(priceOracleContract1);
    pocListener.update(state);
  }

  static void setFakeChallengePeriod(
      Map<BlockchainAddress, Client.Listener> listeners,
      ContractStateHandler contractStateHandler,
      RoundData roundData,
      long challengePeriodEnd,
      List<BlockchainAddress> notifyingNodes) {

    PriceOracleContractState state =
        getContractState(
            priceOracleContract1, PriceOracleContractState.class, contractStateHandler);
    state = setFakeActiveChallengePeriod(state, roundData, challengePeriodEnd, notifyingNodes);
    Client.Listener pocListener = listeners.get(priceOracleContract1);
    pocListener.update(state);
  }

  static RoundData createRoundDataWithNewRoundId(RoundData roundData, Unsigned256 roundId) {
    return new RoundData(
        roundId,
        roundData.answer(),
        roundData.startedAt(),
        roundData.updatedAt(),
        roundData.answeredInRound());
  }

  static PriceOracleContractState setFakeDispute(
      PriceOracleContractState state, PriceOracleDispute dispute) {
    return new PriceOracleContractState(
        state.largeOracleContractAddress(),
        state.referenceContractAddress(),
        state.roundPriceUpdates(),
        state.latestPublishedRoundId(),
        state.symbol(),
        state.oracleNodes(),
        new ChallengePeriod(dispute.claims().get(0).roundId(), 0),
        new DisputeInfo(dispute, false, null),
        state.chainId());
  }

  static PriceOracleContractState setLatestPublishedRoundId(
      Unsigned256 latestPublishedRoundId, PriceOracleContractState state) {
    return new PriceOracleContractState(
        state.largeOracleContractAddress(),
        state.referenceContractAddress(),
        state.roundPriceUpdates(),
        latestPublishedRoundId,
        state.symbol(),
        state.oracleNodes(),
        state.challengePeriod(),
        state.disputeInfo(),
        state.chainId());
  }

  static final class EthCallErrorRequest<S> extends Request<S, EthCall> {
    private final int code;
    private final String message;

    public EthCallErrorRequest(int code, String message) {
      this.code = code;
      this.message = message;
    }

    @Override
    public EthCall send() {
      EthCall ethCall = new EthCall();
      ethCall.setError(new Response.Error(code, message));
      return ethCall;
    }
  }

  static final class EthCallRoundDataRequest<S> extends Request<S, EthCall> {

    private final Unsigned256 roundId;
    private final Unsigned256 answer;
    private final Unsigned256 startedAt;
    private final Unsigned256 updatedAt;
    private final Unsigned256 answeredInRound;

    public EthCallRoundDataRequest(RoundData roundData) {
      this.roundId = roundData.roundId();
      this.answer = roundData.answer();
      this.startedAt = roundData.startedAt();
      this.updatedAt = roundData.updatedAt();
      this.answeredInRound = roundData.answeredInRound();
    }

    @Override
    public EthCall send() {
      EthCall ethCall = new EthCall();
      DefaultFunctionEncoder encoder = new DefaultFunctionEncoder();
      String encodedRoundData =
          encoder.encodeParameters(
              List.of(
                  new Uint80(new BigInteger(roundId.serialize())),
                  new Uint256(new BigInteger(answer.serialize())),
                  new Uint256(new BigInteger(startedAt.serialize())),
                  new Uint256(new BigInteger(updatedAt.serialize())),
                  new Uint80(new BigInteger(answeredInRound.serialize()))));
      ethCall.setResult(encodedRoundData);
      return ethCall;
    }
  }
}
