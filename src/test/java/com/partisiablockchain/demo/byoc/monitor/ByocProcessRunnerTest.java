package com.partisiablockchain.demo.byoc.monitor;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;

import com.secata.tools.coverage.ExceptionConverter;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;

/** Tests. */
public final class ByocProcessRunnerTest {

  private ByocProcessRunner processRunner;
  private final DummyByocProcess dummyProcess = new DummyByocProcess();
  private static final int delay = 10;

  /** Close deposit processor if created. */
  @AfterEach
  public void tearDown() {
    if (processRunner != null) {
      processRunner.close();
    }
  }

  @Test
  public void processingCalled() throws InterruptedException {
    CountDownLatch latch = new CountDownLatch(1);
    processRunner =
        new ByocProcessRunner(
            new ByocProcess() {
              @Override
              public boolean run() {
                latch.countDown();
                ExceptionConverter.run(() -> Thread.sleep(10_000), "Sleepy");
                return false;
              }

              @Override
              public String name() {
                return "Test";
              }
            },
            delay,
            0);

    assertThat(latch.await(500, TimeUnit.MILLISECONDS)).isTrue();
  }

  /** Test that a ByocProcessRunner closes correctly. */
  @Test
  public void shouldWaitBeforeCallingAndAllowSkippingNextSleep() throws Exception {
    CountDownLatch latch = new CountDownLatch(2);
    long before = System.currentTimeMillis();
    processRunner =
        new ByocProcessRunner(
            new ByocProcess() {
              @Override
              public boolean run() {
                latch.countDown();
                return latch.getCount() != 0;
              }

              @Override
              public String name() {
                return "Test";
              }
            },
            delay,
            1000);

    assertThat(latch.await(1500, TimeUnit.MILLISECONDS)).isTrue();

    assertThat(System.currentTimeMillis()).isGreaterThanOrEqualTo(before + 1_000);
  }

  @Test
  public void close() {
    processRunner = new ByocProcessRunner(dummyProcess, delay, 0);
    assertThat(processRunner.isRunning()).isTrue();

    processRunner.close();
    assertThat(processRunner.isRunning()).isFalse();
  }

  /** Test that ByocProcessRunner correctly pauses on error. */
  @Test
  public void onError() {
    int delay = 50;
    processRunner = new ByocProcessRunner(dummyProcess, delay, 0);

    Instant before = Instant.now();
    processRunner.onError();
    Instant after = Instant.now();

    Instant minus = after.minus(before.toEpochMilli(), ChronoUnit.MILLIS);
    assertThat(minus.toEpochMilli()).isGreaterThanOrEqualTo(delay);
  }
}
