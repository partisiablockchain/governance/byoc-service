package com.partisiablockchain.demo.byoc.monitor;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.AesHelper;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.BlsKeyPair;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.KeyPair;
import com.partisiablockchain.demo.byoc.Address;
import com.partisiablockchain.demo.byoc.rest.dto.DiscoveryResponseDto;
import com.partisiablockchain.demo.byoc.rest.dto.EncryptedPayloadDto;
import com.partisiablockchain.demo.byoc.rest.dto.HandshakeDto;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import com.secata.tools.coverage.ExceptionConverter;
import jakarta.ws.rs.WebApplicationException;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.crypto.Cipher;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/** Test. */
public final class PeerDiscoveryResourceTest {

  private static final VersionInformation versionInformation = new VersionInformation("version");
  private static final KeyPair serverPrivateKey = new KeyPair(BigInteger.valueOf(123));
  private static final KeyPair clientEphemeralKey = new KeyPair(BigInteger.valueOf(321));
  private static final KeyPair clientPrivateKey = new KeyPair(BigInteger.valueOf(456));
  private static final BlockchainPublicKey clientAddress = clientPrivateKey.getPublic();
  private static final Address myEndpoint = new Address("server", 2219);
  private final Map<BlockchainPublicKey, Address> peers = new HashMap<>();
  private final BlockchainAddress clientPbcAddress =
      BlockchainAddress.fromString("000000000000000000000000000000000000000001");
  private final BlockchainAddress serverPbcAddress =
      BlockchainAddress.fromString("000000000000000000000000000000000000000009");
  private PeerDatabase peerDatabase;

  private PublicProvider publicProvider;

  /** Before. */
  @BeforeEach
  public void before() {
    peers.put(new KeyPair(BigInteger.valueOf(2)).getPublic(), new Address("peer0", 10));
    peers.put(new KeyPair(BigInteger.valueOf(3)).getPublic(), new Address("peer1", 1234));
    peers.put(new KeyPair(BigInteger.valueOf(4)).getPublic(), new Address("peer2", 28364));
    peers.put(serverPrivateKey.getPublic(), myEndpoint);
    peers.put(clientAddress, new Address("endpoint", 2345));
    peerDatabase = new SimplePeerDatabase(peers);
    peerDatabase.setRegisteredBlockProducers(peers.keySet().stream().toList());
    publicProvider =
        new SystemPublicProvider(
            serverPrivateKey.getPublic(),
            serverPbcAddress,
            new BlsKeyPair(BigInteger.ONE).getPublicKey());
  }

  @Test
  void discover() {
    PeerDiscoveryResource resource =
        new PeerDiscoveryResource(
            versionInformation, peerDatabase, serverPrivateKey, publicProvider);
    byte[] payload =
        createEncryptedPayloadForAddress(
            clientAddress, myEndpoint, peerDatabase.getBlockProducersWithEndpoint());
    Hash message = createMessage(payload);
    byte[] sign = createSignature(clientPrivateKey, message);
    DiscoveryResponseDto discover =
        resource.discover(
            EncryptedPayloadDto.create(clientEphemeralKey.getPublic().asBytes(), sign, payload));
    List<PeerDiscoveryClient.DiscoveredPeer> discovered =
        decryptDiscoveryResponse(discover, message);
    Assertions.assertThat(discovered).hasSize(5);
    for (PeerDiscoveryClient.DiscoveredPeer peer : discovered) {
      Assertions.assertThat(peers).containsKey(peer.key);
      Assertions.assertThat(peers.get(peer.key)).isEqualTo(peer.endpoint);
    }
  }

  @Test
  void discoverOnlyTheEndpointRequested() {
    PeerDiscoveryResource resource =
        new PeerDiscoveryResource(
            versionInformation, peerDatabase, serverPrivateKey, publicProvider);
    byte[] payload =
        createEncryptedPayloadForAddress(
            clientAddress,
            myEndpoint,
            List.of(peerDatabase.getBlockProducersWithEndpoint().get(0)));
    Hash message = createMessage(payload);
    byte[] sign = createSignature(clientPrivateKey, message);
    DiscoveryResponseDto discover =
        resource.discover(
            EncryptedPayloadDto.create(clientEphemeralKey.getPublic().asBytes(), sign, payload));
    List<PeerDiscoveryClient.DiscoveredPeer> discovered =
        decryptDiscoveryResponse(discover, message);
    Assertions.assertThat(discovered).hasSize(1);
    for (PeerDiscoveryClient.DiscoveredPeer peer : discovered) {
      Assertions.assertThat(peers).containsKey(peer.key);
      Assertions.assertThat(peers.get(peer.key)).isEqualTo(peer.endpoint);
    }
  }

  @Test
  void unknownRequesterIsAddedToPeers() {
    KeyPair anotherClient = new KeyPair(BigInteger.valueOf(5666));
    BlockchainPublicKey anotherAddress = anotherClient.getPublic();
    List<BlockchainPublicKey> registered = new ArrayList<>(this.peers.keySet().stream().toList());
    registered.add(anotherAddress);
    peerDatabase.setRegisteredBlockProducers(registered);
    PeerDiscoveryResource resource =
        new PeerDiscoveryResource(
            versionInformation, peerDatabase, serverPrivateKey, publicProvider);
    ArrayList<BlockchainPublicKey> neededPeers =
        new ArrayList<>(peerDatabase.getBlockProducersWithEndpoint());
    neededPeers.add(anotherAddress);
    byte[] payload = createEncryptedPayloadForAddress(anotherAddress, myEndpoint, neededPeers);
    Hash message = createMessage(payload);
    byte[] signature = createSignature(anotherClient, message);
    DiscoveryResponseDto discover =
        resource.discover(
            EncryptedPayloadDto.create(
                clientEphemeralKey.getPublic().asBytes(), signature, payload));
    List<PeerDiscoveryClient.DiscoveredPeer> discoveredPeers =
        decryptDiscoveryResponse(discover, message);
    Assertions.assertThat(discoveredPeers).hasSize(6);
    for (PeerDiscoveryClient.DiscoveredPeer peer : discoveredPeers) {
      if (peer.key.equals(anotherAddress)) {
        Assertions.assertThat(peer.endpoint).isEqualTo(new Address("endpoint", 2345));
      } else {
        Assertions.assertThat(this.peers).containsKey(peer.key);
        Assertions.assertThat(this.peers.get(peer.key)).isEqualTo(peer.endpoint);
      }
    }
  }

  @Test
  void discoverNotMeantForUs() {
    PeerDiscoveryResource resource =
        new PeerDiscoveryResource(
            versionInformation, peerDatabase, serverPrivateKey, publicProvider);
    byte[] payload =
        createEncryptedPayloadForAddress(clientAddress, new Address("endpoint", 2345), List.of());
    Hash message = createMessage(payload);
    byte[] sign = createSignature(clientPrivateKey, message);
    Assertions.assertThatThrownBy(
            () ->
                resource.discover(
                    EncryptedPayloadDto.create(
                        clientEphemeralKey.getPublic().asBytes(), sign, payload)))
        .isInstanceOf(WebApplicationException.class);
  }

  @Test
  void discoverUnknownAddress() {
    PeerDiscoveryResource resource =
        new PeerDiscoveryResource(
            versionInformation, peerDatabase, serverPrivateKey, publicProvider);
    byte[] sign = createSignature(clientPrivateKey, Hash.create(s -> s.writeInt(123)));
    Assertions.assertThatThrownBy(
            () ->
                resource.discover(
                    EncryptedPayloadDto.create(
                        clientEphemeralKey.getPublic().asBytes(), sign, new byte[0])))
        .isInstanceOf(WebApplicationException.class);
  }

  @Test
  void discoverBogusPayload() {
    PeerDiscoveryResource resource =
        new PeerDiscoveryResource(
            versionInformation, peerDatabase, serverPrivateKey, publicProvider);
    BlockchainPublicKey bogus = new KeyPair().getPublic();
    byte[] payload = createEncryptedPayloadForAddress(bogus, myEndpoint, List.of());
    Hash message = createMessage(payload);
    byte[] sign = createSignature(clientPrivateKey, message);
    Assertions.assertThatThrownBy(
            () ->
                resource.discover(
                    EncryptedPayloadDto.create(
                        clientEphemeralKey.getPublic().asBytes(), sign, payload)))
        .isInstanceOf(WebApplicationException.class);
  }

  @Test
  void handshake() {
    PeerDiscoveryResource resource =
        new PeerDiscoveryResource(
            versionInformation, peerDatabase, serverPrivateKey, publicProvider);
    Assertions.assertThat(peerDatabase.getBlockProducerEndpoint(clientPrivateKey.getPublic()))
        .isEqualTo(new Address("endpoint", 2345));
    byte[] bytes =
        handshakeMessage(
            new Address("server", 2219),
            new Address("new_endpoint", 2324),
            clientPbcAddress,
            serverPbcAddress);
    byte[] payload = createEncryptedHandshake(bytes);
    Hash message = createMessage(payload);
    byte[] sign = createSignature(clientPrivateKey, message);
    EncryptedPayloadDto encryptedPayloadDto =
        EncryptedPayloadDto.create(clientEphemeralKey.getPublic().asBytes(), sign, payload);
    HandshakeDto handshake = resource.handshake(encryptedPayloadDto);
    Assertions.assertThat(peerDatabase.getBlockProducerEndpoint(clientPrivateKey.getPublic()))
        .isEqualTo(new Address("new_endpoint", 2324));
    byte[] decrypted = decryptHandshakeResponse(handshake);
    // only the client endpoint is returned
    Assertions.assertThat(decrypted)
        .isEqualTo(
            SafeDataOutputStream.serialize(
                s -> {
                  new Address("new_endpoint", 2324).write(s);
                  clientPbcAddress.write(s);
                }));
  }

  @Test
  void handshakeUnknownPeer() {
    PeerDiscoveryResource resource =
        new PeerDiscoveryResource(
            versionInformation, peerDatabase, serverPrivateKey, publicProvider);
    byte[] bytes =
        handshakeMessage(
            new Address("server", 2239),
            new Address("endpoint", 2234),
            clientPbcAddress,
            serverPbcAddress);
    byte[] payload = createEncryptedHandshake(bytes);
    Hash message = createMessage(payload);
    KeyPair otherKey = new KeyPair(BigInteger.valueOf(44));
    byte[] sign = createSignature(otherKey, message);
    EncryptedPayloadDto encryptedPayloadDto =
        EncryptedPayloadDto.create(clientEphemeralKey.getPublic().asBytes(), sign, payload);
    Assertions.assertThatThrownBy(() -> resource.handshake(encryptedPayloadDto))
        .isInstanceOf(WebApplicationException.class);
  }

  @Test
  void handshakeWasNotForUs() {
    PeerDiscoveryResource resource =
        new PeerDiscoveryResource(
            versionInformation, peerDatabase, serverPrivateKey, publicProvider);
    byte[] bytes =
        handshakeMessage(
            new Address("someone_else", 29),
            new Address("endpoint", 234),
            clientPbcAddress,
            serverPbcAddress);
    byte[] payload = createEncryptedHandshake(bytes);
    Hash message = createMessage(payload);
    byte[] sign = createSignature(clientPrivateKey, message);
    EncryptedPayloadDto encryptedPayloadDto =
        EncryptedPayloadDto.create(clientEphemeralKey.getPublic().asBytes(), sign, payload);
    Assertions.assertThatThrownBy(() -> resource.handshake(encryptedPayloadDto))
        .isInstanceOf(WebApplicationException.class);

    byte[] bytesWrongServer =
        handshakeMessage(
            new Address("server", 2219),
            new Address("endpoint", 234),
            clientPbcAddress,
            // Incorrect server pbc Address
            BlockchainAddress.fromString("000000000000000000000000000000000000000007"));
    byte[] payloadWrongServer = createEncryptedHandshake(bytesWrongServer);
    Hash messageWrongServer = createMessage(payloadWrongServer);
    byte[] signWrongServer = createSignature(clientPrivateKey, messageWrongServer);
    EncryptedPayloadDto encryptedPayloadDtoWrongServer =
        EncryptedPayloadDto.create(
            clientEphemeralKey.getPublic().asBytes(), signWrongServer, payloadWrongServer);
    Assertions.assertThatThrownBy(() -> resource.handshake(encryptedPayloadDtoWrongServer))
        .isInstanceOf(WebApplicationException.class);
  }

  private byte[] decryptHandshakeResponse(HandshakeDto handshake) {
    byte[] extraData =
        SafeDataOutputStream.serialize(
            s -> {
              s.write(versionInformation.toString().getBytes(StandardCharsets.UTF_8));
              s.writeString("server -> client");
            });
    Cipher aesForDecrypt =
        AesHelper.createAesForDecrypt(
            clientEphemeralKey.getPrivateKey(), serverPrivateKey.getPublic(), extraData);
    return ExceptionConverter.call(() -> aesForDecrypt.doFinal(handshake.data), "");
  }

  private byte[] createEncryptedHandshake(byte[] payload) {
    byte[] extraData =
        SafeDataOutputStream.serialize(
            s -> {
              s.write(versionInformation.toString().getBytes(StandardCharsets.UTF_8));
              s.writeString("client -> server");
            });
    Cipher aesForEncrypt =
        AesHelper.createAesForEncrypt(
            clientEphemeralKey.getPrivateKey(), serverPrivateKey.getPublic(), extraData);
    return ExceptionConverter.call(() -> aesForEncrypt.doFinal(payload), "");
  }

  private byte[] handshakeMessage(
      Address endpoint,
      Address clientEndpoint,
      BlockchainAddress clientPbcAddress,
      BlockchainAddress serverPbcAddress) {
    return SafeDataOutputStream.serialize(
        s -> {
          endpoint.write(s);
          serverPbcAddress.write(s);
          clientEndpoint.write(s);
          clientPbcAddress.write(s);
        });
  }

  private byte[] createSignature(KeyPair signingKey, Hash message) {
    return SafeDataOutputStream.serialize(s -> signingKey.sign(message).write(s));
  }

  private Hash createMessage(byte[] payload) {
    return Hash.create(
        s -> {
          serverPrivateKey.getPublic().write(s);
          clientEphemeralKey.getPublic().write(s);
          s.write(payload);
          s.writeString(versionInformation.toString());
        });
  }

  private List<PeerDiscoveryClient.DiscoveredPeer> decryptDiscoveryResponse(
      DiscoveryResponseDto discover, Hash extraData) {
    Cipher cipher =
        AesHelper.createAesForDecrypt(
            clientEphemeralKey.getPrivateKey(), serverPrivateKey.getPublic(), extraData.getBytes());
    byte[] data = ExceptionConverter.call(() -> cipher.doFinal(discover.data), "");
    SafeDataInputStream fromBytes = SafeDataInputStream.createFromBytes(data);
    long n = fromBytes.readInt();
    List<PeerDiscoveryClient.DiscoveredPeer> peers = new ArrayList<>();
    for (int i = 0; i < n; i++) {
      BlockchainPublicKey publicKey = BlockchainPublicKey.read(fromBytes);
      Address endpoint = Address.read(fromBytes);
      peers.add(new PeerDiscoveryClient.DiscoveredPeer(publicKey, endpoint));
    }
    return peers;
  }

  private byte[] createEncryptedPayloadForAddress(
      BlockchainPublicKey publicKey, Address myEndpoint, List<BlockchainPublicKey> neededPeers) {
    byte[] payload =
        SafeDataOutputStream.serialize(
            s -> {
              publicKey.write(s);
              new Address("endpoint", 2345).write(s);
              myEndpoint.write(s);
              BlockchainPublicKey.LIST_SERIALIZER.writeDynamic(s, neededPeers);
            });
    Cipher cipher =
        AesHelper.createAesForEncrypt(
            serverPrivateKey.getPrivateKey(),
            clientEphemeralKey.getPublic(),
            versionInformation.toString().getBytes(StandardCharsets.UTF_8));
    return ExceptionConverter.call(() -> cipher.doFinal(payload), "");
  }
}
