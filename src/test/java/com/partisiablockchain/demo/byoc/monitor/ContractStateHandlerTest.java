package com.partisiablockchain.demo.byoc.monitor;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.contract.StateSerializableEquality;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.KeyPair;
import com.partisiablockchain.crypto.Signature;
import com.partisiablockchain.demo.byoc.governance.largeoracle.Dispute;
import com.partisiablockchain.demo.byoc.governance.largeoracle.LargeOracleContractState;
import com.partisiablockchain.demo.byoc.governance.largeoracle.OldLargeOracleContractState;
import com.partisiablockchain.demo.byoc.governance.largeoracle.OracleDisputeId;
import com.partisiablockchain.demo.byoc.governance.largeoracle.OracleMember;
import com.partisiablockchain.demo.byoc.governance.largeoracle.SigningRequest;
import com.partisiablockchain.demo.byoc.governance.largeoracle.StakedTokens;
import com.partisiablockchain.demo.byoc.governance.priceoracle.DisputeInfo;
import com.partisiablockchain.demo.byoc.governance.priceoracle.PriceOracleContractState;
import com.partisiablockchain.demo.byoc.governance.priceoracle.PriceOracleNodes;
import com.partisiablockchain.demo.byoc.governance.priceoracle.RegistrationStatus;
import com.partisiablockchain.demo.byoc.governance.priceoracle.RoundPriceUpdates;
import com.partisiablockchain.math.Unsigned256;
import com.partisiablockchain.tree.AvlTree;
import com.secata.tools.immutable.FixedList;
import java.math.BigInteger;
import java.util.List;
import java.util.Map;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

final class ContractStateHandlerTest {

  @Test
  void priceOracleContract() {
    BlockchainAddress priceOracleContractAddress =
        BlockchainAddress.fromHash(
            BlockchainAddress.Type.CONTRACT_GOV, Hash.create(h -> h.writeString("PriceOracle")));
    PriceOracleContractState expectedState =
        new PriceOracleContractState(
            null,
            "REFERENCE_CONTRACT_ADDRESS",
            new RoundPriceUpdates(AvlTree.create()),
            Unsigned256.ZERO,
            "TEST",
            new PriceOracleNodes(
                AvlTree.create(
                    Map.of(
                        BlockchainAddress.fromHash(
                            BlockchainAddress.Type.ACCOUNT,
                            Hash.create(h -> h.writeString("NodeAddress"))),
                        RegistrationStatus.REGISTERED))),
            null,
            new DisputeInfo(null, false, null),
            "ChainID");

    ContractStateHandler contractStateHandler =
        new ContractStateHandler(List.of(priceOracleContractAddress));
    contractStateHandler.createContract(
        null, null, priceOracleContractAddress, null, expectedState);
    PriceOracleContractState state =
        contractStateHandler.getPriceOracleState(priceOracleContractAddress);

    StateSerializableEquality.assertStatesEqual(state, expectedState);
  }

  /**
   * Retrieving the state of the Large Oracle contract after the contract has been created returns
   * the state it was created with.
   */
  @Test
  public void getLargeOracleState() {
    final BlockchainAddress largeOracleContractAddress =
        BlockchainAddress.fromHash(
            BlockchainAddress.Type.CONTRACT_GOV, Hash.create(h -> h.writeString("LargeOracle")));

    final BlockchainAddress addressA =
        BlockchainAddress.fromHash(
            BlockchainAddress.Type.ACCOUNT, Hash.create(h -> h.writeString("AddressA")));
    final BlockchainAddress addressB =
        BlockchainAddress.fromHash(
            BlockchainAddress.Type.ACCOUNT, Hash.create(h -> h.writeString("AddressB")));

    final OracleMember oracleMemberA = new OracleMember(addressA);
    final OracleMember oracleMemberB = new OracleMember(addressB);
    final FixedList<OracleMember> oracleMembers =
        FixedList.create(List.of(oracleMemberA, oracleMemberB));

    final StakedTokens stakedTokensA = new StakedTokens(1111, 2222, null);
    final StakedTokens stakedTokensB = new StakedTokens(3333, 4444, 100L);
    final AvlTree<BlockchainAddress, StakedTokens> stakedTokens =
        AvlTree.create(Map.of(addressA, stakedTokensA, addressB, stakedTokensB));

    final Hash hashA = Hash.create(h -> h.writeString("HashA"));
    final Hash hashB = Hash.create(h -> h.writeString("HashB"));
    final SigningRequest signingRequestA = new SigningRequest(1111, hashA, hashB);
    final SigningRequest signingRequestB = new SigningRequest(2222, hashA, hashB);
    final FixedList<SigningRequest> pendingMessages =
        FixedList.create(List.of(signingRequestA, signingRequestB));

    final Signature signatureA = new KeyPair(BigInteger.valueOf(1111)).sign(hashA);
    final Signature signatureB = new KeyPair(BigInteger.valueOf(2222)).sign(hashB);
    final AvlTree<SigningRequest, Signature> signedMessages =
        AvlTree.create(Map.of(signingRequestA, signatureA, signingRequestB, signatureB));

    final OracleDisputeId oracleDisputeIdA = new OracleDisputeId(addressA, 1111, 2222);
    final OracleDisputeId oracleDisputeIdB = new OracleDisputeId(addressB, 3333, 4444);
    final Dispute disputeA = new Dispute(FixedList.create(List.of(1, 2, 3, 4)));
    final Dispute disputeB = new Dispute(FixedList.create(List.of(5, 6, 7, 8)));
    final AvlTree<OracleDisputeId, Dispute> activeDisputes =
        AvlTree.create(Map.of(oracleDisputeIdA, disputeA, oracleDisputeIdB, disputeB));

    LargeOracleContractState expectedState =
        new LargeOracleContractState(
            oracleMembers, stakedTokens, pendingMessages, 0, 0, signedMessages, activeDisputes);

    ContractStateHandler contractStateHandler =
        new ContractStateHandler(List.of(largeOracleContractAddress));
    contractStateHandler.createContract(
        null, null, largeOracleContractAddress, null, expectedState);

    LargeOracleContractState state =
        contractStateHandler.getLargeOracleState(largeOracleContractAddress);

    StateSerializableEquality.assertStatesEqual(state, expectedState);
  }

  @Test
  @DisplayName(
      "Retrieving the state of an old version of Large Oracle contract, returns the new version"
          + " with default values in the new fields.")
  void getOldLargeOracleState() {
    final BlockchainAddress largeOracleContractAddress =
        BlockchainAddress.fromHash(
            BlockchainAddress.Type.CONTRACT_GOV, Hash.create(h -> h.writeString("LargeOracle")));

    final BlockchainAddress addressA =
        BlockchainAddress.fromHash(
            BlockchainAddress.Type.ACCOUNT, Hash.create(h -> h.writeString("AddressA")));
    final BlockchainAddress addressB =
        BlockchainAddress.fromHash(
            BlockchainAddress.Type.ACCOUNT, Hash.create(h -> h.writeString("AddressB")));

    final OldLargeOracleContractState.OldStakedTokens stakedTokensA =
        new OldLargeOracleContractState.OldStakedTokens(1111, 2222);
    final OldLargeOracleContractState.OldStakedTokens stakedTokensB =
        new OldLargeOracleContractState.OldStakedTokens(3333, 4444);
    final AvlTree<BlockchainAddress, OldLargeOracleContractState.OldStakedTokens> stakedTokens =
        AvlTree.create(Map.of(addressA, stakedTokensA, addressB, stakedTokensB));

    OldLargeOracleContractState expectedState =
        new OldLargeOracleContractState(
            FixedList.create(),
            stakedTokens,
            FixedList.create(),
            0,
            0,
            AvlTree.create(),
            AvlTree.create());

    ContractStateHandler contractStateHandler =
        new ContractStateHandler(List.of(largeOracleContractAddress));
    contractStateHandler.createContract(
        null, null, largeOracleContractAddress, null, expectedState);

    LargeOracleContractState state =
        contractStateHandler.getLargeOracleState(largeOracleContractAddress);

    Assertions.assertThat(state.stakedTokens().getValue(addressA).expirationTimestamp())
        .isEqualTo(null);
    Assertions.assertThat(state.stakedTokens().getValue(addressB).expirationTimestamp())
        .isEqualTo(null);
  }
}
