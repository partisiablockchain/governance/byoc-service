package com.partisiablockchain.demo.byoc.monitor;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.client.transaction.SignedTransaction;
import java.util.List;

/** Common assertions for byoc process transactions. */
final class ByocProcessTestHelper {

  @SuppressWarnings("rawtypes")
  static TransactionRpc runProcess(
      ByocProcess process0,
      ByocProcess process1,
      DummyPbcClient client1,
      DummyPbcClient client2,
      List<BlockchainAddress> oracleNodes) {
    process0.run();
    process1.run();

    SignedTransaction transaction1 = client1.getLatestPut();
    assertThat(transaction1).isNotNull();
    assertThat(transaction1.getSignature().recoverSender(transaction1.getHash()))
        .isEqualTo(oracleNodes.get(0));

    SignedTransaction transaction2 = client2.getLatestPut();
    assertThat(transaction2).isNotNull();
    assertThat(transaction2.getSignature().recoverSender(transaction2.getHash()))
        .isEqualTo(oracleNodes.get(1));

    byte[] rpc1 = transaction1.getInner().getRpc();
    byte[] rpc2 = transaction2.getInner().getRpc();
    TransactionRpc result = new TransactionRpc();
    result.rpc1 = rpc1;
    result.rpc2 = rpc2;
    return result;
  }

  static final class TransactionRpc {

    byte[] rpc1;
    byte[] rpc2;
  }
}
