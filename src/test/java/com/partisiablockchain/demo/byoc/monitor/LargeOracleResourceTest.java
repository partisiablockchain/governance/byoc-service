package com.partisiablockchain.demo.byoc.monitor;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.crypto.AesHelper;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.KeyPair;
import com.partisiablockchain.crypto.Signature;
import com.partisiablockchain.demo.byoc.rest.dto.BroadcastMessageDto;
import com.partisiablockchain.demo.byoc.rest.dto.LargeOracleStatusDto;
import com.partisiablockchain.demo.byoc.rest.dto.PartialSignatureDto;
import com.partisiablockchain.demo.byoc.rest.dto.SignatureAndKeyDto;
import com.partisiablockchain.demo.byoc.rest.dto.UnicastMessageDto;
import com.partisiablockchain.governance.ThresholdSessionId;
import com.secata.tools.coverage.ExceptionConverter;
import jakarta.ws.rs.WebApplicationException;
import java.math.BigInteger;
import java.util.List;
import javax.crypto.Cipher;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

/** Test. */
public final class LargeOracleResourceTest {

  private final InMemoryMessageStorage storage = new InMemoryMessageStorage();
  private final LargeOracleStatusInformation statusInformation =
      Mockito.mock(LargeOracleStatusInformation.class);
  private final KeyPair serverRestKey = new KeyPair(BigInteger.valueOf(123));
  private final KeyPair clientRestKey = new KeyPair(BigInteger.valueOf(321));
  private final KeyPair clientEphemeralKey = new KeyPair(BigInteger.valueOf(456));

  private final LargeOracleResource resource =
      new LargeOracleResource(storage, statusInformation, serverRestKey);
  private final byte[] data = new byte[] {1, 2, 3, 4, 5};

  @Test
  void getBroadcastMessage() {
    Hash messageHash = Hash.create(s -> s.write(data));
    storage.setBroadcastMessage(messageHash, data);
    BroadcastMessageDto broadcastMessage = resource.getBroadcastMessage(messageHash.toString());
    Assertions.assertThat(broadcastMessage.message).isEqualTo(data);
  }

  @Test
  void getBroadcastMessageNoMessage() {
    Hash messageHash = Hash.create(s -> s.write(data));
    Assertions.assertThatThrownBy(() -> resource.getBroadcastMessage(messageHash.toString()))
        .isInstanceOf(WebApplicationException.class);
  }

  @Test
  void getUnicastMessages() {
    ThresholdSessionId sessionId = new ThresholdSessionId(1, 2);
    storage.setRoundUnicastMessage(sessionId, 3, clientRestKey.getPublic(), data);
    Hash message =
        Hash.create(
            s -> {
              serverRestKey.getPublic().write(s);
              clientEphemeralKey.getPublic().write(s);
              s.writeInt(1);
              s.writeInt(2);
              s.writeInt(3);
            });
    Signature sign = clientRestKey.sign(message);
    SignatureAndKeyDto dto = SignatureAndKeyDto.create(sign, clientEphemeralKey.getPublic());
    UnicastMessageDto unicastMessage = resource.getUnicastMessage(1, 2, 3, dto);
    Cipher cipher =
        AesHelper.createAesForDecrypt(
            clientEphemeralKey.getPrivateKey(), serverRestKey.getPublic(), message.getBytes());
    byte[] call = ExceptionConverter.call(() -> cipher.doFinal(unicastMessage.data), "");
    Assertions.assertThat(call).isEqualTo(data);
  }

  @Test
  void getUnicastMessageNoMessage() {
    Hash message = resource.getMessage(clientEphemeralKey.getPublic(), 1, 2, 3);
    Signature sign = clientRestKey.sign(message);
    Assertions.assertThatThrownBy(
            () ->
                resource.getUnicastMessage(
                    1, 2, 3, SignatureAndKeyDto.create(sign, clientEphemeralKey.getPublic())))
        .isInstanceOf(WebApplicationException.class);
  }

  @Test
  void getPartialSignature() {
    String signatureId = "abc123";
    storage.setPartialSignature(new ThresholdSessionId(1, 2), signatureId, data);
    PartialSignatureDto partialSignature = resource.getPartialSignature(1, 2, signatureId);
    Assertions.assertThat(partialSignature.partialSignature).isEqualTo(data);
  }

  @Test
  void getPartialSignatureNoData() {
    String signatureId = "abc123";
    Assertions.assertThatThrownBy(() -> resource.getPartialSignature(1, 2, signatureId))
        .isInstanceOf(WebApplicationException.class);
  }

  @Test
  void getStatus() {
    ThresholdSessionId thresholdSessionId = new ThresholdSessionId(1, 2);
    Mockito.when(statusInformation.getCurrentRoundNumber(thresholdSessionId)).thenReturn(3);
    Mockito.when(statusInformation.getSignatureIdsLeft(thresholdSessionId)).thenReturn(6);
    List<BlockchainPublicKey> parties =
        List.of(
            new KeyPair(BigInteger.valueOf(5)).getPublic(),
            new KeyPair(BigInteger.valueOf(6)).getPublic(),
            new KeyPair(BigInteger.valueOf(7)).getPublic(),
            new KeyPair(BigInteger.valueOf(8)).getPublic());
    Mockito.when(statusInformation.getHonestParties(thresholdSessionId)).thenReturn(parties);
    Mockito.when(statusInformation.getUnicastResponders(thresholdSessionId))
        .thenReturn(parties.subList(1, 4));
    LargeOracleStatusDto status = resource.status(1, 2);
    Assertions.assertThat(status.currentKeyGenRound).isEqualTo(3);
    Assertions.assertThat(status.preSignaturesLeft).isEqualTo(6);
    Assertions.assertThat(status.honestParties)
        .containsExactlyElementsOf(parties.stream().map(BlockchainPublicKey::asBytes).toList());
    Assertions.assertThat(status.unicastResponders)
        .containsExactlyElementsOf(
            parties.subList(1, 4).stream().map(BlockchainPublicKey::asBytes).toList());
  }
}
