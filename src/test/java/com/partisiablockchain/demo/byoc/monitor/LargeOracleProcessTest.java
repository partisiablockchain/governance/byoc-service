package com.partisiablockchain.demo.byoc.monitor;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.client.BlockchainContractClient;
import com.partisiablockchain.client.transaction.BlockchainTransactionClient;
import com.partisiablockchain.client.transaction.SenderAuthenticationKeyPair;
import com.partisiablockchain.client.transaction.SignedTransaction;
import com.partisiablockchain.client.web.WebClient;
import com.partisiablockchain.crypto.AesHelper;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.KeyPair;
import com.partisiablockchain.crypto.Signature;
import com.partisiablockchain.demo.byoc.Address;
import com.partisiablockchain.demo.byoc.governance.bporchestration.Bitmap;
import com.partisiablockchain.demo.byoc.governance.bporchestration.BlockProducer;
import com.partisiablockchain.demo.byoc.governance.bporchestration.BpOrchestrationContractHelper;
import com.partisiablockchain.demo.byoc.governance.bporchestration.BpOrchestrationContractState;
import com.partisiablockchain.demo.byoc.governance.bporchestration.LargeOracleUpdate;
import com.partisiablockchain.demo.byoc.governance.bporchestration.ThresholdKey;
import com.partisiablockchain.demo.byoc.governance.largeoracle.LargeOracleContractHelper;
import com.partisiablockchain.demo.byoc.governance.largeoracle.LargeOracleContractState;
import com.partisiablockchain.demo.byoc.governance.largeoracle.OracleMember;
import com.partisiablockchain.demo.byoc.governance.largeoracle.SigningRequest;
import com.partisiablockchain.demo.byoc.rest.dto.BroadcastMessageDto;
import com.partisiablockchain.demo.byoc.rest.dto.HandshakeDto;
import com.partisiablockchain.demo.byoc.rest.dto.PartialSignatureDto;
import com.partisiablockchain.demo.byoc.rest.dto.UnicastMessageDto;
import com.partisiablockchain.dto.ContractState;
import com.partisiablockchain.governance.KeyGenMessageRetriever;
import com.partisiablockchain.governance.MessageStorage;
import com.partisiablockchain.governance.ThresholdKeyGenProtocol;
import com.partisiablockchain.governance.ThresholdPublicKey;
import com.partisiablockchain.governance.ThresholdSessionId;
import com.partisiablockchain.governance.ThresholdSignatureProvider;
import com.partisiablockchain.serialization.StateObjectMapper;
import com.partisiablockchain.serialization.StateSerializable;
import com.partisiablockchain.serialization.StateVoid;
import com.partisiablockchain.server.tcp.client.Client;
import com.partisiablockchain.tree.AvlTree;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import com.secata.tools.coverage.ExceptionConverter;
import com.secata.tools.immutable.FixedList;
import jakarta.ws.rs.WebApplicationException;
import jakarta.ws.rs.core.Response;
import java.io.IOException;
import java.math.BigInteger;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;
import javax.crypto.Cipher;
import org.assertj.core.api.Assertions;
import org.bouncycastle.util.encoders.Hex;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

/** Test. */
public final class LargeOracleProcessTest {

  private static final byte BPO_ADD_CANDIDATE_KEY = LargeOracleProcess.BPO_ADD_CANDIDATE_KEY;
  private static final byte BPO_ADD_HEARTBEAT = LargeOracleProcess.BPO_ADD_HEARTBEAT;
  private static final byte BPO_ADD_BROADCAST_HASH = LargeOracleProcess.BPO_ADD_BROADCAST_HASH;
  private static final byte BPO_ADD_BROADCAST_BITS = LargeOracleProcess.BPO_ADD_BROADCAST_BITS;
  private static final byte BPO_AUTHORIZE_NEW_ORACLE = LargeOracleProcess.BPO_AUTHORIZE_NEW_ORACLE;

  private static final byte LO_ADD_SIGNATURE = LargeOracleProcess.LO_ADD_SIGNATURE;

  static final String SHARD = "shard";

  static final VersionInformation VERSION_INFORMATION = new VersionInformation("1.2.3");
  static final BlockchainAddress BPO_CONTRACT =
      BlockchainAddress.fromString("010000000000000000000000000000000000000001");
  private static final BlockchainAddress LARGE_ORACLE_CONTRACT =
      BlockchainAddress.fromString("010000000000000000000000000000000000000002");

  // Initial contract state values
  private static final BlockchainAddress INITIAL_BP_ADDRESS =
      BlockchainAddress.fromString("000000000000000000000000000000000000000999");
  private static final KeyPair INITIAL_BP_KEY = new KeyPair(BigInteger.TEN);
  private static final BlockchainPublicKey INITIAL_BP_PUBLIC_KEY = INITIAL_BP_KEY.getPublic();

  private static final BlockProducer INITIAL_BP =
      new BlockProducer(INITIAL_BP_ADDRESS, INITIAL_BP_PUBLIC_KEY);

  private static final KeyPair INITIAL_THRESHOLD_KEY = new KeyPair(BigInteger.valueOf(42));
  private static final BlockchainPublicKey INITIAL_THRESHOLD_PUBLIC_KEY =
      INITIAL_THRESHOLD_KEY.getPublic();
  private static final String INITIAL_THRESHOLD_KEY_ID = "INITIAL_KEY";

  static final KeyPair DUMMY_EPHEMERAL_KEY = new KeyPair(BigInteger.valueOf(12345));
  private final ContractStateHandler contractStateHandler =
      new ContractStateHandler(List.of(BPO_CONTRACT, LARGE_ORACLE_CONTRACT));

  private Map<BlockchainAddress, Client.Listener> listeners;
  // these are populated by calling different setup methods. After calling setup(N, *) all these
  // lists each contain N elements.
  private final List<BlockProducer> producers = new ArrayList<>();
  private final List<KeyPair> producerRestServerKeys = new ArrayList<>();
  private final List<BlockchainTransactionClient> producerTransactionClients = new ArrayList<>();
  private final List<BlockchainContractClient> producerOracleClients = new ArrayList<>();
  private final List<WebClient> producerWebClients = new ArrayList<>();
  private final List<InMemoryMessageStorage> producerStorages = new ArrayList<>();
  private final List<PeerDatabase> producerPeerDatabases = new ArrayList<>();
  private final List<LargeOracleProcess> processes = new ArrayList<>();

  private final StateWrapper contractStates =
      new StateWrapper(
          new BpOrchestrationContractState(
              0,
              0,
              new ThresholdKey(INITIAL_THRESHOLD_PUBLIC_KEY, INITIAL_THRESHOLD_KEY_ID),
              null,
              AvlTree.create(
                  Stream.of(INITIAL_BP)
                      .collect(Collectors.toMap(BlockProducer::identity, producer -> producer))),
              FixedList.create(List.of(INITIAL_BP)),
              Hash.fromString("5f02a327a959a08bee16235910ad712c36507992cb310c9e064c96f27d92a3e7")),
          new LargeOracleContractState(
              FixedList.create(List.of(new OracleMember(INITIAL_BP_ADDRESS))),
              AvlTree.create(),
              FixedList.create(),
              42,
              42,
              AvlTree.create(),
              AvlTree.create()));

  /** Create default listeners. */
  @BeforeEach
  public void setupTest() {
    createListeners();
  }

  /** PbcClient used for testing. Will return a contract state the content of which we control. */
  static final class LargeOracleClient extends DummyPbcClient<StateVoid> {
    private final StateWrapper wrapper;

    public LargeOracleClient(StateWrapper wrapper) {
      super(null);
      this.wrapper = wrapper;
    }

    @Override
    public ContractState getContractState(BlockchainAddress address) {
      ObjectMapper objectMapper = StateObjectMapper.createObjectMapper();
      JsonNode serializedContract;
      if (address.equals(LARGE_ORACLE_CONTRACT)) {
        serializedContract =
            objectMapper.convertValue(wrapper.largeOracleContractState, JsonNode.class);
      } else if (address.equals(BPO_CONTRACT)) {
        serializedContract =
            objectMapper.convertValue(wrapper.bpOrchestrationContractState, JsonNode.class);
      } else {
        throw new IllegalArgumentException("unknown address: " + address);
      }

      return new ContractState(null, null, null, 0, serializedContract, null);
    }
  }

  @Test
  void heartbeatWithEveryoneAlive() {
    setup(4, 0);
    initializeOracleUpdate(producers);

    prepareHandshake(0, 1);
    prepareHandshake(0, 2);
    prepareHandshake(0, 3);

    prepareTransactions(1, 0);

    run(0);

    assertThatProducerSentHeartbeat(0, new byte[] {0b1111});
    // party 0 already produced a heartbeat and so wont produce a second.
    run(0);
  }

  @Test
  void heartbeatWithEveryoneExceptOne() {
    setup(4, 0);
    initializeOracleUpdate(producers);

    prepareHandshake(0, 1);
    prepareHandshake(0, 3);

    prepareTransactions(1, 0);
    run(0);

    assertThatProducerSentHeartbeat(0, new byte[] {0b1011});
  }

  @Test
  void noHeartbeatIfNotParticipant() {
    setup(4, 1);

    final LargeOracleProcess notParticipating = createProcessForInitialProducer();
    initializeOracleUpdate(producers);
    updateListeners();
    notParticipating.run();
  }

  @Test
  void insufficientNumberOfHeartbeats() {
    setup(4, 0);
    initializeOracleUpdate(producers);
    prepareHandshake(0, 1);
    run(0);
    assertThatProducersDidNotSendTransaction(0, 1, 2, 3);
  }

  @Test
  void wontSendHeartbeatBeforeThresholdHasBeenMet() {
    setup(7, 0);
    initializeOracleUpdate(producers);

    prepareInvalidHandshake(0);

    prepareHandshake(0, 1);
    prepareHandshake(0, 2);
    run(0);
    prepareHandshake(0, 3);
    prepareHandshake(0, 4);
    prepareTransactions(1);
    run(0);
  }

  @Test
  void statusInformationWithInvalidSessionId() {
    setup(1, 0);
    LargeOracleProcess process = processes.get(0);
    ThresholdSessionId invalid = new ThresholdSessionId(123, 456);
    Assertions.assertThat(process.getCurrentRoundNumber(invalid)).isEqualTo(-1);
    Assertions.assertThat(process.getSignatureIdsLeft(invalid)).isEqualTo(-1);
    Assertions.assertThat(process.getHonestParties(invalid)).isEqualTo(null);
    Assertions.assertThat(process.getUnicastResponders(invalid)).isEmpty();
  }

  @Test
  void onlySendCandidateKeyOnce() {
    runProcessTillCandidateKeyIsGenerated();
    getCandidateKeySentByProducer(0);
    final LargeOracleClient client = (LargeOracleClient) producerOracleClients.get(0);
    run(0);
    assertThatProducersSentSameCandidateKey(1, 2);
    final int before = client.getTransactionsCount();
    updateListeners();
    runEager(3);
    run();
    int after = client.getTransactionsCount();
    Assertions.assertThat(before).isEqualTo(after);
  }

  @Test
  void keyGeneration() {
    runProcessTillCandidateKeyIsGenerated();
    assertThatProducersSentSameCandidateKey(0, 1, 2);
    runEager(3);

    // no more work to do for the parties until the new oracle has been signed.
    run();
    confirmInitialOracle();
    run();

    ThresholdSessionId currentSessionId = getCurrentSessionId();
    List<BlockchainPublicKey> honest = producers.stream().map(BlockProducer::publicKey).toList();
    for (LargeOracleProcess process : processes) {
      Assertions.assertThat(process.getHonestParties(currentSessionId)).isEqualTo(honest);
      Assertions.assertThat(process.getSignatureIdsLeft(currentSessionId)).isEqualTo(9);
      ThresholdSignatureProvider signer = process.getSigner(currentSessionId);
      Assertions.assertThat(signer.getHonestParties())
          .containsExactlyInAnyOrderElementsOf(
              producers.stream().map(BlockProducer::publicKey).toList());
    }
  }

  private void runProcessTillCandidateKeyIsGenerated() {
    setup(4, 10);
    initializeOracleUpdate(producers);
    setActiveProducers(0, 1, 2, 3);

    for (LargeOracleProcess process : processes) {
      Assertions.assertThat(process.getUnicastMessages()).isEmpty();
    }
    // create new key-gen object, send broadcast hashes
    prepareTransactions(1);
    runEager();
    assertThatProducersSentBroadcastHashes(2);
    assertThatProtocolRoundIs(2);

    // collect unicast and broadcast messages. 2t+1 producers will send a bitmap.
    prepareTransactions(1, 0, 1, 2);
    prepareUnicastMessagesForRound2();
    prepareBroadcastEchos(2);
    for (LargeOracleProcess process : processes) {
      Assertions.assertThat(process.getUnicastMessages()).hasSize(1);
    }
    run(0, 1, 2);
    run(0, 1, 2);
    assertThatProducersSentBitmaps(0, 1, 2);
    assertThatWeCanAdvanceToNextRound();
    runEager(3);
    for (LargeOracleProcess process : processes) {
      Assertions.assertThat(process.getUnicastMessages()).hasSize(4);
    }

    // advance to next round
    prepareTransactions(1);
    runEager();
    assertThatProtocolRoundIs(3);
    for (LargeOracleProcess process : processes) {
      Assertions.assertThat(process.getUnicastMessages()).isEmpty();
    }

    assertThatProducersSentBroadcastHashes(3);

    // echo. Note that this round, everyone will broadcast the same message, so the parties will
    // immediately see that they
    prepareTransactions(1, 0, 1, 2);
    run(0, 1, 2);
    run(0, 1, 2);
    assertThatProducersSentBitmaps(0, 1, 2);
    assertThatWeCanAdvanceToNextRound();

    // advance
    prepareTransactions(1);
    runEager();
    assertThatProtocolRoundIs(4);
    assertThatProducersSentBroadcastHashes(4);

    // this round is the same as the previous: Everyone sends the same message, so technically no
    // need to echo anything.
    prepareTransactions(1, 0, 1, 3);
    run(0, 1, 3);
    run(0, 1, 3);
    assertThatProducersSentBitmaps(0, 1, 3);
    assertThatWeCanAdvanceToNextRound();

    // advance
    prepareTransactions(1);
    runEager();
    assertThatProtocolRoundIs(5);
    assertThatProducersSentBroadcastHashes(5);

    // now parties are sending different messages again, so the last party have to run in order to
    // catch up.
    prepareTransactions(1, 0, 1, 2);
    prepareBroadcastEchos(5);
    run(0, 1, 2);
    run(0, 1, 2);
    assertThatProducersSentBitmaps(0, 1, 2);
    assertThatWeCanAdvanceToNextRound();
    runEager(3);

    // advance
    prepareTransactions(1);
    runEager();
    assertThatProtocolRoundIs(6);
    assertThatProducersSentBroadcastHashes(6);

    prepareTransactions(1, 0, 1, 2);
    prepareBroadcastEchos(6);
    run(0, 1, 2);
    run(0, 1, 2);
    assertThatProducersSentBitmaps(0, 1, 2);
    assertThatWeCanAdvanceToNextRound();

    // advance
    prepareTransactions(1);
    runEager();
    assertThatProtocolRoundIs(7);
    assertThatProducersSentBroadcastHashes(7);

    prepareTransactions(1, 1, 2, 3);
    prepareBroadcastEchos(7);
    run(1, 2, 3);
    run(1, 2, 3);
    assertThatProducersSentBitmaps(1, 2, 3);
    assertThatWeCanAdvanceToNextRound();
    runEager(0);

    // generate signature providers
    prepareTransactions(1, 0, 1, 2);
    runEager(0, 1, 2);
  }

  @Test
  void protocolIdsToAddresses() {
    setup(5, 0);
    List<BlockchainPublicKey> keys = producers.stream().map(BlockProducer::publicKey).toList();
    List<BlockchainPublicKey> someOfTheKeys = keys.subList(0, 3);
    Bitmap all = LargeOracleProcess.protocolIdsToBitmap(producers, keys);
    Assertions.assertThat(all.popCount()).isEqualTo(5);
    Bitmap some = LargeOracleProcess.protocolIdsToBitmap(producers, someOfTheKeys);
    Assertions.assertThat(some.popCount()).isEqualTo(3);
  }

  @Test
  void missingMessagesAreNotIncludedInPublishedBitmap() {
    setup(4, 10);
    initializeOracleUpdate(producers);
    setAllActive();

    prepareTransactions(1);
    runEager();
    assertThatProducersSentBroadcastHashes(2);

    prepareTransactions(1, 0, 1, 3);
    prepareUnicastMessagesForRound2();
    prepareBroadcastEchos(2, 0, 1, 3);
    prepareBroadcastEchos(2, 1, 0, 3);
    prepareBroadcastEchos(2, 3, 0, 1);
    run(0, 1, 3);
    runEager(2);
    run(0, 1, 3);
    runEager(2);
    assertThatProducerSentBroadcastBitmap(0, new byte[] {0b1011});
    assertThatProducerSentBroadcastBitmap(1, new byte[] {0b1011});
    assertThatProducerSentBroadcastBitmap(3, new byte[] {0b1011});
  }

  @Test
  void wontPublishBitmapIfMissingUnicast() {
    setup(7, 5);
    initializeOracleUpdate(producers);
    setAllActive();

    prepareTransactions(1);
    runEager();
    assertThatProducersSentBroadcastHashes(2);

    // required to prevent null pointer exceptions
    prepareDummyUnicastMessages();

    prepareBroadcastEchos(2, 0, 1, 2, 3, 4);
    prepareUnicastMessagesForRound2(0, 1);
    prepareUnicastMessagesForRound2(0, 2);
    prepareUnicastMessagesForRound2(0, 3);
    runEager(0);
    List<BlockchainPublicKey> unicastResponders =
        processes.get(0).getUnicastResponders(getCurrentSessionId());
    Assertions.assertThat(unicastResponders)
        .containsExactlyInAnyOrder(
            producers.get(1).publicKey(),
            producers.get(2).publicKey(),
            producers.get(3).publicKey(),
            producers.get(0).publicKey());

    // now 0 has 2t+1 unicast and broadcast messages. The bitmap is only published at this point.
    prepareTransactions(1, 0);
    prepareUnicastMessagesForRound2(0, 4);
    run(0);
    run(0);
    assertThatProducerSentBroadcastBitmap(0, new byte[] {0b11111});
  }

  @Test
  void wontPublishBitmapIfMissingBroadcasts() {
    setup(7, 5);
    initializeOracleUpdate(producers);
    setAllActive();

    prepareTransactions(1);
    runEager();
    assertThatProducersSentBroadcastHashes(2);
    prepareTransactions(1, 0, 1, 2, 3, 4);
    prepareUnicastMessagesForRound2();
    prepareBroadcastEchos(2);
    run(0, 1, 2, 3, 4);
    run(0, 1, 2, 3, 4);
    assertThatProducersSentBitmaps(0, 1, 2, 3, 4);
    assertThatWeCanAdvanceToNextRound();
    runEager(5, 6);

    prepareTransactions(1);
    runEager();
    assertThatProducersSentBroadcastHashes(3);
    prepareBroadcastEchos(3);
    prepareTransactions(1, 0, 1, 2, 3, 4);
    run(0, 1, 2, 3, 4);
    run(0, 1, 2, 3, 4);
    assertThatProducersSentBitmaps(0, 1, 2, 3, 4);
    assertThatWeCanAdvanceToNextRound();

    prepareTransactions(1);
    runEager();
    assertThatProducersSentBroadcastHashes(4);
    prepareBroadcastEchos(4);
    prepareTransactions(1, 0, 1, 2, 3, 4);
    run(0, 1, 2, 3, 4);
    run(0, 1, 2, 3, 4);
    assertThatProducersSentBitmaps(0, 1, 2, 3, 4);
    assertThatWeCanAdvanceToNextRound();

    prepareTransactions(1);
    runEager();
    assertThatProducersSentBroadcastHashes(5);
    prepareBroadcastEchos(5, 0, 1);
    prepareBroadcastEchos(5, 0, 2);
    runEager(0);
    prepareBroadcastEchos(5, 0, 3);
    runEager(0);
    prepareBroadcastEchos(5, 0, 4);
    prepareTransactions(1, 0);
    run(0);
    run(0);
  }

  @Test
  void doNotCollectNonExistingBroadcastMessages() {
    setup(4, 5);
    initializeOracleUpdate(producers);
    setAllActive();

    prepareTransactions(1, 1, 2, 3);
    runEager(1, 2, 3);
    assertThatProducersSentBroadcastHashes(2, 1, 2, 3);
    prepareBroadcastEchos(2);
    prepareTransactions(2, 0);
    runEager(0);
    prepareUnicastMessagesForRound2();
    run();
  }

  @Test
  void broadcastHashIsSentEvenIfSomeTransactionsFail() {
    setup(4, 5);
    initializeOracleUpdate(producers);
    setAllActive();

    prepareTransactions(1);
    runEager();
    assertThatProducersSentBroadcastHashes(2);
    prepareTransactions(1, 0, 1, 2);
    prepareBroadcastEchos(2);
    prepareUnicastMessagesForRound2();
    run(0, 1, 2);
    run(0, 1, 2);
    assertThatProducersSentBitmaps(0, 1, 2);
    assertThatWeCanAdvanceToNextRound();

    prepareTransactions(2, 0);
    LargeOracleClient client = (LargeOracleClient) producerOracleClients.get(0);
    final int before = client.getTransactionsCount();
    runEager(0);
    runEager(0);
    int after = client.getTransactionsCount();
    Assertions.assertThat(after).isEqualTo(before + 2);
    ThresholdSessionId nextSessionId = getNextSessionId();
    Hash hash = producerStorages.get(0).getMyBroadcastMessageHash(nextSessionId, 3);
    byte[] sendBroadcastBytes = getSendBroadcastTxBytes(nextSessionId, 2, hash);
    SignedTransaction first = client.getTransactions().get(after - 2);
    SignedTransaction last = client.getTransactions().get(after - 1);
    Assertions.assertThat(first.getInner().getRpc()).isEqualTo(sendBroadcastBytes);
    Assertions.assertThat(last.getInner().getRpc()).isEqualTo(sendBroadcastBytes);
  }

  @Test
  void threshold() {
    // This silly test is needed because the (n - 1)-must-divide-3 restriction on the number of
    // active producers results in a pitest mutation that's impossible to cover with more meaningful
    // test (like the one above).
    Assertions.assertThat(LargeOracleProcess.computeThreshold(8)).isEqualTo(2);
  }

  @Test
  void shouldNotSendBroadcastIfBitmapHasNotChanged() {
    setup(4, 10);
    initializeOracleUpdate(producers);
    setAllActive();
    producerPeerDatabases.get(0).updateBlockProducerEndpoint(producers.get(1).publicKey(), null);

    prepareTransactions(1);
    runEager();
    assertThatProducersSentBroadcastHashes(2);
    LargeOracleClient client = (LargeOracleClient) producerOracleClients.get(0);
    final int producerZeroTransactionCount = client.getTransactionsCount();

    prepareTransactions(2, 0, 2, 3);
    prepareUnicastMessagesForRound2();
    prepareBroadcastEchos(2);
    run(0, 2, 3);
    run(0, 2, 3);

    int producerZeroTransactionCountUpdated = client.getTransactionsCount();
    assertThatProducerSentBroadcastBitmap(0, new byte[] {0b1101});
    Assertions.assertThat(producerZeroTransactionCount + 1)
        .isEqualTo(producerZeroTransactionCountUpdated);
    run();

    // The producer should not have sent a transaction as the bitmap did not change
    int numberOfTransactionForProducerZeroNotChanged = client.getTransactionsCount();
    Assertions.assertThat(producerZeroTransactionCountUpdated)
        .isEqualTo(numberOfTransactionForProducerZeroNotChanged);
  }

  @Test
  void producerWithMissingEndpointStillGetsMessages() {
    setup(4, 10);
    initializeOracleUpdate(producers);
    setAllActive();

    // remove party 1 from party 0's peer database
    producerPeerDatabases.get(0).updateBlockProducerEndpoint(producers.get(1).publicKey(), null);

    prepareTransactions(1);
    runEager();
    assertThatProducersSentBroadcastHashes(2);

    prepareTransactions(1, 0, 2, 3);
    prepareUnicastMessagesForRound2();
    prepareBroadcastEchos(2);
    run(0);
    run(0);
    assertThatProducerSentBroadcastBitmap(0, new byte[] {0b1101});
    run(2);
    run(2);
    assertThatProducerSentBroadcastBitmap(2, new byte[] {0b1111});
    run(3);
    run(3);
    assertThatProducerSentBroadcastBitmap(3, new byte[] {0b1111});
    assertThatWeCanAdvanceToNextRound();

    // make sure that producer 0 is able to producer 1's message through some other producer.
    prepareBroadcastEcho(2, 0, 2, 1);
    runEager(0);
    KeyGenMessageRetriever messageRetriever =
        processes
            .get(0)
            .getMessageRetriever(contractStates.bpOrchestrationContractState.oracleUpdate());
    byte[] data = messageRetriever.retrieveBroadcastMessage(producers.get(1).publicKey());
    InMemoryMessageStorage storage = producerStorages.get(1);
    Hash missingHash = storage.getMyBroadcastMessageHash(getNextSessionId(), 2);
    byte[] missingData = storage.getBroadcastMessage(missingHash);
    Assertions.assertThat(data).isEqualTo(missingData);

    prepareTransactions(1, 0);
    // advance to next round
    runEager(0);
    Assertions.assertThat(processes.get(0).getCurrentRoundNumber(getNextSessionId())).isEqualTo(3);
    assertThatProducersSentBroadcastHashes(3, 0);

    List<BlockProducer> broadcasters =
        contractStates.bpOrchestrationContractState.oracleUpdate().getBroadcasters(1);
    Assertions.assertThat(broadcasters).isEqualTo(producers);
  }

  @Test
  void canResumeKeyGen() throws IOException {
    setupProducers(4, 0);
    // producer 0 uses a real database
    Path tempDir = Files.createTempDirectory("canResumeAndCompleteKeyGen");
    String databaseName = tempDir + "/lo.db";
    createProcessFromIndexWithDatabase(0, databaseName);

    // other producers use in-memory db
    createProcessFromIndex(1, 5);
    createProcessFromIndex(2, 5);
    createProcessFromIndex(3, 5);

    initializeOracleUpdate(producers);
    setAllActive();

    prepareTransactions(1);
    runEager();
    assertThatProducersSentBroadcastHashes(2);
    prepareTransactions(1, 0, 1, 2);
    prepareBroadcastEchos(2);
    prepareUnicastMessagesForRound2();
    run(0, 1, 2);
    run(0, 1, 2);
    assertThatProducersSentBitmaps(0, 1, 2);

    assertThatWeCanAdvanceToNextRound();
    runEager(3);

    BlockchainTransactionClient transactionClient = producerTransactionClients.get(0);
    producerStorages.set(0, new InMemoryMessageStorage());
    LargeOracleProcess restartedProcess =
        createProcess(
            transactionClient,
            producerStorages.get(0),
            producers.get(0).identity(),
            producerRestServerKeys.get(0),
            producerPeerDatabases.get(0),
            5,
            databaseName,
            "",
            producerWebClients.get(0),
            contractStateHandler);
    processes.set(0, restartedProcess);

    runEager(0);
    ThresholdKeyGenProtocol.Step step = processes.get(0).getCurrentStep(getNextSessionId());
    Assertions.assertThat(step).isNotNull();
    Assertions.assertThat(processes.get(0).getCurrentRoundNumber(getNextSessionId())).isEqualTo(3);
  }

  @Test
  void messageRetriever() {
    setup(4, 1);
    initializeOracleUpdate(producers);
    setAllActive();

    // ensure that broadcasters for the first round has been picked
    Hash hash = Hash.create(s -> s.writeString("message"));
    LargeOracleUpdate update = contractStates.bpOrchestrationContractState.oracleUpdate();
    update = BpOrchestrationContractHelper.addBroadcast(update, producers.get(0).identity(), hash);
    update = BpOrchestrationContractHelper.addBroadcast(update, producers.get(1).identity(), hash);
    update = BpOrchestrationContractHelper.addBroadcast(update, producers.get(2).identity(), hash);
    byte[] bits = {0b111};
    update =
        BpOrchestrationContractHelper.updateBroadcastBits(
            update, producers.get(0).identity(), bits);
    update =
        BpOrchestrationContractHelper.updateBroadcastBits(
            update, producers.get(1).identity(), bits);
    update =
        BpOrchestrationContractHelper.updateBroadcastBits(
            update, producers.get(2).identity(), bits);
    update = BpOrchestrationContractHelper.advanceBroadcastRound(update);

    KeyGenMessageRetriever messageRetriever = processes.get(0).getMessageRetriever(update);

    byte[] bytes = messageRetriever.retrieveBroadcastMessage(INITIAL_BP_PUBLIC_KEY);
    Assertions.assertThat(bytes).isNull();
  }

  @Test
  void loadFromDatabase() {
    setupForSigning();
    run();
    ThresholdPublicKey expected =
        new ThresholdPublicKey(
            BlockchainPublicKey.fromEncodedEcPoint(
                Hex.decode("0318ba1b86ee6a2656f1e454e203cccacaf6fdb24617eff12d111f909634e7e584")),
            "I7f6NX3qHgvmVlVks2inPsStqcRP5aF3rioegzikNSw");
    for (LargeOracleProcess process : processes) {
      ThresholdSessionId currentSessionId = getCurrentSessionId();
      Assertions.assertThat(process.getSignatureIdsLeft(currentSessionId)).isEqualTo(4);
      ThresholdSignatureProvider signer = process.getSigner(currentSessionId);
      Assertions.assertThat(signer.getHonestParties())
          .isEqualTo(producerRestServerKeys.stream().map(KeyPair::getPublic).toList());
      Assertions.assertThat(signer.getPublicKey().getPublicKey())
          .isEqualTo(expected.getPublicKey());
      Assertions.assertThat(signer.getPublicKey().getPublicKeyId())
          .isEqualTo(expected.getPublicKeyId());
    }
  }

  @Test
  void cannotLoadFromDatabase() {
    setup(4, 10);
    LargeOracleProcess faulty =
        createProcess(
            createTransactionClient(contractStates, INITIAL_BP_KEY),
            new InMemoryMessageStorage(),
            INITIAL_BP_ADDRESS,
            INITIAL_BP_KEY,
            new SimplePeerDatabase(new HashMap<>()),
            10,
            "not_a_database.db",
            "also_not_a_database.db",
            Mockito.mock(WebClient.class),
            contractStateHandler);
    Assertions.assertThat(faulty.getSigner(getCurrentSessionId())).isNull();
    updateListeners();
    faulty.run();
    Assertions.assertThat(faulty.getSigner(getCurrentSessionId())).isNull();

    ContractStateHandler anotherContractStateHandler =
        new ContractStateHandler(List.of(LARGE_ORACLE_CONTRACT, BPO_CONTRACT));
    LargeOracleProcess anotherFaultyProcess =
        createProcess(
            createTransactionClient(contractStates, INITIAL_BP_KEY),
            new InMemoryMessageStorage(),
            INITIAL_BP_ADDRESS,
            INITIAL_BP_KEY,
            new SimplePeerDatabase(new HashMap<>()),
            10,
            "src/test/resources/faulty.db",
            "",
            Mockito.mock(WebClient.class),
            anotherContractStateHandler);

    processes.set(0, anotherFaultyProcess);
    initializeOracleUpdate(
        List.of(INITIAL_BP, producers.get(0), producers.get(1), producers.get(2)));
    setAllActive();
    createListeners(anotherContractStateHandler);
    Assertions.assertThatThrownBy(() -> run(0)).isInstanceOf(RuntimeException.class);
  }

  @Test
  void databaseExistsButKeyDoesNot() {
    LargeOracleProcess realDbMissingKey =
        createProcess(
            createTransactionClient(contractStates, INITIAL_BP_KEY),
            new InMemoryMessageStorage(),
            INITIAL_BP_ADDRESS,
            INITIAL_BP_KEY,
            new SimplePeerDatabase(new HashMap<>()),
            10,
            "src/test/resources/data_0.db",
            "",
            Mockito.mock(WebClient.class),
            contractStateHandler);
    Assertions.assertThat(realDbMissingKey.getSigner(getCurrentSessionId())).isNull();
    realDbMissingKey.run();
    Assertions.assertThat(realDbMissingKey.getSigner(getCurrentSessionId())).isNull();
  }

  @Test
  public void setProducerInTheStart() {
    setup(1, 3);
    List<BlockProducer> producersInBp =
        contractStates.bpOrchestrationContractState.getConfirmedBlockProducers();
    Assertions.assertThat(
            producerPeerDatabases
                .get(0)
                .isRegisteredBlockProducer(producersInBp.get(0).publicKey()))
        .isFalse();
    run();

    Assertions.assertThat(
            producerPeerDatabases
                .get(0)
                .isRegisteredBlockProducer(producersInBp.get(0).publicKey()))
        .isTrue();
  }

  @Test
  void signMessages() {
    setupForSigning();

    Hash message0 = Hash.create(s -> s.writeString("Sign me"));
    Hash transactionHash0 = Hash.create(s -> s.writeString("transaction hash 0"));
    Hash message1 = Hash.create(s -> s.writeString("Sign me as well"));
    Hash transactionHash1 = Hash.create(s -> s.writeString("transaction hash 1"));
    final int currentMessageNonce = contractStates.largeOracleContractState.currentMessageNonce();
    set(
        LargeOracleContractHelper.addMessageForSigning(
            contractStates.largeOracleContractState, message0, transactionHash0));
    set(
        LargeOracleContractHelper.addMessageForSigning(
            contractStates.largeOracleContractState, message1, transactionHash1));

    // load from DB + sign message
    run();
    Assertions.assertThat(currentMessageNonce + 2)
        .isEqualTo(contractStates.largeOracleContractState.currentMessageNonce());

    preparePartialSignatures(message0);
    prepareTransactions(1, 0);
    run(0);
    final AvlTree<SigningRequest, Signature> signedMessages =
        contractStates.largeOracleContractState.signedMessages();
    assertThatProducerSentSignature(0, new SigningRequest(42, message0, transactionHash0));
    run(1, 2, 3);
    Assertions.assertThat(signedMessages.size() + 1)
        .isEqualTo(contractStates.largeOracleContractState.signedMessages().size());
    preparePartialSignatures(message1);
    prepareTransactions(1, 2);
    run(2);
    assertThatProducerSentSignature(2, new SigningRequest(43, message1, transactionHash1));
    run(0, 1, 3);

    // attempt to sign 2 more messages -- remembering that we have 5 partial signatures in total
    // where one is reserved for signing the next oracle.
    for (LargeOracleProcess process : processes) {
      int unusedSignatureIds = process.getSignatureIdsLeft(getCurrentSessionId());
      Assertions.assertThat(unusedSignatureIds).isEqualTo(2);
    }

    Hash message2 = Hash.create(s -> s.writeString("Hello"));
    Hash transactionHash2 = Hash.create(s -> s.writeString("Hello 2"));
    Hash message3 = Hash.create(s -> s.writeString("World!"));
    Hash transactionHash3 = Hash.create(s -> s.writeString("World! 3"));
    set(
        LargeOracleContractHelper.addMessageForSigning(
            contractStates.largeOracleContractState, message2, transactionHash2));
    set(
        LargeOracleContractHelper.addMessageForSigning(
            contractStates.largeOracleContractState, message3, transactionHash3));

    run();
    preparePartialSignatures(message2);
    prepareTransactions(1, 1);
    run(1);
    assertThatProducerSentSignature(1, new SigningRequest(44, message2, transactionHash2));
    preparePartialSignatures(message3);
    prepareTransactions(1, 1);
    run(1);
    assertThatProducerSentSignature(1, new SigningRequest(45, message3, transactionHash3));
    run(0, 2, 3);

    // cannot sign more messages despite there being one pre-signature left, since that one reserved
    // for signing the next oracle.
    Hash message4 = Hash.create(s -> s.writeString("straggler"));
    Hash transactionHash4 = Hash.create(s -> s.writeString("straggler 4"));
    set(
        LargeOracleContractHelper.addMessageForSigning(
            contractStates.largeOracleContractState, message4, transactionHash4));

    run();

    for (int i = 0; i < 4; i++) {
      ThresholdSignatureProvider signer = processes.get(i).getSigner(getCurrentSessionId());
      Assertions.assertThat(signer.getRemainingSignatures()).isEqualTo(1);
      List<String> signatureIds = signer.getSignatureIds();
      InMemoryMessageStorage storage = producerStorages.get(i);
      // only the 0th pre-signature remains unused by all. This is the one used for the next oracle.
      Assertions.assertThat(storage.getPartialSignature(getCurrentSessionId(), signatureIds.get(0)))
          .isNull();
      for (int j = 1; j < signatureIds.size(); j++) {
        Assertions.assertThat(
                storage.getPartialSignature(getCurrentSessionId(), signatureIds.get(j)))
            .isNotNull();
      }
    }
  }

  @Test
  void combineSignatureDespiteFaultyPreSignature() {
    setupForSigning();

    Hash message = Hash.create(s -> s.writeString("message"));
    Hash transactionHash = Hash.create(s -> s.writeString("message 2"));
    set(
        LargeOracleContractHelper.addMessageForSigning(
            contractStates.largeOracleContractState, message, transactionHash));

    run();
    preparePartialSignatures(message);
    preparePartialSignatureJunkReply(0, 1, message);

    prepareTransactions(1, 0);
    run(0);
    assertThatProducerSentSignature(0, new SigningRequest(42, message, transactionHash));
  }

  @Test
  void combineSignatureWithMissingPeerEndpoint() {
    setupForSigning();
    Hash message = Hash.create(s -> s.writeString(":)"));
    Hash transactionHash = Hash.create(s -> s.writeString(":("));
    set(
        LargeOracleContractHelper.addMessageForSigning(
            contractStates.largeOracleContractState, message, transactionHash));
    producerPeerDatabases.get(0).updateBlockProducerEndpoint(producers.get(1).publicKey(), null);
    run();
    prepareTransactions(1, 0);
    preparePartialSignatures(message);
    run(0);
    assertThatProducerSentSignature(0, new SigningRequest(42, message, transactionHash));
  }

  @Test
  void oldOracleSignsNewOracle() {
    // 7 producers total. 4 in existing committee and 3 extra ones.
    setupForSigning();

    Hash ignoredMessage = Hash.create(s -> s.writeString(":("));
    Hash transactionHash = Hash.create(s -> s.writeString("xd"));
    set(
        LargeOracleContractHelper.addMessageForSigning(
            contractStates.largeOracleContractState, ignoredMessage, transactionHash));

    run();
    preparePartialSignatures(ignoredMessage);

    setupProducers(3, 4);
    createProcessFromIndex(4, 5);
    createProcessFromIndex(5, 5);
    createProcessFromIndex(6, 5);

    Assertions.assertThat(producers).hasSize(7);

    initializeOracleUpdate(producers);
    setAllActive();
    prepareTransactions(1, 0);
    // This mutates the test database for party 0. That should probably be fixed.
    runEager(0);

    final int[] all = {0, 1, 2, 3, 4, 5, 6};

    BlockchainPublicKey pk = new KeyPair(BigInteger.valueOf(1234)).getPublic();
    for (int p : all) {
      addCandidateKeyToState(
          producers.get(p).identity(),
          new ThresholdPublicKey(pk, "keyId"),
          Hash.create(s -> s.writeInt(123)));
      var signer = processes.get(0).getSigner(getCurrentSessionId());
      processes.get(p).putSigner(new ThresholdSessionId(1, 0), signer);
    }

    run();
    prepareIncomingOraclePartialSignature();

    prepareTransactions(1, 0);
    run(0);
    assertThatProducerSentSignatureOnNewOracle(0);
  }

  @Test
  void messageValidCheck() {
    byte[] bytes0 = {'1', '2', '3'};
    byte[] bytes1 = {'1', '2', '3', '4'};
    Hash message0 = Hash.create(s -> s.write(bytes0));
    Hash message1 = Hash.create(s -> s.write(bytes1));
    Assertions.assertThat(LargeOracleProcess.messageValid(bytes0, message0)).isTrue();
    Assertions.assertThat(LargeOracleProcess.messageValid(bytes1, message0)).isFalse();
    Assertions.assertThat(LargeOracleProcess.messageValid(bytes0, message1)).isFalse();
  }

  @Test
  void databaseExists() {
    Assertions.assertThat(LargeOracleProcess.databaseExists("src/test/resources/data_0.db"))
        .isTrue();
    Assertions.assertThat(LargeOracleProcess.databaseExists("src/test/resources/data_42.db"))
        .isFalse();
  }

  @Test
  void cannotResumeForUnknownSessionId() {
    setup(4, 5);
    BpOrchestrationContractState state = contractStates.bpOrchestrationContractState;
    set(
        new BpOrchestrationContractState(
            42,
            24,
            state.thresholdKey(),
            state.oracleUpdate(),
            state.blockProducers(),
            FixedList.create(state.committee()),
            state.domainSeparator()));
    initializeOracleUpdate(producers);
    setAllActive();
    createProcessFromIndexWithDatabase(2);
    prepareTransactions(1, 2);
    updateListeners();
    processes.get(2).run();
    assertThatProducersSentBroadcastHashes(2, 2);
  }

  @Test
  void partOfCurrentOracle() {
    setup(1, 0);
    LargeOracleProcess process = processes.get(0);
    Assertions.assertThat(
            process.isPartOfCurrentOracle(contractStates.bpOrchestrationContractState.committee()))
        .isFalse();
    LargeOracleProcess processForInitialProducer = createProcessForInitialProducer();
    Assertions.assertThat(
            processForInitialProducer.isPartOfCurrentOracle(
                contractStates.bpOrchestrationContractState.committee()))
        .isTrue();
  }

  @Test
  void isActiveParticipant() {
    setup(4, 0);
    initializeOracleUpdate(producers);
    setAllActive();

    LargeOracleProcess process = processes.get(0);
    LargeOracleUpdate update = contractStates.bpOrchestrationContractState.oracleUpdate();
    Assertions.assertThat(process.isActiveParticipant(update)).isTrue();
    Assertions.assertThat(process.isParticipant(update)).isTrue();
    LargeOracleProcess processForInitialProducer = createProcessForInitialProducer();
    updateListeners();
    Assertions.assertThat(processForInitialProducer.isActiveParticipant(update)).isFalse();
    Assertions.assertThat(processForInitialProducer.isParticipant(update)).isFalse();
  }

  @Test
  void createJerseyClient() {
    Assertions.assertThat(LargeOracleProcess.createJerseyWebClient()).isNotNull();
  }

  @Test
  void stateCorrectlyResetAfterProtocolInit() {
    setup(4, 2);
    initializeOracleUpdate(producers);
    setAllActive();

    for (LargeOracleProcess process : processes) {
      Assertions.assertThat(process.getUnicastMessages()).isEmpty();
      Assertions.assertThat(process.getBroadcastEchos()).isEmpty();
      Assertions.assertThat(process.getShouldPublishBitmap()).isFalse();
    }

    // run round 1 but do not  advance to next round.
    prepareTransactions(1);
    runEager();
    prepareTransactions(1, 0, 1, 2);
    assertThatProducersSentBroadcastHashes(2);
    prepareUnicastMessagesForRound2();
    prepareBroadcastEchos(2);
    run(0, 1, 2);
    run(0, 1, 2);
    assertThatProducersSentBitmaps(0, 1, 2);
    assertThatWeCanAdvanceToNextRound();
    runEager(3);

    for (int i = 0; i < 4; i++) {
      LargeOracleProcess process = processes.get(i);
      Assertions.assertThat(process.getUnicastMessages()).hasSize(4);
      Assertions.assertThat(process.getBroadcastEchos()).hasSize(4);
      if (i == 3) {
        Assertions.assertThat(process.getShouldPublishBitmap()).isFalse();
      } else {
        Assertions.assertThat(process.getShouldPublishBitmap()).isTrue();
      }
    }

    // create new oracle update with a different session ID.
    bumpRetryNonce();
    setAllActive();

    prepareTransactions(1);
    runEager();

    for (int i = 0; i < 4; i++) {
      LargeOracleProcess process = processes.get(i);
      BlockchainPublicKey publicKey = producers.get(i).publicKey();
      // we only have our own message in here, since it's added immediately
      Assertions.assertThat(process.getUnicastMessages()).hasSize(1);
      Assertions.assertThat(process.getUnicastMessages()).containsKey(publicKey);
      Assertions.assertThat(process.getBroadcastEchos()).hasSize(1);
      Assertions.assertThat(process.getBroadcastEchos()).containsKey(publicKey);
      Assertions.assertThat(process.getShouldPublishBitmap()).isFalse();
    }
  }

  @Test
  public void processDoesNotRunWithoutContractState() {
    setup(1, 0);
    LargeOracleProcess process = processes.get(0);
    listeners.get(BPO_CONTRACT).remove();
    listeners.get(LARGE_ORACLE_CONTRACT).remove();
    // process should bail out early as no contract state is registered
    Assertions.assertThat(process.run()).isFalse();
    contractStateHandler.createContract(
        null, null, BPO_CONTRACT, null, contractStates.bpOrchestrationContractState);
    // Should still bail out early as large oracle contract state is still missing
    Assertions.assertThat(process.run()).isFalse();
  }

  private void bumpRetryNonce() {
    LargeOracleUpdate update = contractStates.bpOrchestrationContractState.oracleUpdate();
    set(
        BpOrchestrationContractHelper.withLargeOracleUpdate(
            contractStates.bpOrchestrationContractState,
            new LargeOracleUpdate(
                update.producers(),
                null,
                null,
                Bitmap.create(update.producers().size()),
                AvlTree.create(),
                null,
                update.retryNonce() + 1,
                FixedList.create(),
                null)));
  }

  private LargeOracleProcess createProcessForInitialProducer() {
    return createProcess(
        createTransactionClient(contractStates, INITIAL_BP_KEY),
        new InMemoryMessageStorage(),
        INITIAL_BP_ADDRESS,
        INITIAL_BP_KEY,
        new SimplePeerDatabase(new HashMap<>()),
        10,
        "",
        "",
        Mockito.mock(WebClient.class),
        contractStateHandler);
  }

  private void confirmInitialOracle() {
    LargeOracleUpdate update = contractStates.bpOrchestrationContractState.oracleUpdate();
    set(
        BpOrchestrationContractHelper.withNewLargeOracle(
            contractStates.bpOrchestrationContractState));

    List<OracleMember> oracleMembers =
        update.getActiveProducers().stream().map(p -> new OracleMember(p.identity())).toList();
    set(
        LargeOracleContractHelper.replaceLargeOracle(
            contractStates.largeOracleContractState, oracleMembers));
  }

  private void run() {
    int[] idx = IntStream.range(0, producers.size()).toArray();
    run(idx);
  }

  private void run(int... producers) {
    updateListeners();
    for (int producer : producers) {
      LargeOracleProcess process = processes.get(producer);
      Assertions.assertThat(process.run()).isFalse();
    }
  }

  private void runEager() {
    int[] idx = IntStream.range(0, producers.size()).toArray();
    runEager(idx);
  }

  private void runEager(int... producers) {
    updateListeners();
    for (int producer : producers) {
      Assertions.assertThat(processes.get(producer).run()).isTrue();
    }
  }

  private void assertThatProducersSentSameCandidateKey(int firstProducer, int... producers) {
    CandidateKey key = getCandidateKeySentByProducer(firstProducer);
    assertThatCandidateKeyLooksSane(key);
    for (int producer : producers) {
      CandidateKey anotherKey = getCandidateKeySentByProducer(producer);
      assertThatCandidateKeyLooksSane(anotherKey);
      Assertions.assertThat(key.key.getPublicKeyId()).isEqualTo(anotherKey.key.getPublicKeyId());
      Assertions.assertThat(key.key.getPublicKey()).isEqualTo(anotherKey.key.getPublicKey());
      Assertions.assertThat(key.honestParties.asBytes())
          .isEqualTo(anotherKey.honestParties.asBytes());
    }
  }

  private void assertThatCandidateKeyLooksSane(CandidateKey key) {
    Assertions.assertThat(key.key.getPublicKeyId()).isNotEmpty();
    Assertions.assertThat(key.key.getPublicKey()).isNotNull();
    Assertions.assertThat(key.honestParties).isNotNull();
  }

  private CandidateKey getCandidateKeySentByProducer(int producer) {
    LargeOracleClient client = (LargeOracleClient) producerOracleClients.get(producer);
    byte[] rpc = client.getLatestPut().getInner().getRpc();
    SafeDataInputStream fromBytes = SafeDataInputStream.createFromBytes(rpc);
    int invocation = fromBytes.readUnsignedByte();
    Hash transactionHash = client.getLatestPut().getHash();
    Assertions.assertThat(invocation).isEqualTo(BPO_ADD_CANDIDATE_KEY);
    ThresholdPublicKey key =
        new ThresholdPublicKey(BlockchainPublicKey.read(fromBytes), fromBytes.readString());
    Bitmap honestParties = BpOrchestrationContractHelper.bitmapFromStream(fromBytes);
    addCandidateKeyToState(producers.get(producer).identity(), key, transactionHash);
    return new CandidateKey(key, honestParties);
  }

  private void addCandidateKeyToState(
      BlockchainAddress sender, ThresholdPublicKey key, Hash transactionHash) {
    LargeOracleUpdate update = contractStates.bpOrchestrationContractState.oracleUpdate();
    update =
        BpOrchestrationContractHelper.addCandidateKey(
            update,
            sender,
            new ThresholdKey(key.getPublicKey(), key.getPublicKeyId()),
            transactionHash);
    set(
        BpOrchestrationContractHelper.withLargeOracleUpdate(
            contractStates.bpOrchestrationContractState, update));
  }

  private void assertThatProtocolRoundIs(int roundNumber) {
    for (LargeOracleProcess process : processes) {
      int currentRoundNumber = process.getCurrentRoundNumber(getNextSessionId());
      Assertions.assertThat(currentRoundNumber).isEqualTo(roundNumber);
    }
  }

  private void assertThatProducersSentBitmaps(int... senders) {
    Bitmap allOnes = getAllOnes();
    for (int producer : senders) {
      assertThatProducerSentBroadcastBitmap(producer, allOnes.asBytes());
    }
  }

  private Bitmap getAllOnes() {
    Bitmap bitmap = Bitmap.create(producers.size());
    for (int i = 0; i < producers.size(); i++) {
      bitmap = bitmap.setBit(i);
    }
    return bitmap;
  }

  private void assertThatProducerSentBroadcastBitmap(int producer, byte[] bitmap) {
    LargeOracleClient client = (LargeOracleClient) producerOracleClients.get(producer);
    ThresholdSessionId currentSessionId = getCurrentSessionId();
    LargeOracleUpdate update = contractStates.bpOrchestrationContractState.oracleUpdate();
    byte[] rpc = client.getLatestPut().getInner().getRpc();
    byte[] expected =
        rpc(
            s -> {
              s.writeByte(BPO_ADD_BROADCAST_BITS);
              s.writeInt(currentSessionId.getSessionId() + 1);
              s.writeInt(currentSessionId.getRetryNonce());
              s.writeInt(update.getCurrentBroadcastRound());
              s.writeDynamicBytes(bitmap);
            });
    Assertions.assertThat(rpc).isEqualTo(expected);
    LargeOracleUpdate oracleUpdate =
        BpOrchestrationContractHelper.updateBroadcastBits(
            update, producers.get(producer).identity(), bitmap);
    set(
        BpOrchestrationContractHelper.withLargeOracleUpdate(
            contractStates.bpOrchestrationContractState, oracleUpdate));
  }

  private void assertThatWeCanAdvanceToNextRound() {
    LargeOracleUpdate update = contractStates.bpOrchestrationContractState.oracleUpdate();
    set(
        BpOrchestrationContractHelper.withLargeOracleUpdate(
            contractStates.bpOrchestrationContractState,
            BpOrchestrationContractHelper.advanceBroadcastRound(update)));
  }

  private void assertThatProducersSentBroadcastHashes(int roundNumber) {
    int[] idx = IntStream.range(0, producers.size()).toArray();
    assertThatProducersSentBroadcastHashes(roundNumber, idx);
  }

  private void assertThatProducersSentBroadcastHashes(int roundNumber, int... producers) {
    ThresholdSessionId nextSessionId = getNextSessionId();
    LargeOracleUpdate update = contractStates.bpOrchestrationContractState.oracleUpdate();
    int currentBroadcastRound = update.getCurrentBroadcastRound();
    Assertions.assertThat(currentBroadcastRound).isEqualTo(roundNumber - 1);
    for (int producer : producers) {
      InMemoryMessageStorage messageStorage = producerStorages.get(producer);
      BlockProducer broadcaster = this.producers.get(producer);
      BlockchainAddress identity = broadcaster.identity();
      Hash messageHash =
          Hash.create(
              s -> {
                Hash hash = messageStorage.getMyBroadcastMessageHash(nextSessionId, roundNumber);
                s.write(messageStorage.getBroadcastMessage(hash));
              });
      LargeOracleClient client = (LargeOracleClient) producerOracleClients.get(producer);
      byte[] rpc = client.getLatestPut().getInner().getRpc();
      byte[] expected = getSendBroadcastTxBytes(nextSessionId, currentBroadcastRound, messageHash);
      Assertions.assertThat(rpc).isEqualTo(expected);
      update = BpOrchestrationContractHelper.addBroadcast(update, identity, messageHash);
    }
    set(
        BpOrchestrationContractHelper.withLargeOracleUpdate(
            contractStates.bpOrchestrationContractState, update));
  }

  private byte[] getSendBroadcastTxBytes(
      ThresholdSessionId sessionId, int broadcastRound, Hash messageHash) {
    return rpc(
        s -> {
          s.writeByte(BPO_ADD_BROADCAST_HASH);
          s.writeInt(sessionId.getSessionId());
          s.writeInt(sessionId.getRetryNonce());
          s.writeInt(broadcastRound);
          messageHash.write(s);
        });
  }

  private void assertThatProducerSentHeartbeat(int producer, byte[] heartbeat) {
    LargeOracleClient client = (LargeOracleClient) producerOracleClients.get(producer);
    byte[] rpc = client.getLatestPut().getInner().getRpc();
    byte[] expected =
        rpc(
            s -> {
              s.writeByte(BPO_ADD_HEARTBEAT);
              s.writeDynamicBytes(heartbeat);
            });
    Assertions.assertThat(rpc).isEqualTo(expected);
    addHeartbeat(producer, heartbeat);
  }

  private void assertThatProducersDidNotSendTransaction(int... producers) {
    assertThatProducersSentTransactions(0, producers);
  }

  private void assertThatProducersSentTransactions(int numberOfTransactions, int... producers) {
    for (int producer : producers) {
      LargeOracleClient client = (LargeOracleClient) producerOracleClients.get(producer);
      Assertions.assertThat(client.getTransactionsCount()).isEqualTo(numberOfTransactions);
    }
  }

  private void assertThatProducerSentSignatureOnNewOracle(int signer) {
    LargeOracleClient client = (LargeOracleClient) producerOracleClients.get(signer);
    byte[] rpc = client.getLatestPut().getInner().getRpc();
    SafeDataInputStream fromBytes = SafeDataInputStream.createFromBytes(rpc);
    Assertions.assertThat(fromBytes.readUnsignedByte()).isEqualTo(BPO_AUTHORIZE_NEW_ORACLE);
    int nextSessionId = getNextSessionId().getSessionId();
    LargeOracleUpdate update = contractStates.bpOrchestrationContractState.oracleUpdate();
    Hash message =
        update.getMessageToBeSigned(
            nextSessionId, contractStates.bpOrchestrationContractState.domainSeparator());
    Signature read = Signature.read(fromBytes);
    BlockchainPublicKey publicKey = read.recoverPublicKey(message);
    ThresholdKey currentKey = contractStates.bpOrchestrationContractState.thresholdKey();
    Assertions.assertThat(publicKey).isEqualTo(currentKey.key());
    set(
        BpOrchestrationContractHelper.withNewLargeOracle(
            contractStates.bpOrchestrationContractState));
    set(
        LargeOracleContractHelper.replaceLargeOracle(
            contractStates.largeOracleContractState,
            update.getActiveProducers().stream()
                .map(bp -> new OracleMember(bp.identity()))
                .toList()));
  }

  private void assertThatProducerSentSignature(int producer, SigningRequest message) {
    LargeOracleClient client = (LargeOracleClient) producerOracleClients.get(producer);
    byte[] rpc = client.getLatestPut().getInner().getRpc();
    SafeDataInputStream fromBytes = SafeDataInputStream.createFromBytes(rpc);
    Assertions.assertThat(fromBytes.readUnsignedByte()).isEqualTo(LO_ADD_SIGNATURE);
    Assertions.assertThat(Hash.read(fromBytes)).isEqualTo(message.messageHash());
    Assertions.assertThat(fromBytes.readInt()).isEqualTo(message.nonce());
    Signature read = Signature.read(fromBytes);
    set(
        LargeOracleContractHelper.addSignature(
            contractStates.largeOracleContractState, message, read));
  }

  private void setAllActive() {
    setActiveProducers(IntStream.range(0, producers.size()).toArray());
  }

  private void setActiveProducers(int... producers) {
    int offset = producers[0];
    Bitmap bitmap = Bitmap.create(producers.length);
    for (int producer : producers) {
      bitmap = bitmap.setBit(producer - offset);
    }
    for (int producer : producers) {
      addHeartbeat(producer, bitmap.asBytes());
    }
  }

  private void addHeartbeat(int producer, byte[] heartbeat) {
    LargeOracleUpdate update = contractStates.bpOrchestrationContractState.oracleUpdate();
    update =
        BpOrchestrationContractHelper.addHeartbeat(
            update, producers.get(producer).identity(), heartbeat);
    set(
        BpOrchestrationContractHelper.withLargeOracleUpdate(
            contractStates.bpOrchestrationContractState, update));
  }

  private static byte[] rpc(Consumer<SafeDataOutputStream> stream) {
    return SafeDataOutputStream.serialize(stream);
  }

  private void prepareIncomingOraclePartialSignature() {
    ThresholdSessionId sessionId = getCurrentSessionId();
    String signatureId = processes.get(0).getSigner(sessionId).getSignatureIds().get(0);
    for (int sender = 0; sender < producers.size(); sender++) {
      InMemoryMessageStorage storage = producerStorages.get(sender);
      byte[] partialSignature = storage.getPartialSignature(sessionId, signatureId);
      for (int receiver = 0; receiver < producers.size(); receiver++) {
        if (sender != receiver) {
          preparePartialSignature(receiver, sender, sessionId, signatureId, partialSignature);
        }
      }
    }
  }

  private void preparePartialSignatureJunkReply(int receiver, int sender, Hash message) {
    String signatureId = getSignatureIdFromHash(message);
    ThresholdSessionId currentSessionId = getCurrentSessionId();
    preparePartialSignature(
        receiver, sender, currentSessionId, signatureId, new byte[] {'j', 'u', 'n', 'k'});
  }

  private void preparePartialSignature(
      int receiver, int sender, ThresholdSessionId sessionId, String signatureId, byte[] data) {
    WebClient webClient = producerWebClients.get(receiver);
    PeerDatabase peerDatabase = producerPeerDatabases.get(receiver);
    Address endpoint = peerDatabase.getBlockProducerEndpoint(producers.get(sender).publicKey());
    if (endpoint != null) {
      String url = partialSignatureUrl(sessionId, signatureId, endpoint.getEndpoint());
      Mockito.when(webClient.get(Mockito.eq(url), Mockito.eq(PartialSignatureDto.class)))
          .thenReturn(PartialSignatureDto.create(data));
    }
  }

  private void preparePartialSignatures(Hash message) {
    String signatureId = getSignatureIdFromHash(message);
    ThresholdSessionId currentSessionId = getCurrentSessionId();
    for (int receiver = 0; receiver < producers.size(); receiver++) {
      for (int sender = 0; sender < producers.size(); sender++) {
        MessageStorage messageStorage = producerStorages.get(sender);
        byte[] partialSignature = messageStorage.getPartialSignature(currentSessionId, signatureId);
        if (receiver != sender) {
          preparePartialSignature(
              receiver, sender, currentSessionId, signatureId, partialSignature);
        }
      }
    }
  }

  private String getSignatureIdFromHash(Hash message) {
    FixedList<SigningRequest> pendingMessages =
        contractStates.largeOracleContractState.pendingMessages();
    Integer initialMessageNonce = contractStates.largeOracleContractState.initialMessageNonce();
    for (SigningRequest pendingMessage : pendingMessages) {
      if (pendingMessage.messageHash().equals(message)) {
        ThresholdSignatureProvider signer = processes.get(0).getSigner(getCurrentSessionId());
        return signer.getSignatureIds().get(1 + pendingMessage.nonce() - initialMessageNonce);
      }
    }
    // hopefully there's never going to be a signature ID with the valid "INVALID"
    return "INVALID";
  }

  static String partialSignatureUrl(
      ThresholdSessionId sessionId, String signatureId, String endpoint) {
    return endpoint
        + "/threshold/getPartialSignature/"
        + sessionId.getSessionId()
        + "/"
        + sessionId.getRetryNonce()
        + "/"
        + signatureId;
  }

  /**
   * Makes calls to webClient.getUnicastMessages for all clients throw a NOT_FOUND error, simulating
   * that there's no unicast message ready yet.
   */
  private void prepareDummyUnicastMessages() {
    ThresholdSessionId sessionId = getNextSessionId();
    for (int receiver = 0; receiver < producers.size(); receiver++) {
      PeerDatabase peerDatabase = producerPeerDatabases.get(receiver);
      WebClient webClient = producerWebClients.get(receiver);
      for (BlockProducer producer : producers) {
        Address endpoint = peerDatabase.getBlockProducerEndpoint(producer.publicKey());
        String url = unicastMessageUrl(sessionId, endpoint);
        Mockito.when(webClient.post(Mockito.eq(url), Mockito.any(), Mockito.any()))
            .thenThrow(new WebApplicationException(Response.Status.NOT_FOUND));
      }
    }
  }

  private void prepareUnicastMessagesForRound2() {
    for (int receiver = 0; receiver < producers.size(); receiver++) {
      // need to remove receiver because we do not store messages to ourselves in the message
      // storage.
      int finalReceiver = receiver;
      int[] idx = IntStream.range(0, producers.size()).filter(i -> i != finalReceiver).toArray();
      prepareUnicastMessagesForRound2(receiver, idx);
    }
  }

  /**
   * Makes calls to webClient{receiver}.getUnicastMessage{sender} return the right unicast message
   * for round 2, the only round in which unicast messages are present.
   *
   * @param receiver the index of the receiver of the unicast message
   * @param senders the index of the sender of the unicast message
   */
  private void prepareUnicastMessagesForRound2(int receiver, int... senders) {
    ThresholdSessionId sessionId = getNextSessionId();
    WebClient webClient = producerWebClients.get(receiver);
    PeerDatabase peerDatabase = producerPeerDatabases.get(receiver);
    BlockchainPublicKey address = producers.get(receiver).publicKey();
    for (int sender : senders) {
      BlockProducer producer = producers.get(sender);
      MessageStorage storage = producerStorages.get(sender);
      Address endpoint = peerDatabase.getBlockProducerEndpoint(producer.publicKey());
      if (endpoint != null) {
        String url = unicastMessageUrl(sessionId, endpoint);
        byte[] roundUnicastMessage = storage.getRoundUnicastMessage(sessionId, 2, address);
        UnicastMessageDto dto = createUnicastDto(roundUnicastMessage, sender);
        Mockito.when(webClient.post(Mockito.eq(url), Mockito.any(), Mockito.any())).thenReturn(dto);
      }
    }
  }

  private UnicastMessageDto createUnicastDto(byte[] roundUnicastMessage, int sender) {
    ThresholdSessionId sessionId = getNextSessionId();
    BlockchainPublicKey senderPublicKey = producers.get(sender).publicKey();
    // receiver is implicit because everyone uses the same ephemeral key
    BlockchainPublicKey ephemeralPublicKey = DUMMY_EPHEMERAL_KEY.getPublic();
    Hash hash =
        Hash.create(
            s -> {
              senderPublicKey.write(s);
              ephemeralPublicKey.write(s);
              s.writeInt(sessionId.getSessionId());
              s.writeInt(sessionId.getRetryNonce());
              s.writeInt(2);
            });
    Cipher cipher =
        AesHelper.createAesForEncrypt(
            producerRestServerKeys.get(sender).getPrivateKey(),
            ephemeralPublicKey,
            hash.getBytes());
    return UnicastMessageDto.create(
        ExceptionConverter.call(() -> cipher.doFinal(roundUnicastMessage), ""));
  }

  private static String unicastMessageUrl(ThresholdSessionId sessionId, Address endpoint) {
    return endpoint.getEndpoint()
        + "/threshold/getUnicastMessage/"
        + sessionId.getSessionId()
        + "/"
        + sessionId.getRetryNonce()
        + "/2";
  }

  private void prepareInvalidHandshake(int to) {
    WebClient webClient = producerWebClients.get(to);
    PeerDatabase peerDatabase = producerPeerDatabases.get(to);
    for (int i = 0; i < producers.size(); i++) {
      if (i != to) {
        Address endpoint = peerDatabase.getBlockProducerEndpoint(producers.get(i).publicKey());
        String url = handshakeUrl(endpoint.getEndpoint());
        Mockito.when(webClient.post(Mockito.eq(url), Mockito.any(), Mockito.any()))
            .thenThrow(new WebApplicationException(Response.Status.NOT_FOUND));
      }
    }
  }

  /**
   * Mock a handshake message response from one producer to another.
   *
   * @param to the sender of the handshake request
   * @param from the responder to the handshake request
   */
  private void prepareHandshake(int to, int from) {
    KeyPair senderKey = producerRestServerKeys.get(from);
    BlockchainPublicKey receiverPublicKey = producers.get(to).publicKey();
    byte[] extra = PeerDiscoveryResource.handshakeExtraDataEncrypt(VERSION_INFORMATION);
    Cipher cipher =
        AesHelper.createAesForEncrypt(
            DUMMY_EPHEMERAL_KEY.getPrivateKey(), senderKey.getPublic(), extra);
    Address receiverEndpoint =
        producerPeerDatabases.get(from).getBlockProducerEndpoint(receiverPublicKey);

    HandshakeDto dto =
        ExceptionConverter.call(
            () -> {
              byte[] encrypted =
                  cipher.doFinal(
                      SafeDataOutputStream.serialize(
                          stream -> {
                            receiverEndpoint.write(stream);
                            producers.get(to).identity().write(stream);
                          }));
              return HandshakeDto.create(encrypted);
            },
            "");

    WebClient webClient = producerWebClients.get(to);
    Address endpoint =
        producerPeerDatabases.get(to).getBlockProducerEndpoint(producers.get(from).publicKey());
    String url = handshakeUrl(endpoint.getEndpoint());
    Mockito.when(webClient.post(Mockito.eq(url), Mockito.any(), Mockito.any())).thenReturn(dto);
  }

  private String handshakeUrl(String endpoint) {
    return endpoint + "/discover/handshake";
  }

  private void prepareBroadcastEcho(
      int roundNumber, int receiver, int sender, int originalBroadcaster) {
    Hash hash =
        producerStorages
            .get(originalBroadcaster)
            .getMyBroadcastMessageHash(getNextSessionId(), roundNumber);
    WebClient webClient = producerWebClients.get(receiver);
    PeerDatabase peerDatabase = producerPeerDatabases.get(sender);
    String endpoint =
        peerDatabase.getBlockProducerEndpoint(producers.get(sender).publicKey()).getEndpoint();
    String url = broadcastMessageUrl(hash, endpoint);
    byte[] data = producerStorages.get(sender).getBroadcastMessage(hash);
    Mockito.when(webClient.get(Mockito.eq(url), Mockito.eq(BroadcastMessageDto.class)))
        .thenReturn(BroadcastMessageDto.create(data));
  }

  /**
   * Prepares a broadcast echo for all producers in a certain round.
   *
   * @param roundNumber the round number
   */
  private void prepareBroadcastEchos(int roundNumber) {
    int[] indices = IntStream.range(0, producers.size()).toArray();
    for (int receiver = 0; receiver < producers.size(); receiver++) {
      prepareBroadcastEchos(roundNumber, receiver, indices);
    }
  }

  /**
   * Makes it possible for a receiver to retrieve the right broadcast data when it asks a number of
   * producers.
   *
   * @param roundNumber the round that the broadcast belongs to
   * @param receiver the receiver of the broadcast data
   * @param senders a list of senders of the broadcast data
   */
  private void prepareBroadcastEchos(int roundNumber, int receiver, int... senders) {
    ThresholdSessionId sessionId = getNextSessionId();
    WebClient webClient = producerWebClients.get(receiver);
    PeerDatabase peerDatabase = producerPeerDatabases.get(receiver);
    for (int sender : senders) {
      InMemoryMessageStorage messageStorage = producerStorages.get(sender);
      BlockProducer producer = producers.get(sender);
      BlockchainPublicKey broadcasterProtocolId = producer.publicKey();
      Address endpoint = peerDatabase.getBlockProducerEndpoint(broadcasterProtocolId);
      if (endpoint != null) {
        Hash hash = messageStorage.getMyBroadcastMessageHash(sessionId, roundNumber);
        String url = broadcastMessageUrl(hash, endpoint.getEndpoint());
        byte[] broadcastMessage = messageStorage.getBroadcastMessage(hash);
        Mockito.when(webClient.get(Mockito.eq(url), Mockito.eq(BroadcastMessageDto.class)))
            .thenReturn(BroadcastMessageDto.create(broadcastMessage));
      }
    }
  }

  private static String broadcastMessageUrl(Hash broadcastHash, String endpoint) {
    return endpoint + "/threshold/getBroadcastMessage/" + broadcastHash;
  }

  private void prepareTransactions(int numberOfTransactions) {
    int[] idx = IntStream.range(0, producers.size()).toArray();
    prepareTransactions(numberOfTransactions, idx);
  }

  private void prepareTransactions(int numberOfTransactions, int... producers) {
    for (int producer : producers) {
      LargeOracleClient client = (LargeOracleClient) producerOracleClients.get(producer);
      for (int i = 0; i < numberOfTransactions; i++) {
        client.addDefaultExecutedTransactions(SHARD);
      }
    }
  }

  private ThresholdSessionId getCurrentSessionId() {
    int sessionId = contractStates.bpOrchestrationContractState.sessionId();
    int retryNonce = contractStates.bpOrchestrationContractState.retryNonce();
    return new ThresholdSessionId(sessionId, retryNonce);
  }

  private ThresholdSessionId getNextSessionId() {
    LargeOracleUpdate update = contractStates.bpOrchestrationContractState.oracleUpdate();
    return new ThresholdSessionId(
        contractStates.bpOrchestrationContractState.sessionId() + 1, update.retryNonce());
  }

  /**
   * Marks all producers in a supplied list as active; i.e., makes it seem as if the supplied
   * producers were the output of the heartbeat protocol.
   *
   * @param producers the producers to be marked as active
   */
  private void initializeOracleUpdate(List<BlockProducer> producers) {
    LargeOracleUpdate update =
        new LargeOracleUpdate(
            FixedList.create(producers),
            null,
            FixedList.create(),
            Bitmap.create(producers.size()),
            AvlTree.create(),
            null,
            0,
            FixedList.create(),
            null);
    set(
        BpOrchestrationContractHelper.withLargeOracleUpdate(
            contractStates.bpOrchestrationContractState, update));
  }

  private void set(BpOrchestrationContractState state) {
    contractStates.bpOrchestrationContractState = state;
  }

  private void set(LargeOracleContractState state) {
    contractStates.largeOracleContractState = state;
  }

  private void setupForSigning() {
    setupProducers(4, 0);
    for (int i = 0; i < 4; i++) {
      createProcessFromIndexWithDatabase(i);
    }
    BlockchainPublicKey thresholdPublicKey =
        BlockchainPublicKey.fromEncodedEcPoint(
            Hex.decode("0318ba1b86ee6a2656f1e454e203cccacaf6fdb24617eff12d111f909634e7e584"));
    set(
        new BpOrchestrationContractState(
            0,
            0,
            new ThresholdKey(thresholdPublicKey, "I7f6NX3qHgvmVlVks2inPsStqcRP5aF3rioegzikNSw"),
            null,
            AvlTree.create(
                producers.stream()
                    .collect(Collectors.toMap(BlockProducer::identity, producer -> producer))),
            FixedList.create(producers),
            Hash.fromString("5f02a327a959a08bee16235910ad712c36507992cb310c9e064c96f27d92a3e7")));
    set(
        LargeOracleContractHelper.replaceLargeOracle(
            contractStates.largeOracleContractState,
            producers.stream().map(bp -> new OracleMember(bp.identity())).toList()));
  }

  private void createProcessFromIndexWithDatabase(int index, String databaseName) {
    BlockchainTransactionClient transactionClient = producerTransactionClients.get(index);
    LargeOracleProcess process =
        createProcess(
            transactionClient,
            producerStorages.get(index),
            producers.get(index).identity(),
            producerRestServerKeys.get(index),
            producerPeerDatabases.get(index),
            5,
            databaseName,
            "",
            producerWebClients.get(index),
            contractStateHandler);
    processes.add(process);
  }

  private void createProcessFromIndexWithDatabase(int index) {
    createProcessFromIndexWithDatabase(index, "src/test/resources/data_" + index + ".db");
  }

  private void setup(int numberOfProducers, int preSignatureCount) {
    setupProducers(numberOfProducers, 0);
    createListeners();
    for (int i = 0; i < producers.size(); i++) {
      createProcessFromIndex(i, preSignatureCount);
    }
  }

  private void setupProducers(int numberOfProducers, int indexOffset) {
    for (int i = 0; i < numberOfProducers; i++) {
      int index = i + indexOffset;
      KeyPair restKey = new KeyPair(BigInteger.valueOf(24 + index));
      KeyPair producerSigningKey = new KeyPair(BigInteger.valueOf(42 + index));
      producerRestServerKeys.add(restKey);
      producers.add(createProducer(index, restKey.getPublic()));

      // Need access to oracleClient.
      LargeOracleClient oracleClient = new LargeOracleClient(contractStates);

      producerTransactionClients.add(createTransactionClient(oracleClient, producerSigningKey));
      producerOracleClients.add(oracleClient);
      producerWebClients.add(Mockito.mock(WebClient.class));
      producerStorages.add(new InMemoryMessageStorage());
      producerPeerDatabases.add(createPeerDatabase());
    }
  }

  static BlockProducer createProducer(int pid, BlockchainPublicKey publicKey) {
    BlockchainAddress address = BlockchainAddress.fromString(String.format("%042d", pid));
    return new BlockProducer(address, publicKey);
  }

  private void createProcessFromIndex(int index, int preSignatureCount) {
    BlockchainTransactionClient transactionClient = producerTransactionClients.get(index);
    LargeOracleProcess process =
        createProcess(
            transactionClient,
            producerStorages.get(index),
            producers.get(index).identity(),
            producerRestServerKeys.get(index),
            producerPeerDatabases.get(index),
            preSignatureCount,
            ":memory:",
            "",
            producerWebClients.get(index),
            contractStateHandler);
    processes.add(process);
  }

  private static LargeOracleProcess createProcess(
      BlockchainTransactionClient transactionClient,
      InMemoryMessageStorage producerMessageStorage,
      BlockchainAddress producerIdentity,
      KeyPair producerRestKey,
      PeerDatabase producerPeerDatabase,
      int preSignatureCount,
      String databaseName,
      String databaseBackupName,
      WebClient producerWebClient,
      ContractStateHandler contractStateHandler) {
    return new LargeOracleProcess(
        producerMessageStorage,
        () -> DUMMY_EPHEMERAL_KEY,
        VERSION_INFORMATION,
        BPO_CONTRACT,
        LARGE_ORACLE_CONTRACT,
        producerIdentity,
        producerRestKey,
        (tx, cost) ->
            transactionClient.waitForSpawnedEvents(transactionClient.signAndSend(tx, cost)),
        0,
        producerPeerDatabase,
        preSignatureCount,
        databaseName,
        databaseBackupName,
        new byte[32],
        producerWebClient,
        contractStateHandler);
  }

  static BlockchainTransactionClient createTransactionClient(
      StateWrapper contractStates, KeyPair keyPair) {
    LargeOracleClient client = new LargeOracleClient(contractStates);
    BlockchainTransactionClient transactionClient =
        BlockchainTransactionClient.create(
            client, new SenderAuthenticationKeyPair(keyPair), 100, 600000L);
    return transactionClient;
  }

  private BlockchainTransactionClient createTransactionClient(
      BlockchainContractClient client, KeyPair keyPair) {
    BlockchainTransactionClient transactionClient =
        BlockchainTransactionClient.create(
            client, new SenderAuthenticationKeyPair(keyPair), 100, 600000L);
    return transactionClient;
  }

  private PeerDatabase createPeerDatabase() {
    return new PeerDatabase() {

      private final List<BlockchainPublicKey> invalid = new ArrayList<>();
      private final List<BlockchainPublicKey> registeredProducers = new ArrayList<>();

      @Override
      public void setRegisteredBlockProducers(List<BlockchainPublicKey> addresses) {
        registeredProducers.addAll(addresses);
      }

      @Override
      public boolean isRegisteredBlockProducer(BlockchainPublicKey address) {
        // all producers are assumed to be registered.
        return containsBlockProducer(address);
      }

      @Override
      public Address getBlockProducerEndpoint(BlockchainPublicKey address) {
        if (invalid.contains(address)) {
          return null;
        }
        Optional<BlockProducer> producer =
            producers.stream().filter(p -> p.publicKey().equals(address)).findFirst();
        if (producer.isPresent()) {
          // this gives a way to differentiate "endpoints" of producers
          return new Address(producer.toString() + producer.hashCode(), 1234);
        }
        return null;
      }

      @Override
      public List<BlockchainPublicKey> getBlockProducersWithEndpoint() {
        return producerRestServerKeys.stream().map(KeyPair::getPublic).toList();
      }

      @Override
      public boolean containsBlockProducer(BlockchainPublicKey address) {
        return producers.stream().anyMatch(p -> p.publicKey().equals(address))
            || registeredProducers.contains(address);
      }

      @Override
      public void updateBlockProducerEndpoint(BlockchainPublicKey address, Address endpoint) {
        // repurposing this method to mark which addresses we wish to become "invalid"
        invalid.add(address);
      }
    };
  }

  record CandidateKey(ThresholdPublicKey key, Bitmap honestParties) {}

  private void createListeners() {
    createListeners(contractStateHandler);
  }

  private void createListeners(ContractStateHandler contractStateHandler) {
    listeners =
        Map.of(
            LARGE_ORACLE_CONTRACT,
            contractStateHandler.createContract(
                null, null, LARGE_ORACLE_CONTRACT, null, contractStates.largeOracleContractState),
            BPO_CONTRACT,
            contractStateHandler.createContract(
                null, null, BPO_CONTRACT, null, contractStates.bpOrchestrationContractState));
  }

  private void updateListeners(List<Client.Listener> listeners, List<StateSerializable> states) {
    int i = 0;
    for (Client.Listener listener : listeners) {
      listener.update(states.get(i));
      i++;
    }
  }

  private void updateListeners() {
    updateListeners(
        List.of(listeners.get(LARGE_ORACLE_CONTRACT), listeners.get(BPO_CONTRACT)),
        List.of(
            contractStates.largeOracleContractState, contractStates.bpOrchestrationContractState));
  }
}
