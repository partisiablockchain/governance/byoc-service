package com.partisiablockchain.demo.byoc.monitor;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import org.web3j.protocol.core.Request;
import org.web3j.protocol.core.methods.response.EthBlockNumber;

/**
 * Request stub used to control the block number response of a call to {@link
 * org.web3j.protocol.core.Ethereum#ethBlockNumber()}.
 *
 * @param <S> unused
 */
public final class EthBlockNumberRequestStub<S> extends Request<S, EthBlockNumber> {

  private final long blockNumber;

  /**
   * Default constructor.
   *
   * @param blockNumber block number to respond
   */
  public EthBlockNumberRequestStub(long blockNumber) {
    this.blockNumber = blockNumber;
  }

  @Override
  public EthBlockNumber send() {
    EthBlockNumber ethBlockNumber = new EthBlockNumber();
    ethBlockNumber.setResult(Long.toString(blockNumber));
    return ethBlockNumber;
  }
}
