package com.partisiablockchain.demo.byoc.monitor;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import java.math.BigInteger;

/** Test stub for Ethereum.BlockPagination. */
final class BlockPaginationStub implements Ethereum.BlockPagination {

  private Ethereum.BlockRange nextRange;
  private BigInteger latestCheckedBlock;

  BlockPaginationStub(Ethereum.BlockRange initRange) {
    this.nextRange = initRange;
  }

  void setNextRange(Ethereum.BlockRange nextRange) {
    this.nextRange = nextRange;
  }

  void incrementRange(BigInteger rangeLength, boolean moreBlocksAvailable) {
    BigInteger nextFromBlock = nextRange.toBlock();
    BigInteger nextToBlock = nextFromBlock.add(rangeLength);
    this.nextRange = new Ethereum.BlockRange(nextFromBlock, nextToBlock, moreBlocksAvailable);
  }

  BigInteger getLatestCheckedBlock() {
    return latestCheckedBlock;
  }

  @Override
  public Ethereum.BlockRange nextRange(BigInteger latestCheckedBlock) {
    this.latestCheckedBlock = latestCheckedBlock;
    return nextRange;
  }
}
