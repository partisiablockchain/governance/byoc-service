package com.partisiablockchain.demo.byoc.monitor;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static java.lang.Thread.sleep;

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.client.web.WebClient;
import com.partisiablockchain.crypto.AesHelper;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.KeyPair;
import com.partisiablockchain.demo.byoc.Address;
import com.partisiablockchain.demo.byoc.governance.bporchestration.BlockProducer;
import com.partisiablockchain.demo.byoc.governance.bporchestration.BpOrchestrationContractState;
import com.partisiablockchain.demo.byoc.governance.bporchestration.ThresholdKey;
import com.partisiablockchain.demo.byoc.rest.dto.DiscoveryResponseDto;
import com.partisiablockchain.demo.byoc.rest.dto.HandshakeDto;
import com.partisiablockchain.server.tcp.client.Client;
import com.partisiablockchain.tree.AvlTree;
import com.secata.stream.SafeDataOutputStream;
import com.secata.tools.coverage.ExceptionConverter;
import com.secata.tools.immutable.FixedList;
import jakarta.ws.rs.WebApplicationException;
import jakarta.ws.rs.core.Response;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import javax.crypto.Cipher;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

/** Test. */
public final class PeerDiscoveryProcessTest {

  private final List<BlockProducer> producers = new ArrayList<>();
  private final List<KeyPair> producerRestServerKeys = new ArrayList<>();
  private final List<Address> producerEndpoints = new ArrayList<>();
  private final ContractStateHandler contractStateHandler =
      new ContractStateHandler(List.of(LargeOracleProcessTest.BPO_CONTRACT));
  private Client.Listener listener;
  private final BlockchainAddress clientPbcAddress =
      BlockchainAddress.fromString("000000000000000000000000000000000000000001");

  private WebClient webClient = null;
  private PeerDatabase peerDatabase = null;

  AtomicInteger counter;

  private StateWrapper contractStates;

  /** Before. */
  @BeforeEach
  public void before() {
    // setup 4 producers
    for (int i = 0; i < 4; i++) {
      KeyPair restKey = new KeyPair(BigInteger.valueOf(24 + i));
      BlockProducer producer = LargeOracleProcessTest.createProducer(i, restKey.getPublic());
      producers.add(producer);
      producerRestServerKeys.add(restKey);
      producerEndpoints.add(new Address("endpoint_" + i, 5000 + i));
    }
    webClient = Mockito.mock(WebClient.class);
    // we only care about the value of the registered block producers, so that's the only thing
    // being set.
    contractStates =
        new StateWrapper(
            new BpOrchestrationContractState(
                0,
                0,
                new ThresholdKey(null, null),
                null,
                AvlTree.create(
                    producers.stream()
                        .collect(Collectors.toMap(BlockProducer::identity, producer -> producer))),
                FixedList.create(producers),
                Hash.fromString(
                    "5f02a327a959a08bee16235910ad712c36507992cb310c9e064c96f27d92a3e7")),
            null);
    counter = new AtomicInteger(0);
    createListeners();
  }

  @Test
  void noDiscoveryIfNoUnknownNodes() {
    PeerDiscoveryProcess process = createProcess(1, 2, 3);
    Assertions.assertThat(
            process.determineUnknownNodes(
                contractStates.bpOrchestrationContractState.committee(),
                contractStates.bpOrchestrationContractState.getConfirmedBlockProducers()))
        .isEmpty();
    Assertions.assertThat(process.run()).isFalse();
  }

  @Test
  void noDiscoveryIfNotRegisteredProducer() {
    KeyPair keyPair = new KeyPair(BigInteger.valueOf(555));
    PeerDiscoveryProcess process =
        new PeerDiscoveryProcess(
            () -> LargeOracleProcessTest.DUMMY_EPHEMERAL_KEY,
            webClient,
            keyPair,
            new SimplePeerDatabase(new HashMap<>()),
            LargeOracleProcessTest.BPO_CONTRACT,
            LargeOracleProcessTest.VERSION_INFORMATION,
            new Address("endpoint", 9057),
            clientPbcAddress,
            contractStateHandler);
    Assertions.assertThat(
            process.determineUnknownNodes(
                contractStates.bpOrchestrationContractState.committee(),
                contractStates.bpOrchestrationContractState.getConfirmedBlockProducers()))
        .isNotEmpty();
    Assertions.assertThat(process.run()).isFalse();
    Assertions.assertThat(process.isRegisteredProducer()).isFalse();
  }

  @Test
  void discoversRegisteredUnKnownPeer() {
    PeerDiscoveryProcess process = createProcess(1, 2);
    Assertions.assertThat(
            process.determineUnknownNodes(
                contractStates.bpOrchestrationContractState.committee(),
                contractStates.bpOrchestrationContractState.getConfirmedBlockProducers()))
        .isNotEmpty();
    preparePeerDiscoveryResponse(2, 3);
    prepareHandshake(3);
    prepareHandshake(0);
    prepareHandshake(1);
    prepareHandshake(2);
    updateListeners();
    process.run();

    // Node 2 returns everything needed so we never ask node 1
    assertNoDiscoverRequest(1);
    Assertions.assertThat(process.isRegisteredProducer()).isTrue();
    Assertions.assertThat(
            process.determineUnknownNodes(
                contractStates.bpOrchestrationContractState.committee(),
                contractStates.bpOrchestrationContractState.getConfirmedBlockProducers()))
        .isEmpty();
  }

  @Test
  public void getIdentity() {
    Assertions.assertThatThrownBy(
            () ->
                PeerDiscoveryProcess.getIdentity(
                    contractStates.bpOrchestrationContractState.committee(),
                    contractStates.bpOrchestrationContractState.getConfirmedBlockProducers(),
                    new KeyPair(BigInteger.ONE).getPublic()))
        .isInstanceOf(NoSuchElementException.class);

    Assertions.assertThat(
            PeerDiscoveryProcess.getIdentity(
                contractStates.bpOrchestrationContractState.committee(),
                contractStates.bpOrchestrationContractState.getConfirmedBlockProducers(),
                producers.get(0).publicKey()))
        .isEqualTo(producers.get(0).identity());
  }

  @Test
  public void justStarted() {
    final PeerDiscoveryProcess process = createProcess(1, 2, 3);
    prepareHandshake(1);
    prepareHandshake(2);
    prepareHandshake(3);
    updateListeners();
    process.run();
    Assertions.assertThat(counter.get()).isEqualTo(3);
    updateListeners();
    process.run();
    Assertions.assertThat(counter.get()).isEqualTo(3);
  }

  @Test
  void discoversPeerButPeerSendsBadHandshake() {
    PeerDiscoveryProcess process = createProcess(1, 2);
    Assertions.assertThat(
            process.determineUnknownNodes(
                contractStates.bpOrchestrationContractState.committee(),
                contractStates.bpOrchestrationContractState.getConfirmedBlockProducers()))
        .hasSize(1);
    preparePeerDiscoveryResponse(2, 3);
    updateListeners();
    process.run();
    assertDiscoverRequest(1);
    Assertions.assertThat(
            process.determineUnknownNodes(
                contractStates.bpOrchestrationContractState.committee(),
                contractStates.bpOrchestrationContractState.getConfirmedBlockProducers()))
        .hasSize(1);
  }

  @Test
  void includeCommitteeMembersInDiscovery() {
    Assertions.assertThat(
            PeerDiscoveryProcess.includeCommitteeMembersInDiscovery(
                contractStates.bpOrchestrationContractState.committee(),
                contractStates.bpOrchestrationContractState.getConfirmedBlockProducers()))
        .containsExactlyElementsOf(producers);

    KeyPair key = new KeyPair(BigInteger.ONE);
    BlockProducer pendingProducer =
        new BlockProducer(key.getPublic().createAddress(), key.getPublic());
    FixedList<BlockProducer> newProducers =
        FixedList.create(
            List.of(producers.get(0), producers.get(1), producers.get(2), pendingProducer));

    StateWrapper contractStatesPending =
        new StateWrapper(
            new BpOrchestrationContractState(
                0,
                0,
                null,
                null,
                AvlTree.create(
                    newProducers.stream()
                        .collect(Collectors.toMap(BlockProducer::identity, producer -> producer))),
                newProducers,
                null),
            null);
    Assertions.assertThat(
            PeerDiscoveryProcess.includeCommitteeMembersInDiscovery(
                contractStatesPending.bpOrchestrationContractState.committee(),
                contractStatesPending.bpOrchestrationContractState.getConfirmedBlockProducers()))
        .containsExactlyElementsOf(newProducers);
    contractStates.bpOrchestrationContractState =
        new BpOrchestrationContractState(
            0,
            0,
            null,
            null,
            contractStates.bpOrchestrationContractState.blockProducers(),
            contractStatesPending
                .bpOrchestrationContractState
                .committee()
                .addElements(List.of(pendingProducer)),
            null);
    producers.add(pendingProducer);

    FixedList<BlockProducer> actual =
        PeerDiscoveryProcess.includeCommitteeMembersInDiscovery(
            contractStates.bpOrchestrationContractState.committee(),
            contractStates.bpOrchestrationContractState.getConfirmedBlockProducers());
    for (BlockProducer bp : actual) {
      Assertions.assertThat(
              producers.stream().anyMatch(producer -> producer.identity().equals(bp.identity())))
          .isTrue();
    }
  }

  @Test
  void discoveryReturnsBogusPeer() {
    PeerDiscoveryProcess process = createProcess(1, 2);
    Assertions.assertThat(
            process.determineUnknownNodes(
                contractStates.bpOrchestrationContractState.committee(),
                contractStates.bpOrchestrationContractState.getConfirmedBlockProducers()))
        .isNotEmpty();
    preparePeerDiscoveryResponseWithUnknownPeer(1, 3);
    prepareNoResponse(2);
    prepareHandshake(0);
    prepareHandshake(1);
    prepareHandshake(2);
    updateListeners();
    process.run();
    Assertions.assertThat(
            process.determineUnknownNodes(
                contractStates.bpOrchestrationContractState.committee(),
                contractStates.bpOrchestrationContractState.getConfirmedBlockProducers()))
        .isNotEmpty();
  }

  @Test
  void processName() {
    PeerDiscoveryProcess process = createProcess();
    Assertions.assertThat(process.name()).isEqualTo(PeerDiscoveryProcess.class.getSimpleName());
  }

  @Test
  void processWithNoStateReturnsFalse() {
    PeerDiscoveryProcess process = createProcess();
    listener.remove();
    Assertions.assertThat(process.run()).isFalse();
  }

  @Test
  void processWaitsIfNoChange() throws InterruptedException {
    PeerDiscoveryProcess process = createProcess();
    Thread thread =
        new Thread(
            () -> {
              process.run();
              process.run();
            });
    thread.start();
    sleep(300);
    Assertions.assertThat(thread.getState().toString()).isEqualTo("WAITING");
  }

  private void prepareHandshake(int responder) {
    byte[] extra =
        PeerDiscoveryResource.handshakeExtraDataEncrypt(LargeOracleProcessTest.VERSION_INFORMATION);
    Cipher cipher =
        AesHelper.createAesForEncrypt(
            producerRestServerKeys.get(responder).getPrivateKey(),
            LargeOracleProcessTest.DUMMY_EPHEMERAL_KEY.getPublic(),
            extra);
    byte[] serialize =
        SafeDataOutputStream.serialize(
            s -> {
              producerEndpoints.get(0).write(s);
              clientPbcAddress.write(s);
            });
    byte[] encrypted = ExceptionConverter.call(() -> cipher.doFinal(serialize), "");

    String url = producerEndpoints.get(responder).getEndpoint() + "/discover/handshake";
    Mockito.when(webClient.post(Mockito.eq(url), Mockito.any(), Mockito.any()))
        .thenAnswer(
            ret -> {
              counter.getAndIncrement();
              return HandshakeDto.create(encrypted);
            });
  }

  private void assertNoDiscoverRequest(int responder) {
    Address endpoint = producerEndpoints.get(responder);
    Mockito.verify(webClient, Mockito.never())
        .post(Mockito.eq(endpoint.getEndpoint() + "/discover"), Mockito.any(), Mockito.any());
  }

  private void assertDiscoverRequest(int responder) {
    Address endpoint = producerEndpoints.get(responder);
    Mockito.verify(webClient)
        .post(Mockito.eq(endpoint.getEndpoint() + "/discover"), Mockito.any(), Mockito.any());
  }

  private void prepareNoResponse(int responder) {
    Address endpoint = producerEndpoints.get(responder);
    Mockito.when(
            webClient.post(
                Mockito.eq(endpoint.getEndpoint() + "/discover"), Mockito.any(), Mockito.any()))
        .thenThrow(new WebApplicationException(Response.Status.NOT_FOUND));
  }

  private void preparePeerDiscoveryResponseWithUnknownPeer(int responder, int... endpoints) {
    Address endpoint = producerEndpoints.get(responder);
    BlockProducer producer = producers.get(responder);
    byte[] payload = createDiscoveryPayload(responder, endpoints);
    Hash message =
        PeerDiscoveryResource.message(
            producer.publicKey(),
            LargeOracleProcessTest.DUMMY_EPHEMERAL_KEY.getPublic(),
            payload,
            LargeOracleProcessTest.VERSION_INFORMATION);
    Cipher cipher =
        AesHelper.createAesForEncrypt(
            producerRestServerKeys.get(responder).getPrivateKey(),
            LargeOracleProcessTest.DUMMY_EPHEMERAL_KEY.getPublic(),
            message.getBytes());
    byte[] data =
        ExceptionConverter.call(
            () ->
                cipher.doFinal(
                    SafeDataOutputStream.serialize(
                        s -> {
                          s.writeInt(1);
                          LargeOracleProcessTest.DUMMY_EPHEMERAL_KEY.getPublic().write(s);
                          new Address("host", 1234).write(s);
                        })),
            "");
    Mockito.when(
            webClient.post(
                Mockito.eq(endpoint.getEndpoint() + "/discover"), Mockito.any(), Mockito.any()))
        .thenReturn(DiscoveryResponseDto.create(data));
  }

  private void preparePeerDiscoveryResponse(int responder, int... endpoints) {
    Address endpoint = producerEndpoints.get(responder);
    BlockProducer producer = producers.get(responder);
    byte[] payload = createDiscoveryPayload(responder, endpoints);
    Hash message =
        PeerDiscoveryResource.message(
            producer.publicKey(),
            LargeOracleProcessTest.DUMMY_EPHEMERAL_KEY.getPublic(),
            payload,
            LargeOracleProcessTest.VERSION_INFORMATION);
    byte[] discovery = createDiscoveryResponse(responder, message.getBytes(), endpoints);
    Mockito.when(
            webClient.post(
                Mockito.eq(endpoint.getEndpoint() + "/discover"), Mockito.any(), Mockito.any()))
        .thenReturn(DiscoveryResponseDto.create(discovery));
  }

  private byte[] createDiscoveryResponse(int responder, byte[] extraData, int[] endpoints) {
    Cipher cipher =
        AesHelper.createAesForEncrypt(
            producerRestServerKeys.get(responder).getPrivateKey(),
            LargeOracleProcessTest.DUMMY_EPHEMERAL_KEY.getPublic(),
            extraData);
    byte[] data =
        SafeDataOutputStream.serialize(
            s -> {
              s.writeInt(endpoints.length);
              for (int endpoint : endpoints) {
                producers.get(endpoint).publicKey().write(s);
                producerEndpoints.get(endpoint).write(s);
              }
            });
    return ExceptionConverter.call(() -> cipher.doFinal(data), "");
  }

  private byte[] createDiscoveryPayload(int responder, int... endpoints) {
    byte[] payload =
        SafeDataOutputStream.serialize(
            s -> {
              producerRestServerKeys.get(0).getPublic().write(s);
              producerEndpoints.get(0).write(s);
              producerEndpoints.get(responder).write(s);
              BlockchainPublicKey.LIST_SERIALIZER.writeDynamic(
                  s, Arrays.stream(endpoints).mapToObj(i -> producers.get(i).publicKey()).toList());
            });
    Cipher cipher =
        AesHelper.createAesForEncrypt(
            LargeOracleProcessTest.DUMMY_EPHEMERAL_KEY.getPrivateKey(),
            producers.get(responder).publicKey(),
            LargeOracleProcessTest.VERSION_INFORMATION.toString().getBytes(StandardCharsets.UTF_8));
    return ExceptionConverter.call(() -> cipher.doFinal(payload), "");
  }

  /**
   * Populate the peer database of producer and create a peer discovery process for test.
   *
   * @param knownProducers the producers that producer 0 knows
   * @return a new peer discovery process.
   */
  private PeerDiscoveryProcess createProcess(int... knownProducers) {
    Map<BlockchainPublicKey, Address> producerMap = new HashMap<>();
    Address endpoint = producerEndpoints.get(0);
    // assumed to know ourselves
    producerMap.put(producers.get(0).publicKey(), endpoint);
    for (int knownProducer : knownProducers) {
      BlockchainPublicKey producerKey = producers.get(knownProducer).publicKey();
      Address producerEndpoint = producerEndpoints.get(knownProducer);
      producerMap.put(producerKey, producerEndpoint);
    }
    peerDatabase = new SimplePeerDatabase(producerMap);
    return createPeerDiscoverProcess();
  }

  private PeerDiscoveryProcess createPeerDiscoverProcess() {
    return new PeerDiscoveryProcess(
        () -> LargeOracleProcessTest.DUMMY_EPHEMERAL_KEY,
        webClient,
        producerRestServerKeys.get(0),
        peerDatabase,
        LargeOracleProcessTest.BPO_CONTRACT,
        LargeOracleProcessTest.VERSION_INFORMATION,
        producerEndpoints.get(0),
        clientPbcAddress,
        contractStateHandler);
  }

  private void createListeners() {
    listener =
        contractStateHandler.createContract(
            null,
            null,
            LargeOracleProcessTest.BPO_CONTRACT,
            null,
            contractStates.bpOrchestrationContractState);
  }

  private void updateListeners() {
    listener.update(contractStates.bpOrchestrationContractState);
  }
}
