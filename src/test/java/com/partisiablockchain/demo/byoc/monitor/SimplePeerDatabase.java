package com.partisiablockchain.demo.byoc.monitor;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.demo.byoc.Address;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/** Test. */
public final class SimplePeerDatabase implements PeerDatabase {

  private final Map<BlockchainPublicKey, Address> map = new HashMap<>();
  private List<BlockchainPublicKey> registered;

  /**
   * Default constructor.
   *
   * @param map map
   */
  public SimplePeerDatabase(Map<BlockchainPublicKey, Address> map) {
    this.map.putAll(map);
    registered = new ArrayList<>();
  }

  @Override
  public void setRegisteredBlockProducers(List<BlockchainPublicKey> addresses) {
    registered = addresses;
  }

  @Override
  public boolean isRegisteredBlockProducer(BlockchainPublicKey address) {
    return registered.contains(address);
  }

  @Override
  public Address getBlockProducerEndpoint(BlockchainPublicKey address) {
    return map.get(address);
  }

  @Override
  public List<BlockchainPublicKey> getBlockProducersWithEndpoint() {
    return map.keySet().stream().filter(p -> map.get(p) != null).toList();
  }

  @Override
  public boolean containsBlockProducer(BlockchainPublicKey address) {
    return map.containsKey(address) && map.get(address) != null;
  }

  @Override
  public void updateBlockProducerEndpoint(BlockchainPublicKey address, Address endpoint) {
    map.put(address, endpoint);
  }

  Map<BlockchainPublicKey, Address> getInternalMap() {
    return map;
  }
}
