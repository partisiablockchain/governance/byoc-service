package com.partisiablockchain.demo.byoc.monitor;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.client.transaction.Transaction;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.demo.byoc.governance.largeoracle.Dispute;
import com.partisiablockchain.demo.byoc.governance.largeoracle.LargeOracleContractHelper;
import com.partisiablockchain.demo.byoc.governance.largeoracle.LargeOracleContractState;
import com.partisiablockchain.demo.byoc.governance.largeoracle.OracleDisputeId;
import com.partisiablockchain.demo.byoc.governance.priceoracle.PriceOracleContractState;
import com.partisiablockchain.demo.byoc.governance.priceoracle.PriceOracleNodes;
import com.partisiablockchain.demo.byoc.governance.priceoracle.PriceOracleRoundData;
import com.partisiablockchain.demo.byoc.governance.priceoracle.PriceUpdates;
import com.partisiablockchain.demo.byoc.governance.priceoracle.RegistrationStatus;
import com.partisiablockchain.demo.byoc.governance.priceoracle.RoundPriceUpdates;
import com.partisiablockchain.demo.byoc.monitor.chainlink.ChainlinkClient;
import com.partisiablockchain.demo.byoc.settings.EvmPriceOracleChainConfig;
import com.partisiablockchain.demo.byoc.settings.PriceOracleSettings;
import com.partisiablockchain.demo.byoc.settings.SettingsTest;
import com.partisiablockchain.math.Unsigned256;
import com.partisiablockchain.server.tcp.client.Client;
import com.partisiablockchain.tree.AvlTree;
import com.secata.stream.SafeDataOutputStream;
import com.secata.tools.immutable.FixedList;
import java.math.BigInteger;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.web3j.protocol.Web3j;

final class PriceOracleNotifyProcessTest {

  private final Web3j web3jMock = Mockito.mock(Web3j.class);
  private final String referenceContractAddress = "1".repeat(20);

  ContractStateHandler contractStateHandler =
      new ContractStateHandler(
          List.of(
              PriceOracleTestUtility.priceOracleContract1,
              PriceOracleTestUtility.priceOracleContract2,
              PriceOracleTestUtility.largeOracleContract));
  Map<BlockchainAddress, Client.Listener> listeners;
  long systemTime;

  private final PriceOracleNotifyProcess process =
      new PriceOracleNotifyProcess(
          () -> systemTime,
          PriceOracleTestUtility.nodeAddress,
          PriceOracleTestUtility.priceOracleContracts,
          123,
          (transaction, cost) -> latestSentTransaction = transaction,
          contractStateHandler,
          Map.of(
              "ChainId",
              new ChainlinkClient(
                  web3jMock, PriceOracleTestUtility.ethereumBlockValidityThreshold)));

  private Transaction latestSentTransaction;

  @BeforeEach
  public void setup() {
    listeners = PriceOracleTestUtility.createListeners(contractStateHandler);
  }

  @Test
  void create() {
    PriceOracleSettings settings =
        SettingsTest.defaultBase(PriceOracleSettings.builder())
            .setChainConfigs(
                List.of(
                    new EvmPriceOracleChainConfig(
                        "ChainId", "https://endpoint.url.test", BigInteger.TEN)))
            .setPriceOracleContracts(PriceOracleTestUtility.priceOracleContracts)
            .build();
    PriceOracleNotifyProcess priceOracleNotifyProcess = PriceOracleNotifyProcess.create(settings);
    assertThat(priceOracleNotifyProcess).isNotNull();
  }

  @Test
  void name() {
    assertThat(process.name()).isEqualTo(PriceOracleNotifyProcess.class.getSimpleName());
  }

  @Test
  void run() {
    Mockito.when(web3jMock.ethBlockNumber()).thenReturn(new EthBlockNumberRequestStub<>(50L));
    Mockito.when(web3jMock.ethCall(Mockito.any(), Mockito.any()))
        .thenReturn(
            new PriceOracleTestUtility.EthCallRoundDataRequest<>(PriceOracleTestUtility.roundData));

    // No nodes registered
    latestSentTransaction = null;
    assertThat(process.run()).isFalse();
    assertThat(latestSentTransaction).isNull();

    PriceOracleContractState contractState =
        PriceOracleTestUtility.getContractState(
            PriceOracleTestUtility.priceOracleContract2,
            PriceOracleContractState.class,
            contractStateHandler);
    Client.Listener listener = listeners.get(PriceOracleTestUtility.priceOracleContract2);
    // mark node as pending
    listener.update(setNodeAsPending(contractState, PriceOracleTestUtility.nodeAddress));
    latestSentTransaction = null;
    assertThat(process.run()).isFalse();
    assertThat(latestSentTransaction).isNull();

    // register nodes
    listener.update(
        PriceOracleTestUtility.registerNode(contractState, PriceOracleTestUtility.nodeAddress));

    latestSentTransaction = null;
    process.run();

    // A price update was notified
    assertThat(PriceOracleTestUtility.priceOracleContract2)
        .isEqualTo(latestSentTransaction.getAddress());
    assertThat(latestSentTransaction.getRpc())
        .isEqualTo(
            SafeDataOutputStream.serialize(
                s -> {
                  s.writeByte(
                      PriceOracleNotifyPriceUpdateSender.NOTIFY_PRICE_UPDATE_INVOCATION_BYTE);
                  PriceOracleTestUtility.roundData.write(s);
                }));

    // Ignore if latestSentRound is newer
    RoundData newerRoundData =
        new RoundData(
            PriceOracleTestUtility.roundData.roundId(),
            PriceOracleTestUtility.roundData.answer(),
            PriceOracleTestUtility.roundData.startedAt().add(Unsigned256.ONE),
            PriceOracleTestUtility.roundData.updatedAt().add(Unsigned256.ONE),
            PriceOracleTestUtility.roundData.answeredInRound());
    PriceOracleContractState withPriceUpdates =
        setPriceUpdates(
            PriceOracleTestUtility.getContractState(
                PriceOracleTestUtility.priceOracleContract2,
                PriceOracleContractState.class,
                contractStateHandler),
            AvlTree.create(
                Map.of(
                    PriceOracleTestUtility.roundData.roundId(),
                    new PriceUpdates(
                        AvlTree.create(
                            Map.of(
                                PriceOracleTestUtility.nodeAddress,
                                PriceOracleRoundData.convert(newerRoundData)))))));
    listener.update(withPriceUpdates);
    latestSentTransaction = null;
    process.run();
    assertThat(latestSentTransaction).isNull();
  }

  @Test
  void notifyPriceUpdateIfChallengePeriodIsOver() {
    PriceOracleTestUtility.setupMockEthReturnValue(web3jMock, PriceOracleTestUtility.roundData);

    Client.Listener listener = listeners.get(PriceOracleTestUtility.priceOracleContract2);
    PriceOracleContractState contractState =
        PriceOracleTestUtility.getContractState(
            PriceOracleTestUtility.priceOracleContract2,
            PriceOracleContractState.class,
            contractStateHandler);
    listener.update(
        PriceOracleTestUtility.registerNode(contractState, PriceOracleTestUtility.nodeAddress));

    latestSentTransaction = null;
    process.run();
    assertThat(latestSentTransaction).isNotNull();

    assertThat(latestSentTransaction.getAddress())
        .isEqualTo(PriceOracleTestUtility.priceOracleContract2);
    assertThat(latestSentTransaction.getRpc())
        .isEqualTo(
            SafeDataOutputStream.serialize(
                s -> {
                  s.writeByte(
                      PriceOracleNotifyPriceUpdateSender.NOTIFY_PRICE_UPDATE_INVOCATION_BYTE);
                  PriceOracleTestUtility.roundData.write(s);
                }));

    // Create a challenge period
    long challengePeriodEnd = 1L;
    listener.update(
        PriceOracleTestUtility.setFakeActiveChallengePeriod(
            (PriceOracleContractState)
                contractStateHandler.contractStates.get(
                    PriceOracleTestUtility.priceOracleContract2),
            PriceOracleTestUtility.roundData,
            challengePeriodEnd,
            List.of(
                PriceOracleTestUtility.nodeAddress,
                PriceOracleTestUtility.oracleAccounts.get(1),
                PriceOracleTestUtility.oracleAccounts.get(2))));

    // Run after challenge period is over
    RoundData newRoundData =
        new RoundData(
            PriceOracleTestUtility.roundData.roundId().add(Unsigned256.ONE),
            PriceOracleTestUtility.roundData.answer(),
            PriceOracleTestUtility.roundData.startedAt(),
            PriceOracleTestUtility.roundData.updatedAt(),
            PriceOracleTestUtility.roundData.answeredInRound());
    PriceOracleTestUtility.setupMockEthReturnValue(web3jMock, newRoundData);
    latestSentTransaction = null;
    setSystemTime(challengePeriodEnd + 1);
    process.run();
    assertThat(latestSentTransaction).isNotNull();

    assertThat(latestSentTransaction.getRpc())
        .isEqualTo(
            SafeDataOutputStream.serialize(
                s -> {
                  s.writeByte(
                      PriceOracleNotifyPriceUpdateSender.NOTIFY_PRICE_UPDATE_INVOCATION_BYTE);
                  newRoundData.write(s);
                }));
  }

  /**
   * If a challenge period is over but no new round data is available the process should do nothing.
   */
  @Test
  void ignoreIfChallengePeriodIsOverButNoNewerRoundData() {
    PriceOracleTestUtility.setupMockEthReturnValue(web3jMock, PriceOracleTestUtility.roundData);
    PriceOracleTestUtility.registerNode(
        listeners, contractStateHandler, PriceOracleTestUtility.nodeAddress);
    long challengePeriodEnd = 4L;
    PriceOracleTestUtility.setFakeChallengePeriod(
        listeners,
        contractStateHandler,
        PriceOracleTestUtility.roundData,
        challengePeriodEnd,
        List.of(
            PriceOracleTestUtility.nodeAddress,
            PriceOracleTestUtility.oracleAccounts.get(0),
            PriceOracleTestUtility.oracleAccounts.get(1)));

    PriceOracleContractState state =
        PriceOracleTestUtility.getContractState(
            PriceOracleTestUtility.priceOracleContract1,
            PriceOracleContractState.class,
            contractStateHandler);
    PriceOracleContractState withLatestPublishedRoundId =
        PriceOracleTestUtility.setLatestPublishedRoundId(
            PriceOracleTestUtility.roundData.roundId(), state);
    listeners.get(PriceOracleTestUtility.priceOracleContract1).update(withLatestPublishedRoundId);

    setSystemTime(challengePeriodEnd + 1);
    process.run();
    assertThat(latestSentTransaction).isNull();
  }

  @Test
  void ignoreIfChallengePeriodIsOngoing() {
    setSystemTime(5L);
    RoundData roundData =
        new RoundData(
            Unsigned256.ONE, Unsigned256.ONE, Unsigned256.ONE, Unsigned256.ONE, Unsigned256.ONE);
    PriceOracleTestUtility.setupMockEthReturnValue(web3jMock, roundData);
    PriceOracleTestUtility.registerNode(
        listeners, contractStateHandler, PriceOracleTestUtility.nodeAddress);
    PriceOracleTestUtility.setFakeChallengePeriod(
        listeners,
        contractStateHandler,
        roundData,
        5L,
        List.of(
            PriceOracleTestUtility.nodeAddress,
            PriceOracleTestUtility.oracleAccounts.get(0),
            PriceOracleTestUtility.oracleAccounts.get(1)));
    PriceOracleTestUtility.setupMockEthReturnValue(
        web3jMock,
        new RoundData(
            roundData.roundId().add(Unsigned256.ONE),
            Unsigned256.ONE,
            Unsigned256.ONE,
            Unsigned256.ONE,
            Unsigned256.ONE));
    process.run();
    assertThat(latestSentTransaction).isNull();
  }

  @Test
  void waitIfActiveChallengePeriodIsNewerThanLatestEthereumTimestamp() {
    Mockito.when(web3jMock.ethBlockNumber()).thenReturn(new EthBlockNumberRequestStub<>(50L));
    Mockito.when(web3jMock.ethCall(Mockito.any(), Mockito.any()))
        .thenReturn(
            new PriceOracleTestUtility.EthCallRoundDataRequest<>(PriceOracleTestUtility.roundData));

    PriceOracleContractState state =
        PriceOracleTestUtility.getContractState(
            PriceOracleTestUtility.priceOracleContract1,
            PriceOracleContractState.class,
            contractStateHandler);
    state = PriceOracleTestUtility.registerNode(state, PriceOracleTestUtility.nodeAddress);
    state =
        PriceOracleTestUtility.setFakeActiveChallengePeriod(
            state,
            PriceOracleTestUtility.roundData,
            systemTime,
            List.of(
                PriceOracleTestUtility.nodeAddress,
                PriceOracleTestUtility.oracleAccounts.get(1),
                PriceOracleTestUtility.oracleAccounts.get(2)));
    Client.Listener pocListener = listeners.get(PriceOracleTestUtility.priceOracleContract1);
    pocListener.update(state);

    Unsigned256 roundDataTimeStampInSeconds =
        PriceOracleTestUtility.roundData.updatedAt().add(Unsigned256.ONE);
    Mockito.when(web3jMock.ethGetBlockByNumber(Mockito.any(), Mockito.anyBoolean()))
        .thenReturn(new EthereumTest.EthBlockRequest<>(roundDataTimeStampInSeconds.toString()));

    latestSentTransaction = null;
    process.run();

    // Ensure no price update was notified
    assertThat(latestSentTransaction).isNull();
  }

  /**
   * Process does not notify retrieved round data if identifier is the same as the latest published.
   */
  @Test
  void ignoreIfLatestRoundIdIsTheSameAsLatestPublished() {
    RoundData roundData =
        new RoundData(
            Unsigned256.ONE, Unsigned256.ONE, Unsigned256.ONE, Unsigned256.ONE, Unsigned256.ONE);
    PriceOracleTestUtility.setupMockEthReturnValue(web3jMock, roundData);
    PriceOracleTestUtility.registerNode(
        listeners, contractStateHandler, PriceOracleTestUtility.nodeAddress);

    PriceOracleContractState state =
        PriceOracleTestUtility.getContractState(
            PriceOracleTestUtility.priceOracleContract1,
            PriceOracleContractState.class,
            contractStateHandler);
    PriceOracleContractState withLatestPublishedRoundId =
        PriceOracleTestUtility.setLatestPublishedRoundId(roundData.roundId(), state);
    listeners.get(PriceOracleTestUtility.priceOracleContract1).update(withLatestPublishedRoundId);

    process.run();
    assertThat(latestSentTransaction).isNull();
  }

  @Test
  void canNotVoteOnDisputeWhenNotOracleMember() {
    Mockito.when(web3jMock.ethCall(Mockito.any(), Mockito.any()))
        .thenReturn(
            new PriceOracleTestUtility.EthCallRoundDataRequest<>(PriceOracleTestUtility.roundData));

    LargeOracleContractState locState =
        PriceOracleTestUtility.getContractState(
            PriceOracleTestUtility.largeOracleContract,
            LargeOracleContractState.class,
            contractStateHandler);

    OracleDisputeId id =
        new OracleDisputeId(
            PriceOracleTestUtility.priceOracleContract1,
            0,
            PriceOracleTestUtility.roundData.roundId().longValueExact());
    Dispute dispute = new Dispute(FixedList.create(Collections.nCopies(1, null)));
    dispute = LargeOracleContractHelper.addVote(dispute, 0, 1);
    locState = LargeOracleContractHelper.setDispute(locState, id, dispute);
    locState = LargeOracleContractHelper.replaceLargeOracle(locState, List.of());
    listeners.get(PriceOracleTestUtility.largeOracleContract).update(locState);

    PriceOracleContractState pocState =
        PriceOracleTestUtility.getContractState(
            PriceOracleTestUtility.priceOracleContract1,
            PriceOracleContractState.class,
            contractStateHandler);
    listeners
        .get(PriceOracleTestUtility.priceOracleContract1)
        .update(setOracleNodes(pocState, new PriceOracleNodes(AvlTree.create())));

    latestSentTransaction = null;
    process.run();
    assertThat(latestSentTransaction).isNull();
  }

  @Test
  void doNotNotifyPriceUpdateWhenDisputeRoundIdDoesNotMatchLatest() {
    Mockito.when(web3jMock.ethBlockNumber()).thenReturn(new EthBlockNumberRequestStub<>(50L));
    Mockito.when(web3jMock.ethCall(Mockito.any(), Mockito.any()))
        .thenReturn(
            new PriceOracleTestUtility.EthCallRoundDataRequest<>(PriceOracleTestUtility.roundData));
    Mockito.when(web3jMock.ethGetBlockByNumber(Mockito.any(), Mockito.anyBoolean()))
        .thenReturn(new EthereumTest.EthBlockRequest<>("1652874975"));
    PriceOracleContractState state =
        PriceOracleTestUtility.getContractState(
            PriceOracleTestUtility.priceOracleContract1,
            PriceOracleContractState.class,
            contractStateHandler);
    state = fakeReferenceContractAddress(state, referenceContractAddress);
    state = PriceOracleTestUtility.registerNode(state, PriceOracleTestUtility.nodeAddress);
    AvlTree<Unsigned256, PriceUpdates> priceUpdates =
        createPriceUpdates(PriceOracleTestUtility.roundData.roundId().add(Unsigned256.ONE));
    state = setPriceUpdates(state, priceUpdates);
    listeners.get(PriceOracleTestUtility.priceOracleContract1).update(state);
    assertThat(process.run()).isFalse();
  }

  @Test
  void processDoesNotRunIfContractStatesAreNull() {
    Mockito.when(web3jMock.ethCall(Mockito.any(), Mockito.any()))
        .thenReturn(
            new PriceOracleTestUtility.EthCallRoundDataRequest<>(PriceOracleTestUtility.roundData));
    long newestBlock = 50L;
    Mockito.when(web3jMock.ethBlockNumber())
        .thenReturn(new EthBlockNumberRequestStub<>(newestBlock));
    assertThat(process.run()).isFalse();
    Client.Listener priceListener =
        contractStateHandler.createContract(
            null,
            null,
            PriceOracleTestUtility.priceOracleContract1,
            null,
            PriceOracleTestUtility.dummyPriceOracleState());
    assertThat(process.run()).isFalse();
    priceListener.remove();
    Client.Listener largeListener =
        contractStateHandler.createContract(
            null,
            null,
            PriceOracleTestUtility.largeOracleContract,
            null,
            PriceOracleTestUtility.dummyLargeOracleState());
    assertThat(process.run()).isFalse();
    priceListener.update(
        PriceOracleTestUtility.registerNode(
            PriceOracleTestUtility.dummyPriceOracleState(), PriceOracleTestUtility.nodeAddress));
    largeListener.remove();
    contractStateHandler.createContract(
        null,
        null,
        PriceOracleTestUtility.priceOracleContract2,
        null,
        PriceOracleTestUtility.dummyPriceOracleState());
    assertThat(process.run()).isFalse();
  }

  /**
   * A node should not send a price notification if the price oracle contract has registered the
   * node as opted out.
   */
  @Test
  void nodeDoesNotNotifyIfItIsOptedOut() {
    PriceOracleContractState state = PriceOracleTestUtility.dummyPriceOracleState();
    Client.Listener listener = listeners.get(PriceOracleTestUtility.priceOracleContract1);
    listener.update(PriceOracleTestUtility.registerNode(state, PriceOracleTestUtility.nodeAddress));
    listener.update(
        PriceOracleTestUtility.setNodeAsOptedOut(state, PriceOracleTestUtility.nodeAddress));
    process.run();
    assertThat(latestSentTransaction).isNull();
  }

  /**
   * Run process without necessary external chain client for the chain identifier. Should do
   * nothing.
   */
  @Test
  void withoutNecessaryExternalChainClient() {
    PriceOracleNotifyProcess processWithoutExternalChainClients =
        new PriceOracleNotifyProcess(
            () -> systemTime,
            PriceOracleTestUtility.nodeAddress,
            PriceOracleTestUtility.priceOracleContracts,
            123,
            (transaction, cost) -> latestSentTransaction = transaction,
            contractStateHandler,
            Map.of());
    PriceOracleContractState state = PriceOracleTestUtility.dummyPriceOracleState();
    Client.Listener listener = listeners.get(PriceOracleTestUtility.priceOracleContract1);
    listener.update(PriceOracleTestUtility.registerNode(state, PriceOracleTestUtility.nodeAddress));

    processWithoutExternalChainClients.run();

    assertThat(latestSentTransaction).isNull();
  }

  /**
   * Getting latest notified round id by node returns latest notified round id for the specified
   * node even if a later round id has been notified by another node.
   */
  @Test
  void getLatestNotifiedRoundIdByNodeMultipleRounds() {
    BlockchainAddress nodeAddressA =
        BlockchainAddress.fromHash(
            BlockchainAddress.Type.ACCOUNT, Hash.create(h -> h.writeString("NodeAddressA")));
    BlockchainAddress nodeAddressB =
        BlockchainAddress.fromHash(
            BlockchainAddress.Type.ACCOUNT, Hash.create(h -> h.writeString("NodeAddressB")));

    PriceOracleRoundData roundDataA =
        new PriceOracleRoundData(
            Unsigned256.ONE, Unsigned256.ONE, Unsigned256.ONE, Unsigned256.ONE, Unsigned256.ONE);
    PriceOracleRoundData roundDataB =
        new PriceOracleRoundData(
            Unsigned256.create(2),
            Unsigned256.ONE,
            Unsigned256.ONE,
            Unsigned256.ONE,
            Unsigned256.ONE);
    AvlTree<BlockchainAddress, PriceOracleRoundData> priceUpdatesA =
        AvlTree.create(Map.of(nodeAddressA, roundDataA));
    AvlTree<BlockchainAddress, PriceOracleRoundData> priceUpdatesB =
        AvlTree.create(Map.of(nodeAddressB, roundDataB));
    AvlTree<Unsigned256, PriceUpdates> roundPriceUpdates = AvlTree.create();
    roundPriceUpdates =
        roundPriceUpdates.set(roundDataA.roundId(), new PriceUpdates(priceUpdatesA));
    roundPriceUpdates =
        roundPriceUpdates.set(roundDataB.roundId(), new PriceUpdates(priceUpdatesB));

    Unsigned256 latestNotifiedRoundIdByNode =
        PriceOracleNotifyProcess.getLatestNotifiedRoundIdByNode(
            nodeAddressA, new RoundPriceUpdates(roundPriceUpdates));

    assertThat(latestNotifiedRoundIdByNode).isEqualTo(roundDataA.roundId());
  }

  /**
   * Getting latest notified round id by node for node with no notified rounds should return zero.
   */
  @Test
  void getLatestNotifiedRoundIdByNodeNoNotifiedRounds() {
    BlockchainAddress nodeAddressA =
        BlockchainAddress.fromHash(
            BlockchainAddress.Type.ACCOUNT, Hash.create(h -> h.writeString("NodeAddressA")));
    BlockchainAddress nodeAddressB =
        BlockchainAddress.fromHash(
            BlockchainAddress.Type.ACCOUNT, Hash.create(h -> h.writeString("NodeAddressB")));
    PriceOracleRoundData roundDataA =
        new PriceOracleRoundData(
            Unsigned256.ONE, Unsigned256.ONE, Unsigned256.ONE, Unsigned256.ONE, Unsigned256.ONE);
    AvlTree<BlockchainAddress, PriceOracleRoundData> priceUpdatesA =
        AvlTree.create(Map.of(nodeAddressA, roundDataA));
    AvlTree<Unsigned256, PriceUpdates> roundPriceUpdates = AvlTree.create();
    roundPriceUpdates =
        roundPriceUpdates.set(roundDataA.roundId(), new PriceUpdates(priceUpdatesA));

    Unsigned256 latestNotifiedRoundIdByNode =
        PriceOracleNotifyProcess.getLatestNotifiedRoundIdByNode(
            nodeAddressB, new RoundPriceUpdates(roundPriceUpdates));
    assertThat(latestNotifiedRoundIdByNode).isEqualTo(Unsigned256.ZERO);
  }

  private static PriceOracleContractState setNodeAsPending(
      PriceOracleContractState state, BlockchainAddress nodeAddress) {
    AvlTree<BlockchainAddress, RegistrationStatus> nodes = state.oracleNodes().nodes();
    nodes = nodes.set(nodeAddress, RegistrationStatus.PENDING);
    return new PriceOracleContractState(
        state.largeOracleContractAddress(),
        state.referenceContractAddress(),
        state.roundPriceUpdates(),
        state.latestPublishedRoundId(),
        state.symbol(),
        new PriceOracleNodes(nodes),
        state.challengePeriod(),
        state.disputeInfo(),
        state.chainId());
  }

  private static PriceOracleContractState setPriceUpdates(
      PriceOracleContractState state, AvlTree<Unsigned256, PriceUpdates> roundPriceUpdates) {
    return new PriceOracleContractState(
        state.largeOracleContractAddress(),
        state.referenceContractAddress(),
        new RoundPriceUpdates(roundPriceUpdates),
        state.latestPublishedRoundId(),
        state.symbol(),
        state.oracleNodes(),
        state.challengePeriod(),
        state.disputeInfo(),
        state.chainId());
  }

  private static PriceOracleContractState setOracleNodes(
      PriceOracleContractState state, PriceOracleNodes oracleNodes) {
    return new PriceOracleContractState(
        state.largeOracleContractAddress(),
        state.referenceContractAddress(),
        state.roundPriceUpdates(),
        state.latestPublishedRoundId(),
        state.symbol(),
        oracleNodes,
        state.challengePeriod(),
        state.disputeInfo(),
        state.chainId());
  }

  private static PriceOracleContractState fakeReferenceContractAddress(
      PriceOracleContractState state, String referenceContractAddress) {
    return new PriceOracleContractState(
        state.largeOracleContractAddress(),
        referenceContractAddress,
        state.roundPriceUpdates(),
        state.latestPublishedRoundId(),
        state.symbol(),
        state.oracleNodes(),
        state.challengePeriod(),
        state.disputeInfo(),
        state.chainId());
  }

  private AvlTree<Unsigned256, PriceUpdates> createPriceUpdates(Unsigned256 roundId) {
    PriceUpdates priceUpdates =
        new PriceUpdates(
            AvlTree.create(
                Map.of(
                    PriceOracleTestUtility.nodeAddress,
                    PriceOracleRoundData.convert(PriceOracleTestUtility.roundData))));
    return AvlTree.create(Map.of(roundId, priceUpdates));
  }

  void setSystemTime(long time) {
    systemTime = time;
  }
}
