package com.partisiablockchain.demo.byoc.monitor;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.BlsKeyPair;
import com.partisiablockchain.crypto.BlsPublicKey;
import com.partisiablockchain.crypto.KeyPair;
import com.partisiablockchain.demo.byoc.Address;
import com.partisiablockchain.demo.byoc.rest.dto.AdditionalStatusProviderDto;
import com.partisiablockchain.demo.byoc.rest.dto.ResourceStatusDto;
import com.partisiablockchain.providers.AdditionalStatusProvider;
import com.partisiablockchain.providers.AdditionalStatusProviders;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test. */
public final class StatusResourceTest {

  private final KeyPair keyPair = new KeyPair(BigInteger.valueOf(500));
  private final BlsKeyPair blsKeyPair = new BlsKeyPair(BigInteger.valueOf(500));

  @Test
  void statusResource() {
    VersionInformation version = new VersionInformation("test_version");
    AdditionalStatusProviders additionalProvider = new DummyAdditionalStatusProviders();
    AdditionalStatusProvider provider = new SimpleStatusProvider();
    additionalProvider.register(provider);
    StatusResource statusResource =
        new StatusResource(
            () -> 123,
            () -> 456,
            version,
            new SimplePeerDatabase(Map.of()),
            new SystemPublicProvider(
                keyPair.getPublic(),
                keyPair.getPublic().createAddress(),
                blsKeyPair.getPublicKey()),
            new DummyCpuInformationProvider(),
            new DummyMemoryUsageProvider(),
            additionalProvider);
    ResourceStatusDto status = statusResource.status();
    Assertions.assertThat(status.uptime).isEqualTo(123);
    Assertions.assertThat(status.systemTime).isEqualTo(456);
    Assertions.assertThat(status.versionIdentifier).isEqualTo("test_version");
    Assertions.assertThat(status.knownPeersNetworkKeys).isEqualTo(List.of());
    Assertions.assertThat(status.systemLoad).isEqualTo(456);
    Assertions.assertThat(status.numberOfProcessors).isEqualTo(123);
    AdditionalStatusProviderDto actual = status.additionalStatus.get(0);
    Assertions.assertThat(actual.name).isEqualTo("provider");
    Assertions.assertThat(actual.status).isEqualTo(Map.of("key", "value"));
    byte[] keyBytes =
        new byte[] {
          4, 121, -66, 102, 126, -7, -36, -69, -84, 85, -96, 98, -107, -50, -121, 11, 7, 2, -101,
          -4, -37, 45, -50, 40, -39, 89, -14, -127, 91, 22, -8, 23, -104, 72, 58, -38, 119, 38, -93,
          -60, 101, 93, -92, -5, -4, 14, 17, 8, -88, -3, 23, -76, 72, -90, -123, 84, 25, -100, 71,
          -48, -113, -5, 16, -44, -72
        };
    byte[] keyBytesCompressed =
        new byte[] {
          2, 121, -66, 102, 126, -7, -36, -69, -84, 85, -96, 98, -107, -50, -121, 11, 7, 2, -101,
          -4, -37, 45, -50, 40, -39, 89, -14, -127, 91, 22, -8, 23, -104
        };
    BlockchainPublicKey key = BlockchainPublicKey.fromEncodedEcPoint(keyBytes);
    statusResource =
        new StatusResource(
            () -> 123,
            () -> 456,
            version,
            new SimplePeerDatabase(Map.of(key, new Address("test", 209))),
            new SystemPublicProvider(
                keyPair.getPublic(),
                keyPair.getPublic().createAddress(),
                blsKeyPair.getPublicKey()),
            new DummyCpuInformationProvider(),
            new DummyMemoryUsageProvider(),
            additionalProvider);
    Assertions.assertThat(statusResource.status().knownPeersNetworkKeys)
        .contains(keyBytesCompressed);
    Assertions.assertThat(
            BlockchainPublicKey.fromEncodedEcPoint(statusResource.status().networkKey))
        .isEqualTo(keyPair.getPublic());
    Assertions.assertThat(BlockchainAddress.fromString(statusResource.status().blockchainAddress))
        .isEqualTo(keyPair.getPublic().createAddress());
    Assertions.assertThat(BlsPublicKey.createUnsafe(statusResource.status().finalizationKey))
        .isEqualTo(blsKeyPair.getPublicKey());
  }

  private static final class DummyCpuInformationProvider implements CpuInformationProvider {
    @Override
    public int getNumberOfProcessors() {
      return 123;
    }

    @Override
    public double getSystemLoad() {
      return 456;
    }
  }

  private static final class DummyMemoryUsageProvider implements MemoryUsageProvider {
    @Override
    public long getFreeMemory() {
      return 42;
    }

    @Override
    public long getTotalMemory() {
      return 420;
    }

    @Override
    public long getMaxMemory() {
      return 1337;
    }
  }

  private static final class DummyAdditionalStatusProviders implements AdditionalStatusProviders {

    List<AdditionalStatusProvider> providers = new ArrayList<>();

    @Override
    public List<AdditionalStatusProvider> getProviders() {
      return providers;
    }

    @Override
    public void register(AdditionalStatusProvider statusProvider) {
      providers.add(statusProvider);
    }
  }
}
