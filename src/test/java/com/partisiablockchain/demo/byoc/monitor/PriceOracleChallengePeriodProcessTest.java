package com.partisiablockchain.demo.byoc.monitor;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.client.transaction.Transaction;
import com.partisiablockchain.demo.byoc.governance.priceoracle.PriceOracleContractState;
import com.partisiablockchain.demo.byoc.monitor.chainlink.ChainlinkClient;
import com.partisiablockchain.math.Unsigned256;
import com.partisiablockchain.server.tcp.client.Client;
import com.secata.stream.SafeDataOutputStream;
import java.util.List;
import java.util.Map;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.web3j.protocol.Web3j;

final class PriceOracleChallengePeriodProcessTest {

  private final Web3j web3jMock = Mockito.mock(Web3j.class);

  ContractStateHandler contractStateHandler =
      new ContractStateHandler(
          List.of(
              PriceOracleTestUtility.priceOracleContract1,
              PriceOracleTestUtility.priceOracleContract2,
              PriceOracleTestUtility.largeOracleContract));
  Map<BlockchainAddress, Client.Listener> listeners;
  long systemTime;
  private final PriceOracleChallengePeriodProcess challengePeriodProcess =
      new PriceOracleChallengePeriodProcess(
          () -> systemTime,
          PriceOracleTestUtility.nodeAddress,
          PriceOracleTestUtility.priceOracleContracts,
          123,
          (transaction, cost) -> latestSentTransaction = transaction,
          contractStateHandler,
          Map.of(
              "ChainId",
              new ChainlinkClient(
                  web3jMock, PriceOracleTestUtility.ethereumBlockValidityThreshold)));

  private Transaction latestSentTransaction;

  @BeforeEach
  public void setup() {
    listeners = PriceOracleTestUtility.createListeners(contractStateHandler);
  }

  @Test
  void name() {
    assertThat(challengePeriodProcess.name())
        .isEqualTo(PriceOracleChallengePeriodProcess.class.getSimpleName());
  }

  @Test
  void processWaitsForStateChange() {
    // Thread gets blocked on second run due to no change in PriceOracleContract
    Thread thread =
        new Thread(
            () -> {
              challengePeriodProcess.run();
              challengePeriodProcess.run();
            });
    thread.start();
    PriceOracleTestUtility.pauseThread(100);
    assertThat(thread.getState().toString()).isEqualTo("WAITING");
    Client.Listener listener = listeners.get(PriceOracleTestUtility.priceOracleContract1);
    // Make a "change" to allow the process to run
    listener.update(
        contractStateHandler.contractStates.get(PriceOracleTestUtility.priceOracleContract1));
    PriceOracleTestUtility.pauseThread(100);
    assertThat(thread.getState().toString()).isEqualTo("TERMINATED");
  }

  @Test
  void waitIfActiveDisputePeriodIsNewerThanLatestEthereumTimestamp() {
    PriceOracleTestUtility.setupMockEthReturnValue(web3jMock, PriceOracleTestUtility.roundData);
    // Ensure that the "latest" block is an old block
    Mockito.when(web3jMock.ethGetBlockByNumber(Mockito.any(), Mockito.anyBoolean()))
        .thenReturn(new EthereumTest.EthBlockRequest<>("5"));

    PriceOracleContractState state =
        PriceOracleTestUtility.getContractState(
            PriceOracleTestUtility.priceOracleContract1,
            PriceOracleContractState.class,
            contractStateHandler);
    state = PriceOracleTestUtility.registerNode(state, PriceOracleTestUtility.nodeAddress);
    state =
        PriceOracleTestUtility.setFakeActiveChallengePeriod(
            state,
            PriceOracleTestUtility.roundData,
            10L,
            List.of(
                PriceOracleTestUtility.nodeAddress,
                PriceOracleTestUtility.oracleAccounts.get(1),
                PriceOracleTestUtility.oracleAccounts.get(2)));
    Client.Listener pocListener = listeners.get(PriceOracleTestUtility.priceOracleContract1);
    pocListener.update(state);

    latestSentTransaction = null;
    challengePeriodProcess.run();

    // Ensure no price update was notified
    assertThat(latestSentTransaction).isNull();
  }

  @Test
  void ignoreIfChallengePeriodHasEnded() {
    PriceOracleTestUtility.setupMockEthReturnValue(web3jMock, PriceOracleTestUtility.roundData);
    setSystemTime(5L);
    PriceOracleTestUtility.registerNode(
        listeners, contractStateHandler, PriceOracleTestUtility.nodeAddress);
    PriceOracleTestUtility.setFakeChallengePeriod(
        listeners,
        contractStateHandler,
        PriceOracleTestUtility.roundData,
        4L,
        List.of(
            PriceOracleTestUtility.nodeAddress,
            PriceOracleTestUtility.oracleAccounts.get(1),
            PriceOracleTestUtility.oracleAccounts.get(2)));
    challengePeriodProcess.run();
    assertThat(latestSentTransaction).isNull();
  }

  @Test
  void ignoreIfNotBlockProducer() {
    PriceOracleTestUtility.setupMockEthReturnValue(web3jMock, PriceOracleTestUtility.roundData);
    PriceOracleTestUtility.setFakeChallengePeriod(
        listeners,
        contractStateHandler,
        PriceOracleTestUtility.roundData,
        1L,
        List.of(
            PriceOracleTestUtility.nodeAddress,
            PriceOracleTestUtility.oracleAccounts.get(1),
            PriceOracleTestUtility.oracleAccounts.get(2)));
    challengePeriodProcess.run();
    assertThat(latestSentTransaction).isNull();
  }

  @Test
  void notifyPriceUpdateForActiveChallengePeriodIfNotSeenBefore() {
    PriceOracleTestUtility.setupMockEthReturnValue(web3jMock, PriceOracleTestUtility.roundData);

    Client.Listener pocListener = listeners.get(PriceOracleTestUtility.priceOracleContract1);

    PriceOracleContractState contractState =
        PriceOracleTestUtility.getContractState(
            PriceOracleTestUtility.priceOracleContract1,
            PriceOracleContractState.class,
            contractStateHandler);
    pocListener.update(
        PriceOracleTestUtility.registerNode(contractState, PriceOracleTestUtility.nodeAddress));

    latestSentTransaction = null;
    challengePeriodProcess.run();

    // Reflect sent round price update in state and create a challenge period
    PriceOracleTestUtility.setFakeChallengePeriod(
        listeners,
        contractStateHandler,
        PriceOracleTestUtility.roundData,
        2L,
        List.of(
            PriceOracleTestUtility.nodeAddress,
            PriceOracleTestUtility.oracleAccounts.get(1),
            PriceOracleTestUtility.oracleAccounts.get(2)));

    setSystemTime(1);
    challengePeriodProcess.run();

    // Create a not-seen-before challenge period
    RoundData otherRoundData =
        PriceOracleTestUtility.createRoundDataWithNewRoundId(
            PriceOracleTestUtility.roundData,
            PriceOracleTestUtility.roundData.roundId().add(Unsigned256.ONE));
    PriceOracleTestUtility.setFakeChallengePeriod(
        listeners,
        contractStateHandler,
        otherRoundData,
        3L,
        List.of(
            PriceOracleTestUtility.oracleAccounts.get(1),
            PriceOracleTestUtility.oracleAccounts.get(2),
            PriceOracleTestUtility.oracleAccounts.get(3)));

    Mockito.when(web3jMock.ethCall(Mockito.any(), Mockito.any()))
        .thenReturn(new PriceOracleTestUtility.EthCallRoundDataRequest<>(otherRoundData));

    latestSentTransaction = null;
    challengePeriodProcess.run();

    // Assert round data was sent for the not-seen-before challenge period
    assertThat(latestSentTransaction.getRpc())
        .isEqualTo(
            SafeDataOutputStream.serialize(
                s -> {
                  s.writeByte(
                      PriceOracleNotifyPriceUpdateSender.NOTIFY_PRICE_UPDATE_INVOCATION_BYTE);
                  otherRoundData.write(s);
                }));

    // Ignore challenge period if updateAt timestamp is not historic on reference blockchain
    Mockito.when(web3jMock.ethGetBlockByNumber(Mockito.any(), Mockito.anyBoolean()))
        .thenReturn(
            new EthereumTest.EthBlockRequest<>(
                PriceOracleTestUtility.roundData.updatedAt().subtract(Unsigned256.ONE).toString()));
    latestSentTransaction = null;
    pocListener.update(
        contractStateHandler.contractStates.get(PriceOracleTestUtility.priceOracleContract1));
    challengePeriodProcess.run();
    assertThat(latestSentTransaction).isNull();
  }

  @Test
  void notifyPriceUpdateIfDisputeHasNotBeenHandled() {
    PriceOracleTestUtility.setupMockEthReturnValue(web3jMock, PriceOracleTestUtility.roundData);

    Client.Listener listener = listeners.get(PriceOracleTestUtility.priceOracleContract1);
    PriceOracleContractState contractState =
        PriceOracleTestUtility.getContractState(
            PriceOracleTestUtility.priceOracleContract1,
            PriceOracleContractState.class,
            contractStateHandler);
    listener.update(
        PriceOracleTestUtility.registerNode(contractState, PriceOracleTestUtility.nodeAddress));

    // Create a challenge period
    long challengePeriodEnd = 1L;
    PriceOracleTestUtility.setFakeChallengePeriod(
        listeners,
        contractStateHandler,
        PriceOracleTestUtility.roundData,
        challengePeriodEnd,
        List.of(
            PriceOracleTestUtility.oracleAccounts.get(2),
            PriceOracleTestUtility.oracleAccounts.get(3)));

    latestSentTransaction = null;
    challengePeriodProcess.run();
    assertThat(latestSentTransaction).isNotNull();
  }

  @Test
  void doNotNotifyIfOptedOut() {
    Client.Listener listener = listeners.get(PriceOracleTestUtility.priceOracleContract1);
    PriceOracleContractState contractState =
        contractStateHandler.getPriceOracleState(PriceOracleTestUtility.priceOracleContract1);
    listener.update(
        PriceOracleTestUtility.setNodeAsOptedOut(
            contractState, PriceOracleTestUtility.nodeAddress));

    // Create challenge period
    long challengePeriodEnd = 1L;
    PriceOracleTestUtility.setFakeChallengePeriod(
        listeners,
        contractStateHandler,
        PriceOracleTestUtility.roundData,
        challengePeriodEnd,
        List.of(
            PriceOracleTestUtility.oracleAccounts.get(2),
            PriceOracleTestUtility.oracleAccounts.get(3)));

    latestSentTransaction = null;
    challengePeriodProcess.run();
    // Should still be null since node opted out
    assertThat(latestSentTransaction).isNull();
  }

  @Test
  void processDoesNotRunIfContractStatesAreNull() {
    PriceOracleTestUtility.setupMockEthReturnValue(web3jMock, PriceOracleTestUtility.roundData);
    for (Client.Listener listener : listeners.values()) {
      listener.remove(); // Remove all contract states
    }
    assertThat(challengePeriodProcess.run()).isFalse();
    Client.Listener priceListener =
        contractStateHandler.createContract(
            null,
            null,
            PriceOracleTestUtility.priceOracleContract1,
            null,
            PriceOracleTestUtility.dummyPriceOracleState());
    assertThat(challengePeriodProcess.run()).isFalse();
    priceListener.remove();
    Client.Listener largeListener =
        contractStateHandler.createContract(
            null,
            null,
            PriceOracleTestUtility.largeOracleContract,
            null,
            PriceOracleTestUtility.dummyLargeOracleState());
    assertThat(challengePeriodProcess.run()).isFalse();
    priceListener.update(
        PriceOracleTestUtility.registerNode(
            PriceOracleTestUtility.dummyPriceOracleState(), PriceOracleTestUtility.nodeAddress));
    largeListener.remove();
    contractStateHandler.createContract(
        null,
        null,
        PriceOracleTestUtility.priceOracleContract2,
        null,
        PriceOracleTestUtility.dummyPriceOracleState());
    assertThat(challengePeriodProcess.run()).isFalse();
  }

  /**
   * Run process without necessary external chain client for the chain identifier. Should do
   * nothing.
   */
  @Test
  void withoutNecessaryExternalChainClient() {
    PriceOracleChallengePeriodProcess processWithoutExternalChainClients =
        new PriceOracleChallengePeriodProcess(
            () -> systemTime,
            PriceOracleTestUtility.nodeAddress,
            PriceOracleTestUtility.priceOracleContracts,
            123,
            (transaction, cost) -> latestSentTransaction = transaction,
            contractStateHandler,
            Map.of());
    PriceOracleContractState state = PriceOracleTestUtility.dummyPriceOracleState();
    Client.Listener listener = listeners.get(PriceOracleTestUtility.priceOracleContract1);
    listener.update(PriceOracleTestUtility.registerNode(state, PriceOracleTestUtility.nodeAddress));

    PriceOracleTestUtility.setFakeChallengePeriod(
        listeners,
        contractStateHandler,
        PriceOracleTestUtility.roundData,
        1L,
        List.of(
            PriceOracleTestUtility.oracleAccounts.get(2),
            PriceOracleTestUtility.oracleAccounts.get(3)));

    processWithoutExternalChainClients.run();

    assertThat(latestSentTransaction).isNull();
  }

  void setSystemTime(long time) {
    systemTime = time;
  }
}
