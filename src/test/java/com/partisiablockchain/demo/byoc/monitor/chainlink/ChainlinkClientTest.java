package com.partisiablockchain.demo.byoc.monitor.chainlink;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.contract.StateSerializableEquality;
import com.partisiablockchain.demo.byoc.monitor.RoundData;
import com.partisiablockchain.math.Unsigned256;
import java.math.BigInteger;
import java.util.List;
import java.util.stream.Stream;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mockito;
import org.web3j.abi.DefaultFunctionEncoder;
import org.web3j.abi.datatypes.generated.Uint256;
import org.web3j.abi.datatypes.generated.Uint80;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.core.DefaultBlockParameter;
import org.web3j.protocol.core.Request;
import org.web3j.protocol.core.Response;
import org.web3j.protocol.core.methods.response.EthBlock;
import org.web3j.protocol.core.methods.response.EthBlockNumber;
import org.web3j.protocol.core.methods.response.EthCall;

final class ChainlinkClientTest {

  private static final String DUMMY_CHAINLINK_CONTRACT =
      "0x0000000000000000000000000000000000000123";

  /**
   * Getting the latest round data should be parsed to a {@link RoundData} object given a
   * well-formed response without errors from the external EVM chain.
   */
  @Test
  void getLatestRoundData() {
    String roundId = "110680464442257332917";
    String answer = "229086660000";
    String startedAt = "1726480127";
    String updatedAt = "1726480127";
    String response = createRoundDataEthCallResponse(roundId, answer, startedAt, updatedAt);

    Web3j web3j = Mockito.mock(Web3j.class);
    mockEthBlockNumber(web3j, "1234");
    mockEthCall(web3j, createEthCall(response));

    ChainlinkClient chainlinkClient = new ChainlinkClient(web3j, BigInteger.TEN);
    RoundData latestRoundData = chainlinkClient.getLatestRoundData(DUMMY_CHAINLINK_CONTRACT);

    Assertions.assertThat(latestRoundData.roundId()).isEqualTo(Unsigned256.create(roundId));
    Assertions.assertThat(latestRoundData.answer()).isEqualTo(Unsigned256.create(answer));
    Assertions.assertThat(latestRoundData.startedAt()).isEqualTo(Unsigned256.create(startedAt));
    Assertions.assertThat(latestRoundData.updatedAt()).isEqualTo(Unsigned256.create(updatedAt));
    Assertions.assertThat(latestRoundData.answeredInRound()).isEqualTo(Unsigned256.create(roundId));
  }

  /**
   * Getting the latest round data should throw an exception given a response with an error from the
   * external EVM chain.
   */
  @Test
  void getLatestRoundDataError() {
    Response.Error error = createError("unknown error", 1234, "data");
    EthCall ethCall = createEthCall("value", error);

    Web3j web3j = Mockito.mock(Web3j.class);
    mockEthBlockNumber(web3j, "1234");
    mockEthCall(web3j, ethCall);

    ChainlinkClient chainlinkClient = new ChainlinkClient(web3j, BigInteger.TEN);
    Assertions.assertThatThrownBy(
            () -> chainlinkClient.getLatestRoundData(DUMMY_CHAINLINK_CONTRACT), "")
        .hasMessage(
            "Call function 'getLatestRoundData' resulted in error "
                + ChainlinkClient.errorString(error))
        .isInstanceOf(RuntimeException.class);
  }

  /**
   * Getting round data for a specific identifier should be parsed to a {@link RoundData} object
   * given a well-formed response without errors from the external EVM chain.
   */
  @Test
  void getRoundDataById() {
    String roundId = "110680464442257332917";
    String answer = "229086660000";
    String startedAt = "1726480127";
    String updatedAt = "1726480127";
    String response = createRoundDataEthCallResponse(roundId, answer, startedAt, updatedAt);

    Web3j web3j = Mockito.mock(Web3j.class);
    mockEthBlockNumber(web3j, "1234");
    mockEthCall(web3j, createEthCall(response));

    ChainlinkClient chainlinkClient = new ChainlinkClient(web3j, BigInteger.TEN);
    RoundData roundData =
        chainlinkClient.getRoundDataById(DUMMY_CHAINLINK_CONTRACT, Unsigned256.create(roundId));

    Assertions.assertThat(roundData.roundId()).isEqualTo(Unsigned256.create(roundId));
    Assertions.assertThat(roundData.answer()).isEqualTo(Unsigned256.create(answer));
    Assertions.assertThat(roundData.startedAt()).isEqualTo(Unsigned256.create(startedAt));
    Assertions.assertThat(roundData.updatedAt()).isEqualTo(Unsigned256.create(updatedAt));
    Assertions.assertThat(roundData.answeredInRound()).isEqualTo(Unsigned256.create(roundId));
  }

  /**
   * Old retry attempts should be cleaned up after getting a round data from a round identifier
   * without errors. This means all attempts will be available when getting round data for a
   * previously failed round identifier before deciding that it is malicious.
   */
  @Test
  void cleanUpOldRetryAttempts() {
    String oldRoundId = "110680464442257332917";
    String answer = "229086660000";
    String startedAt = "1726480127";
    String updatedAt = "1726480127";
    String response = createRoundDataEthCallResponse(oldRoundId, answer, startedAt, updatedAt);

    Response.Error error = createError("execution reverted", 1234, "data");
    final EthCall ethCallWithError = createEthCall(response, error);

    Web3j web3j = Mockito.mock(Web3j.class);
    mockEthBlockNumber(web3j, "1234");

    ChainlinkClient chainlinkClient = new ChainlinkClient(web3j, BigInteger.TEN);

    // Old round with error increments the number of attempts
    mockEthCall(web3j, ethCallWithError);
    chainlinkClient.getRoundDataById(DUMMY_CHAINLINK_CONTRACT, Unsigned256.create(oldRoundId));

    // New round without errors cleans up old retry attempts
    String roundId = oldRoundId + 1;
    response = createRoundDataEthCallResponse(roundId, answer, startedAt, updatedAt);
    mockEthCall(web3j, createEthCall(response));
    chainlinkClient.getRoundDataById(DUMMY_CHAINLINK_CONTRACT, Unsigned256.create(roundId));

    // Old round with error has all its attempts before returning special 'malicious round data'
    mockEthCall(web3j, ethCallWithError);
    for (int i = 0; i < ChainlinkRetryHandler.MAX_RETRIES; i++) {
      RoundData roundData =
          chainlinkClient.getRoundDataById(
              DUMMY_CHAINLINK_CONTRACT, Unsigned256.create(oldRoundId));
      Assertions.assertThat(roundData).isNull();
    }

    RoundData roundData =
        chainlinkClient.getRoundDataById(DUMMY_CHAINLINK_CONTRACT, Unsigned256.create(oldRoundId));
    StateSerializableEquality.assertStatesEqual(
        roundData, ChainlinkClient.maliciousRoundData(Unsigned256.create(oldRoundId)));
  }

  /**
   * Getting round data for a specific identifier should return the special {@link RoundData} object
   * to indicate a malicious round after having retried the maximum number of times given a round
   * data response for an unfinished round from the external EVM chain.
   */
  @ParameterizedTest
  @MethodSource("startedAtUpdatedAtInTheFuture")
  void getRoundDataByIdRoundIsNotFinished(String startedAt, String updatedAt) {
    String roundId = "110680464442257332917";
    String answer = "229086660000";
    String response = createRoundDataEthCallResponse(roundId, answer, startedAt, updatedAt);

    Web3j web3j = Mockito.mock(Web3j.class);
    mockEthBlockNumber(web3j, "1234");
    mockEthCall(web3j, createEthCall(response));

    ChainlinkClient chainlinkClient = new ChainlinkClient(web3j, BigInteger.TEN);

    for (int i = 0; i < ChainlinkRetryHandler.MAX_RETRIES; i++) {
      RoundData roundData =
          chainlinkClient.getRoundDataById(DUMMY_CHAINLINK_CONTRACT, Unsigned256.create(roundId));
      Assertions.assertThat(roundData).isNull();
    }
    RoundData maliciousRoundData =
        chainlinkClient.getRoundDataById(DUMMY_CHAINLINK_CONTRACT, Unsigned256.create(roundId));
    StateSerializableEquality.assertStatesEqual(
        maliciousRoundData, ChainlinkClient.maliciousRoundData(Unsigned256.create(roundId)));
  }

  private static Stream<Arguments> startedAtUpdatedAtInTheFuture() {
    return Stream.of(
        Arguments.arguments("0", "0"),
        Arguments.arguments("0", "1"),
        Arguments.arguments("1", "0"));
  }

  /**
   * Getting round data for a specific identifier should throw an exception without retrying given a
   * response with an unknown error from the external EVM chain.
   */
  @Test
  void getRoundDataByIdError() {
    Response.Error error = createError("unknown error", 1234, "data");
    EthCall ethCall = createEthCall("value", error);

    Web3j web3j = Mockito.mock(Web3j.class);
    mockEthBlockNumber(web3j, "1234");
    mockEthCall(web3j, ethCall);

    ChainlinkClient chainlinkClient = new ChainlinkClient(web3j, BigInteger.TEN);

    Assertions.assertThatThrownBy(
            () -> chainlinkClient.getRoundDataById(DUMMY_CHAINLINK_CONTRACT, Unsigned256.ONE), "")
        .hasMessage("Unknown error for EthCall 'getRoundData': " + error.getMessage())
        .isInstanceOf(RuntimeException.class);
  }

  /**
   * Getting round data for a specific identifier should return the special {@link RoundData} object
   * to indicate a malicious round after having retried given a response with an error containing
   * 'execution reverted' from the external EVM chain.
   */
  @Test
  void getRoundDataByIdExecutionRevertedError() {
    Response.Error error = createError("bla bla. execution reverted. bla bla.", 1234, "data");
    EthCall ethCall = createEthCall("value", error);

    Web3j web3j = Mockito.mock(Web3j.class);
    mockEthBlockNumber(web3j, "1234");
    mockEthCall(web3j, ethCall);

    ChainlinkClient chainlinkClient = new ChainlinkClient(web3j, BigInteger.TEN);

    Unsigned256 roundId = Unsigned256.ONE;
    for (int i = 0; i < ChainlinkRetryHandler.MAX_RETRIES; i++) {
      RoundData roundData = chainlinkClient.getRoundDataById(DUMMY_CHAINLINK_CONTRACT, roundId);
      Assertions.assertThat(roundData).isNull();
    }
    RoundData maliciousRoundData =
        chainlinkClient.getRoundDataById(DUMMY_CHAINLINK_CONTRACT, roundId);
    StateSerializableEquality.assertStatesEqual(
        maliciousRoundData, ChainlinkClient.maliciousRoundData(roundId));
  }

  /**
   * The latest valid block is the block number of the latest block on the external chain subtracted
   * by the block validity depth specified for the client.
   */
  @Test
  void getLatestValidBlock() {
    String latestBlockNumberOnExternalChain = "1234";
    BigInteger blockValidityDepth = BigInteger.TEN;

    Web3j web3j = Mockito.mock(Web3j.class);
    mockEthBlockNumber(web3j, latestBlockNumberOnExternalChain);

    ChainlinkClient chainlinkClient = new ChainlinkClient(web3j, blockValidityDepth);
    DefaultBlockParameter latestValidBlock = chainlinkClient.getLatestValidBlock();

    BigInteger expectedLatestValidBlockNumber =
        new BigInteger(latestBlockNumberOnExternalChain).subtract(blockValidityDepth);
    Assertions.assertThat(latestValidBlock.getValue())
        .isEqualTo(DefaultBlockParameter.valueOf(expectedLatestValidBlockNumber).getValue());
  }

  /**
   * A timestamp has passed on the external chain if it is less than or equal to the latest
   * timestamp on the external chain.
   *
   * @param timestamp timestamp under test
   * @param hasExceeded expected result
   */
  @ParameterizedTest
  @MethodSource("timestampAndHasTimestampOfLatestValidBlockExceeded")
  void hasTimestampOfLatestValidBlockExceeded(long timestamp, boolean hasExceeded) {
    String latestTimestampOnExternalChain = "100";

    Web3j web3j = Mockito.mock(Web3j.class);
    mockEthBlockNumber(web3j, "1234");
    mockEthBlock(web3j, latestTimestampOnExternalChain);

    ChainlinkClient chainlinkClient = new ChainlinkClient(web3j, BigInteger.TEN);
    Assertions.assertThat(
            chainlinkClient.hasTimestampOfLatestValidBlockExceeded(Unsigned256.create(timestamp)))
        .isEqualTo(hasExceeded);
  }

  private static Stream<Arguments> timestampAndHasTimestampOfLatestValidBlockExceeded() {
    return Stream.of(
        Arguments.arguments(99, true),
        Arguments.arguments(100, true),
        Arguments.arguments(101, false));
  }

  /**
   * The round data object to indicate a malicious round should consist of the round id of the
   * malicious round and zeros.
   */
  @Test
  void maliciousRoundData() {
    Unsigned256 roundId = Unsigned256.TEN;
    RoundData maliciousRoundData = ChainlinkClient.maliciousRoundData(roundId);
    StateSerializableEquality.assertStatesEqual(
        maliciousRoundData,
        new RoundData(
            roundId, Unsigned256.ZERO, Unsigned256.ZERO, Unsigned256.ZERO, Unsigned256.ZERO));
  }

  /** Error string should have expected formatting. */
  @Test
  void errorString() {
    int code = 1234;
    String message = "message";
    String data = "data";
    Response.Error error = new Response.Error();
    error.setCode(code);
    error.setMessage(message);
    error.setData(data);
    String errorString = ChainlinkClient.errorString(error);
    Assertions.assertThat(errorString)
        .isEqualTo(String.format("[code=%s, message=%s, data=%s]", code, message, data));
  }

  private static EthCall createEthCall(String response) {
    return createEthCall(response, null);
  }

  private static EthCall createEthCall(String response, Response.Error error) {
    EthCall ethCall = new EthCall();
    if (error != null) {
      ethCall.setError(error);
    }
    ethCall.setResult("0x" + response);
    return ethCall;
  }

  private static String createRoundDataEthCallResponse(
      String roundId, String answer, String startedAt, String updatedAt) {
    return new DefaultFunctionEncoder()
        .encodeParameters(
            List.of(
                new Uint80(new BigInteger(roundId)),
                new Uint256(new BigInteger(answer)),
                new Uint256(new BigInteger(startedAt)),
                new Uint256(new BigInteger(updatedAt)),
                new Uint80(new BigInteger(roundId))));
  }

  private static Response.Error createError(String message, int code, String data) {
    Response.Error error = new Response.Error();
    error.setMessage(message);
    error.setCode(code);
    error.setData(data);
    return error;
  }

  private static void mockEthBlockNumber(Web3j web3j, String blockNumber) {
    EthBlockNumber ethBlockNumber = new EthBlockNumber();
    ethBlockNumber.setResult(blockNumber);
    Mockito.when(web3j.ethBlockNumber()).thenReturn(new Web3jRequestStub<>(ethBlockNumber));
  }

  private static void mockEthCall(Web3j web3j, EthCall ethCall) {
    Mockito.when(web3j.ethCall(Mockito.any(), Mockito.any()))
        .thenReturn(new Web3jRequestStub<>(ethCall));
  }

  private static void mockEthBlock(Web3j web3j, String timestamp) {
    EthBlock ethBlock = new EthBlock();
    EthBlock.Block block = new EthBlock.Block();
    block.setTimestamp(timestamp);
    ethBlock.setResult(block);
    Mockito.when(web3j.ethGetBlockByNumber(Mockito.any(), Mockito.anyBoolean()))
        .thenReturn(new Web3jRequestStub<>(ethBlock));
  }

  private static final class Web3jRequestStub<S, T extends Response<?>> extends Request<S, T> {

    private final T response;

    public Web3jRequestStub(T response) {
      this.response = response;
    }

    @Override
    public T send() {
      return response;
    }
  }
}
