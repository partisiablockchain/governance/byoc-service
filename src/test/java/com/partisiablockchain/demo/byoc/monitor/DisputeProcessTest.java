package com.partisiablockchain.demo.byoc.monitor;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.argThat;

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.client.transaction.BlockchainTransactionClient;
import com.partisiablockchain.client.transaction.SenderAuthenticationKeyPair;
import com.partisiablockchain.client.transaction.SignedTransaction;
import com.partisiablockchain.client.transaction.Transaction;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.KeyPair;
import com.partisiablockchain.demo.byoc.governance.byocincoming.ByocHelper;
import com.partisiablockchain.demo.byoc.governance.byocincoming.ByocIncomingContractState;
import com.partisiablockchain.demo.byoc.governance.byocincoming.Deposit;
import com.partisiablockchain.demo.byoc.governance.byocincoming.DepositTransaction;
import com.partisiablockchain.demo.byoc.governance.byocoutgoing.ByocOutgoingContractState;
import com.partisiablockchain.demo.byoc.governance.byocoutgoing.Epoch;
import com.partisiablockchain.demo.byoc.governance.byocoutgoing.EthereumAddress;
import com.partisiablockchain.demo.byoc.governance.byocoutgoing.OracleMember;
import com.partisiablockchain.demo.byoc.governance.byocoutgoing.OutgoingHelper;
import com.partisiablockchain.demo.byoc.governance.byocoutgoing.WithdrawalTransaction;
import com.partisiablockchain.demo.byoc.governance.largeoracle.Dispute;
import com.partisiablockchain.demo.byoc.governance.largeoracle.LargeOracleContractHelper;
import com.partisiablockchain.demo.byoc.governance.largeoracle.LargeOracleContractState;
import com.partisiablockchain.demo.byoc.governance.largeoracle.OracleDisputeId;
import com.partisiablockchain.demo.byoc.governance.largeoracle.StakedTokens;
import com.partisiablockchain.math.Unsigned256;
import com.partisiablockchain.server.tcp.client.Client;
import com.partisiablockchain.tree.AvlTree;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import com.secata.tools.immutable.FixedList;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import org.bouncycastle.util.encoders.Hex;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatcher;
import org.mockito.Mockito;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.core.DefaultBlockParameter;
import org.web3j.protocol.core.Request;
import org.web3j.protocol.core.methods.request.EthFilter;
import org.web3j.protocol.core.methods.request.Filter;
import org.web3j.protocol.core.methods.response.EthLog;

/** Test. */
public final class DisputeProcessTest {

  private final List<KeyPair> oracleKeys =
      List.of(new KeyPair(BigInteger.ONE), new KeyPair(BigInteger.valueOf(5)));
  private final List<BlockchainPublicKey> oraclePubKeys =
      oracleKeys.stream().map(KeyPair::getPublic).toList();
  private final List<BlockchainAddress> oracleAccounts =
      oraclePubKeys.stream().map(BlockchainPublicKey::createAddress).collect(Collectors.toList());
  private final List<OracleMember> oracleMembers = createMembers(oracleAccounts);
  private final KeyPair unknownOracleKey = new KeyPair(BigInteger.TEN);
  private final BlockchainAddress unknownOracle = unknownOracleKey.getPublic().createAddress();

  private final BlockchainAddress largeOracleContract =
      BlockchainAddress.fromString("040000000000000000000000000000000000000000");
  private final BlockchainAddress depositContract =
      BlockchainAddress.fromString("040000000000000000000000000000000000000001");
  private final BlockchainAddress withdrawalContract =
      BlockchainAddress.fromString("040000000000000000000000000000000000000002");
  private final BlockchainAddress contract =
      BlockchainAddress.fromString("040000000000000000000000000000000000000003");

  private static final long votingCost = 10L;

  private static final int bitmask = 0b111;

  private static final String loShard = "shard0";
  private final List<com.partisiablockchain.demo.byoc.governance.largeoracle.OracleMember>
      loOracleMembers = createLoOracleMembers(oracleAccounts);
  private final DummyPbcClient<LargeOracleContractState> largeOracleClient =
      DummyPbcClient.forLargeOracle(loOracleMembers);
  private final DummyPbcClient<ByocIncomingContractState> depositClient =
      DummyPbcClient.forByocIncoming(oracleAccounts);

  private static final long DEPLOYED_IN_BLOCK = 3L;
  private static final long LATEST_BLOCK = 20L;
  private static final String ethContract = "0000000000000000000000000000000000000000";
  private static final String ethAccount0 = "4200000000000000000000000000000000001337";
  private static final String ethAccount1 = "cafebabe00000000000000000000000000001337";
  private final DummyPbcClient<ByocOutgoingContractState> withdrawalClient =
      DummyPbcClient.forByocOutgoing(OutgoingHelper.ethereumAddress(ethContract), oracleMembers);

  private final BlockchainAddress identity = oracleKeys.get(0).getPublic().createAddress();
  private final ByocTransactionSender senderKey =
      (trx, cost) -> {
        BlockchainTransactionClient transactionClient =
            BlockchainTransactionClient.create(
                largeOracleClient, new SenderAuthenticationKeyPair(oracleKeys.get(0)));
        transactionClient.waitForSpawnedEvents(transactionClient.signAndSend(trx, cost));
      };

  private final Web3j web3jMock = Mockito.mock(Web3j.class);
  private final BigInteger validityThreshold = BigInteger.TEN;
  private final EthereumEventsCache cache = new DefaultEthereumEventsCache();

  private final Ethereum.BlockRange initialBlockRange =
      new Ethereum.BlockRange(
          BigInteger.valueOf(DEPLOYED_IN_BLOCK),
          BigInteger.valueOf(LATEST_BLOCK).subtract(validityThreshold),
          false);
  private final BlockPaginationStub blockPagination = new BlockPaginationStub(initialBlockRange);
  private final ContractInformationStub contractInformation =
      new ContractInformationStub(BigInteger.valueOf(DEPLOYED_IN_BLOCK));
  private final Ethereum ethereumClient =
      new Ethereum(web3jMock, ethContract, cache, blockPagination, contractInformation);

  private DisputeProcess disputeProcess;

  private final ContractStateHandler contractStateHandler =
      new ContractStateHandler(List.of(largeOracleContract, depositContract, withdrawalContract));

  private Map<BlockchainAddress, Client.Listener> listeners;

  LargeOracleContractState getLargeOracleDummyState() {
    return new LargeOracleContractState(
        FixedList.create(loOracleMembers),
        AvlTree.create(),
        FixedList.create(),
        0,
        0,
        AvlTree.create(),
        AvlTree.create());
  }

  ByocIncomingContractState getDepositDummyState() {
    return new ByocIncomingContractState(
        0,
        AvlTree.create(),
        FixedList.create(oracleAccounts),
        null,
        AvlTree.create(),
        0,
        AvlTree.create());
  }

  ByocOutgoingContractState getWithdrawalDummyState() {
    return new ByocOutgoingContractState(
        OutgoingHelper.ethereumAddress(ethContract),
        0,
        0,
        0,
        AvlTree.<Long, Epoch>create()
            .set(
                0L,
                new Epoch(AvlTree.create(), AvlTree.create(), FixedList.create(oracleMembers))));
  }

  @Test
  void disputeOnCurrentTransaction() {
    setupProcess(1);

    DepositTransaction tx0 = new DepositTransaction(0L, unknownOracle, Unsigned256.ONE);
    DepositTransaction tx0Fraud =
        new DepositTransaction(0L, unknownOracle, Unsigned256.create(1_000_000L));

    setupCurrentDepositDispute(tx0, tx0Fraud);

    runProcess();

    assertVotedOnDispute(depositContract, 0L, 0);
  }

  private void setupCurrentDepositDispute(DepositTransaction tx0, DepositTransaction tx0Fraud) {
    List<DepositTransaction> ethereumTransactions = List.of(tx0);
    String fromBlock = "0x" + BigInteger.valueOf(3).toString(16);
    String toBlock = "0x" + BigInteger.TEN.toString(16);

    Mockito.when(web3jMock.ethGetLogs(argThat(new EthFilterMatcher(fromBlock, toBlock))))
        .thenReturn(new EthLogRequest<>(ethereumTransactions, List.of()));

    ByocIncomingContractState depositState =
        (ByocIncomingContractState) contractStateHandler.contractStates.get(depositContract);
    Deposit deposit = new Deposit(tx0Fraud.receiver(), tx0Fraud.amount(), 0);
    depositState = ByocHelper.setDeposit(depositState, deposit);

    LargeOracleContractState loState =
        (LargeOracleContractState) contractStateHandler.contractStates.get(largeOracleContract);

    depositState = ByocHelper.addDispute(depositState, tx0.nonce(), tx0.receiver(), tx0.amount());

    OracleDisputeId id = new OracleDisputeId(depositContract, 0L, tx0Fraud.nonce());
    Dispute dispute = new Dispute(FixedList.create(Collections.nCopies(2, null)));
    loState = LargeOracleContractHelper.setDispute(loState, id, dispute);

    contractStateHandler.contractStates.put(depositContract, depositState);
    contractStateHandler.contractStates.put(largeOracleContract, loState);
  }

  @Test
  void previousOracleOnlyDisputesMissingOnPbc() {
    setupProcess(1);
    addStakedTokens(2_500_0000L, null);

    WithdrawalTransaction tx0 =
        new WithdrawalTransaction(0, ethAccount0, Unsigned256.ONE, 0L, bitmask);
    WithdrawalTransaction tx1 =
        new WithdrawalTransaction(0, ethAccount0, Unsigned256.ONE, 1L, bitmask);

    new DisputeBuilder().setWithdrawals(List.of(tx0, tx1), List.of(), List.of()).build();

    ByocOutgoingContractState state =
        (ByocOutgoingContractState) contractStateHandler.contractStates.get(withdrawalContract);
    state = OutgoingHelper.endCurrentEpoch(state);
    state = OutgoingHelper.startNewEpoch(state, oracleMembers);
    contractStateHandler.contractStates.put(withdrawalContract, state);

    runProcess();
    assertThat(disputeProcess.getNextWithdrawalNonceForOracle(0)).isEqualTo(0);
    assertThat(disputeProcess.getNextWithdrawalNonceForOracle(1)).isNull();
  }

  @Test
  void dontDisputeAlreadyDisputedWithdrawal() {
    setupProcess(1);
    addStakedTokens(2_500_0000L, null);

    WithdrawalTransaction tx0 =
        new WithdrawalTransaction(0, ethAccount0, Unsigned256.ONE, 0L, bitmask);
    WithdrawalTransaction tx1 =
        new WithdrawalTransaction(0, ethAccount0, Unsigned256.ONE, 1L, bitmask);

    new DisputeBuilder().setWithdrawals(List.of(tx0, tx1), List.of(), List.of(tx0)).build();

    ByocOutgoingContractState state =
        (ByocOutgoingContractState) contractStateHandler.contractStates.get(withdrawalContract);
    state = OutgoingHelper.endCurrentEpoch(state);
    state = OutgoingHelper.startNewEpoch(state, oracleMembers);
    contractStateHandler.contractStates.put(withdrawalContract, state);

    runProcess();
    assertThat(withdrawalClient.getTransactionsCount()).isEqualTo(0);
  }

  @Test
  void previousOracleNotStakedEnough() {
    setupProcess(1);
    addStakedTokens(2_500_0000L * 2L - 1L, null);

    WithdrawalTransaction tx0 =
        new WithdrawalTransaction(0, ethAccount0, Unsigned256.ONE, 0L, bitmask);
    WithdrawalTransaction tx1 =
        new WithdrawalTransaction(1, ethAccount0, Unsigned256.ONE, 0L, bitmask);
    WithdrawalTransaction tx2 =
        new WithdrawalTransaction(1, ethAccount0, Unsigned256.ONE, 1L, bitmask);

    new DisputeBuilder().setWithdrawals(List.of(tx0, tx1, tx2), List.of(), List.of()).build();

    ByocOutgoingContractState state =
        (ByocOutgoingContractState) contractStateHandler.contractStates.get(withdrawalContract);
    state = OutgoingHelper.endCurrentEpoch(state);
    state = OutgoingHelper.startNewEpoch(state, oracleMembers);
    contractStateHandler.contractStates.put(withdrawalContract, state);
    new DisputeBuilder().setWithdrawals(List.of(tx0, tx1), List.of(), List.of()).build();

    runProcess();
    assertThat(disputeProcess.getNextWithdrawalNonceForOracle(0)).isEqualTo(1L);
    assertThat(disputeProcess.getNextWithdrawalNonceForOracle(1)).isNull();
  }

  @Test
  void previousOracleDisputeMismatchingWithdrawals() {
    setupProcess(2);
    addStakedTokens(2_500_0000L * 3L - 1L, null);

    WithdrawalTransaction tx0 =
        new WithdrawalTransaction(0, ethAccount0, Unsigned256.ONE, 0L, bitmask);
    WithdrawalTransaction tx00 =
        new WithdrawalTransaction(0, ethAccount0, Unsigned256.TEN, 0L, bitmask);
    WithdrawalTransaction tx1 =
        new WithdrawalTransaction(1, ethAccount0, Unsigned256.ONE, 0L, bitmask);
    WithdrawalTransaction tx2 =
        new WithdrawalTransaction(1, ethAccount0, Unsigned256.ONE, 1L, bitmask);

    new DisputeBuilder().setWithdrawals(List.of(tx0, tx1, tx2), List.of(tx00), List.of()).build();

    ByocOutgoingContractState state =
        (ByocOutgoingContractState) contractStateHandler.contractStates.get(withdrawalContract);
    state = OutgoingHelper.endCurrentEpoch(state);
    state = OutgoingHelper.startNewEpoch(state, oracleMembers);
    contractStateHandler.contractStates.put(withdrawalContract, state);
    new DisputeBuilder().setWithdrawals(List.of(tx0, tx1), List.of(tx00), List.of()).build();

    runProcess();
    assertThat(disputeProcess.getNextWithdrawalNonceForOracle(0)).isEqualTo(0L);
    assertThat(disputeProcess.getNextWithdrawalNonceForOracle(1)).isNull();
    assertThat(largeOracleClient.getTransactionsCount()).isEqualTo(2);
    byte[] receiver = Hex.decode(tx00.destination());
    Transaction tx =
        Transaction.create(
            withdrawalContract,
            SafeDataOutputStream.serialize(
                stream -> {
                  stream.writeByte(2);
                  stream.writeLong(tx00.oracleNonce());
                  stream.writeLong(tx00.withdrawalNonce());
                  stream.write(receiver);
                  tx0.amount().write(stream);
                  stream.writeInt(tx00.bitmask());
                }));
    assertThat(largeOracleClient.getLatestPut().getInner()).isEqualTo(tx);
  }

  @Test
  void nonLargeOracleMembersDoNotVoteOnDisputes() {
    ByocTransactionSender sender =
        (trx, cost) -> {
          BlockchainTransactionClient transactionClient =
              BlockchainTransactionClient.create(
                  largeOracleClient, new SenderAuthenticationKeyPair(unknownOracleKey));
          transactionClient.waitForSpawnedEvents(transactionClient.signAndSend(trx, cost));
        };
    createListeners();
    disputeProcess =
        new DisputeProcess(
            ethereumClient,
            unknownOracle,
            sender,
            largeOracleContract,
            depositContract,
            withdrawalContract,
            votingCost,
            contractStateHandler,
            new SystemTimeProviderImpl());

    DepositTransaction disputedTx = new DepositTransaction(0L, unknownOracle, Unsigned256.ONE);
    new DisputeBuilder().setDeposits(List.of(), List.of(), List.of(disputedTx)).build();
    runProcess();

    assertThat(largeOracleClient.getTransactionsCount()).isEqualTo(0);
    assertThat(depositClient.getTransactionsCount()).isEqualTo(0);
    assertThat(withdrawalClient.getTransactionsCount()).isEqualTo(0);
  }

  @Test
  void voteOnFraudulentDeposit() {
    setupProcess(1);
    // Stake tokens to test that dispute it not created twice

    DepositTransaction tx0 = new DepositTransaction(0L, unknownOracle, Unsigned256.create(10_000L));
    DepositTransaction tx1 = new DepositTransaction(1L, unknownOracle, Unsigned256.ONE);
    DepositTransaction tx2 = new DepositTransaction(2L, identity, Unsigned256.create(20_000L));
    DepositTransaction tx1Fraud =
        new DepositTransaction(1L, unknownOracle, Unsigned256.create(1_000_000L));

    new DisputeBuilder()
        .setDeposits(List.of(tx0, tx1, tx2), List.of(tx0, tx1Fraud, tx2), List.of(tx1))
        .build();

    runProcess();

    assertVotedOnDispute(depositContract, 1L, 0);
  }

  @Test
  void votesAreOnlyCastOnce() {
    setupProcess(1);

    DepositTransaction tx0 = new DepositTransaction(0L, unknownOracle, Unsigned256.create(10_000L));
    DepositTransaction tx0Fraud =
        new DepositTransaction(0L, unknownOracle, Unsigned256.create(1_000_000L));

    new DisputeBuilder().setDeposits(List.of(tx0), List.of(tx0Fraud), List.of(tx0)).build();
    LargeOracleContractState state =
        (LargeOracleContractState) contractStateHandler.contractStates.get(largeOracleContract);
    OracleDisputeId id = new OracleDisputeId(depositContract, 0L, 0L);
    Dispute dispute = state.activeDisputes().getValue(id);
    dispute = LargeOracleContractHelper.addVote(dispute, 0, 0);
    dispute = LargeOracleContractHelper.addVote(dispute, 1, 0);
    state = LargeOracleContractHelper.setDispute(state, id, dispute);
    contractStateHandler.contractStates.put(largeOracleContract, state);

    runProcess();

    assertThat(largeOracleClient.getTransactionsCount()).isEqualTo(0);
  }

  @Test
  void willNotVoteOnUnknownContractAddress() {
    setupProcess(1);

    LargeOracleContractState state =
        (LargeOracleContractState) contractStateHandler.contractStates.get(largeOracleContract);
    OracleDisputeId id = new OracleDisputeId(contract, 0L, 0L);
    Dispute dispute = new Dispute(FixedList.create(Collections.nCopies(2, null)));
    state = LargeOracleContractHelper.setDispute(state, id, dispute);
    contractStateHandler.contractStates.put(largeOracleContract, state);

    new DisputeBuilder().build();

    runProcess();

    assertThat(largeOracleClient.getTransactionsCount()).isEqualTo(0);
  }

  @Test
  void voteOnDepositDisputeCounterClaim() {
    setupProcess(1);

    DepositTransaction tx0 = new DepositTransaction(0L, unknownOracle, Unsigned256.create(10_000L));
    DepositTransaction tx1 = new DepositTransaction(1L, unknownOracle, Unsigned256.ONE);
    DepositTransaction tx1Fraud =
        new DepositTransaction(1L, unknownOracle, Unsigned256.create(10_000_000L));
    DepositTransaction tx1OriginalClaimedFraud =
        new DepositTransaction(1L, unknownOracle, Unsigned256.create(1_000_000L));
    DepositTransaction tx2 = new DepositTransaction(2L, identity, Unsigned256.create(20_000L));
    DepositTransaction tx3 =
        new DepositTransaction(3L, oracleAccounts.get(1), Unsigned256.create(200_000L));

    new DisputeBuilder()
        .setDeposits(
            List.of(tx0, tx1, tx2, tx3),
            List.of(tx0, tx1Fraud, tx2),
            List.of(tx1OriginalClaimedFraud))
        .build();
    ByocIncomingContractState state =
        (ByocIncomingContractState) contractStateHandler.contractStates.get(depositContract);
    state = ByocHelper.addCounterClaim(state, tx1);
    contractStateHandler.contractStates.put(depositContract, state);

    runProcess();

    assertVotedOnDispute(depositContract, 1L, 1);
  }

  @Test
  void addCounterClaimToDeposit() {
    setupProcess(1);

    DepositTransaction tx0 = new DepositTransaction(0L, unknownOracle, Unsigned256.create(1_000L));
    DepositTransaction tx1 = new DepositTransaction(1L, unknownOracle, Unsigned256.create(50L));
    DepositTransaction tx1Fraud =
        new DepositTransaction(1L, unknownOracle, Unsigned256.create(50_000_000L));
    DepositTransaction tx1OriginalClaimedFraud =
        new DepositTransaction(1L, unknownOracle, Unsigned256.create(5L));
    DepositTransaction tx2 = new DepositTransaction(2L, identity, Unsigned256.create(10_000L));
    DepositTransaction tx3 =
        new DepositTransaction(3L, oracleAccounts.get(1), Unsigned256.create(4_000L));

    new DisputeBuilder()
        .setDeposits(
            List.of(tx0, tx1, tx2, tx3),
            List.of(tx0, tx1Fraud, tx2),
            List.of(tx1OriginalClaimedFraud))
        .build();

    runProcess();

    SafeDataInputStream stream = assertCounterClaim(depositContract, 1L);
    SafeDataInputStream counterClaim =
        SafeDataInputStream.createFromBytes(stream.readDynamicBytes());
    assertThat(counterClaim.readLong()).isEqualTo(1L); // nonce
    assertThat(BlockchainAddress.read(counterClaim)).isEqualTo(unknownOracle);
    assertThat(Unsigned256.read(counterClaim)).isEqualTo(Unsigned256.create(50L));
  }

  @Test
  void voteNoDepositFraudFound() {
    setupProcess(1);

    DepositTransaction tx0 = new DepositTransaction(0L, unknownOracle, Unsigned256.create(10_000L));
    DepositTransaction tx1 = new DepositTransaction(1L, unknownOracle, Unsigned256.ONE);
    DepositTransaction tx1Fraud =
        new DepositTransaction(1L, unknownOracle, Unsigned256.create(1_000_000L));
    DepositTransaction tx2 = new DepositTransaction(2L, identity, Unsigned256.create(20_000L));

    new DisputeBuilder()
        .setDeposits(List.of(tx0, tx1, tx2), List.of(tx0, tx1, tx2), List.of(tx1Fraud))
        .build();

    runProcess();

    assertVotedOnDispute(depositContract, 1L, -1);
  }

  @Test
  void voteOnFraudulentWithdrawal() {
    setupProcess(1);
    // Stake tokens to test that dispute it not created twice
    addStakedTokens(2_500_0000L, null);

    WithdrawalTransaction tx0 =
        new WithdrawalTransaction(0L, ethAccount0, Unsigned256.ONE, 0L, bitmask);
    WithdrawalTransaction tx1 =
        new WithdrawalTransaction(1L, ethAccount1, Unsigned256.ONE, 0L, bitmask);
    WithdrawalTransaction tx2 =
        new WithdrawalTransaction(2L, ethAccount0, Unsigned256.create(100_000L), 0L, bitmask);
    WithdrawalTransaction tx2Fraud =
        new WithdrawalTransaction(2L, ethAccount1, Unsigned256.create(500L), 0L, bitmask);

    new DisputeBuilder()
        .setWithdrawals(List.of(tx0, tx1, tx2Fraud), List.of(tx0, tx1, tx2), List.of(tx2Fraud))
        .build();

    runProcess();

    assertVotedOnDispute(withdrawalContract, 2L, 0);
  }

  @Test
  void addCounterClaimToWithdrawal() {
    setupProcess(1);

    WithdrawalTransaction tx0 =
        new WithdrawalTransaction(0L, ethAccount0, Unsigned256.ONE, 0L, bitmask);
    WithdrawalTransaction tx1 =
        new WithdrawalTransaction(1L, ethAccount1, Unsigned256.ONE, 0L, bitmask);
    WithdrawalTransaction tx1Fraud =
        new WithdrawalTransaction(1L, ethAccount1, Unsigned256.create(500L), 0L, bitmask);
    WithdrawalTransaction tx1OriginalClaim =
        new WithdrawalTransaction(1L, ethAccount1, Unsigned256.create(501L), 0L, bitmask);

    new DisputeBuilder()
        .setWithdrawals(List.of(tx0, tx1Fraud), List.of(tx0, tx1), List.of(tx1OriginalClaim))
        .build();

    runProcess();

    SafeDataInputStream stream = assertCounterClaim(withdrawalContract, 1L);
    SafeDataInputStream counterClaim =
        SafeDataInputStream.createFromBytes(stream.readDynamicBytes());
    assertThat(counterClaim.readLong()).isEqualTo(0L); // oracle nonce
    assertThat(counterClaim.readLong()).isEqualTo(1L); // withdrawal nonce
    assertThat(OutgoingHelper.readEthereumAddress(counterClaim)).isEqualTo(ethAccount1);
    assertThat(Unsigned256.read(counterClaim)).isEqualTo(Unsigned256.create(500L));
  }

  @Test
  void addCounterClaimIfBitMaskDoesNotMatch() {
    setupProcess(1);

    WithdrawalTransaction tx0 =
        new WithdrawalTransaction(0L, ethAccount1, Unsigned256.ONE, 0L, bitmask);
    WithdrawalTransaction tx0Fraud =
        new WithdrawalTransaction(0L, ethAccount1, Unsigned256.create(500L), 0L, 0b101);
    WithdrawalTransaction tx0OriginalClaim =
        new WithdrawalTransaction(0L, ethAccount1, Unsigned256.create(501L), 0L, 0b111);

    new DisputeBuilder()
        .setWithdrawals(List.of(tx0Fraud), List.of(tx0), List.of(tx0OriginalClaim))
        .build();

    runProcess();

    SafeDataInputStream stream = assertCounterClaim(withdrawalContract, 0L);
    SafeDataInputStream counterClaim =
        SafeDataInputStream.createFromBytes(stream.readDynamicBytes());
    assertThat(counterClaim.readLong()).isEqualTo(0L); // oracle nonce
    assertThat(counterClaim.readLong()).isEqualTo(0L); // withdrawal
    assertThat(OutgoingHelper.readEthereumAddress(counterClaim)).isEqualTo(ethAccount1);
    assertThat(Unsigned256.read(counterClaim)).isEqualTo(Unsigned256.create(500L));
    assertThat(counterClaim.readInt()).isEqualTo(0b101);
  }

  @Test
  void voteOnWithdrawalDisputeCounterClaim() {
    setupProcess(1);

    WithdrawalTransaction tx0 =
        new WithdrawalTransaction(0L, ethAccount0, Unsigned256.ONE, 0L, bitmask);
    WithdrawalTransaction tx1 =
        new WithdrawalTransaction(1L, ethAccount1, Unsigned256.ONE, 0L, bitmask);
    WithdrawalTransaction tx1Fraud =
        new WithdrawalTransaction(1L, ethAccount1, Unsigned256.create(500L), 0L, bitmask);
    WithdrawalTransaction tx1OriginalClaim =
        new WithdrawalTransaction(1L, ethAccount1, Unsigned256.create(501L), 0L, bitmask);
    WithdrawalTransaction tx2 =
        new WithdrawalTransaction(2L, ethAccount0, Unsigned256.create(100_000L), 0L, bitmask);

    new DisputeBuilder()
        .setWithdrawals(
            List.of(tx0, tx1Fraud, tx2), List.of(tx0, tx1, tx2), List.of(tx1OriginalClaim))
        .build();
    ByocOutgoingContractState state =
        (ByocOutgoingContractState) contractStateHandler.contractStates.get(withdrawalContract);
    state = OutgoingHelper.addCounterClaim(state, tx1Fraud);
    contractStateHandler.contractStates.put(withdrawalContract, state);

    runProcess();

    assertVotedOnDispute(withdrawalContract, 1L, 1);
  }

  @Test
  void voteNoWithdrawalFraudFound() {
    setupProcess(1);

    WithdrawalTransaction tx0 =
        new WithdrawalTransaction(0L, ethAccount0, Unsigned256.ONE, 0L, bitmask);
    WithdrawalTransaction tx0Fraud =
        new WithdrawalTransaction(0L, ethAccount1, Unsigned256.create(501L), 0L, bitmask);
    WithdrawalTransaction tx1 =
        new WithdrawalTransaction(1L, ethAccount1, Unsigned256.ONE, 0L, bitmask);

    new DisputeBuilder()
        .setWithdrawals(List.of(tx0, tx1), List.of(tx0, tx1), List.of(tx0Fraud))
        .build();

    runProcess();

    assertVotedOnDispute(withdrawalContract, 0L, -1);
  }

  @Test
  void noVoteCastIfWithdrawalCannotBeFoundOnEthereum() {
    setupProcess(1);

    final WithdrawalTransaction tx =
        new WithdrawalTransaction(0L, ethAccount0, Unsigned256.ONE, 0L, bitmask);
    final WithdrawalTransaction claimedFraudTx =
        new WithdrawalTransaction(0L, ethAccount1, Unsigned256.create(501L), 1L, bitmask);

    ByocOutgoingContractState state =
        (ByocOutgoingContractState) contractStateHandler.contractStates.get(withdrawalContract);
    long epoch = state.currentEpoch();
    state = OutgoingHelper.endCurrentEpoch(state);
    assertThat(epoch + 1).isEqualTo(state.currentEpoch());
    state = OutgoingHelper.startNewEpoch(state, oracleMembers);
    contractStateHandler.contractStates.put(withdrawalContract, state);

    new DisputeBuilder().setWithdrawals(List.of(tx), List.of(tx), List.of(claimedFraudTx)).build();

    runProcess();

    assertThat(largeOracleClient.getTransactionsCount()).isEqualTo(0);
  }

  @Test
  void doNotStartDisputeWhenNotFoundOnEthereum() {
    setupProcess(1);

    addStakedTokens(2_500_0000L, null);

    WithdrawalTransaction tx =
        new WithdrawalTransaction(0L, ethAccount0, Unsigned256.ONE, 0L, bitmask);

    new DisputeBuilder().setWithdrawals(List.of(), List.of(tx), List.of()).build();

    runProcess();

    assertThat(disputeProcess.getNextWithdrawalNonceForOracle(0)).isNull();
    assertThat(largeOracleClient.getTransactionsCount()).isEqualTo(0);
  }

  @Test
  void voteOnWithdrawalDisputeWithNoCorrespondingStateOnPbc() {
    setupProcess(1);

    WithdrawalTransaction txFraud =
        new WithdrawalTransaction(0L, ethAccount1, Unsigned256.create(501L), 0L, bitmask);

    new DisputeBuilder().setWithdrawals(List.of(txFraud), List.of(), List.of(txFraud)).build();

    runProcess();

    assertVotedOnDispute(withdrawalContract, 0L, 0);
  }

  @Test
  void voteOnMultipleDisputes() {
    setupProcess(4);

    DepositTransaction depTx0 = new DepositTransaction(0L, unknownOracle, Unsigned256.create(500));
    DepositTransaction depTx1 = new DepositTransaction(1L, identity, Unsigned256.create(5_000));
    DepositTransaction depTx1Fraud =
        new DepositTransaction(1L, identity, Unsigned256.create(50_000));
    DepositTransaction depTx2 =
        new DepositTransaction(2L, oracleAccounts.get(1), Unsigned256.create(100));
    DepositTransaction depTx2Fraud =
        new DepositTransaction(2L, unknownOracle, Unsigned256.create(100));
    DepositTransaction depTx3 =
        new DepositTransaction(3L, unknownOracle, Unsigned256.create(150_000));

    WithdrawalTransaction withdrawTx0 =
        new WithdrawalTransaction(0L, ethAccount0, Unsigned256.create(500), 0L, bitmask);
    WithdrawalTransaction withdrawTx0Fraud =
        new WithdrawalTransaction(0L, ethAccount0, Unsigned256.create(5_000_000), 0L, bitmask);
    WithdrawalTransaction withdrawTx1 =
        new WithdrawalTransaction(1L, ethAccount0, Unsigned256.create(1000), 0L, bitmask);
    WithdrawalTransaction withdrawTx2 =
        new WithdrawalTransaction(2L, ethAccount1, Unsigned256.create(250), 0L, bitmask);
    WithdrawalTransaction withdrawTx3 =
        new WithdrawalTransaction(3L, ethAccount0, Unsigned256.create(20_000), 0L, bitmask);
    WithdrawalTransaction withdrawTx3Fraud =
        new WithdrawalTransaction(3L, ethAccount0, Unsigned256.create(25_000), 0L, bitmask);

    new DisputeBuilder()
        .setDeposits(
            List.of(depTx0, depTx1, depTx2, depTx3),
            List.of(depTx0, depTx1Fraud, depTx2Fraud),
            List.of(depTx1, depTx2))
        .setWithdrawals(
            List.of(withdrawTx0Fraud, withdrawTx1, withdrawTx2, withdrawTx3Fraud),
            List.of(withdrawTx0, withdrawTx1, withdrawTx2, withdrawTx3),
            List.of(withdrawTx0Fraud, withdrawTx3Fraud))
        .build();

    runProcess();

    assertThat(largeOracleClient.getTransactionsCount()).isEqualTo(4);
    assertThat(depositClient.getTransactionsCount()).isEqualTo(0);
    assertThat(withdrawalClient.getTransactionsCount()).isEqualTo(0);

    List<SignedTransaction> transactions = largeOracleClient.getTransactions();
    assertVoteTransactionSent(transactions.get(0), depositContract, 0, 1);
    assertVoteTransactionSent(transactions.get(1), depositContract, 0, 2);
    assertVoteTransactionSent(transactions.get(2), withdrawalContract, 0, 0);
    assertVoteTransactionSent(transactions.get(3), withdrawalContract, 0, 3);
  }

  private void assertVoteTransactionSent(
      SignedTransaction transaction,
      BlockchainAddress depositContract,
      long oracleNonce,
      int nonce) {
    assertThat(transaction.getInner().getRpc())
        .isEqualTo(
            SafeDataOutputStream.serialize(
                data -> {
                  data.writeByte(2);
                  depositContract.write(data);
                  data.writeLong(oracleNonce);
                  data.writeLong(nonce);
                  data.writeInt(0);
                }));
  }

  @Test
  void reportDiscrepancyInDepositTransactions() {
    setupProcess(1);
    addStakedTokens(2_500_0000L, null);

    DepositTransaction tx = new DepositTransaction(0L, unknownOracle, Unsigned256.ONE);
    DepositTransaction txFraud =
        new DepositTransaction(0L, unknownOracle, Unsigned256.create(1_000_000L));

    new DisputeBuilder().setDeposits(List.of(tx), List.of(txFraud), List.of()).build();

    runProcess();

    assertCreateDepositDispute(tx);
  }

  @Test
  void depositNotReportedIfAlreadyDisputed() {
    setupProcess(1);
    addStakedTokens(2_500_0000L, null);

    DepositTransaction tx = new DepositTransaction(0L, unknownOracle, Unsigned256.ONE);
    DepositTransaction txFraud =
        new DepositTransaction(0L, unknownOracle, Unsigned256.create(1_000_000L));

    new DisputeBuilder().setDeposits(List.of(tx), List.of(txFraud), List.of(tx)).build();

    LargeOracleContractState byocState =
        (LargeOracleContractState) contractStateHandler.contractStates.get(largeOracleContract);
    OracleDisputeId disputeId = new OracleDisputeId(depositContract, 0, 0);
    Dispute dispute =
        LargeOracleContractHelper.addVote(byocState.activeDisputes().getValue(disputeId), 0, 0);
    dispute = LargeOracleContractHelper.addVote(dispute, 1, 0);
    byocState = LargeOracleContractHelper.setDispute(byocState, disputeId, dispute);
    contractStateHandler.contractStates.put(largeOracleContract, byocState);

    runProcess();

    assertThat(largeOracleClient.getTransactionsCount()).isEqualTo(0);
  }

  @Test
  void throwExceptionIfNotEthCounterpartFoundForPbcDeposit() {
    setupProcess(1);
    addStakedTokens(2_500_0000L, null);
    DepositTransaction txFraud =
        new DepositTransaction(0L, unknownOracle, Unsigned256.create(1_000_000L));

    new DisputeBuilder().setDeposits(List.of(), List.of(txFraud), List.of()).build();

    ethereumClient.run();
    assertThatThrownBy(() -> disputeProcess.run())
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Ethereum deposit transaction with nonce 0 does not exist!");
  }

  @Test
  void reportDiscrepancyInWithdrawalTransaction() {
    setupProcess(1);
    addStakedTokens(2_500_0000L, null);

    WithdrawalTransaction tx =
        new WithdrawalTransaction(0L, ethAccount0, Unsigned256.ONE, 0L, bitmask);
    WithdrawalTransaction txFraud =
        new WithdrawalTransaction(0L, ethAccount0, Unsigned256.create(100L), 0L, bitmask);

    new DisputeBuilder().setWithdrawals(List.of(txFraud), List.of(tx), List.of()).build();

    runProcess();

    assertCreateWithdrawalDispute(txFraud);
  }

  @Test
  void reportOnlyWithdrawalsThatThereIsSufficientTokensFor() {
    setupProcess(1);
    addStakedTokens(2_500_0000L * 2L - 1L, null);

    WithdrawalTransaction tx0 =
        new WithdrawalTransaction(0L, ethAccount0, Unsigned256.ONE, 0L, bitmask);
    WithdrawalTransaction tx0Fraud =
        new WithdrawalTransaction(0L, ethAccount0, Unsigned256.create(100L), 0L, bitmask);
    WithdrawalTransaction tx1 =
        new WithdrawalTransaction(1L, ethAccount0, Unsigned256.ONE, 0L, bitmask);
    WithdrawalTransaction tx1Fraud =
        new WithdrawalTransaction(1L, ethAccount0, Unsigned256.create(100L), 0L, bitmask);

    new DisputeBuilder()
        .setWithdrawals(List.of(tx0Fraud, tx1Fraud), List.of(tx0, tx1), List.of())
        .build();

    runProcess();

    assertThat(disputeProcess.getNextWithdrawalNonceForOracle(0)).isEqualTo(1);
    assertCreateWithdrawalDispute(tx0Fraud);
  }

  @Test
  void withdrawalTransactionMadeOutOfOrderIsNotAudited() {
    setupProcess(0);
    addStakedTokens(2_500_0000L, null);

    WithdrawalTransaction actualTx =
        new WithdrawalTransaction(0L, ethAccount0, Unsigned256.create(101L), 0L, 0b000);
    WithdrawalTransaction outOfOrderTx =
        new WithdrawalTransaction(0L, ethAccount0, Unsigned256.create(100L), 0L, 0b000);

    new DisputeBuilder()
        .setWithdrawals(List.of(outOfOrderTx), List.of(actualTx), List.of())
        .build();

    runProcess();

    assertThat(disputeProcess.getNextWithdrawalNonceForOracle(0)).isEqualTo(1);
    assertThat(largeOracleClient.getTransactionsCount()).isEqualTo(0);
  }

  @Test
  void withdrawalsFromOldOraclesAreNotAudited() {
    setupProcess(1);
    addStakedTokens(2_500_0000L, null);

    WithdrawalTransaction tx0 =
        new WithdrawalTransaction(0, ethAccount0, Unsigned256.ONE, 0L, bitmask);
    WithdrawalTransaction tx1 =
        new WithdrawalTransaction(0, ethAccount0, Unsigned256.ONE, 1L, bitmask);
    WithdrawalTransaction tx2 =
        new WithdrawalTransaction(0, ethAccount0, Unsigned256.ONE, 2L, bitmask);
    WithdrawalTransaction tx3 =
        new WithdrawalTransaction(1, ethAccount0, Unsigned256.ONE, 1L, bitmask);

    new DisputeBuilder()
        .setWithdrawals(List.of(tx0, tx1, tx2, tx3), List.of(tx0, tx1, tx2, tx3), List.of())
        .build();

    // Start new epochs
    new DisputeBuilder().setWithdrawals(List.of(tx0), List.of(tx0), List.of()).build();
    ByocOutgoingContractState state =
        (ByocOutgoingContractState) contractStateHandler.contractStates.get(withdrawalContract);
    state = OutgoingHelper.endCurrentEpoch(state);
    state = OutgoingHelper.startNewEpoch(state, oracleMembers);
    contractStateHandler.contractStates.put(withdrawalContract, state);
    new DisputeBuilder().setWithdrawals(List.of(tx1, tx3), List.of(tx1, tx3), List.of()).build();
    state = (ByocOutgoingContractState) contractStateHandler.contractStates.get(withdrawalContract);
    state = OutgoingHelper.endCurrentEpoch(state);
    state = OutgoingHelper.startNewEpoch(state, oracleMembers);
    contractStateHandler.contractStates.put(withdrawalContract, state);
    new DisputeBuilder()
        .setWithdrawals(List.of(tx0, tx1, tx2, tx3), List.of(tx2), List.of(tx3))
        .build();

    runProcess();
    assertThat(disputeProcess.getNextWithdrawalNonceForOracle(0)).isNull();
    assertThat(disputeProcess.getNextWithdrawalNonceForOracle(1)).isEqualTo(0);
    assertThat(disputeProcess.getNextWithdrawalNonceForOracle(2)).isEqualTo(1);
  }

  @Test
  void subsequentTransactionAuditsIncreasesLastSeenDepositNonce() {
    setupProcess(1);
    addStakedTokens(2_500_0000L, null);

    DepositTransaction tx0 = new DepositTransaction(0L, unknownOracle, Unsigned256.ONE);
    DepositTransaction tx1 =
        new DepositTransaction(1L, oracleAccounts.get(1), Unsigned256.create(100));
    DepositTransaction tx2 = new DepositTransaction(2L, unknownOracle, Unsigned256.ONE);

    new DisputeBuilder().setDeposits(List.of(tx0, tx1, tx2), List.of(tx0, tx1), List.of()).build();

    final Client.Listener depositListener = listeners.get(depositContract);
    final Client.Listener withdrawalListener = listeners.get(withdrawalContract);

    runProcess();

    assertThat(disputeProcess.getNextDepositNonce()).isEqualTo(2L);

    Mockito.when(web3jMock.ethBlockNumber()).thenReturn(new EthBlockNumberRequestStub<>(55L));
    DepositTransaction tx3 =
        new DepositTransaction(3L, oracleAccounts.get(0), Unsigned256.create(100));
    DepositTransaction tx3Fraud =
        new DepositTransaction(3L, oracleAccounts.get(0), Unsigned256.create(100_000));
    DepositTransaction tx4 = new DepositTransaction(4L, unknownOracle, Unsigned256.ONE);

    new DisputeBuilder()
        .setBlockRange(
            "0x" + BigInteger.TEN.toString(16), "0x" + BigInteger.valueOf(45).toString(16))
        .setDeposits(List.of(tx2, tx3, tx4), List.of(tx2, tx3Fraud, tx4), List.of())
        .build();

    withdrawalListener.update(contractStateHandler.contractStates.get(withdrawalContract));
    depositListener.update(contractStateHandler.contractStates.get(depositContract));

    Ethereum.BlockRange range =
        new Ethereum.BlockRange(BigInteger.TEN, BigInteger.valueOf(45), false);
    blockPagination.setNextRange(range);
    runProcess();

    // The next nonce to check is 4 since the account did not have enough stakes to continue.
    assertThat(disputeProcess.getNextDepositNonce()).isEqualTo(4L);

    assertCreateDepositDispute(tx3);
  }

  @Test
  void largeOracleContractOracleMembers() {
    assertThat(largeOracleClient.getDummyContractState().oracleMembers())
        .containsExactlyElementsOf(loOracleMembers);
  }

  @Test
  void signaturesRegistered() {
    Deposit deposit = new Deposit(identity, Unsigned256.create(100), 1);
    assertThat(deposit.signaturesRegistered()).isEqualTo(1);
  }

  @Test
  void depositNonceStartsWithTheFromNonceOfTheLastEpoch() {
    setupProcess(1);

    ByocIncomingContractState depositState =
        (ByocIncomingContractState) contractStateHandler.contractStates.get(depositContract);
    LargeOracleContractState largeOracleState =
        (LargeOracleContractState) contractStateHandler.contractStates.get(largeOracleContract);
    AvlTree<BlockchainAddress, StakedTokens> stakedTokens = largeOracleState.stakedTokens();
    addStakedTokens(2_500_0000L, null);
    LargeOracleContractState newState =
        (LargeOracleContractState) contractStateHandler.contractStates.get(largeOracleContract);
    assertThat(stakedTokens.size() + 1).isEqualTo(newState.stakedTokens().size());

    for (int i = 0; i < 4; i++) {
      Deposit deposit = new Deposit(identity, Unsigned256.create(100), 1);
      depositState = ByocHelper.setDeposit(depositState, deposit);
      depositState = ByocHelper.clearDeposit(depositState);
    }
    long oracleNonce = depositState.oracleNonce();
    depositState = ByocHelper.endEpoch(depositState);
    assertThat(oracleNonce + 1).isEqualTo(depositState.oracleNonce());
    depositState = ByocHelper.startEpoch(depositState, oracleAccounts);

    List<DepositTransaction> transactions = new ArrayList<>();
    for (int i = 4; i < 10; i++) {
      Deposit deposit = new Deposit(identity, Unsigned256.create(100), 0);
      depositState = ByocHelper.setDeposit(depositState, deposit);
      depositState = ByocHelper.clearDeposit(depositState);
      DepositTransaction ethTx = new DepositTransaction(i, identity, Unsigned256.create(100));
      transactions.add(ethTx);
    }
    depositState = ByocHelper.endEpoch(depositState);
    depositState = ByocHelper.startEpoch(depositState, oracleAccounts);
    contractStateHandler.contractStates.put(depositContract, depositState);

    DepositTransaction tx10 = new DepositTransaction(10L, unknownOracle, Unsigned256.ONE);
    DepositTransaction tx11 =
        new DepositTransaction(11L, oracleAccounts.get(1), Unsigned256.create(100));
    DepositTransaction tx12 = new DepositTransaction(12L, unknownOracle, Unsigned256.ONE);
    transactions.addAll(List.of(tx10, tx11, tx12));

    new DisputeBuilder().setDeposits(transactions, List.of(tx10, tx11), List.of()).build();

    runProcess();

    assertThat(disputeProcess.getNextDepositNonce()).isEqualTo(12L);
  }

  /**
   * With two fraudulent deposits (and one honest) and a single fraudulent withdrawal where an
   * account only has enough staked tokens to start two disputes, only the first two disputes will
   * be started.
   */
  @Test
  void reportOnMultipleDiscrepancies() {
    setupProcess(3);
    addStakedTokens(2_500_0000L * 3 - 1, null); // Only enough stakes for the first two

    DepositTransaction depositTx0 = new DepositTransaction(0L, unknownOracle, Unsigned256.ONE);
    DepositTransaction depositTx0Fraud =
        new DepositTransaction(0L, unknownOracle, Unsigned256.create(1_000_000L));
    DepositTransaction depositTx1 = new DepositTransaction(1L, unknownOracle, Unsigned256.ONE);
    DepositTransaction depositTx2 = new DepositTransaction(2L, unknownOracle, Unsigned256.ONE);
    DepositTransaction depositTx2Fraud =
        new DepositTransaction(2L, unknownOracle, Unsigned256.create(1_000_000L));

    WithdrawalTransaction withdrawalTx =
        new WithdrawalTransaction(0L, ethAccount0, Unsigned256.ONE, 0L, bitmask);
    WithdrawalTransaction withdrawalTxFraud =
        new WithdrawalTransaction(0L, ethAccount0, Unsigned256.create(100L), 0L, bitmask);

    new DisputeBuilder()
        .setDeposits(
            List.of(depositTx0, depositTx1, depositTx2),
            List.of(depositTx0Fraud, depositTx1, depositTx2Fraud),
            List.of())
        .setWithdrawals(List.of(withdrawalTxFraud), List.of(withdrawalTx), List.of())
        .build();

    runProcess();

    assertThat(largeOracleClient.getTransactionsCount()).isEqualTo(2);
  }

  /**
   * A dispute is never created if the account has staked an insufficient amount of tokens to start
   * the dispute.
   */
  @Test
  void noDisputeIfInsufficientTokensAreStaked() {
    setupProcess(1);
    addStakedTokens(2_500_0000L - 1L, null);

    DepositTransaction depositTx = new DepositTransaction(0L, unknownOracle, Unsigned256.ONE);
    DepositTransaction depositTxFraud =
        new DepositTransaction(0L, unknownOracle, Unsigned256.create(1_000_000L));

    new DisputeBuilder()
        .setDeposits(List.of(depositTx), List.of(depositTxFraud), List.of())
        .build();

    runProcess();

    assertThat(largeOracleClient.getTransactionsCount()).isEqualTo(0);
  }

  /**
   * A dispute is never created if the account has reserved tokens such that an insufficient amount
   * of staked tokens are available for the dispute.
   */
  @Test
  void noDisputeIfInsufficientTokensBecauseOfReserved() {
    setupProcess(1);
    addStakedTokens(2_500_0000L, null);
    reserveTokens(1L);

    DepositTransaction depositTx = new DepositTransaction(0L, unknownOracle, Unsigned256.ONE);
    DepositTransaction depositTxFraud =
        new DepositTransaction(0L, unknownOracle, Unsigned256.create(1_000_000L));

    new DisputeBuilder()
        .setDeposits(List.of(depositTx), List.of(depositTxFraud), List.of())
        .build();

    runProcess();

    assertThat(largeOracleClient.getTransactionsCount()).isEqualTo(0);
  }

  @Test
  @DisplayName(
      "Trying to create a dispute after the expiration timestamp, fails to create the dispute.")
  void noDisputeIfTokensHasExpiredOfReserved() {
    setupProcess(1);
    addStakedTokens(2_500_0000L, 0L);

    DepositTransaction depositTx = new DepositTransaction(0L, unknownOracle, Unsigned256.ONE);
    DepositTransaction depositTxFraud =
        new DepositTransaction(0L, unknownOracle, Unsigned256.create(1_000_000L));

    new DisputeBuilder()
        .setDeposits(List.of(depositTx), List.of(depositTxFraud), List.of())
        .build();

    runProcess();

    assertThat(largeOracleClient.getTransactionsCount()).isEqualTo(0);
  }

  @Test
  @DisplayName(
      "Trying to create a dispute before the expiration timestamp, successfully creates a dispute.")
  void creatingDisputeBeforeExpirationSucceeds() {
    long systemTime = 100L;
    largeOracleClient.addDefaultExecutedTransactions(loShard);

    createListeners();
    disputeProcess =
        new DisputeProcess(
            ethereumClient,
            identity,
            senderKey,
            largeOracleContract,
            depositContract,
            withdrawalContract,
            votingCost,
            contractStateHandler,
            () -> systemTime);
    addStakedTokens(2_500_0000L, systemTime + 1);

    DepositTransaction depositTx = new DepositTransaction(0L, unknownOracle, Unsigned256.ONE);
    DepositTransaction depositTxFraud =
        new DepositTransaction(0L, unknownOracle, Unsigned256.create(1_000_000L));

    new DisputeBuilder()
        .setDeposits(List.of(depositTx), List.of(depositTxFraud), List.of())
        .build();

    runProcess();

    assertThat(largeOracleClient.getTransactionsCount()).isEqualTo(1);
  }

  @Test
  @DisplayName(
      "Trying to create a dispute on exactly the expiration timestamp, fails to create the"
          + " dispute.")
  void noDisputeIfTokensHasExpirationAtTheSameTimeOfSystemTime() {
    long expirationTimestamp = 100L;
    largeOracleClient.addDefaultExecutedTransactions(loShard);

    createListeners();
    disputeProcess =
        new DisputeProcess(
            ethereumClient,
            identity,
            senderKey,
            largeOracleContract,
            depositContract,
            withdrawalContract,
            votingCost,
            contractStateHandler,
            () -> expirationTimestamp);
    addStakedTokens(2_500_0000L, expirationTimestamp);

    DepositTransaction depositTx = new DepositTransaction(0L, unknownOracle, Unsigned256.ONE);
    DepositTransaction depositTxFraud =
        new DepositTransaction(0L, unknownOracle, Unsigned256.create(1_000_000L));

    new DisputeBuilder()
        .setDeposits(List.of(depositTx), List.of(depositTxFraud), List.of())
        .build();

    runProcess();

    assertThat(largeOracleClient.getTransactionsCount()).isEqualTo(0);
  }

  @Test
  void processDoesNotRunWithSomeContractsMissing() {
    setupProcess(0);
    for (Client.Listener listener : listeners.values()) {
      listener.remove();
    }
    contractStateHandler.createContract(
        null, null, largeOracleContract, null, largeOracleClient.getDummyContractState());
    assertThat(disputeProcess.run()).isFalse();
    contractStateHandler.createContract(
        null, null, depositContract, null, depositClient.getDummyContractState());

    assertThat(disputeProcess.run()).isFalse();
  }

  private void runProcess() {
    ethereumClient.run();
    assertThat(disputeProcess.run()).isFalse();
  }

  private void assertCreateWithdrawalDispute(WithdrawalTransaction transaction) {
    assertThat(largeOracleClient.getTransactionsCount()).isEqualTo(1);
    Transaction innerTransaction = largeOracleClient.getLatestPut().getInner();
    assertThat(innerTransaction.getAddress()).isEqualTo(withdrawalContract);
    byte[] data = innerTransaction.getRpc();
    assertThat(data)
        .isEqualTo(
            SafeDataOutputStream.serialize(
                s -> {
                  s.writeByte(2); // CREATE_DISPUTE_WITHDRAWAL
                  s.writeLong(transaction.oracleNonce());
                  s.writeLong(transaction.withdrawalNonce());
                  OutgoingHelper.ethereumAddress(transaction.destination()).write(s);
                  transaction.amount().write(s);
                  s.writeInt(transaction.bitmask());
                }));
  }

  private void assertCreateDepositDispute(DepositTransaction transaction) {
    assertThat(largeOracleClient.getTransactionsCount()).isEqualTo(1);
    Transaction innerTransaction = largeOracleClient.getLatestPut().getInner();
    assertThat(innerTransaction.getAddress()).isEqualTo(depositContract);
    byte[] data = innerTransaction.getRpc();
    assertThat(data)
        .isEqualTo(
            SafeDataOutputStream.serialize(
                s -> {
                  s.writeByte(1); // CREATE_DISPUTE_DEPOSIT
                  s.writeLong(transaction.nonce());
                  transaction.receiver().write(s);
                  transaction.amount().write(s);
                }));
  }

  private void setupProcess(int expectedTransactions) {
    for (int i = 0; i < expectedTransactions; i++) {
      largeOracleClient.addDefaultExecutedTransactions(loShard);
    }

    createListeners();
    disputeProcess =
        new DisputeProcess(
            ethereumClient,
            identity,
            senderKey,
            largeOracleContract,
            depositContract,
            withdrawalContract,
            votingCost,
            contractStateHandler,
            new SystemTimeProviderImpl());
  }

  private void assertVotedOnDispute(BlockchainAddress contract, long transactionNonce, int vote) {
    assertCallToLoContract(contract, 2, transactionNonce, Optional.of(vote));
  }

  private SafeDataInputStream assertCounterClaim(BlockchainAddress contract, long nonce) {
    return assertCallToLoContract(contract, 1, nonce, Optional.empty());
  }

  private SafeDataInputStream assertCallToLoContract(
      BlockchainAddress contract, int invocation, long transactionNonce, Optional<Integer> vote) {
    // Assert that interactions only happens through the large oracle
    assertThat(largeOracleClient.getTransactionsCount()).isEqualTo(1);
    assertThat(depositClient.getTransactionsCount()).isEqualTo(0);
    assertThat(withdrawalClient.getTransactionsCount()).isEqualTo(0);

    SignedTransaction latestPut = largeOracleClient.getLatestPut();
    Transaction inner = latestPut.getInner();
    assertThat(inner.getAddress()).isEqualTo(largeOracleContract);
    byte[] rpc = inner.getRpc();
    SafeDataInputStream stream = SafeDataInputStream.createFromBytes(rpc);
    assertThat(stream.readUnsignedByte()).isEqualTo(invocation);
    assertThat(BlockchainAddress.read(stream)).isEqualTo(contract);
    assertThat(stream.readLong()).isEqualTo(0);
    assertThat(stream.readLong()).isEqualTo(transactionNonce);
    vote.ifPresent(integer -> assertThat(stream.readInt()).isEqualTo(integer));
    return stream;
  }

  private final class DisputeBuilder {
    private String fromBlock;
    private String toBlock;

    private List<DepositTransaction> ethereumDeposits;
    private List<DepositTransaction> pbcDeposits;
    private List<DepositTransaction> disputedDeposits;

    private List<WithdrawalTransaction> ethereumWithdrawals;
    private List<WithdrawalTransaction> pbcWithdrawals;
    private List<WithdrawalTransaction> disputedWithdrawals;

    public DisputeBuilder() {
      fromBlock = DefaultBlockParameter.valueOf(BigInteger.valueOf(DEPLOYED_IN_BLOCK)).getValue();
      toBlock =
          DefaultBlockParameter.valueOf(
                  BigInteger.valueOf(LATEST_BLOCK).subtract(validityThreshold))
              .getValue();

      ethereumDeposits = new ArrayList<>();
      pbcDeposits = new ArrayList<>();
      disputedDeposits = new ArrayList<>();

      ethereumWithdrawals = new ArrayList<>();
      pbcWithdrawals = new ArrayList<>();
      disputedWithdrawals = new ArrayList<>();
    }

    DisputeBuilder setBlockRange(String fromBlock, String toBlock) {
      this.fromBlock = fromBlock;
      this.toBlock = toBlock;
      return this;
    }

    DisputeBuilder setDeposits(
        List<DepositTransaction> ethereumTransactions,
        List<DepositTransaction> pbcTransactions,
        List<DepositTransaction> disputedTransactions) {
      this.ethereumDeposits = ethereumTransactions;
      this.pbcDeposits = pbcTransactions;
      this.disputedDeposits = disputedTransactions;
      return this;
    }

    DisputeBuilder setWithdrawals(
        List<WithdrawalTransaction> ethereumTransactions,
        List<WithdrawalTransaction> pbcTransactions,
        List<WithdrawalTransaction> disputedTransactions) {
      this.ethereumWithdrawals = ethereumTransactions;
      this.pbcWithdrawals = pbcTransactions;
      this.disputedWithdrawals = disputedTransactions;
      return this;
    }

    void build() {
      // Setup web3j to return event logs
      Mockito.when(web3jMock.ethGetLogs(argThat(new EthFilterMatcher(fromBlock, toBlock))))
          .thenReturn(new EthLogRequest<>(ethereumDeposits, ethereumWithdrawals));

      // Setup deposit state
      ByocIncomingContractState depositState =
          (ByocIncomingContractState) contractStateHandler.contractStates.get(depositContract);
      for (DepositTransaction pbcTx : pbcDeposits) {
        Deposit deposit = new Deposit(pbcTx.receiver(), pbcTx.amount(), 0);
        depositState = ByocHelper.setDeposit(depositState, deposit);
        depositState = ByocHelper.clearDeposit(depositState);
      }

      // Setup withdrawal state
      ByocOutgoingContractState withdrawalState =
          (ByocOutgoingContractState) contractStateHandler.contractStates.get(withdrawalContract);
      for (WithdrawalTransaction pbcTx : pbcWithdrawals) {
        EthereumAddress receiver = OutgoingHelper.ethereumAddress(pbcTx.destination());
        // Ignore tax for these tests.
        withdrawalState =
            OutgoingHelper.createPendingWithdrawal(withdrawalState, receiver, pbcTx.amount());
      }

      // Setup disputes
      LargeOracleContractState loState =
          (LargeOracleContractState) contractStateHandler.contractStates.get(largeOracleContract);
      for (DepositTransaction tx : disputedDeposits) {
        depositState = ByocHelper.addDispute(depositState, tx.nonce(), tx.receiver(), tx.amount());

        OracleDisputeId id = new OracleDisputeId(depositContract, 0L, tx.nonce());
        Dispute dispute = new Dispute(FixedList.create(Collections.nCopies(2, null)));
        loState = LargeOracleContractHelper.setDispute(loState, id, dispute);
      }
      for (WithdrawalTransaction pbcTx : disputedWithdrawals) {
        withdrawalState = OutgoingHelper.addDispute(withdrawalState, pbcTx);

        OracleDisputeId id =
            new OracleDisputeId(withdrawalContract, pbcTx.oracleNonce(), pbcTx.withdrawalNonce());
        Dispute loDispute = new Dispute(FixedList.create(Collections.nCopies(2, null)));
        loState = LargeOracleContractHelper.setDispute(loState, id, loDispute);
      }

      // Update state listeners
      listeners.get(depositContract).update(depositState);
      listeners.get(withdrawalContract).update(withdrawalState);
      listeners.get(largeOracleContract).update(loState);
    }
  }

  private void addStakedTokens(long amount, Long expirationTimestamp) {
    LargeOracleContractState state =
        (LargeOracleContractState) contractStateHandler.contractStates.get(largeOracleContract);
    state = LargeOracleContractHelper.stakeTokens(state, identity, amount, expirationTimestamp);
    listeners.get(largeOracleContract).update(state);
  }

  private void reserveTokens(long amount) {
    LargeOracleContractState state =
        (LargeOracleContractState) contractStateHandler.contractStates.get(largeOracleContract);
    state = LargeOracleContractHelper.reserveTokens(state, identity, amount);
    listeners.get(largeOracleContract).update(state);
  }

  private static List<OracleMember> createMembers(List<BlockchainAddress> identities) {
    List<OracleMember> members = new ArrayList<>();
    for (BlockchainAddress address : identities) {
      members.add(new OracleMember(address));
    }
    return members;
  }

  private static List<com.partisiablockchain.demo.byoc.governance.largeoracle.OracleMember>
      createLoOracleMembers(List<BlockchainAddress> identities) {
    List<com.partisiablockchain.demo.byoc.governance.largeoracle.OracleMember> members =
        new ArrayList<>();
    for (BlockchainAddress address : identities) {
      members.add(
          new com.partisiablockchain.demo.byoc.governance.largeoracle.OracleMember(address));
    }
    return members;
  }

  private void createListeners() {
    listeners =
        Map.of(
            depositContract,
            contractStateHandler.createContract(
                null, null, depositContract, null, getDepositDummyState()),
            largeOracleContract,
            contractStateHandler.createContract(
                null, null, largeOracleContract, null, getLargeOracleDummyState()),
            withdrawalContract,
            contractStateHandler.createContract(
                null, null, withdrawalContract, null, getWithdrawalDummyState()));
  }

  private static final class EthFilterMatcher implements ArgumentMatcher<EthFilter> {

    private final String fromBlock;
    private final String toBlock;

    private EthFilterMatcher(String fromBlock, String toBlock) {
      this.fromBlock = fromBlock;
      this.toBlock = toBlock;
    }

    @Override
    public boolean matches(EthFilter ethFilter) {
      if (ethFilter == null) {
        return false;
      }

      if (!(ethFilter.getTopics().get(0) instanceof Filter.ListTopic eventSignatures)) {
        return false;
      }
      List<String> encodedSignatures =
          eventSignatures.getValue().stream().map(Filter.SingleTopic::getValue).toList();

      return encodedSignatures.containsAll(
              List.of(Ethereum.ENCODED_DEPOSIT_EVENT, Ethereum.ENCODED_WITHDRAWAL_EVENT))
          && ethFilter.getFromBlock().getValue().equals(fromBlock)
          && ethFilter.getToBlock().getValue().equals(toBlock);
    }
  }

  private static final class EthLogRequest<S> extends Request<S, EthLog> {

    private final List<DepositTransaction> deposits;
    private final List<WithdrawalTransaction> withdrawals;

    private EthLogRequest(
        List<DepositTransaction> deposits, List<WithdrawalTransaction> withdrawals) {
      this.deposits = deposits;
      this.withdrawals = withdrawals;
    }

    @Override
    @SuppressWarnings("rawtypes")
    public EthLog send() {
      EthLog ethLog = new EthLog();
      List<EthLog.LogResult> logs = new ArrayList<>();
      for (DepositTransaction deposit : deposits) {
        long amount = new BigInteger(deposit.amount().serialize()).longValueExact();
        logs.add(
            EthereumTest.createDepositLog(
                deposit.nonce(), deposit.receiver().writeAsString(), amount));
      }
      for (WithdrawalTransaction withdrawal : withdrawals) {
        logs.add(
            EthereumTest.createWithdrawalLog(
                withdrawal.withdrawalNonce(),
                withdrawal.destination(),
                withdrawal.amount(),
                withdrawal.oracleNonce(),
                withdrawal.bitmask()));
      }
      ethLog.setResult(logs);
      return ethLog;
    }
  }
}
