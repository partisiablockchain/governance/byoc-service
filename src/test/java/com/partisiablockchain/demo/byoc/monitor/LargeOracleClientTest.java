package com.partisiablockchain.demo.byoc.monitor;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.client.web.WebClient;
import com.partisiablockchain.crypto.KeyPair;
import com.partisiablockchain.governance.ThresholdSessionId;
import jakarta.ws.rs.WebApplicationException;
import jakarta.ws.rs.core.Response;
import java.math.BigInteger;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

/** Test. */
public final class LargeOracleClientTest {

  @Test
  void clientThrowsErrorOnUnExpectedException() {
    KeyPair keyPair = new KeyPair(BigInteger.valueOf(123));
    WebClient mock = Mockito.mock(WebClient.class);
    LargeOracleClient client =
        new LargeOracleClient(() -> "endpoint", keyPair.getPublic(), keyPair, keyPair, mock);
    Mockito.when(
            mock.post(
                Mockito.eq("endpoint/threshold/getUnicastMessage/1/2/3"),
                Mockito.any(),
                Mockito.any()))
        .thenThrow(new WebApplicationException(Response.Status.REQUEST_TIMEOUT));
    ThresholdSessionId thresholdSessionId = new ThresholdSessionId(1, 2);
    Assertions.assertThatThrownBy(() -> client.getUnicastMessage(thresholdSessionId, 3))
        .isInstanceOf(WebApplicationException.class);
  }
}
