package com.partisiablockchain.demo.byoc.monitor;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.demo.byoc.governance.byocincoming.DepositTransaction;
import com.partisiablockchain.demo.byoc.rest.dto.DepositDto;
import com.partisiablockchain.demo.byoc.settings.EthereumSettings;
import com.partisiablockchain.demo.byoc.settings.SettingsTest;
import com.partisiablockchain.math.Unsigned256;
import com.secata.tools.coverage.ExceptionConverter;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.TimeUnit;
import okhttp3.OkHttpClient;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.web3j.abi.DefaultFunctionEncoder;
import org.web3j.abi.datatypes.Address;
import org.web3j.abi.datatypes.generated.Bytes21;
import org.web3j.abi.datatypes.generated.Uint256;
import org.web3j.abi.datatypes.generated.Uint32;
import org.web3j.abi.datatypes.generated.Uint64;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.core.DefaultBlockParameter;
import org.web3j.protocol.core.DefaultBlockParameterName;
import org.web3j.protocol.core.Request;
import org.web3j.protocol.core.methods.request.EthFilter;
import org.web3j.protocol.core.methods.request.Filter;
import org.web3j.protocol.core.methods.response.EthBlock;
import org.web3j.protocol.core.methods.response.EthLog;
import org.web3j.utils.Numeric;

/** Test. */
public final class EthereumTest {

  private static final String pbcAddress = "00339896577caf2fc1cdcb1b144683e1312b791c24";
  private static final String ethContract = "ethereum contract address";

  private static final BigInteger DEPLOYED_IN_BLOCK = BigInteger.valueOf(14_873_415L);
  private static final BigInteger LATEST_BLOCK = BigInteger.valueOf(18_669_571L);

  private final Web3j web3jMock = Mockito.mock(Web3j.class);
  private final BigInteger blockValidityDepth = BigInteger.TEN;

  private final Ethereum.BlockRange initialBlockRange =
      new Ethereum.BlockRange(DEPLOYED_IN_BLOCK, LATEST_BLOCK, false);

  private final BlockPaginationStub blockPagination = new BlockPaginationStub(initialBlockRange);
  private final ContractInformationStub contractInformation =
      new ContractInformationStub(DEPLOYED_IN_BLOCK);
  private final EthereumEventsCache cache = new DefaultEthereumEventsCache();

  private final Ethereum ethereum =
      new Ethereum(web3jMock, ethContract, cache, blockPagination, contractInformation);

  @Test
  public void createEthereumDataSource() {
    EthereumSettings settings =
        SettingsTest.defaultBase(EthereumSettings.builder())
            .setEthereumEndpoint("")
            .setEthereumBlockValidityDepth(blockValidityDepth)
            .setEthereumByocContract(ethContract)
            .setMaxBlockRange(BigInteger.valueOf(100))
            .build();
    Ethereum ethereum = Ethereum.create(settings);
    Assertions.assertThat(ethereum).isNotNull();
  }

  @Test
  public void createHttpClient() {
    OkHttpClient client = Ethereum.createHttpClient();
    Assertions.assertThat(client.connectTimeoutMillis()).isEqualTo(30_000L);
    Assertions.assertThat(client.readTimeoutMillis()).isEqualTo(30_000L);
  }

  @SuppressWarnings("unchecked")
  @Test
  public void waitForDeposit() {
    ArgumentCaptor<EthFilter> capture = ArgumentCaptor.forClass(EthFilter.class);
    Mockito.when(web3jMock.ethGetLogs(capture.capture()))
        .thenReturn(new EthDepositLogRequest<>(List.of(1L)));
    ethereum.run();

    DepositDto deposit = ethereum.waitForDeposit(1L);
    Assertions.assertThat(deposit.pbcAddress).isEqualTo(pbcAddress);
    Assertions.assertThat(deposit.amount).isEqualTo("10000");

    EthFilter filter = capture.getAllValues().get(0);
    Assertions.assertThat(filter.getFromBlock().getValue())
        .isEqualTo(DefaultBlockParameter.valueOf(DEPLOYED_IN_BLOCK).getValue());
    DefaultBlockParameter expectedToBlock = DefaultBlockParameter.valueOf(LATEST_BLOCK);
    Assertions.assertThat(filter.getToBlock().getValue()).isEqualTo(expectedToBlock.getValue());
    Assertions.assertThat(filter.getAddress().size()).isEqualTo(1);
    Assertions.assertThat(filter.getAddress().get(0)).isEqualTo(ethContract);
    Assertions.assertThat(filter.getTopics().size()).isEqualTo(1);
    Assertions.assertThat(filter.getTopics().get(0)).isInstanceOf(Filter.ListTopic.class);
    List<String> actualTopics =
        ((List<Filter.SingleTopic>) filter.getTopics().get(0).getValue())
            .stream().map(Filter.SingleTopic::getValue).toList();
    Assertions.assertThat(actualTopics)
        .contains(Ethereum.ENCODED_DEPOSIT_EVENT, Ethereum.ENCODED_WITHDRAWAL_EVENT);
  }

  @Test
  public void name() {
    Assertions.assertThat(ethereum.name()).isEqualTo("Ethereum");
  }

  @Test
  public void multipleDeposits() {
    ArgumentCaptor<EthFilter> capture = ArgumentCaptor.forClass(EthFilter.class);
    Mockito.when(web3jMock.ethGetLogs(capture.capture()))
        .thenReturn(new EthDepositLogRequest<>(List.of(1L, 2L)));
    Assertions.assertThat(ethereum.run()).isFalse();

    DepositDto deposit = ethereum.waitForDeposit(1L);
    Assertions.assertThat(deposit.pbcAddress).isEqualTo(pbcAddress);
    Assertions.assertThat(deposit.amount).isEqualTo("10000");

    deposit = ethereum.waitForDeposit(2L);
    Assertions.assertThat(deposit.pbcAddress).isEqualTo(pbcAddress);
    Assertions.assertThat(deposit.amount).isEqualTo("20000");
    Assertions.assertThat(cache.loadAllDeposits()).hasSize(2);
  }

  @Test
  public void newLogEntriesAreNotFetchedIfDepositItPresentInCache() {
    DepositTransaction pendingDeposit =
        new DepositTransaction(
            1L, BlockchainAddress.fromString(pbcAddress), Unsigned256.create(1000));
    cache.saveDeposits(List.of(pendingDeposit));

    DepositDto fetchedDeposit = ethereum.waitForDeposit(1L);
    Assertions.assertThat(fetchedDeposit.pbcAddress).isEqualTo(pbcAddress);
    Assertions.assertThat(fetchedDeposit.amount).isEqualTo("1000");

    Mockito.verify(web3jMock, Mockito.never()).ethBlockNumber();
    ArgumentCaptor<EthFilter> capture = ArgumentCaptor.forClass(EthFilter.class);
    Mockito.verify(web3jMock, Mockito.never()).ethGetLogs(capture.capture());
  }

  /**
   * When retrieving a new deposit, the datasource waits for the corresponding deposit log on
   * Ethereum to arrive.
   */
  @Test
  public void waitForDepositWaitsUntilTheRightLogsAreFetched() throws InterruptedException {
    ArgumentCaptor<EthFilter> capture = ArgumentCaptor.forClass(EthFilter.class);
    final List<Long> blockNumbers =
        List.of(
            DEPLOYED_IN_BLOCK.longValue(),
            DEPLOYED_IN_BLOCK.longValue() + 100L,
            DEPLOYED_IN_BLOCK.longValue() + 250L,
            DEPLOYED_IN_BLOCK.longValue() + 400L);
    Mockito.when(web3jMock.ethGetLogs(capture.capture()))
        .thenReturn(new EthDepositLogRequest<>(List.of(1L, 2L)))
        .thenReturn(new EthDepositLogRequest<>(List.of(3L, 4L)))
        .thenReturn(new EthDepositLogRequest<>(List.of(5L, 6L, 7L)));
    Ethereum.BlockRange range =
        new Ethereum.BlockRange(
            DEPLOYED_IN_BLOCK, DEPLOYED_IN_BLOCK.add(BigInteger.valueOf(100)), true);
    blockPagination.setNextRange(range);

    SynchronousQueue<DepositDto> queue = new SynchronousQueue<>();
    new Thread(
            () -> {
              DepositDto deposit = ethereum.waitForDeposit(6L);
              ExceptionConverter.run(() -> queue.put(deposit));
            })
        .start();

    for (int i = 0; i < 3; i++) {
      Assertions.assertThat(ethereum.run()).isTrue();
      BigInteger expectedLatestBlock = BigInteger.valueOf(blockNumbers.get(i));
      Assertions.assertThat(blockPagination.getLatestCheckedBlock()).isEqualTo(expectedLatestBlock);
      blockPagination.incrementRange(BigInteger.valueOf(150), true);
    }

    DepositDto deposit = queue.poll(5, TimeUnit.SECONDS);
    Assertions.assertThat(deposit).isNotNull();
    Assertions.assertThat(deposit.amount).isEqualTo("60000");
    Assertions.assertThat(deposit.pbcAddress).isEqualTo(pbcAddress);
  }

  @Test
  public void blockPaginationDeterminesBlockRangeInFilter() {
    ArgumentCaptor<EthFilter> capture = ArgumentCaptor.forClass(EthFilter.class);
    Mockito.when(web3jMock.ethGetLogs(capture.capture()))
        .thenReturn(new EthDepositLogRequest<>(List.of(2L)));
    Ethereum.BlockRange range =
        new Ethereum.BlockRange(
            DEPLOYED_IN_BLOCK, DEPLOYED_IN_BLOCK.add(BigInteger.valueOf(20L)), false);
    blockPagination.setNextRange(range);
    Assertions.assertThat(ethereum.run()).isFalse();

    ethereum.waitForDeposit(2L);
    EthFilter filter = capture.getValue();
    Assertions.assertThat(filter.getFromBlock().getValue())
        .isEqualTo(DefaultBlockParameter.valueOf(DEPLOYED_IN_BLOCK).getValue());
    Assertions.assertThat(filter.getToBlock().getValue())
        .isEqualTo(
            DefaultBlockParameter.valueOf(DEPLOYED_IN_BLOCK.add(BigInteger.valueOf(20L)))
                .getValue());

    Mockito.when(web3jMock.ethGetLogs(capture.capture()))
        .thenReturn(new EthDepositLogRequest<>(List.of(3L)));
    blockPagination.incrementRange(BigInteger.valueOf(20), true);
    Assertions.assertThat(ethereum.run()).isTrue();

    ethereum.waitForDeposit(3L);
    filter = capture.getValue();
    Assertions.assertThat(filter.getFromBlock().getValue())
        .isEqualTo(
            DefaultBlockParameter.valueOf(DEPLOYED_IN_BLOCK.add(BigInteger.valueOf(20L)))
                .getValue());
    Assertions.assertThat(filter.getToBlock().getValue())
        .isEqualTo(
            DefaultBlockParameter.valueOf(DEPLOYED_IN_BLOCK.add(BigInteger.valueOf(40L)))
                .getValue());
  }

  @Test
  public void removedLogsAreNotSavedToCache() {
    ArgumentCaptor<EthFilter> capture = ArgumentCaptor.forClass(EthFilter.class);
    Mockito.when(web3jMock.ethGetLogs(capture.capture()))
        .thenReturn(new EthRemovedDepositLogRequest<>(List.of(1L)));
    ethereum.run();
    Assertions.assertThat(cache.loadDeposit(1L)).isNull();
  }

  @Test
  void blockRangeFromZeroIsEarliestBlock() {
    cache.saveLatestCheckedBlock(Unsigned256.ZERO);
    Ethereum.BlockRange range = new Ethereum.BlockRange(BigInteger.ZERO, BigInteger.TEN, false);
    blockPagination.setNextRange(range);

    ArgumentCaptor<EthFilter> capture = ArgumentCaptor.forClass(EthFilter.class);
    Mockito.when(web3jMock.ethGetLogs(capture.capture()))
        .thenReturn(new EthDepositLogRequest<>(List.of(1L)));
    ethereum.run();
    EthFilter filter = capture.getAllValues().get(0);
    Assertions.assertThat(filter.getFromBlock()).isEqualTo(DefaultBlockParameterName.EARLIEST);
  }

  static EthLog.LogObject createDepositLog(long id, String pbcAddress, long amount) {
    byte[] address = Numeric.hexStringToByteArray(pbcAddress);
    String hash = Numeric.toHexString(new byte[32]);
    DefaultFunctionEncoder encoder = new DefaultFunctionEncoder();
    String data =
        encoder.encodeParameters(
            List.of(new Uint64(id), new Bytes21(address), new Uint256(amount)));
    return new EthLog.LogObject(
        false,
        "0",
        "0",
        hash,
        hash,
        "9",
        ethContract,
        data,
        null,
        List.of(Ethereum.ENCODED_DEPOSIT_EVENT));
  }

  static EthLog.LogObject createRemovedDepositLog(long id, String pbcAddress, long amount) {
    byte[] address = Numeric.hexStringToByteArray(pbcAddress);
    String hash = Numeric.toHexString(new byte[32]);
    DefaultFunctionEncoder encoder = new DefaultFunctionEncoder();
    String data =
        encoder.encodeParameters(
            List.of(new Uint64(id), new Bytes21(address), new Uint256(amount)));
    return new EthLog.LogObject(
        true,
        "0",
        "0",
        hash,
        hash,
        "9",
        ethContract,
        data,
        null,
        List.of(Ethereum.ENCODED_DEPOSIT_EVENT));
  }

  static EthLog.LogObject createWithdrawalLog(
      long withdrawalNonce, String receiver, Unsigned256 amount, long oracleNonce, int bitmask) {
    String hash = Numeric.toHexString(new byte[32]);
    DefaultFunctionEncoder encoder = new DefaultFunctionEncoder();
    String data =
        encoder.encodeParameters(
            List.of(
                new Uint64(withdrawalNonce),
                new Address(receiver),
                new Uint256(new BigInteger(amount.serialize())),
                new Uint64(oracleNonce),
                new Uint32(bitmask)));
    return new EthLog.LogObject(
        false,
        "0",
        "0",
        hash,
        hash,
        "9",
        ethContract,
        data,
        null,
        List.of(Ethereum.ENCODED_WITHDRAWAL_EVENT));
  }

  private static final class EthDepositLogRequest<S> extends Request<S, EthLog> {

    private final List<Long> nonces;

    private EthDepositLogRequest(List<Long> nonces) {
      this.nonces = nonces;
    }

    @Override
    @SuppressWarnings("rawtypes")
    public EthLog send() {
      EthLog ethLog = new EthLog();
      List<EthLog.LogResult> logs = new ArrayList<>();
      for (Long nonce : nonces) {
        logs.add(createDepositLog(nonce, pbcAddress, nonce * 10_000L));
      }
      ethLog.setResult(logs);
      return ethLog;
    }
  }

  private static final class EthRemovedDepositLogRequest<S> extends Request<S, EthLog> {

    private final List<Long> nonces;

    private EthRemovedDepositLogRequest(List<Long> nonces) {
      this.nonces = nonces;
    }

    @Override
    @SuppressWarnings("rawtypes")
    public EthLog send() {
      EthLog ethLog = new EthLog();
      List<EthLog.LogResult> logs = new ArrayList<>();
      for (Long nonce : nonces) {
        logs.add(createRemovedDepositLog(nonce, pbcAddress, nonce * 10_000L));
      }
      ethLog.setResult(logs);
      return ethLog;
    }
  }

  static final class EthBlockRequest<S> extends Request<S, EthBlock> {
    String timestamp;

    EthBlockRequest(String timestamp) {
      this.timestamp = timestamp;
    }

    @Override
    public EthBlock send() {
      EthBlock ethBlock = new EthBlock();
      ethBlock.setResult(new EthBlock.Block());
      ethBlock.getBlock().setTimestamp(timestamp);
      ethBlock.setResult(ethBlock.getBlock());
      return ethBlock;
    }
  }
}
