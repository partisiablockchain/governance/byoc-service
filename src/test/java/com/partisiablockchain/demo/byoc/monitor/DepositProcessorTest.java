package com.partisiablockchain.demo.byoc.monitor;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static java.lang.Thread.sleep;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.blockchain.contract.CoreContractState;
import com.partisiablockchain.client.transaction.BlockchainTransactionClient;
import com.partisiablockchain.client.transaction.SenderAuthenticationKeyPair;
import com.partisiablockchain.client.transaction.Transaction;
import com.partisiablockchain.crypto.KeyPair;
import com.partisiablockchain.demo.byoc.governance.byocincoming.ByocHelper;
import com.partisiablockchain.demo.byoc.governance.byocincoming.ByocIncomingContractState;
import com.partisiablockchain.demo.byoc.governance.byocincoming.Deposit;
import com.partisiablockchain.demo.byoc.rest.dto.DepositDto;
import com.partisiablockchain.math.Unsigned256;
import com.partisiablockchain.serialization.StateSerializable;
import com.partisiablockchain.server.tcp.client.Client;
import com.partisiablockchain.tree.AvlTree;
import com.secata.stream.SafeDataInputStream;
import com.secata.tools.immutable.FixedList;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.stream.Collectors;
import org.junit.jupiter.api.Test;

/** Test. */
public final class DepositProcessorTest {

  private final List<KeyPair> oracleKeys =
      List.of(new KeyPair(BigInteger.ONE), new KeyPair(BigInteger.valueOf(5)));
  private final List<BlockchainAddress> oracleNodes =
      oracleKeys.stream().map(k -> k.getPublic().createAddress()).collect(Collectors.toList());
  private final KeyPair unknownOracleKey = new KeyPair(BigInteger.TEN);
  private final BlockchainAddress contractAddress =
      BlockchainAddress.fromString("010000000000000000000000000000000000123456");
  private static final String shard1 = "s1";
  private static final String shard2 = "s2";
  private final DummyPbcClient<ByocIncomingContractState> pbcClient1 =
      DummyPbcClient.forByocIncoming(oracleNodes);
  private final DummyPbcClient<ByocIncomingContractState> pbcClient2 =
      DummyPbcClient.forByocIncoming(oracleNodes);
  private ByocIncomingContractState state =
      new ByocIncomingContractState(
          0,
          AvlTree.create(),
          FixedList.create(oracleNodes),
          null,
          AvlTree.create(),
          0,
          AvlTree.create());
  private final BlockchainTransactionClient transactionClient0 =
      BlockchainTransactionClient.create(
          pbcClient1, new SenderAuthenticationKeyPair(oracleKeys.get(0)));

  private final BlockchainTransactionClient transactionClient1 =
      BlockchainTransactionClient.create(
          pbcClient2, new SenderAuthenticationKeyPair(oracleKeys.get(1)));

  private final BlockchainAddress receiver =
      BlockchainAddress.fromString("000000000000000000000000000000000000123456");
  private final DummyDataSource dataSource = new DummyDataSource(state::depositNonce);
  private DepositProcessor depositProcessor0;
  private DepositProcessor depositProcessor1;

  private final ByocTransactionSender senderKey0 =
      (trx, cost) ->
          transactionClient0.waitForSpawnedEvents(transactionClient0.signAndSend(trx, cost));
  private final ByocTransactionSender senderKey1 =
      (trx, cost) ->
          transactionClient1.waitForSpawnedEvents(transactionClient1.signAndSend(trx, cost));

  private final ContractStateHandler contractStateHandler =
      new ContractStateHandler(List.of(contractAddress));
  private Client.Listener listener =
      contractStateHandler.createContract(null, null, contractAddress, null, state);

  @Test
  public void sendDepositTransactions() {
    pbcClient1.addDefaultExecutedTransactions(shard1);
    pbcClient2.addDefaultExecutedTransactions(shard2);
    ContractStateHandler contractStateHandler2 = new ContractStateHandler(List.of(contractAddress));
    depositProcessor0 =
        new DepositProcessor(
            contractAddress,
            oracleKeys.get(0).getPublic().createAddress(),
            10,
            senderKey0,
            dataSource,
            contractStateHandler);
    depositProcessor1 =
        new DepositProcessor(
            contractAddress,
            oracleKeys.get(1).getPublic().createAddress(),
            10,
            senderKey1,
            dataSource,
            contractStateHandler2);
    ByocIncomingContractState state =
        new ByocIncomingContractState(
            0,
            AvlTree.create(),
            FixedList.create(oracleNodes),
            null,
            AvlTree.create(),
            0,
            AvlTree.create());

    contractStateHandler2.createContract(null, null, contractAddress, null, state);

    DepositDto depositDto = new DepositDto();
    depositDto.pbcAddress = receiver.writeAsString();
    depositDto.amount = "123";

    dataSource.addDeposit(depositDto);

    ByocProcessTestHelper.TransactionRpc result =
        ByocProcessTestHelper.runProcess(
            depositProcessor0, depositProcessor1, pbcClient1, pbcClient2, oracleNodes);
    assertThat(result.rpc1).isEqualTo(result.rpc2);

    SafeDataInputStream stream = SafeDataInputStream.createFromBytes(result.rpc1);
    assertThat(stream.readUnsignedByte()).isEqualTo(0L);
    assertThat(stream.readLong()).isEqualTo(0L);
    assertThat(BlockchainAddress.read(stream)).isEqualTo(receiver);
    Unsigned256 amount = Unsigned256.read(stream);
    assertThat(amount.toString()).isEqualTo(depositDto.amount);
  }

  @Test
  public void multipleDeposits() {
    DepositDto depositDto1 = new DepositDto();
    depositDto1.pbcAddress = receiver.writeAsString();
    depositDto1.amount = "123";

    DepositDto depositDto2 = new DepositDto();
    depositDto2.pbcAddress = receiver.writeAsString();
    depositDto2.amount = "321";

    dataSource.addDeposit(depositDto1);
    dataSource.addDeposit(depositDto2);

    depositProcessor0 =
        new DepositProcessor(
            contractAddress,
            oracleKeys.get(0).getPublic().createAddress(),
            10,
            senderKey0,
            dataSource,
            contractStateHandler);

    assertThat(depositProcessor0.getDataSource().waitForDeposit(0L)).isEqualTo(depositDto1);
    assertThat(depositProcessor0.getDataSource().waitForDeposit(1L)).isEqualTo(depositDto2);
  }

  @Test
  public void existingDeposit() {
    pbcClient1.addDefaultExecutedTransactions(shard1);
    pbcClient2.addDefaultExecutedTransactions(shard2);

    Deposit deposit = new Deposit(receiver, Unsigned256.create(133), 1);
    state = ByocHelper.setDeposit(state, deposit);

    DepositDto dto = new DepositDto();
    dto.pbcAddress = receiver.writeAsString();
    dto.amount = "133";

    dataSource.addDeposit(dto);
    listener.update(state);

    depositProcessor0 =
        new DepositProcessor(
            contractAddress,
            oracleKeys.get(0).getPublic().createAddress(),
            10,
            senderKey0,
            dataSource,
            contractStateHandler);
    depositProcessor1 =
        new DepositProcessor(
            contractAddress,
            oracleKeys.get(1).getPublic().createAddress(),
            10,
            senderKey1,
            dataSource,
            contractStateHandler);

    assertThat(depositProcessor0.run()).isFalse();
    listener.update(state);
    assertThat(depositProcessor1.run()).isTrue();

    assertThat(pbcClient1.getLatestPut()).isNull();
    assertThat(pbcClient2.getLatestPut()).isNotNull();
  }

  /**
   * The deposit processor should wait for a contract state change if it receives a deposit it has
   * already signed. If the processor waits for more than
   * DepositProcessor#MAX_SLEEPS_FOR_STATE_UPDATE, it should re-send the transaction.
   */
  @Test
  public void depositProcessorReturnsFalseOnDuplicateUntilThreshold() {
    Deposit deposit = new Deposit(receiver, Unsigned256.create(133), 1);
    state = ByocHelper.setDeposit(state, deposit);
    DepositDto dto = new DepositDto();
    dto.pbcAddress = receiver.writeAsString();
    dto.amount = "133";
    dataSource.addDeposit(dto);
    listener.update(state);
    List<Transaction> transactions = new ArrayList<>();
    ByocTransactionSender spySender = (tx, cost) -> transactions.add(tx);
    depositProcessor1 =
        new DepositProcessor(
            contractAddress,
            oracleKeys.get(1).getPublic().createAddress(),
            10,
            spySender,
            dataSource,
            contractStateHandler);

    // Number of times to skip the already handled deposit, see
    // DepositProcessor#MAX_SLEEPS_FOR_STATE_UPDATE
    int duplicateThreshold = 6;
    // Deposit processor should send transaction on the first run, ignore on the following runs
    // below the threshold, and then finally try again
    assertThat(depositProcessor1.run()).isTrue();
    for (int i = 0; i < duplicateThreshold; i++) {
      assertThat(depositProcessor1.run()).isFalse();
    }
    assertThat(depositProcessor1.run()).isTrue();
    assertThat(transactions.size()).isEqualTo(2);
  }

  /**
   * If this processor is waiting on data on a deposit, d_1, from the external data source (e.g.
   * Ethereum), and the byoc-incoming contract in the meantime receives enough signatures from other
   * deposit processors on d_1, and thus starts on the next deposit, d_2, this deposit processor
   * should move on to process d_2 immediately.
   */
  @Test
  public void skipDepositIfNewExists() throws InterruptedException, ExecutionException {
    Deposit deposit = new Deposit(receiver, Unsigned256.create(133), 1);
    ExecutorService exec = Executors.newSingleThreadExecutor();
    state = ByocHelper.setDeposit(state, deposit);
    List<Transaction> transactions = new ArrayList<>();
    ByocTransactionSender spySender = (tx, cost) -> transactions.add(tx);
    depositProcessor1 =
        new DepositProcessor(
            contractAddress,
            oracleKeys.get(1).getPublic().createAddress(),
            10,
            spySender,
            dataSource,
            contractStateHandler);
    // Run the processor in separate thread since it will block while waiting for deposit
    final Future<Boolean> future = exec.submit(() -> depositProcessor1.run());
    sleep(50);
    // Bump byoc contract to next deposit
    state = bumpDepositNonce(state);
    listener.update(state);
    // Add the (old) deposit
    DepositDto dto = new DepositDto();
    dto.pbcAddress = receiver.writeAsString();
    dto.amount = "123";
    dataSource.addDeposit(dto);
    // Since new deposit nonce has arrived, processor should return immediately, and not send any
    // transactions
    assertThat(future.get().booleanValue()).isTrue();
    assertThat(transactions.size()).isEqualTo(0);
  }

  @Test
  public void contractStateCreatesListener() {
    Client.Listener listener =
        contractStateHandler.createContract(
            null, null, contractAddress, CoreContractState.create(null, null, null, 0), state);

    // Should be ignored
    contractStateHandler.createContract(
        null, null, receiver, CoreContractState.create(null, null, null, 0), state);
    listener.update(new StateSerializable() {});
    listener.remove();
  }

  @Test
  public void failedPut() {
    DepositDto dto = new DepositDto();
    dto.pbcAddress = receiver.writeAsString();
    dto.amount = "133";

    dataSource.addDeposit(dto);

    depositProcessor0 =
        new DepositProcessor(
            contractAddress,
            oracleKeys.get(0).getPublic().createAddress(),
            10,
            senderKey0,
            null,
            contractStateHandler);
    depositProcessor1 =
        new DepositProcessor(
            contractAddress,
            oracleKeys.get(1).getPublic().createAddress(),
            10,
            senderKey1,
            dataSource,
            contractStateHandler);

    assertThatThrownBy(() -> depositProcessor0.run()).isInstanceOf(NullPointerException.class);

    assertThat(pbcClient1.getLatestPut()).isNull();
    assertThat(pbcClient2.getLatestPut()).isNull();
  }

  @Test
  public void oracleUnknownToContractWillNotSign() {
    pbcClient1.addDefaultExecutedTransactions(shard1);
    pbcClient2.addDefaultExecutedTransactions(shard2);

    DepositDto depositDto = new DepositDto();
    depositDto.pbcAddress = receiver.writeAsString();
    depositDto.amount = "123";
    dataSource.addDeposit(depositDto);

    depositProcessor0 =
        new DepositProcessor(
            contractAddress,
            unknownOracleKey.getPublic().createAddress(),
            10,
            senderKey0,
            dataSource,
            contractStateHandler);
    assertThat(depositProcessor0.run()).isFalse();

    assertThat(pbcClient1.getTransactionsCount()).isEqualTo(0);
  }

  @Test
  void processOnlyRunsOnRelevantContractChanges() throws InterruptedException {
    BlockchainAddress nonRelevantContract =
        BlockchainAddress.fromString("010000000000000000000000000000000000123457");
    ContractStateHandler contractStateHandler =
        new ContractStateHandler(List.of(contractAddress, nonRelevantContract));
    depositProcessor0 =
        new DepositProcessor(
            contractAddress,
            oracleKeys.get(0).getPublic().createAddress(),
            10,
            senderKey0,
            dataSource,
            contractStateHandler);
    assertThat(depositProcessor0.run()).isFalse();
    listener = contractStateHandler.createContract(null, null, contractAddress, null, state);

    Thread thread =
        new Thread(
            () -> {
              pbcClient1.addDefaultExecutedTransactions(shard1);
              pbcClient2.addDefaultExecutedTransactions(shard2);
              setupDeposit();
              // First runs finishes, as it has no contract states
              depositProcessor0.run();

              pbcClient1.addDefaultExecutedTransactions(shard1);
              pbcClient2.addDefaultExecutedTransactions(shard2);
              listener.update(bumpDepositNonce(state));
              setupDeposit();
              // second run should be blocked temporarily
              depositProcessor0.run();
            });
    thread.start();

    // Ensure the process is running regardless of contract change, i.e. not waiting
    sleep(300);
    assertThat(thread.getState().toString()).isEqualTo("TERMINATED");
  }

  private void setupDeposit() {
    DepositDto depositDto1 = new DepositDto();
    depositDto1.pbcAddress = receiver.writeAsString();
    depositDto1.amount = "123";
    dataSource.addDeposit(depositDto1);
  }

  private ByocIncomingContractState bumpDepositNonce(ByocIncomingContractState state) {
    return new ByocIncomingContractState(
        state.depositNonce() + 1,
        state.deposits(),
        state.oracleNodes(),
        state.deposit(),
        state.disputes(),
        state.oracleNonce(),
        state.epochs());
  }
}
