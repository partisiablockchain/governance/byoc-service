package com.partisiablockchain.demo.byoc.monitor;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.client.web.WebClient;
import com.partisiablockchain.crypto.AesHelper;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.KeyPair;
import com.partisiablockchain.crypto.Signature;
import com.partisiablockchain.demo.byoc.Address;
import com.partisiablockchain.demo.byoc.rest.dto.DiscoveryResponseDto;
import com.partisiablockchain.demo.byoc.rest.dto.EncryptedPayloadDto;
import com.partisiablockchain.demo.byoc.rest.dto.HandshakeDto;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import com.secata.tools.coverage.ExceptionConverter;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.util.List;
import javax.crypto.Cipher;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;

/** Test. */
public final class PeerDiscoveryClientTest {

  private static final VersionInformation versionInformation = new VersionInformation("version");

  private static final KeyPair ephemeralKey = new KeyPair(BigInteger.valueOf(12345));

  private final KeyPair serverPrivateKey = new KeyPair(BigInteger.valueOf(123));
  private final BlockchainPublicKey serverPublicKey = serverPrivateKey.getPublic();

  private final BlockchainAddress serverPbcAddress =
      BlockchainAddress.fromString("000000000000000000000000000000000000000009");
  private final BlockchainAddress clientPbcAddress =
      BlockchainAddress.fromString("000000000000000000000000000000000000000001");

  private final KeyPair clientKey = new KeyPair(BigInteger.valueOf(42));
  private final WebClient webClient = Mockito.mock(WebClient.class);
  private static final Address endpoint = new Address("dummy", 9000);
  private static final Address clientEndpoint = new Address("me", 9001);

  private final List<PeerDiscoveryClient.DiscoveredPeer> peers =
      List.of(
          new PeerDiscoveryClient.DiscoveredPeer(
              new KeyPair(BigInteger.valueOf(123)).getPublic(), new Address("dummy", 9000)),
          new PeerDiscoveryClient.DiscoveredPeer(
              new KeyPair(BigInteger.valueOf(321)).getPublic(), new Address("new_peer", 9010)));

  @Test
  void discover() {
    PeerDiscoveryClient client = createClient(clientEndpoint, clientPbcAddress);

    KeyPair ephemeralKey = client.getEphemeralKey();
    List<BlockchainPublicKey> toDiscover = peers.stream().map(p -> p.key).toList();
    byte[] payload = client.getDiscoverPayload(toDiscover);
    Hash message =
        Hash.create(
            s -> {
              serverPublicKey.write(s);
              ephemeralKey.getPublic().write(s);
              s.write(payload);
              s.writeString(versionInformation.toString());
            });
    prepareEncryptedReturnValue(ephemeralKey.getPublic(), message.getBytes());
    List<PeerDiscoveryClient.DiscoveredPeer> discover = client.discover(toDiscover);
    Assertions.assertThat(discover).hasSameSizeAs(peers);
    for (int i = 0; i < peers.size(); i++) {
      Assertions.assertThat(peers.get(i).endpoint).isEqualTo(discover.get(i).endpoint);
      Assertions.assertThat(peers.get(i).key).isEqualTo(discover.get(i).key);
    }
  }

  @Test
  void handshake() {
    PeerDiscoveryClient client = createClient(clientEndpoint, clientPbcAddress);
    ArgumentCaptor<EncryptedPayloadDto> capture =
        prepareEncryptedHandshakeReturnValue(
            ephemeralKey.getPublic(), clientEndpoint, clientPbcAddress);
    boolean handshake = PeerDiscoveryProcess.handshake(client);
    assertThatClientSentValidHandshake(capture);
    Assertions.assertThat(handshake).isTrue();
    prepareEncryptedHandshakeReturnValue(ephemeralKey.getPublic(), new byte[] {'a', 'b', 'c'});
    boolean handshake1 = PeerDiscoveryProcess.handshake(client);
    Assertions.assertThat(handshake1).isFalse();
  }

  @Test
  void handshake_incorrectInformation() {
    // Incorrect endpoint
    PeerDiscoveryClient client = createClient(new Address("incorrect", 1010), clientPbcAddress);
    prepareEncryptedHandshakeReturnValue(
        ephemeralKey.getPublic(), clientEndpoint, clientPbcAddress);
    boolean handshake = PeerDiscoveryProcess.handshake(client);
    Assertions.assertThat(handshake).isFalse();

    // Incorrect pbc address
    client =
        createClient(
            clientEndpoint,
            BlockchainAddress.fromString("000000000000000000000000000000000000000008"));
    prepareEncryptedHandshakeReturnValue(
        ephemeralKey.getPublic(), clientEndpoint, clientPbcAddress);
    handshake = PeerDiscoveryProcess.handshake(client);
    Assertions.assertThat(handshake).isFalse();
  }

  private void assertThatClientSentValidHandshake(ArgumentCaptor<EncryptedPayloadDto> capture) {
    EncryptedPayloadDto dto = capture.getAllValues().get(0);
    Assertions.assertThat(dto.ephemeralKey).isEqualTo(ephemeralKey.getPublic().asBytes());
    byte[] handshakeMessage =
        SafeDataOutputStream.serialize(
            s -> {
              endpoint.write(s);
              serverPbcAddress.write(s);
              clientEndpoint.write(s);
              clientPbcAddress.write(s);
            });
    byte[] extra = PeerDiscoveryResource.handshakeExtraDataDecrypt(versionInformation);
    Cipher cipher =
        AesHelper.createAesForEncrypt(ephemeralKey.getPrivateKey(), serverPublicKey, extra);
    byte[] payload = ExceptionConverter.call(() -> cipher.doFinal(handshakeMessage), "");
    Hash message =
        PeerDiscoveryResource.message(
            serverPublicKey, ephemeralKey.getPublic(), payload, versionInformation);
    Signature read = Signature.read(SafeDataInputStream.createFromBytes(dto.signature));
    BlockchainPublicKey publicKey = read.recoverPublicKey(message);
    Assertions.assertThat(clientKey.getPublic()).isEqualTo(publicKey);
    Assertions.assertThat(dto.payload).isEqualTo(payload);
  }

  private PeerDiscoveryClient createClient(
      Address clientEndpoint, BlockchainAddress clientPbcAddress) {
    return new PeerDiscoveryClient(
        versionInformation,
        endpoint,
        serverPublicKey,
        clientKey,
        webClient,
        clientEndpoint,
        ephemeralKey,
        serverPbcAddress,
        clientPbcAddress);
  }

  private ArgumentCaptor<EncryptedPayloadDto> prepareEncryptedHandshakeReturnValue(
      BlockchainPublicKey publicKey, byte[] data) {
    ArgumentCaptor<EncryptedPayloadDto> capture =
        ArgumentCaptor.forClass(EncryptedPayloadDto.class);
    byte[] extra =
        SafeDataOutputStream.serialize(
            s -> {
              s.write(versionInformation.toString().getBytes(StandardCharsets.UTF_8));
              s.writeString("server -> client");
            });
    Cipher aesForEncrypt =
        AesHelper.createAesForEncrypt(serverPrivateKey.getPrivateKey(), publicKey, extra);
    byte[] encrypted = ExceptionConverter.call(() -> aesForEncrypt.doFinal(data), "");
    Mockito.when(
            webClient.post(
                Mockito.eq(endpoint.getEndpoint() + "/discover/handshake"),
                capture.capture(),
                Mockito.any()))
        .thenReturn(HandshakeDto.create(encrypted));
    return capture;
  }

  private ArgumentCaptor<EncryptedPayloadDto> prepareEncryptedHandshakeReturnValue(
      BlockchainPublicKey publicKey, Address clientEndpoint, BlockchainAddress clientPbcAddress) {
    byte[] serialize =
        SafeDataOutputStream.serialize(
            stream -> {
              clientEndpoint.write(stream);
              clientPbcAddress.write(stream);
            });
    return prepareEncryptedHandshakeReturnValue(publicKey, serialize);
  }

  private void prepareEncryptedReturnValue(
      BlockchainPublicKey clientEphemeralPublicKey, byte[] extraData) {
    byte[] data =
        SafeDataOutputStream.serialize(
            stream -> {
              stream.writeInt(peers.size());
              for (PeerDiscoveryClient.DiscoveredPeer peer : peers) {
                peer.key.write(stream);
                peer.endpoint.write(stream);
              }
            });
    Cipher cipher =
        AesHelper.createAesForEncrypt(
            serverPrivateKey.getPrivateKey(), clientEphemeralPublicKey, extraData);
    byte[] payload = ExceptionConverter.call(() -> cipher.doFinal(data), "");
    Mockito.when(
            webClient.post(
                Mockito.eq(endpoint.getEndpoint() + "/discover"), Mockito.any(), Mockito.any()))
        .thenReturn(DiscoveryResponseDto.create(payload));
  }
}
