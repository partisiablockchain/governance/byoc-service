package com.partisiablockchain.demo.byoc.monitor;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.client.BlockchainContractClient;
import com.partisiablockchain.client.shards.ShardId;
import com.partisiablockchain.client.transaction.SignedTransaction;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.demo.byoc.governance.byocincoming.ByocIncomingContractState;
import com.partisiablockchain.demo.byoc.governance.byocoutgoing.ByocOutgoingContractState;
import com.partisiablockchain.demo.byoc.governance.byocoutgoing.Epoch;
import com.partisiablockchain.demo.byoc.governance.byocoutgoing.EthereumAddress;
import com.partisiablockchain.demo.byoc.governance.byocoutgoing.OracleMember;
import com.partisiablockchain.demo.byoc.governance.largeoracle.LargeOracleContractState;
import com.partisiablockchain.dto.AccountState;
import com.partisiablockchain.dto.BlockState;
import com.partisiablockchain.dto.ChainId;
import com.partisiablockchain.dto.ContractState;
import com.partisiablockchain.dto.Event;
import com.partisiablockchain.dto.ExecutedTransaction;
import com.partisiablockchain.dto.TransactionPointer;
import com.partisiablockchain.serialization.StateObjectMapper;
import com.partisiablockchain.serialization.StateSerializable;
import com.partisiablockchain.tree.AvlTree;
import com.secata.tools.immutable.FixedList;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Test.
 *
 * @param <S> the contract type that this client should work on.
 */
public abstract class DummyPbcClient<S extends StateSerializable>
    implements BlockchainContractClient {

  private static final String hash =
      "0000000000000000000000000000000000000000000000000000000000000000";

  private static final String chainId = "test-chain";
  private static final long nonce = 0L;
  private final List<SignedTransaction> transactions = new ArrayList<>();
  private SignedTransaction latestPut;
  private int transactionsCount = 0;
  private S dummyContractState;
  private final ArrayDeque<ExecutedTransaction> executedTransactions = new ArrayDeque<>();

  /**
   * Create a new client.
   *
   * @param state the contract state to return
   */
  protected DummyPbcClient(S state) {
    this.dummyContractState = state;
  }

  /**
   * Create a new dummy PBC client for testing BYOC incoming (deposits).
   *
   * @param oracleNodes the small oracle
   * @return a new test PBC client.
   */
  public static DummyPbcClient<ByocIncomingContractState> forByocIncoming(
      List<BlockchainAddress> oracleNodes) {
    return new DummyPbcClient<>(
        new ByocIncomingContractState(
            0,
            AvlTree.create(),
            FixedList.create(oracleNodes),
            null,
            AvlTree.create(),
            0,
            AvlTree.create())) {};
  }

  /**
   * Create a new dummy PBC client for testing BYOC outgoing (withdrawals).
   *
   * @param byocContract the address of the ethereum contract
   * @param oracleNodes the small oracle
   * @return a new test PBC client.
   */
  public static DummyPbcClient<ByocOutgoingContractState> forByocOutgoing(
      EthereumAddress byocContract, List<OracleMember> oracleNodes) {
    return new DummyPbcClient<>(
        new ByocOutgoingContractState(
            byocContract,
            0,
            0,
            0,
            AvlTree.<Long, Epoch>create()
                .set(
                    0L,
                    new Epoch(
                        AvlTree.create(), AvlTree.create(), FixedList.create(oracleNodes))))) {};
  }

  /**
   * Create a dummy PBC client for testing the large oracle contract.
   *
   * @param oracleMembers members of the large oracle
   * @return the new test PBC client
   */
  public static DummyPbcClient<LargeOracleContractState> forLargeOracle(
      List<com.partisiablockchain.demo.byoc.governance.largeoracle.OracleMember> oracleMembers) {
    return new DummyPbcClient<>(
        new LargeOracleContractState(
            FixedList.create(oracleMembers),
            AvlTree.create(),
            FixedList.create(),
            0,
            0,
            AvlTree.create(),
            AvlTree.create())) {};
  }

  SignedTransaction getLatestPut() {
    return latestPut;
  }

  int getTransactionsCount() {
    return transactionsCount;
  }

  @Override
  public TransactionPointer putTransaction(SignedTransaction signedTransaction) {
    this.transactionsCount++;
    this.latestPut = signedTransaction;
    this.transactions.add(signedTransaction);
    return new TransactionPointer(signedTransaction.identifier(), "unused");
  }

  S getDummyContractState() {
    return dummyContractState;
  }

  List<SignedTransaction> getTransactions() {
    return transactions;
  }

  @Override
  public ContractState getContractState(BlockchainAddress address) {
    ObjectMapper objectMapper = StateObjectMapper.createObjectMapper();
    return new ContractState(
        null, null, null, 0, objectMapper.convertValue(dummyContractState, JsonNode.class), null);
  }

  @Override
  public AccountState getAccountState(BlockchainAddress address) {
    return new AccountState(nonce);
  }

  @Override
  public ExecutedTransaction getTransaction(ShardId shardId, Hash hash) {
    if (executedTransactions.isEmpty()) {
      return null;
    } else {
      return executedTransactions.remove();
    }
  }

  void addExecutedTransaction(ExecutedTransaction... executedTransaction) {
    this.executedTransactions.addAll(Arrays.asList(executedTransaction));
  }

  void addDefaultExecutedTransactions(String shard) {
    addExecutedTransaction(
        createExecutedTransaction(createEvent(shard)), createExecutedTransaction());
  }

  ExecutedTransaction createExecutedTransaction(Event... events) {
    return new ExecutedTransaction(
        null, null, 0, 0, null, null, true, Arrays.asList(events), false, false, null, null);
  }

  Event createEvent(String shard) {
    return new Event(hash, shard);
  }

  @Override
  public ChainId getChainId() {
    return new ChainId(chainId);
  }

  @Override
  public BlockState getLatestBlock(ShardId shardId) {
    return null;
  }
}
