package com.partisiablockchain.demo.byoc.monitor;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;

import nl.jqno.equalsverifier.EqualsVerifier;
import org.junit.jupiter.api.Test;

/** Test. */
public final class DataSourceTest {

  @Test
  void withdrawalId() {
    EqualsVerifier.forClass(DataSource.WithdrawalId.class).verify();
  }

  @Test
  void compareWithdrawalIds() {
    DataSource.WithdrawalId first = new DataSource.WithdrawalId(4, 4);
    DataSource.WithdrawalId second = new DataSource.WithdrawalId(4, 4);
    DataSource.WithdrawalId third = new DataSource.WithdrawalId(4, 5);
    DataSource.WithdrawalId fourth = new DataSource.WithdrawalId(5, 4);

    assertThat(first).isEqualByComparingTo(second);
    assertThat(first).isLessThan(third);
    assertThat(first).isLessThan(fourth);
  }
}
