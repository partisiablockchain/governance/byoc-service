package com.partisiablockchain.demo.byoc.monitor;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static com.partisiablockchain.demo.byoc.monitor.RoundData.ROUND_ID_BYTE_LENGTH;
import static com.partisiablockchain.demo.byoc.monitor.RoundData.UNSIGNED_256_BYTE_LENGTH;

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.client.transaction.Transaction;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.math.Unsigned256;
import com.secata.stream.SafeDataOutputStream;
import java.util.Arrays;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test. */
final class PriceOracleNotifyPriceUpdateSenderTest {

  /**
   * Notifying a price update sends a NOTIFY_PRICE_UPDATE transaction to the Price Oracle contract
   * containing the provided round data.
   */
  @Test
  void notifyPriceUpdate() {
    BlockchainAddress priceOracleContract =
        BlockchainAddress.fromHash(
            BlockchainAddress.Type.CONTRACT_GOV,
            Hash.create(h -> h.writeString("PriceOracleContract")));
    RoundData roundData =
        new RoundData(
            Unsigned256.ONE, Unsigned256.ONE, Unsigned256.ONE, Unsigned256.ONE, Unsigned256.ONE);
    int transactionCost = 1234;

    TransactionSenderSpy transactionSenderSpy = new TransactionSenderSpy();
    PriceOracleNotifyPriceUpdateSender.notifyPriceUpdate(
        priceOracleContract, roundData, transactionSenderSpy, transactionCost);

    Assertions.assertThat(transactionSenderSpy.cost).isEqualTo(transactionCost);
    Transaction notifyPriceUpdateTransaction = transactionSenderSpy.transaction;
    Assertions.assertThat(notifyPriceUpdateTransaction.getAddress()).isEqualTo(priceOracleContract);
    Assertions.assertThat(notifyPriceUpdateTransaction.getRpc())
        .isEqualTo(
            SafeDataOutputStream.serialize(
                s -> {
                  s.writeByte(
                      PriceOracleNotifyPriceUpdateSender.NOTIFY_PRICE_UPDATE_INVOCATION_BYTE);
                  s.write(
                      Arrays.copyOfRange(
                          roundData.roundId().serialize(),
                          UNSIGNED_256_BYTE_LENGTH - ROUND_ID_BYTE_LENGTH,
                          UNSIGNED_256_BYTE_LENGTH));
                  roundData.answer().write(s);
                  roundData.startedAt().write(s);
                  roundData.updatedAt().write(s);
                  roundData.answeredInRound().write(s);
                }));
  }

  static final class TransactionSenderSpy implements ByocTransactionSender {

    private Transaction transaction;
    private long cost;

    @Override
    public void sendTransactionAndWaitForEvents(Transaction transaction, long cost) {
      this.transaction = transaction;
      this.cost = cost;
    }
  }
}
