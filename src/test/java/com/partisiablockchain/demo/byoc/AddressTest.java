package com.partisiablockchain.demo.byoc;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

import nl.jqno.equalsverifier.EqualsVerifier;
import org.junit.jupiter.api.Test;

/** Test. */
public final class AddressTest {

  @Test
  public void constructor() {
    assertThat(new Address()).isNotNull();
  }

  @Test
  public void getHostAndPort() {
    Address address = new Address("100.100" + ".8.3", 8321);
    assertThat(address.getHost()).isEqualTo("100.100" + ".8.3");
    assertThat(address.getPort()).isEqualTo(8321);
  }

  @Test
  public void getEndPoint() {
    Address address = new Address("100.100" + ".8.3", 8323);
    assertThat(address.getEndpoint()).isEqualTo("http://100.100.8.3:8323");
    address = new Address("localhost", 9400);
    assertThat(address.getEndpoint()).isEqualTo("http://localhost:9400");
  }

  @Test
  public void parsingAnAddressToStringGivesTheSameAddress() {
    assertThat(Address.parseAddress("localhost:9400")).isEqualTo(new Address("localhost", 9400));
  }

  @Test
  public void addressToString() {
    assertThat(Address.parseAddress("localhost:9400").toString()).isEqualTo("localhost:9400");
  }

  @Test
  public void equals() {
    EqualsVerifier.forClass(Address.class).verify();
  }
}
