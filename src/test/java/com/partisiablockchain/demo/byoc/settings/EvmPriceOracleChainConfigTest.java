package com.partisiablockchain.demo.byoc.settings;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import java.math.BigInteger;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

/** Test. */
final class EvmPriceOracleChainConfigTest {

  /**
   * Create EVM chain settings with valid block validity depths.
   *
   * @param blockValidityDepth block validity depth
   */
  @ParameterizedTest
  @ValueSource(ints = {0, 1})
  void constructorValidBlockValidityDepth(int blockValidityDepth) {
    String chainId = "chainId";
    String endpointUrl = "https://endpoint.url.test";
    EvmPriceOracleChainConfig evmPriceOracleChainConfig =
        new EvmPriceOracleChainConfig(chainId, endpointUrl, BigInteger.valueOf(blockValidityDepth));
    Assertions.assertThat(evmPriceOracleChainConfig.chainId).isEqualTo(chainId);
    Assertions.assertThat(evmPriceOracleChainConfig.endpointUrl).isEqualTo(endpointUrl);
    Assertions.assertThat(evmPriceOracleChainConfig.blockValidityDepth)
        .isEqualTo(blockValidityDepth);
  }

  /**
   * Attempting to create EVM chain settings with a negative block validity depth should throw an
   * exception.
   */
  @Test
  void constructorNegativeBlockValidityDepth() {
    Assertions.assertThatThrownBy(
            () ->
                new EvmPriceOracleChainConfig(
                    "chainId", "https://endpoint.url.test", BigInteger.valueOf(-1)))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Block validity depth must be non-negative");
  }
}
