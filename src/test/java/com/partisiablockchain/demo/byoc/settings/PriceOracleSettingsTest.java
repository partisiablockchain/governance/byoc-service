package com.partisiablockchain.demo.byoc.settings;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.demo.byoc.monitor.ByocTransactionSender;
import com.partisiablockchain.demo.byoc.monitor.chainlink.ChainlinkClient;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.junit.jupiter.api.Test;

/** Test. */
public final class PriceOracleSettingsTest {

  private final BlockchainAddress myId =
      BlockchainAddress.fromHash(
          BlockchainAddress.Type.CONTRACT_GOV, Hash.create(h -> h.writeString("myID")));

  private final BlockchainAddress largeOracleContract =
      BlockchainAddress.fromHash(
          BlockchainAddress.Type.CONTRACT_GOV,
          Hash.create(h -> h.writeString("LARGE_CONTRACT_ADDRESS")));

  @Test
  void gettersWork() {
    PriceOracleSettings.Builder builder = new PriceOracleSettings.Builder();
    ByocTransactionSender noopSender = (a, b) -> {};
    PriceOracleSettings settings =
        builder
            .setPbcEndpoint("anEndpoint")
            .setShards(new ArrayList<>())
            .setTransactionSender(noopSender)
            .setPbcAccount(myId)
            .setLargeOracleContract(largeOracleContract)
            .build();
    assertThat(settings.getLargeOracleContract()).isEqualTo(largeOracleContract);
  }

  @Test
  void createChainlinkClientsFromSettings() {
    String chainId = "ChainId";
    String endpointUrl = "https://endpoint.url.test";
    BigInteger blockValidityDepth = BigInteger.TEN;
    PriceOracleSettings settings =
        PriceOracleSettings.builder()
            .setPbcEndpoint("")
            .setShards(List.of())
            .setTransactionSender((transaction, cost) -> {})
            .setPbcAccount(
                BlockchainAddress.fromHash(
                    BlockchainAddress.Type.ACCOUNT, Hash.create(h -> h.writeString("PbcAccount"))))
            .setChainConfigs(
                List.of(new EvmPriceOracleChainConfig(chainId, endpointUrl, blockValidityDepth)))
            .build();
    Map<String, ChainlinkClient> clients = settings.createChainlinkClients();
    assertThat(clients).hasSize(1);
    assertThat(clients.get(chainId)).isNotNull();
  }
}
