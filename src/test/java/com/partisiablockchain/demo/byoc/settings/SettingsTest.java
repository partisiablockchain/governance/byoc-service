package com.partisiablockchain.demo.byoc.settings;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.BlsKeyPair;
import com.partisiablockchain.crypto.KeyPair;
import com.partisiablockchain.demo.byoc.Address;
import com.partisiablockchain.demo.byoc.monitor.ByocTransactionSender;
import com.partisiablockchain.demo.byoc.monitor.ContractStateHandler;
import com.partisiablockchain.demo.byoc.monitor.DataSource;
import com.partisiablockchain.demo.byoc.monitor.DefaultEthereumEventsCache;
import com.partisiablockchain.demo.byoc.monitor.PeerDatabase;
import com.partisiablockchain.demo.byoc.monitor.SimpleAdditionalStatusProvider;
import com.partisiablockchain.demo.byoc.monitor.VersionInformation;
import java.math.BigInteger;
import java.util.List;
import org.bouncycastle.util.encoders.Hex;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

/** Test. */
public final class SettingsTest {
  private static final String pbcEndpoint = "http://localhost:8000";
  private static final List<String> shards = List.of("Shard0", "Shard1");
  private static final BlockchainAddress pbcAccount =
      BlockchainAddress.fromString("00AA00000000000000000000000000000000554433");
  private static final ByocTransactionSender noopSender = (transaction, cost) -> {};
  private final KeyPair signingKey = new KeyPair();

  private final BlockchainAddress depositContract =
      BlockchainAddress.fromString("045dbd4c13df987d7fb4450e54bcd94b34a80f2351");
  private final BlockchainAddress withdrawalContract =
      BlockchainAddress.fromString("043b1822925da011657f9ab3d6ff02cf1e0bfe0146");
  private final BlockchainAddress bpOrchestrationContract =
      BlockchainAddress.fromString("010000000000000000000000000000000000654321");
  private final BlockchainAddress largeOracleContract =
      BlockchainAddress.fromString("010000000000000000000000000000000000554433");
  private final BlockchainAddress priceOracleContract1 =
      BlockchainAddress.fromString("040000000000000000000000000000000000000005");
  private final BlockchainAddress priceOracleContract2 =
      BlockchainAddress.fromString("040000000000000000000000000000000000000006");
  private final List<BlockchainAddress> priceOracleContracts =
      List.of(priceOracleContract1, priceOracleContract2);

  private static final ContractStateHandler contractStateHandler =
      new ContractStateHandler(
          List.of(BlockchainAddress.fromString("045dbd4c13df987d7fb4450e54bcd94b34a80f2351")));
  private final String ethContract = "06012c8cf97bead5deae237070f9587f8e7a266d";
  private final String ethEndpoint = "http://localhost:7545";

  private final byte[] largeOracleDatabaseKey =
      Hex.decode("0000000000000000000000000000000000000000000000000000000000000000");
  private final PeerDatabase peerDatabase = Mockito.mock(PeerDatabase.class);
  private final DataSource dataSource = Mockito.mock(DataSource.class);

  @Test
  void depositSettings() {
    DepositSettings.Builder builder =
        defaultBase(DepositSettings.builder())
            .setDepositContractAddress(depositContract)
            .setDepositDataSource(dataSource);
    DepositSettings settings = builder.build();
    assertBaseSettings(settings);
    assertThat(settings.getDepositContractAddress()).isEqualTo(depositContract);
    assertThat(settings.getDepositDataSource()).isEqualTo(dataSource);
  }

  @Test
  void disputeSettings() {
    DisputeSettings.Builder builder =
        defaultBase(DisputeSettings.builder())
            .setDataSource(dataSource)
            .setLargeOracleContract(largeOracleContract)
            .setDepositContract(depositContract)
            .setWithdrawContract(withdrawalContract);
    DisputeSettings settings = builder.build();
    assertBaseSettings(settings);
    assertThat(settings.getDataSource()).isEqualTo(dataSource);
    assertThat(settings.getDepositContract()).isEqualTo(depositContract);
    assertThat(settings.getWithdrawContract()).isEqualTo(withdrawalContract);
    assertThat(settings.getLargeOracleContract()).isEqualTo(largeOracleContract);
  }

  @Test
  void largeOracleSettings() {
    BlsKeyPair blsKeyPair = new BlsKeyPair(BigInteger.valueOf(500));
    LargeOracleSettings.Builder builder =
        defaultBase(LargeOracleSettings.builder())
            .setLargeOracleDatabaseKey(largeOracleDatabaseKey)
            .setLargeOracleDatabaseName("database")
            .setLargeOracleDatabaseBackupName("backup")
            .setLargeOraclePort(9999)
            .setLargeOracleContract(largeOracleContract)
            .setBpOrchestrationContract(bpOrchestrationContract)
            .setPreSignatureCount(42)
            .setMyEndpoint(new Address("dummy", 213))
            .setPeerDatabase(peerDatabase)
            .setVersionInformation(new VersionInformation("1"))
            .setFinalizationKey(blsKeyPair.getPublicKey())
            .setAdditionalStatusProvider(new SimpleAdditionalStatusProvider())
            .setSigningKey(signingKey);
    LargeOracleSettings settings = builder.build();
    assertBaseSettings(settings);
    assertThat(settings.getLargeOracleDatabaseKey()).isEqualTo(largeOracleDatabaseKey);
    assertThat(settings.getFinalizationKey()).isEqualTo(blsKeyPair.getPublicKey());
    assertThat(settings.getLargeOracleDatabaseName()).isEqualTo("database");
    assertThat(settings.getLargeOracleDatabaseBackupName()).isEqualTo("backup");
    assertThat(settings.getLargeOracleContract()).isEqualTo(largeOracleContract);
    assertThat(settings.getBpOrchestrationContract()).isEqualTo(bpOrchestrationContract);
    assertThat(settings.getPreSignatureCount()).isEqualTo(42);
    assertThat(settings.getMyEndpoint()).isEqualTo(new Address("dummy", 213));
    assertThat(settings.getLargeOraclePort()).isEqualTo(9999);
    assertThat(settings.getBlockProducerPeerDatabase()).isEqualTo(peerDatabase);
    assertThat(settings.getVersionInformation().toString()).isEqualTo("1");
    assertThat(settings.getSigningKey()).isEqualTo(signingKey);
    assertThat(settings.getAdditionalStatusProviders().getProviders().size()).isEqualTo(1);
  }

  @Test
  void withdrawalSettings() {
    WithdrawalSettings.Builder builder =
        defaultBase(WithdrawalSettings.builder())
            .setWithdrawalContractAddress(withdrawalContract)
            .setOracleSigningKey(signingKey);
    WithdrawalSettings settings = builder.build();
    assertBaseSettings(settings);
    assertThat(settings.getOracleSigningKey()).isEqualTo(signingKey);
    assertThat(settings.getWithdrawalContractAddress()).isEqualTo(withdrawalContract);
  }

  @Test
  void ethereumSettings() {
    DefaultEthereumEventsCache eventsCache = new DefaultEthereumEventsCache();
    EthereumSettings.Builder builder =
        defaultBase(EthereumSettings.builder())
            .setEthereumEndpoint(ethEndpoint)
            .setEthereumByocContract(ethContract)
            .setEthereumEventsCache(eventsCache);
    EthereumSettings settings = builder.build();
    assertBaseSettings(settings);
    assertThat(settings.getEthereumEndpoint()).isEqualTo(ethEndpoint);
    assertThat(settings.getEthereumByocContract()).isEqualTo(ethContract);
    assertThat(settings.getCache()).isEqualTo(eventsCache);
    assertThat(settings.getMaxBlockRange()).isEqualTo(BigInteger.valueOf(5_000L));
    builder.setMaxBlockRange(BigInteger.ONE);
    assertThat(builder.build().getMaxBlockRange()).isEqualTo(BigInteger.ONE);
    assertThat(settings.getEthereumBlockValidityDepth()).isEqualTo(BigInteger.TEN);
    builder.setEthereumBlockValidityDepth(BigInteger.ONE);
    assertThat(builder.build().getEthereumBlockValidityDepth()).isEqualTo(BigInteger.ONE);
    builder.setEthereumBlockValidityDepth(BigInteger.ZERO);
    assertThat(builder.build().getEthereumBlockValidityDepth()).isEqualTo(BigInteger.ZERO);
    assertThatThrownBy(() -> builder.setEthereumBlockValidityDepth(BigInteger.valueOf(-1)))
        .isInstanceOf(RuntimeException.class)
        .hasMessageContaining("Ethereum block validity depth must be non-negative");
  }

  @Test
  void priceOracleSettings() {
    List<EvmPriceOracleChainConfig> chainConfigs =
        List.of(
            new EvmPriceOracleChainConfig("ChainA", "https://a.endpoint.url.test", BigInteger.ONE),
            new EvmPriceOracleChainConfig(
                "ChainB", "https://b.endpoint.url.test", BigInteger.valueOf(2)),
            new EvmPriceOracleChainConfig(
                "ChainC", "https://c.endpoint.url.test", BigInteger.valueOf(3)));

    PriceOracleSettings.Builder builder =
        defaultBase(PriceOracleSettings.builder())
            .setPriceOracleContracts(priceOracleContracts)
            .setChainConfigs(chainConfigs);

    PriceOracleSettings settings = builder.build();
    assertBaseSettings(settings);
    assertThat(settings.getChainConfigs()).containsExactlyElementsOf(chainConfigs);
    assertThat(settings.getPriceOracleContracts()).isEqualTo(priceOracleContracts);
  }

  /**
   * Set default settings.
   *
   * @param base base settings builder
   * @param <T> generic
   * @return updated builder
   */
  public static <T extends Settings.Builder<T>> T defaultBase(T base) {
    return base.setPbcAccount(pbcAccount)
        .setPbcEndpoint(pbcEndpoint)
        .setShards(shards)
        .setTransactionSender(noopSender)
        .setTransactionCost(50)
        .setTransactionTimeout(100)
        .setRequestInterval(123)
        .setDelayOnError(321)
        .setContractStateHandler(contractStateHandler);
  }

  private void assertBaseSettings(Settings settings) {
    assertThat(settings.getPbcAccount()).isEqualTo(pbcAccount);
    assertThat(settings.getTransactionSender()).isEqualTo(noopSender);
    assertThat(settings.getShards()).isEqualTo(shards);
    assertThat(settings.getPbcEndpoint()).isEqualTo(pbcEndpoint);
    assertThat(settings.getTransactionTimeout()).isEqualTo(100);
    assertThat(settings.getTransactionCost()).isEqualTo(50L);
    assertThat(settings.getRequestInterval()).isEqualTo(123);
    assertThat(settings.getDelayOnError()).isEqualTo(321);
    assertThat(settings.getContractStateHandler()).isEqualTo(contractStateHandler);
  }
}
