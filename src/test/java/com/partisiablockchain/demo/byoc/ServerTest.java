package com.partisiablockchain.demo.byoc;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.BlsKeyPair;
import com.partisiablockchain.crypto.KeyPair;
import com.partisiablockchain.demo.byoc.monitor.ByocProcess;
import com.partisiablockchain.demo.byoc.monitor.ByocProcessRunner;
import com.partisiablockchain.demo.byoc.monitor.DataSource;
import com.partisiablockchain.demo.byoc.monitor.DepositProcessor;
import com.partisiablockchain.demo.byoc.monitor.DisputeProcess;
import com.partisiablockchain.demo.byoc.monitor.Ethereum;
import com.partisiablockchain.demo.byoc.monitor.LargeOracleProcess;
import com.partisiablockchain.demo.byoc.monitor.PeerDiscoveryProcess;
import com.partisiablockchain.demo.byoc.monitor.PriceOracleChallengePeriodProcess;
import com.partisiablockchain.demo.byoc.monitor.PriceOracleDisputeProcess;
import com.partisiablockchain.demo.byoc.monitor.PriceOracleNotifyProcess;
import com.partisiablockchain.demo.byoc.monitor.SimpleAdditionalStatusProvider;
import com.partisiablockchain.demo.byoc.monitor.SimplePeerDatabase;
import com.partisiablockchain.demo.byoc.monitor.VersionInformation;
import com.partisiablockchain.demo.byoc.monitor.WithdrawalProcess;
import com.partisiablockchain.demo.byoc.settings.DepositSettings;
import com.partisiablockchain.demo.byoc.settings.DisputeSettings;
import com.partisiablockchain.demo.byoc.settings.EthereumSettings;
import com.partisiablockchain.demo.byoc.settings.EvmPriceOracleChainConfig;
import com.partisiablockchain.demo.byoc.settings.LargeOracleSettings;
import com.partisiablockchain.demo.byoc.settings.PriceOracleSettings;
import com.partisiablockchain.demo.byoc.settings.Settings;
import com.partisiablockchain.demo.byoc.settings.SettingsTest;
import com.partisiablockchain.demo.byoc.settings.WithdrawalSettings;
import com.secata.tools.rest.RestServer;
import com.secata.tools.thread.ShutdownHook;
import java.math.BigInteger;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

/** Test. */
public final class ServerTest {

  private final KeyPair signingKey = new KeyPair();

  private final BlockchainAddress depositContract =
      BlockchainAddress.fromString("040000000000000000000000000000000000000001");
  private final BlockchainAddress withdrawalContract =
      BlockchainAddress.fromString("040000000000000000000000000000000000000002");
  private final BlockchainAddress bpOrchestrationContract =
      BlockchainAddress.fromString("040000000000000000000000000000000000000003");
  private final BlockchainAddress largeOracleContract =
      BlockchainAddress.fromString("040000000000000000000000000000000000000004");
  private final BlockchainAddress priceOracleContract1 =
      BlockchainAddress.fromString("040000000000000000000000000000000000000005");
  private final BlockchainAddress priceOracleContract2 =
      BlockchainAddress.fromString("040000000000000000000000000000000000000006");
  private final List<BlockchainAddress> priceOracleContracts =
      Arrays.asList(priceOracleContract1, priceOracleContract2);

  private final String ethContract = "06012c8cf97bead5deae237070f9587f8e7a267d";

  private final String ethEndpoint = "http://localhost:7545";

  private final ShutdownHook shutdownHook = new ShutdownHook();

  private static final AtomicInteger freshPort = new AtomicInteger(13434);
  private final DataSource dataSource = Mockito.mock(DataSource.class);

  @Test
  void unknownSettings() {
    Settings settings = Mockito.mock(Settings.class);
    assertThatThrownBy(() -> Server.run(settings, shutdownHook))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Unknown service settings");
  }

  @Test
  void runDepositService() {
    DepositSettings settings =
        SettingsTest.defaultBase(DepositSettings.builder())
            .setDepositContractAddress(depositContract)
            .setDepositDataSource(dataSource)
            .build();
    Server.run(settings, shutdownHook);

    assertThat(shutdownHook.closeables()).hasSize(1);
    assertThat(shutdownHook.closeables().get(0)).isExactlyInstanceOf(ByocProcessRunner.class);
    ByocProcessRunner processRunner = (ByocProcessRunner) shutdownHook.closeables().get(0);
    DepositProcessor process = (DepositProcessor) processRunner.getProcess();
    assertThat(process.name()).isEqualTo(DepositProcessor.class.getSimpleName());
  }

  @Test
  void runWithdrawalService() {
    WithdrawalSettings settings =
        SettingsTest.defaultBase(WithdrawalSettings.builder())
            .setWithdrawalContractAddress(withdrawalContract)
            .setOracleSigningKey(signingKey)
            .build();
    Server.run(settings, shutdownHook);
    assertThat(shutdownHook.closeables()).hasSize(1);
    assertThat(shutdownHook.closeables().get(0)).isExactlyInstanceOf(ByocProcessRunner.class);
    ByocProcessRunner processRunner = (ByocProcessRunner) shutdownHook.closeables().get(0);
    ByocProcess process = processRunner.getProcess();
    assertThat(process).isExactlyInstanceOf(WithdrawalProcess.class);
    assertThat(process.name()).isEqualTo(WithdrawalProcess.class.getSimpleName());
  }

  @Test
  void runLargeOracleService() {
    BlsKeyPair blsKeyPair = new BlsKeyPair(BigInteger.valueOf(500));
    LargeOracleSettings settings =
        SettingsTest.defaultBase(LargeOracleSettings.builder())
            .setSigningKey(signingKey)
            .setLargeOraclePort(freshPort.getAndIncrement())
            .setBpOrchestrationContract(bpOrchestrationContract)
            .setLargeOracleContract(largeOracleContract)
            .setLargeOracleDatabaseName("database")
            .setLargeOracleDatabaseBackupName("databaseBackup")
            .setLargeOracleDatabaseKey(new byte[32])
            .setVersionInformation(new VersionInformation("test_version"))
            .setPeerDatabase(new SimplePeerDatabase(new HashMap<>()))
            .setMyEndpoint(new Address("dummy", 800))
            .setFinalizationKey(blsKeyPair.getPublicKey())
            .setAdditionalStatusProvider(new SimpleAdditionalStatusProvider())
            .build();

    Server.run(settings, shutdownHook);
    assertThat(shutdownHook.closeables()).hasSize(3);
    assertThat(shutdownHook.closeables().get(0)).isExactlyInstanceOf(RestServer.class);
    assertThat(shutdownHook.closeables().get(1)).isExactlyInstanceOf(ByocProcessRunner.class);
    assertThat(shutdownHook.closeables().get(2)).isExactlyInstanceOf(ByocProcessRunner.class);
    ByocProcessRunner runner = (ByocProcessRunner) shutdownHook.closeables().get(1);
    ByocProcess process = runner.getProcess();
    assertThat(process).isExactlyInstanceOf(LargeOracleProcess.class);
    assertThat(process.name()).isEqualTo(LargeOracleProcess.class.getSimpleName());
    ByocProcessRunner discovery = (ByocProcessRunner) shutdownHook.closeables().get(2);
    ByocProcess discoveryProcess = discovery.getProcess();
    assertThat(discoveryProcess).isExactlyInstanceOf(PeerDiscoveryProcess.class);
    assertThat(discoveryProcess.name()).isEqualTo(PeerDiscoveryProcess.class.getSimpleName());

    RestServer restServer = (RestServer) shutdownHook.closeables().get(0);
    assertThat(restServer.isRunning()).isTrue();
  }

  @Test
  void runDisputeService() {
    DisputeSettings settings =
        SettingsTest.defaultBase(DisputeSettings.builder())
            .setDepositContract(depositContract)
            .setWithdrawContract(withdrawalContract)
            .setLargeOracleContract(largeOracleContract)
            .setDataSource(dataSource)
            .build();

    Server.run(settings, shutdownHook);
    assertThat(shutdownHook.closeables()).hasSize(1);
    assertThat(shutdownHook.closeables().get(0)).isExactlyInstanceOf(ByocProcessRunner.class);
    ByocProcessRunner processRunner = (ByocProcessRunner) shutdownHook.closeables().get(0);
    ByocProcess process = processRunner.getProcess();
    assertThat(process).isExactlyInstanceOf(DisputeProcess.class);
    assertThat(process.name()).isEqualTo(DisputeProcess.class.getSimpleName());
  }

  @Test
  void runEthereumService() {
    EthereumSettings settings =
        SettingsTest.defaultBase(EthereumSettings.builder())
            .setEthereumEndpoint(ethEndpoint)
            .setEthereumByocContract(ethContract)
            .build();

    Server.run(settings, shutdownHook);
    assertThat(shutdownHook.closeables()).hasSize(1);
    assertThat(shutdownHook.closeables().get(0)).isExactlyInstanceOf(ByocProcessRunner.class);
    ByocProcessRunner processRunner = (ByocProcessRunner) shutdownHook.closeables().get(0);
    ByocProcess process = processRunner.getProcess();
    assertThat(process).isExactlyInstanceOf(Ethereum.class);
    assertThat(process.name()).isEqualTo(Ethereum.class.getSimpleName());
  }

  /**
   * Three price oracle processes - {@link PriceOracleNotifyProcess}, {@link
   * PriceOracleChallengePeriodProcess}, and {@link PriceOracleDisputeProcess} - are started when
   * running a service from {@link PriceOracleSettings}.
   */
  @Test
  void runPriceOracleServices() {
    List<EvmPriceOracleChainConfig> chainConfigs =
        List.of(
            new EvmPriceOracleChainConfig("ChainA", "https://a.endpoint.url.test", BigInteger.ONE),
            new EvmPriceOracleChainConfig(
                "ChainB", "https://b.endpoint.url.test", BigInteger.valueOf(2)),
            new EvmPriceOracleChainConfig(
                "ChainC", "https://c.endpoint.url.test", BigInteger.valueOf(3)));

    PriceOracleSettings settings =
        SettingsTest.defaultBase(PriceOracleSettings.builder())
            .setPriceOracleContracts(priceOracleContracts)
            .setChainConfigs(chainConfigs)
            .setLargeOracleContract(largeOracleContract)
            .build();

    Server.run(settings, shutdownHook);
    assertThat(shutdownHook.closeables()).hasSize(3);

    assertThat(shutdownHook.closeables().get(0)).isExactlyInstanceOf(ByocProcessRunner.class);
    ByocProcessRunner processRunner = (ByocProcessRunner) shutdownHook.closeables().get(0);
    ByocProcess process = processRunner.getProcess();
    assertThat(process).isExactlyInstanceOf(PriceOracleNotifyProcess.class);
    assertThat(process.name()).isEqualTo(PriceOracleNotifyProcess.class.getSimpleName());

    assertThat(shutdownHook.closeables().get(1)).isExactlyInstanceOf(ByocProcessRunner.class);
    processRunner = (ByocProcessRunner) shutdownHook.closeables().get(1);
    process = processRunner.getProcess();
    assertThat(process).isExactlyInstanceOf(PriceOracleDisputeProcess.class);
    assertThat(process.name()).isEqualTo(PriceOracleDisputeProcess.class.getSimpleName());

    assertThat(shutdownHook.closeables().get(2)).isExactlyInstanceOf(ByocProcessRunner.class);
    processRunner = (ByocProcessRunner) shutdownHook.closeables().get(2);
    process = processRunner.getProcess();
    assertThat(process).isExactlyInstanceOf(PriceOracleChallengePeriodProcess.class);
    assertThat(process.name()).isEqualTo(PriceOracleChallengePeriodProcess.class.getSimpleName());
  }
}
