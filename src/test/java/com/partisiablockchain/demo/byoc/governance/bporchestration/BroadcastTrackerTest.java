package com.partisiablockchain.demo.byoc.governance.bporchestration;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.tree.AvlTree;
import java.util.List;
import java.util.Map;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test. */
public final class BroadcastTrackerTest {

  @Test
  void getBroadcastersBitmap() {
    Hash hash = Hash.create(s -> s.writeString("3"));
    List<BlockProducer> producers = LargeOracleUpdateTest.createProducers(9);
    List<BlockchainAddress> idents = producers.stream().map(BlockProducer::identity).toList();
    BroadcastTracker tracker =
        new BroadcastTracker(
            AvlTree.create(Map.of(idents.get(0), hash)),
            AvlTree.create(Map.of(idents.get(0), Bitmap.create(9))),
            Bitmap.allOnes(9));
    Assertions.assertThat(tracker.getBroadcastersBitmap(List.of(producers.get(0))).asBytes())
        .isEqualTo(new byte[] {0});
  }

  @Test
  void bitmapsAreUpdatedCorrectly() {
    List<BlockProducer> producers = LargeOracleUpdateTest.createProducers(9);
    BroadcastTracker tracker =
        new BroadcastTracker(AvlTree.create(), AvlTree.create(), Bitmap.create(9));
    BlockchainAddress sender = producers.get(0).identity();
    tracker =
        BpOrchestrationContractHelper.addMessage(
            tracker, sender, Hash.create(s -> s.writeInt(123)), 9);
    List<BlockchainAddress> parties = producers.stream().map(BlockProducer::identity).toList();
    tracker =
        BpOrchestrationContractHelper.updateBitmap(
            tracker, parties.get(1), new byte[] {1, 0}, parties);
    tracker =
        BpOrchestrationContractHelper.updateBitmap(
            tracker, parties.get(2), new byte[] {1, 0}, parties);
    Assertions.assertThat(tracker.getBroadcastersBitmap(producers).asBytes())
        .isEqualTo(new byte[] {0, 0});
    tracker =
        BpOrchestrationContractHelper.updateBitmap(
            tracker, parties.get(3), new byte[] {1, 0}, parties);
    Assertions.assertThat(tracker.getBroadcastersBitmap(producers).asBytes())
        .isEqualTo(new byte[] {1, 0});

    List<BlockchainAddress> idents = producers.stream().map(BlockProducer::identity).toList();
    tracker = new BroadcastTracker(AvlTree.create(), AvlTree.create(), Bitmap.create(9));
    BroadcastTracker newTracker =
        BpOrchestrationContractHelper.updateBitmap(
            tracker, idents.get(0), new byte[] {0b100, 0b0}, idents);
    Assertions.assertThat(newTracker.echos().asBytes()).isEqualTo(new byte[] {1, 0});
  }
}
