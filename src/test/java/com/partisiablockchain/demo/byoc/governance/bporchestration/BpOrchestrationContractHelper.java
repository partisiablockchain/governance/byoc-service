package com.partisiablockchain.demo.byoc.governance.bporchestration;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.serialization.LargeByteArray;
import com.partisiablockchain.tree.AvlTree;
import com.secata.stream.SafeDataInputStream;
import com.secata.tools.immutable.FixedList;
import java.util.List;
import java.util.Objects;

/** Helper to access package-private initialization method. */
public final class BpOrchestrationContractHelper {

  private BpOrchestrationContractHelper() {}

  static BroadcastTracker addMessage(
      BroadcastTracker tracker, BlockchainAddress sender, Hash message, int numberOfParties) {
    Bitmap bitmap = Bitmap.create(numberOfParties);
    return new BroadcastTracker(
        tracker.messages().set(sender, message),
        tracker.bitmaps().set(sender, bitmap),
        tracker.echos());
  }

  /**
   * Create a bitmap from a stream.
   *
   * @param stream the stream
   * @return the bitmap.
   */
  public static Bitmap bitmapFromStream(SafeDataInputStream stream) {
    return new Bitmap(new LargeByteArray(stream.readDynamicBytes()));
  }

  /**
   * Start the process of updating the large oracle.
   *
   * @param state state
   * @param update details about the large oracle update
   * @return state with an active update.
   */
  public static BpOrchestrationContractState withLargeOracleUpdate(
      BpOrchestrationContractState state, LargeOracleUpdate update) {
    return new BpOrchestrationContractState(
        state.sessionId(),
        state.retryNonce(),
        state.thresholdKey(),
        update,
        state.blockProducers(),
        state.committee(),
        state.domainSeparator());
  }

  /**
   * Update the state with a new oracle from the current update.
   *
   * @param state state
   * @return state with a new oracle.
   */
  public static BpOrchestrationContractState withNewLargeOracle(
      BpOrchestrationContractState state) {
    FixedList<BlockProducer> newCommittee =
        FixedList.create(state.oracleUpdate().getActiveProducers());
    BpOrchestrationContractState newState =
        new BpOrchestrationContractState(
            state.sessionId() + 1,
            state.oracleUpdate().retryNonce(),
            state.oracleUpdate().thresholdKey(),
            null,
            state.blockProducers(),
            newCommittee,
            state.domainSeparator());
    // This will trigger a new update in case there is enough producers confirmed during the
    // previous update.
    return newState;
  }

  /**
   * Add a broadcast message hash from a sender.
   *
   * @param update update
   * @param sender the sender
   * @param broadcastHash the hash
   * @return updated state.
   */
  public static LargeOracleUpdate addBroadcast(
      LargeOracleUpdate update, BlockchainAddress sender, Hash broadcastHash) {
    int index = update.getCurrentBroadcastRound() - 1;
    BroadcastTracker broadcastTracker = update.broadcastMessages().get(index);
    Bitmap bitmap = Bitmap.create(update.activeProducers().popCount());
    BroadcastTracker updated =
        new BroadcastTracker(
            broadcastTracker.messages().set(sender, broadcastHash),
            broadcastTracker.bitmaps().set(sender, bitmap),
            broadcastTracker.echos());

    return new LargeOracleUpdate(
        update.producers(),
        update.thresholdKey(),
        update.candidates(),
        update.voters(),
        update.heartbeats(),
        update.activeProducers(),
        update.retryNonce(),
        update.broadcastMessages().setElement(index, updated),
        update.signatureRandomization());
  }

  /**
   * Advance the update to the next broadcast round.
   *
   * @param update update
   * @return update that has advanced to the next broadcast round.
   */
  public static LargeOracleUpdate advanceBroadcastRound(LargeOracleUpdate update) {

    Bitmap echos =
        new Bitmap(new LargeByteArray(new byte[update.activeProducers().bits().getLength()]));
    return new LargeOracleUpdate(
        update.producers(),
        update.thresholdKey(),
        update.candidates(),
        update.voters(),
        update.heartbeats(),
        update.activeProducers(),
        update.retryNonce(),
        update
            .broadcastMessages()
            .addElement(new BroadcastTracker(AvlTree.create(), AvlTree.create(), echos)),
        update.signatureRandomization());
  }

  /**
   * Update the bitmap of a producer indicating which broadcast messages the producer has.
   *
   * @param update update
   * @param keeper the producer
   * @param bits the bitmap
   * @return updated state.
   */
  public static LargeOracleUpdate updateBroadcastBits(
      LargeOracleUpdate update, BlockchainAddress keeper, byte[] bits) {
    int index = update.getCurrentBroadcastRound() - 1;
    BroadcastTracker broadcastTracker = update.broadcastMessages().get(index);
    List<BlockchainAddress> activeProducerIdentities =
        update.getActiveProducers().stream().map(BlockProducer::identity).toList();
    broadcastTracker = updateBitmap(broadcastTracker, keeper, bits, activeProducerIdentities);

    FixedList<BroadcastTracker> updated =
        update.broadcastMessages().setElement(index, broadcastTracker);
    return new LargeOracleUpdate(
        update.producers(),
        update.thresholdKey(),
        update.candidates(),
        update.voters(),
        update.heartbeats(),
        update.activeProducers(),
        update.retryNonce(),
        updated,
        update.signatureRandomization());
  }

  static BroadcastTracker updateBitmap(
      BroadcastTracker broadcastTracker,
      BlockchainAddress party,
      byte[] bitmap,
      List<BlockchainAddress> parties) {
    int partyIndex = parties.indexOf(party);
    Bitmap bm = new Bitmap(new LargeByteArray(bitmap));
    AvlTree<BlockchainAddress, Bitmap> updated = broadcastTracker.bitmaps();
    int broadcasterIndex = 0;
    for (BlockchainAddress broadcaster : parties) {
      if (bm.testBit(broadcasterIndex)) {
        Hash hash = broadcastTracker.messages().getValue(broadcaster);
        if (hash != null) {
          Bitmap current = broadcastTracker.bitmaps().getValue(broadcaster);
          updated = updated.set(broadcaster, current.setBit(partyIndex));
        }
      }
      broadcasterIndex++;
    }
    return new BroadcastTracker(
        broadcastTracker.messages(), updated, broadcastTracker.echos().setBit(partyIndex));
  }

  /**
   * Add a proposal from one of the new oracle members as to what the new oracle public key should
   * look like. When proposals have been received from a majority of the producers, then the
   * threshold public key of this new oracle is set to the majority proposal.
   *
   * @param update update
   * @param from the proposer
   * @param key candidate key
   * @param signatureRandomization the random hash
   * @return the update with the new candidate.
   */
  public static LargeOracleUpdate addCandidateKey(
      LargeOracleUpdate update,
      BlockchainAddress from,
      ThresholdKey key,
      Hash signatureRandomization) {
    int blockProducerIndex = getActiveBlockProducerIndex(update, from);

    // add vote for the proposed key
    FixedList<CandidateKey> candidates = Objects.requireNonNull(update.candidates());
    int candidateIndex = getCandidateIndex(update, key);
    if (candidateIndex == -1) {
      candidates = candidates.addElement(new CandidateKey(key, 1));
    } else {
      CandidateKey candidate = candidates.get(candidateIndex);
      candidates =
          candidates.setElement(
              candidateIndex, new CandidateKey(candidate.thresholdKey(), candidate.votes() + 1));
    }

    // register the voter as having voted.
    Bitmap updated = update.voters().setBit(blockProducerIndex);
    int threshold = 2 * update.producers().size() / 3 + 1;
    if (updated.popCount() >= threshold) {
      ThresholdKey majorityKey = findMajorityKey(candidates);
      return new LargeOracleUpdate(
          update.producers(),
          majorityKey,
          candidates,
          updated,
          update.heartbeats(),
          update.activeProducers(),
          update.retryNonce(),
          update.broadcastMessages(),
          signatureRandomization);
    } else {
      return new LargeOracleUpdate(
          update.producers(),
          null,
          candidates,
          updated,
          update.heartbeats(),
          update.activeProducers(),
          update.retryNonce(),
          update.broadcastMessages(),
          null);
    }
  }

  static int getActiveBlockProducerIndex(LargeOracleUpdate update, BlockchainAddress address) {
    return getIndexOf(address, update.getActiveProducers());
  }

  static int getIndexOf(BlockchainAddress address, List<BlockProducer> list) {
    int index = 0;
    for (BlockProducer producer : list) {
      if (producer.identity().equals(address)) {
        return index;
      }
      index++;
    }
    return -1;
  }

  private static int getCandidateIndex(LargeOracleUpdate update, ThresholdKey key) {
    int index = 0;
    for (CandidateKey candidate : update.candidates()) {
      if (candidate.thresholdKey().equals(key)) {
        return index;
      }
      index++;
    }
    return -1;
  }

  static ThresholdKey findMajorityKey(FixedList<CandidateKey> finalCandidates) {
    int majorityVote = 0;
    ThresholdKey thresholdKey = null;
    for (CandidateKey candidate : finalCandidates) {
      int votes = candidate.votes();
      if (votes > majorityVote) {
        majorityVote = votes;
        thresholdKey = candidate.thresholdKey();
      }
    }
    return thresholdKey;
  }

  /**
   * Add a heartbeat from a producer and attempt to select a large enough subset of active producers
   * if possible.
   *
   * @param update update
   * @param sender the sender of the heartbeat
   * @param heartbeat the heartbeat
   * @return an updated large oracle update object.
   */
  public static LargeOracleUpdate addHeartbeat(
      LargeOracleUpdate update, BlockchainAddress sender, byte[] heartbeat) {
    Bitmap heartbeatBitmap = new Bitmap(new LargeByteArray(heartbeat));
    AvlTree<BlockchainAddress, Bitmap> updated = update.heartbeats().set(sender, heartbeatBitmap);

    Bitmap attemptedSelection = attemptSelectionOfProducers(update, updated);
    if (attemptedSelection == null) {
      return new LargeOracleUpdate(
          update.producers(),
          update.thresholdKey(),
          update.candidates(),
          update.voters(),
          updated,
          null,
          update.retryNonce(),
          update.broadcastMessages(),
          update.signatureRandomization());
    }
    Bitmap echos = new Bitmap(new LargeByteArray(new byte[attemptedSelection.bits().getLength()]));
    return new LargeOracleUpdate(
        update.producers(),
        update.thresholdKey(),
        update.candidates(),
        update.voters(),
        updated,
        attemptedSelection,
        update.retryNonce(),
        FixedList.create(List.of(new BroadcastTracker(AvlTree.create(), AvlTree.create(), echos))),
        update.signatureRandomization());
  }

  private static Bitmap attemptSelectionOfProducers(
      LargeOracleUpdate update, AvlTree<BlockchainAddress, Bitmap> heartbeats) {
    if (heartbeats.size() >= 4) {
      return findActiveProducers(update, heartbeats);
    }
    return null;
  }

  private static int getCorruptionThreshold(LargeOracleUpdate update) {
    // (n - 1)/3 is the amount of producers which are assumed to be dishonest
    return (update.producers().size() - 1) / 3;
  }

  private static Bitmap findActiveProducers(
      LargeOracleUpdate update, AvlTree<BlockchainAddress, Bitmap> heartbeats) {
    int n = update.producers().size();
    Bitmap active = Bitmap.create(n);
    for (int i = 0; i < n; i++) {
      int producerIndex = n - i - 1;
      if (hasEnoughHeartbeats(update, heartbeats, producerIndex)) {
        active = active.setBit(producerIndex);
      }
    }
    return active;
  }

  private static boolean hasEnoughHeartbeats(
      LargeOracleUpdate update, AvlTree<BlockchainAddress, Bitmap> heartbeats, int producerIndex) {
    int count = 0;
    for (Bitmap heartbeat : heartbeats.values()) {
      if (heartbeat.testBit(producerIndex)) {
        count++;
      }
    }
    return count > getCorruptionThreshold(update);
  }
}
