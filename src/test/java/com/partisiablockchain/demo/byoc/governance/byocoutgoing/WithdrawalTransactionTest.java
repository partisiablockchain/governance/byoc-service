package com.partisiablockchain.demo.byoc.governance.byocoutgoing;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;

import com.partisiablockchain.math.Unsigned256;
import org.junit.jupiter.api.Test;

/** Tests. */
public final class WithdrawalTransactionTest {

  @Test
  void equalsIgnoringBitmask() {
    WithdrawalTransaction baseline =
        new WithdrawalTransaction(0, "test", Unsigned256.ONE, 0, 0b000);

    assertThat(baseline.equalsIgnoringBitmask(baseline)).isTrue();

    assertThat(baseline.equalsIgnoringBitmask(null)).isFalse();

    WithdrawalTransaction other = new WithdrawalTransaction(0, "test", Unsigned256.ONE, 0, 0b101);
    assertThat(baseline.equalsIgnoringBitmask(other)).isTrue();

    other = new WithdrawalTransaction(1, "test", Unsigned256.ONE, 0, 0b101);
    assertThat(baseline.equalsIgnoringBitmask(other)).isFalse();

    other = new WithdrawalTransaction(0, "test1", Unsigned256.ONE, 0, 0b101);
    assertThat(baseline.equalsIgnoringBitmask(other)).isFalse();

    other = new WithdrawalTransaction(0, "test", Unsigned256.TEN, 0, 0b101);
    assertThat(baseline.equalsIgnoringBitmask(other)).isFalse();

    other = new WithdrawalTransaction(0, "test", Unsigned256.ONE, 1, 0b101);
    assertThat(baseline.equalsIgnoringBitmask(other)).isFalse();
  }
}
