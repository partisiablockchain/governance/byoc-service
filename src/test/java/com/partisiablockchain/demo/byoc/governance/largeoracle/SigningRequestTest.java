package com.partisiablockchain.demo.byoc.governance.largeoracle;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.crypto.Hash;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test. */
public final class SigningRequestTest {

  @Test
  public void compare() {
    Hash hash0 = Hash.create(s -> s.writeInt(1));
    Hash transactionHash0 = Hash.create(s -> s.writeInt(1));
    Hash hash1 = Hash.create(s -> s.writeInt(2));
    Hash transactionHash1 = Hash.create(s -> s.writeInt(2));
    Assertions.assertThat(
            new SigningRequest(0, hash0, transactionHash0)
                .compareTo(new SigningRequest(0, hash1, transactionHash1)))
        .isEqualTo(hash1.compareTo(hash0));
    Assertions.assertThat(
            new SigningRequest(0, hash0, transactionHash0)
                .compareTo(new SigningRequest(1, hash0, transactionHash0)))
        .isEqualTo(1);
    Assertions.assertThat(
            new SigningRequest(0, hash0, transactionHash1)
                .compareTo(new SigningRequest(0, hash0, transactionHash0)))
        .isEqualTo(transactionHash0.compareTo(transactionHash1));
    Assertions.assertThat(
            new SigningRequest(0, hash0, null).compareTo(new SigningRequest(0, hash0, null)))
        .isEqualTo(0);
    Assertions.assertThat(
            new SigningRequest(0, hash0, null)
                .compareTo(new SigningRequest(0, hash0, transactionHash0)))
        .isEqualTo(-1);
  }
}
