package com.partisiablockchain.demo.byoc.governance.largeoracle;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.crypto.KeyPair;
import com.secata.tools.immutable.FixedList;
import java.math.BigInteger;
import java.util.List;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test. */
public final class LargeOracleContractTest {

  private final List<OracleMember> oracles =
      List.of(
          new OracleMember(new KeyPair(BigInteger.ONE).getPublic().createAddress()),
          new OracleMember(new KeyPair(BigInteger.TWO).getPublic().createAddress()),
          new OracleMember(new KeyPair(BigInteger.valueOf(3)).getPublic().createAddress()));

  private final LargeOracleContractState state =
      new LargeOracleContractState(FixedList.create(oracles), null, null, 0, null, null, null);

  @Test
  void getOracleMemberIndex() {
    Assertions.assertThat(state.getOracleMemberIndex(oracles.get(2).address())).isEqualTo(2);
  }
}
