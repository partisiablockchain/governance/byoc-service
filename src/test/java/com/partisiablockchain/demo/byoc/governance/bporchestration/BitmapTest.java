package com.partisiablockchain.demo.byoc.governance.bporchestration;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.serialization.LargeByteArray;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test. */
public final class BitmapTest {

  @Test
  void toStringTest() {
    Bitmap bitmap = new Bitmap(new LargeByteArray(new byte[] {13, 15}));
    Assertions.assertThat(bitmap.toString()).isEqualTo("Bytes [13, 15]");
  }

  @Test
  void allOnes() {
    Assertions.assertThat(Bitmap.allOnes(4).asBytes()).isEqualTo(new byte[] {15});
  }

  @Test
  void and() {
    Bitmap left = new Bitmap(new LargeByteArray(new byte[] {13}));
    Bitmap right = new Bitmap(new LargeByteArray(new byte[] {9}));
    Bitmap and = left.and(right);
    Assertions.assertThat(and.asBytes()).isEqualTo(new byte[] {9});
  }
}
