package com.partisiablockchain.demo.byoc.governance.priceoracle;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.demo.byoc.monitor.RoundData;
import com.partisiablockchain.math.Unsigned256;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test. */
final class PriceOracleRoundDataTest {

  @Test
  void convert() {
    Unsigned256 roundId = Unsigned256.ONE;
    Unsigned256 answer = Unsigned256.create(2);
    Unsigned256 startedAt = Unsigned256.create(3);
    Unsigned256 updatedAt = Unsigned256.create(4);
    Unsigned256 answeredInRound = Unsigned256.create(5);

    RoundData roundData = new RoundData(roundId, answer, startedAt, updatedAt, answeredInRound);

    PriceOracleRoundData priceOracleRoundData =
        new PriceOracleRoundData(roundId, answer, startedAt, updatedAt, answeredInRound);

    PriceOracleRoundData converted = PriceOracleRoundData.convert(roundData);
    Assertions.assertThat(converted.roundId()).isEqualTo(priceOracleRoundData.roundId());
    Assertions.assertThat(converted.answer()).isEqualTo(priceOracleRoundData.answer());
    Assertions.assertThat(converted.startedAt()).isEqualTo(priceOracleRoundData.startedAt());
    Assertions.assertThat(converted.updatedAt()).isEqualTo(priceOracleRoundData.updatedAt());
    Assertions.assertThat(converted.answeredInRound())
        .isEqualTo(priceOracleRoundData.answeredInRound());
  }
}
