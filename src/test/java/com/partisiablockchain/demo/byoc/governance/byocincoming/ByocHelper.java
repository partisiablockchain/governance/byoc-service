package com.partisiablockchain.demo.byoc.governance.byocincoming;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.math.Unsigned256;
import com.secata.tools.immutable.FixedList;
import java.util.List;
import java.util.Objects;

/**
 * Helper class for accessing package-private methods in the <code>
 * com.partisiablockchain.governance.byocincoming</code> package.
 */
public final class ByocHelper {

  /**
   * Set a deposit on a ByocIncomingContractState state.
   *
   * @param state the state to set a deposit on
   * @param deposit the deposit
   * @return updated state.
   */
  public static ByocIncomingContractState setDeposit(
      ByocIncomingContractState state, Deposit deposit) {
    return new ByocIncomingContractState(
        state.depositNonce(),
        state.deposits(),
        state.oracleNodes(),
        deposit,
        state.disputes(),
        state.oracleNonce(),
        state.epochs());
  }

  /**
   * Clear deposit to archive it.
   *
   * @param state the current state
   * @return the updated state
   */
  public static ByocIncomingContractState clearDeposit(ByocIncomingContractState state) {
    return new ByocIncomingContractState(
        state.depositNonce() + 1,
        state.deposits().set(state.depositNonce(), state.deposit()),
        state.oracleNodes(),
        null,
        state.disputes(),
        state.oracleNonce(),
        state.epochs());
  }

  /**
   * Add dispute to state.
   *
   * @param state the current state
   * @param nonce nonce of the dispute
   * @param receiver receiver of the dispute
   * @param amount amount of the dispute
   * @return the updated state
   */
  public static ByocIncomingContractState addDispute(
      ByocIncomingContractState state, long nonce, BlockchainAddress receiver, Unsigned256 amount) {
    Dispute.Transaction transaction = new Dispute.Transaction(nonce, receiver, amount);
    Dispute dispute = new Dispute(FixedList.create(List.of(transaction)));
    long disputeNonce = dispute.claims().get(0).nonce();
    return new ByocIncomingContractState(
        state.depositNonce(),
        state.deposits(),
        state.oracleNodes(),
        state.deposit(),
        state.disputes().set(disputeNonce, dispute),
        state.oracleNonce(),
        state.epochs());
  }

  /**
   * Add counter-claim to state.
   *
   * @param state the current state
   * @param transaction the counter-claim to add
   * @return the updated state
   */
  public static ByocIncomingContractState addCounterClaim(
      ByocIncomingContractState state, DepositTransaction transaction) {
    Dispute.Transaction claim =
        new Dispute.Transaction(transaction.nonce(), transaction.receiver(), transaction.amount());
    long nonce = transaction.nonce();
    Dispute dispute = state.disputes().getValue(nonce);
    Dispute newDispute = new Dispute(Objects.requireNonNull(dispute.claims()).addElement(claim));
    return new ByocIncomingContractState(
        state.depositNonce(),
        state.deposits(),
        state.oracleNodes(),
        state.deposit(),
        state.disputes().set(nonce, newDispute),
        state.oracleNonce(),
        state.epochs());
  }

  /**
   * End the current epoch in state.
   *
   * @param state the current state
   * @return the updated state
   */
  public static ByocIncomingContractState endEpoch(ByocIncomingContractState state) {
    long epochStart;
    if (state.oracleNonce() > 0) {
      epochStart = state.epochs().getValue(state.oracleNonce() - 1L).toDepositNonce();
    } else {
      epochStart = 0L;
    }
    Epoch epoch = new Epoch(epochStart, state.depositNonce());
    return new ByocIncomingContractState(
        state.depositNonce(),
        state.deposits(),
        FixedList.create(),
        state.deposit(),
        state.disputes(),
        state.oracleNonce() + 1,
        state.epochs().set(state.oracleNonce(), epoch));
  }

  /**
   * Start a new epoch in state.
   *
   * @param state the current state
   * @param newOracles oracles for the new epoch
   * @return the updated state
   */
  public static ByocIncomingContractState startEpoch(
      ByocIncomingContractState state, List<BlockchainAddress> newOracles) {
    return new ByocIncomingContractState(
        state.depositNonce(),
        state.deposits(),
        FixedList.create(newOracles),
        state.deposit(),
        state.disputes(),
        state.oracleNonce(),
        state.epochs());
  }
}
