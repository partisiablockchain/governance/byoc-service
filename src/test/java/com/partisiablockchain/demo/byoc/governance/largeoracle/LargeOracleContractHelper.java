package com.partisiablockchain.demo.byoc.governance.largeoracle;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.Signature;
import com.partisiablockchain.tree.AvlTree;
import com.secata.tools.immutable.FixedList;
import java.util.List;
import java.util.Objects;

/** Helper class. */
public final class LargeOracleContractHelper {

  /**
   * Add vote to dispute.
   *
   * @param dispute dispute
   * @param blockProducer the index of the block producer voting
   * @param vote the vote being cast
   * @return the dispute updated with the newly cast vote
   */
  public static Dispute addVote(Dispute dispute, int blockProducer, int vote) {
    FixedList<Integer> updatedVotes =
        Objects.requireNonNull(dispute.votes()).setElement(blockProducer, vote);
    return new Dispute(updatedVotes);
  }

  /**
   * Add a new message that should be signed by the large oracle.
   *
   * @param state state
   * @param message the message that is to be signed
   * @param transactionHash the random hash for the message to be signed
   * @return the updated state
   */
  public static LargeOracleContractState addMessageForSigning(
      LargeOracleContractState state, Hash message, Hash transactionHash) {
    int nonce = state.currentMessageNonce();
    return new LargeOracleContractState(
        state.oracleMembers(),
        state.stakedTokens(),
        Objects.requireNonNull(state.pendingMessages())
            .addElement(new SigningRequest(nonce, message, transactionHash)),
        nonce + 1,
        state.initialMessageNonce(),
        state.signedMessages(),
        state.activeDisputes());
  }

  /**
   * Replace the large oracle and its key with a new one.
   *
   * @param state state
   * @param newOracle the new oracle
   * @return state with a new oracle.
   */
  public static LargeOracleContractState replaceLargeOracle(
      LargeOracleContractState state, List<OracleMember> newOracle) {
    return new LargeOracleContractState(
        FixedList.create(newOracle),
        state.stakedTokens(),
        state.pendingMessages(),
        state.initialMessageNonce(),
        state.initialMessageNonce(),
        state.signedMessages(),
        state.activeDisputes());
  }

  /**
   * Add a finished signature.
   *
   * @param state state
   * @param message the message that was signed
   * @param signature the signature
   * @return the updated state.
   */
  public static LargeOracleContractState addSignature(
      LargeOracleContractState state, SigningRequest message, Signature signature) {
    return new LargeOracleContractState(
        state.oracleMembers(),
        state.stakedTokens(),
        Objects.requireNonNull(state.pendingMessages()).removeElement(message),
        state.currentMessageNonce(),
        state.initialMessageNonce(),
        Objects.requireNonNull(state.signedMessages()).set(message, signature),
        state.activeDisputes());
  }

  /**
   * Add or update a dispute.
   *
   * @param state state
   * @param oracleDisputeId the id of the dispute
   * @param dispute the dispute to set or update
   * @return a state with an updated dispute
   */
  public static LargeOracleContractState setDispute(
      LargeOracleContractState state, OracleDisputeId oracleDisputeId, Dispute dispute) {
    AvlTree<OracleDisputeId, Dispute> updatedDisputes =
        Objects.requireNonNull(state.activeDisputes()).set(oracleDisputeId, dispute);
    return new LargeOracleContractState(
        state.oracleMembers(),
        state.stakedTokens(),
        state.pendingMessages(),
        state.currentMessageNonce(),
        state.initialMessageNonce(),
        state.signedMessages(),
        updatedDisputes);
  }

  /**
   * Stake mpc tokens to this contract for account.
   *
   * @param state state
   * @param account the account staking mpc tokens
   * @param amount the amount of mpc tokens to stake
   * @return a state where the account has stakes on the contract
   */
  public static LargeOracleContractState stakeTokens(
      LargeOracleContractState state,
      BlockchainAddress account,
      long amount,
      Long expirationTimestamp) {
    StakedTokens staked = state.getTokensStakedBy(account);
    return new LargeOracleContractState(
        state.oracleMembers(),
        state
            .stakedTokens()
            .set(
                account,
                new StakedTokens(
                    staked.freeTokens() + amount, staked.reservedTokens(), expirationTimestamp)),
        state.pendingMessages(),
        state.currentMessageNonce(),
        state.initialMessageNonce(),
        state.signedMessages(),
        state.activeDisputes());
  }

  /**
   * Set reserved MPC tokens for the account.
   *
   * @param state current state
   * @param account account to set reserved tokens for
   * @param amount amount of reserved tokens
   * @return updated state
   */
  public static LargeOracleContractState reserveTokens(
      LargeOracleContractState state, BlockchainAddress account, long amount) {
    StakedTokens staked = state.getTokensStakedBy(account);
    return new LargeOracleContractState(
        state.oracleMembers(),
        state
            .stakedTokens()
            .set(
                account,
                new StakedTokens(
                    staked.freeTokens(),
                    staked.reservedTokens() + amount,
                    staked.expirationTimestamp())),
        state.pendingMessages(),
        state.currentMessageNonce(),
        state.initialMessageNonce(),
        state.signedMessages(),
        state.activeDisputes());
  }
}
