package com.partisiablockchain.demo.byoc.governance.bporchestration;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.KeyPair;
import com.partisiablockchain.serialization.LargeByteArray;
import com.partisiablockchain.tree.AvlTree;
import com.secata.tools.immutable.FixedList;
import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.HexFormat;
import java.util.List;
import java.util.Map;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test. */
public final class LargeOracleUpdateTest {

  List<BlockProducer> producers = createProducers(4);

  @Test
  void getBroadcasters() {
    LargeOracleUpdate update =
        new LargeOracleUpdate(
            FixedList.create(List.of()),
            null,
            FixedList.create(),
            null,
            AvlTree.create(),
            null,
            0,
            FixedList.create(List.of()),
            null);
    Assertions.assertThat(update.getBroadcasters(0)).isNull();
  }

  @Test
  void fieldVariables() {
    BlockchainPublicKey key = new KeyPair(BigInteger.ONE).getPublic();
    AvlTree<BlockchainAddress, Bitmap> heart =
        AvlTree.create(Map.of(key.createAddress(), Bitmap.allOnes(2)));
    Bitmap activeProducers = Bitmap.create(2);
    BroadcastTracker broadcastTracker = new BroadcastTracker(null, null, null);
    FixedList<BroadcastTracker> broadcastMessages = FixedList.create(List.of(broadcastTracker));
    LargeOracleUpdate update =
        new LargeOracleUpdate(
            FixedList.create(List.of()),
            null,
            FixedList.create(),
            null,
            heart,
            activeProducers,
            0,
            broadcastMessages,
            null);
    Assertions.assertThat(update.heartbeats()).isEqualTo(heart);
    Assertions.assertThat(update.activeProducers()).isEqualTo(activeProducers);
    Assertions.assertThat(update.broadcastMessages()).isEqualTo(broadcastMessages);
    Assertions.assertThat(update.candidates().size()).isEqualTo(0);
  }

  @Test
  public void elevenPartiesVotes() {
    byte[] bytes = {(byte) 0b11111110, (byte) 0b0000001};

    List<BlockProducer> producers = createProducers(11);
    LargeOracleUpdate update = create(producers);
    for (int i = 0; i < 4; i++) {
      update =
          BpOrchestrationContractHelper.addHeartbeat(update, producers.get(i).identity(), bytes);
    }
    for (int i = 4; i < 7; i++) {
      update =
          BpOrchestrationContractHelper.addHeartbeat(
              update, producers.get(i).identity(), new byte[] {1, 0});
    }
    Assertions.assertThat(update.getActiveProducers())
        .containsExactlyInAnyOrder(
            producers.get(1),
            producers.get(2),
            producers.get(3),
            producers.get(4),
            producers.get(5),
            producers.get(6),
            producers.get(7),
            producers.get(8));
  }

  @Test
  void messageForSigningIsSameAsOnEth() {
    List<BlockProducer> referenceNodes =
        getProducersWithDifferentKeys(
            List.of(
                "94110db99c8b65f046b7bffb47cf64416be16faff279498af9dbf23cd996848573459cf3b7c9ecd7"
                    + "2bcd961e4630b3dda08f17472861cc0ec3afa7b95478589d",
                "ff35f4cd2e3afdced67ce0423667947dd419c3745a55dda2e879c34c919601dc67010ecb67f288ea"
                    + "17317e5446533e793b4e2d30d1ee594363408ea2a695a858",
                "e6d42c0f9357c1918f764cac9286b0e9b7131fb3dcc54cf33049a6b8f6f23f8ebfb922b8bf6feba6"
                    + "5e7b05e17152f083a5055300032b598f3133fc5f58ac611e",
                "a670c71063f0e6ab2808b7efe25de45e0fe644abc70af74f8b4d0f9a1c24a8e1db6e2fe1775a48e9"
                    + "5d635a9f3080427665f5804a72c6b9719e2adc5bd9e955fa"));
    ThresholdKey largeOracleKey =
        new ThresholdKey(
            hexToKey(
                "c1144469c993c1a0382e7e51d361d5844c764ebcdf3774c70721756668b08ac19eb717772fdbd7d6"
                    + "6c6cdc7a515d90ceec20bf2e4488a5365e61329cb69b6513"),
            "keyId");
    LargeOracleUpdate update =
        new LargeOracleUpdate(
            FixedList.create(referenceNodes),
            largeOracleKey,
            null,
            null,
            null,
            new Bitmap(new LargeByteArray(new byte[] {0b1111})),
            0,
            FixedList.create(List.of()),
            null);
    Hash domainSeparator =
        Hash.fromString("5f02a327a959a08bee16235910ad712c36507992cb310c9e064c96f27d92a3e7");
    // for ethereum, the extraData is the address of the large oracle contract on ethereum.
    Hash computed = update.getMessageToBeSigned(1, domainSeparator);
    String expected = "c678b45a327b6b8465fef43d06f679ebc73ba74d07f9223e19eff610b5eebcba";
    Assertions.assertThat(computed.toString()).isEqualTo(expected);
  }

  @Test
  public void findMajorityTest() {
    BlockchainPublicKey good = new KeyPair(BigInteger.TEN).getPublic();
    BlockchainPublicKey bad = new KeyPair(BigInteger.TWO).getPublic();
    findMajority(List.of(bad, good, good), good);
    findMajority(List.of(good, bad, good), good);
    findMajority(List.of(good, good, good), good);
  }

  @Test
  public void sevenPartiesVotes() {
    List<BlockProducer> producers = createProducers(7);
    LargeOracleUpdate update = create(producers);
    update =
        BpOrchestrationContractHelper.addHeartbeat(
            update, producers.get(0).identity(), new byte[] {0b1110110});
    update =
        BpOrchestrationContractHelper.addHeartbeat(
            update, producers.get(1).identity(), new byte[] {0b1110110});
    update =
        BpOrchestrationContractHelper.addHeartbeat(
            update, producers.get(2).identity(), new byte[] {0b1110110});
    update =
        BpOrchestrationContractHelper.addHeartbeat(
            update, producers.get(3).identity(), new byte[] {0b1110110});
    update =
        BpOrchestrationContractHelper.addHeartbeat(
            update, producers.get(4).identity(), new byte[] {0b1110110});
    Assertions.assertThat(update.getActiveProducers())
        .containsExactlyInAnyOrder(
            producers.get(1),
            producers.get(2),
            producers.get(4),
            producers.get(5),
            producers.get(6));
    Assertions.assertThat(update.getHeartbeat(producers.get(4).identity()).asBytes())
        .isEqualTo(new byte[] {0b1110110});
  }

  @Test
  public void votingOnSameKeyButDifferentKeyId() {
    LargeOracleUpdate largeOracleUpdate = create(producers);
    BlockchainPublicKey key = new KeyPair(BigInteger.TEN).getPublic();
    largeOracleUpdate = markAllActive(largeOracleUpdate, producers);
    String keyId = "keyId";
    LargeOracleUpdate oneCandidateKeyOracleUpdate =
        BpOrchestrationContractHelper.addCandidateKey(
            largeOracleUpdate, producers.get(0).identity(), new ThresholdKey(key, keyId), null);
    LargeOracleUpdate differentKeyIdOracleUpdate =
        BpOrchestrationContractHelper.addCandidateKey(
            oneCandidateKeyOracleUpdate,
            producers.get(1).identity(),
            new ThresholdKey(key, "notKeyId"),
            null);
    Assertions.assertThat(differentKeyIdOracleUpdate.thresholdKey()).isNull();
    Assertions.assertThat(differentKeyIdOracleUpdate.candidates()).hasSize(2);
    LargeOracleUpdate twoCandidateKeysOracleUpdate =
        BpOrchestrationContractHelper.addCandidateKey(
            differentKeyIdOracleUpdate,
            producers.get(2).identity(),
            new ThresholdKey(key, keyId),
            null);
    Assertions.assertThat(twoCandidateKeysOracleUpdate.thresholdKey().key()).isEqualTo(key);
    Assertions.assertThat(twoCandidateKeysOracleUpdate.thresholdKey().keyId()).isEqualTo(keyId);
  }

  private void findMajority(List<BlockchainPublicKey> candidates, BlockchainPublicKey expected) {
    LargeOracleUpdate update = create(producers);
    update = markAllActive(update, producers);
    for (int i = 0; i < candidates.size(); i++) {
      BlockProducer producer = producers.get(i);
      update =
          BpOrchestrationContractHelper.addCandidateKey(
              update,
              producer.identity(),
              new ThresholdKey(candidates.get(i), "candidates.get(i).toString()"),
              null);
    }
    Assertions.assertThat(update.thresholdKey().key()).isEqualTo(expected);
    Assertions.assertThat(update.voters()).isNotNull();
    Assertions.assertThat(update.voters().asBytes().length).isEqualTo(producers.size() / 8 + 1);
  }

  static LargeOracleUpdate markAllActive(LargeOracleUpdate update, List<BlockProducer> producers) {
    byte[] bitmap = createAllOnesBitmap(producers);
    for (BlockProducer producer : producers) {
      update = BpOrchestrationContractHelper.addHeartbeat(update, producer.identity(), bitmap);
    }
    return update;
  }

  static byte[] createAllOnesBitmap(List<BlockProducer> producers) {
    int n = producers.size();
    int m = n / 8 + 1;
    byte[] bitmap = new byte[m];
    for (int i = 0; i < m; i++) {
      bitmap[i] = -1;
    }
    bitmap[bitmap.length - 1] = (byte) ((1 << n % 8) - 1);
    return bitmap;
  }

  private List<BlockProducer> getProducersWithDifferentKeys(List<String> hexPublicKeys) {
    List<BlockProducer> updated = new ArrayList<>();
    for (int i = 0; i < hexPublicKeys.size(); i++) {
      BlockchainPublicKey ecPoint = hexToKey(hexPublicKeys.get(i));
      BlockProducer producer = producers.get(i);
      updated.add(new BlockProducer(producer.identity(), ecPoint));
    }
    return updated;
  }

  private LargeOracleUpdate create(List<BlockProducer> producers) {
    return new LargeOracleUpdate(
        FixedList.create(producers),
        null,
        FixedList.create(List.of()),
        Bitmap.create(producers.size()),
        AvlTree.create(),
        null,
        0,
        FixedList.create(),
        null);
  }

  private BlockchainPublicKey hexToKey(String hex) {
    return BlockchainPublicKey.fromEncodedEcPoint(HexFormat.of().parseHex("04" + hex));
  }

  static List<BlockProducer> createProducers(int numberOfProducers) {
    List<BlockProducer> producers = new ArrayList<>();
    for (int i = 0; i < numberOfProducers; i++) {
      String uniqueName = i + " " + System.currentTimeMillis();
      Hash hash = Hash.create(s -> s.writeString(uniqueName));
      SecureRandom secureRandom = new SecureRandom(hash.getBytes());
      KeyPair keyPair = new KeyPair(new BigInteger(32, secureRandom));
      producers.add(new BlockProducer(keyPair.getPublic().createAddress(), keyPair.getPublic()));
    }
    return producers;
  }
}
