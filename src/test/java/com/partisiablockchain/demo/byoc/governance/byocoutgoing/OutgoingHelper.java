package com.partisiablockchain.demo.byoc.governance.byocoutgoing;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.KeyPair;
import com.partisiablockchain.crypto.Signature;
import com.partisiablockchain.math.Unsigned256;
import com.partisiablockchain.serialization.LargeByteArray;
import com.partisiablockchain.tree.AvlTree;
import com.secata.stream.SafeDataInputStream;
import com.secata.tools.immutable.FixedList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import org.bouncycastle.util.encoders.Hex;

/**
 * Helper class for accessing package-private methods in the <code>
 * com.partisiablockchain.governance.byocoutgoing</code> package.
 */
public final class OutgoingHelper {

  /**
   * creates an ethereum address from string.
   *
   * @param hexAddress ethereum address as string.
   * @return ethereum address
   */
  public static EthereumAddress ethereumAddress(String hexAddress) {
    byte[] data = Hex.decode(hexAddress);
    LargeByteArray identifier = new LargeByteArray(data);
    return new EthereumAddress(identifier);
  }

  /**
   * Reads an ethereum address from the stream.
   *
   * @param stream to read
   * @return ethereum address
   */
  public static String readEthereumAddress(SafeDataInputStream stream) {
    byte[] data = stream.readBytes(20);
    return Hex.toHexString(data);
  }

  /**
   * Update the withdrawal on the state with a valid signature.
   *
   * @param currentState the current state
   * @param nonce the nonce of the withdrawal
   * @param oracle signer
   * @param messageDigest digest to sign
   * @return the update state
   */
  public static ByocOutgoingContractState signWithdrawal(
      ByocOutgoingContractState currentState, long nonce, KeyPair oracle, Hash messageDigest) {
    BlockchainAddress oracleAddress = oracle.getPublic().createAddress();
    Signature signature = oracle.sign(messageDigest);
    return addSignature(currentState, oracleAddress, 0L, nonce, signature);
  }

  /**
   * Add new dispute to the state.
   *
   * @param currentState current state
   * @param transaction transaction to dispute
   * @return the updated state
   */
  public static ByocOutgoingContractState addDispute(
      ByocOutgoingContractState currentState, WithdrawalTransaction transaction) {
    EthereumAddress receiver = ethereumAddress(transaction.destination());
    Dispute.Transaction disputeTx =
        new Dispute.Transaction(receiver, transaction.amount(), transaction.bitmask());
    Dispute dispute = new Dispute(FixedList.create(List.of(disputeTx)));
    return createDispute(
        currentState,
        new WithdrawId(transaction.withdrawalNonce(), transaction.oracleNonce()),
        dispute);
  }

  /**
   * Create a new pending withdrawal.
   *
   * @param state state
   * @param receiver the ethereum address which the coins should be sent to
   * @param amount the amount of coins to send before tax
   * @return the new state containing the new withdrawal
   */
  public static ByocOutgoingContractState createPendingWithdrawal(
      ByocOutgoingContractState state, EthereumAddress receiver, Unsigned256 amount) {
    Withdrawal withdrawal =
        new Withdrawal(receiver, amount, FixedList.create(Collections.nCopies(3, null)));
    Epoch epoch = state.epochs().getValue(state.currentEpoch());
    return new ByocOutgoingContractState(
        state.byocContract(),
        state.withdrawNonce() + 1,
        state.oracleNonce(),
        state.currentEpoch(),
        state
            .epochs()
            .set(
                state.currentEpoch(),
                new Epoch(
                    epoch.withdrawals().set(state.withdrawNonce(), withdrawal),
                    epoch.disputes(),
                    epoch.oracles())));
  }

  /**
   * Start a new epoch be updating the oracle nodes.
   *
   * @param state state
   * @param newOracles the new oracle nodes
   * @return the new state with the new oracles
   */
  public static ByocOutgoingContractState startNewEpoch(
      ByocOutgoingContractState state, List<OracleMember> newOracles) {
    long enabledOracle = state.oracleNonce() + 1;
    Epoch epoch = state.epochs().getValue(enabledOracle);
    return new ByocOutgoingContractState(
        state.byocContract(),
        state.withdrawNonce(),
        enabledOracle,
        state.currentEpoch(),
        state
            .epochs()
            .set(
                enabledOracle,
                new Epoch(epoch.withdrawals(), epoch.disputes(), FixedList.create(newOracles))));
  }

  /**
   * Add counter-claim to current dispute being voted on.
   *
   * @param state state
   * @param disputeId the id of the disputed transaction
   * @param transaction transaction on external chain
   * @return updated state with new counter-claim
   */
  public static ByocOutgoingContractState addCounterClaim(
      ByocOutgoingContractState state, WithdrawId disputeId, Dispute.Transaction transaction) {
    Epoch epoch = state.epochs().getValue(disputeId.oracleNonce());
    Dispute dispute = epoch.disputes().getValue(disputeId.withdrawalNonce());
    Dispute newDispute =
        new Dispute(Objects.requireNonNull(dispute.claims()).addElement(transaction));
    Epoch newEpoch =
        new Epoch(
            epoch.withdrawals(),
            epoch.disputes().set(disputeId.withdrawalNonce(), newDispute),
            epoch.oracles());
    return new ByocOutgoingContractState(
        state.byocContract(),
        state.withdrawNonce(),
        state.oracleNonce(),
        state.currentEpoch(),
        state.epochs().set(disputeId.oracleNonce(), newEpoch));
  }

  /**
   * Add counter-claim to the state.
   *
   * @param state current state
   * @param transaction the counter-claim transaction
   * @return the updated state
   */
  public static ByocOutgoingContractState addCounterClaim(
      ByocOutgoingContractState state, WithdrawalTransaction transaction) {
    EthereumAddress receiver = ethereumAddress(transaction.destination());
    Dispute.Transaction claim =
        new Dispute.Transaction(receiver, transaction.amount(), transaction.bitmask());
    return addCounterClaim(
        state, new WithdrawId(transaction.withdrawalNonce(), transaction.oracleNonce()), claim);
  }

  /**
   * Add a new signature to a pending withdrawal.
   *
   * @param state state
   * @param from the node sending the signature
   * @param oracleNonce the nonce of the oracle responsible for the withdrawal
   * @param nonce the withdraw nonce
   * @param signature the signature to add
   * @return the updated state
   */
  public static ByocOutgoingContractState addSignature(
      ByocOutgoingContractState state,
      BlockchainAddress from,
      long oracleNonce,
      long nonce,
      Signature signature) {

    Epoch epoch = state.epochs().getValue(oracleNonce);
    int nodeIndex = epoch.oracles().stream().map(OracleMember::identity).toList().indexOf(from);
    Withdrawal withdrawal = epoch.withdrawals().getValue(nonce);
    Withdrawal newWithdrawal =
        new Withdrawal(
            withdrawal.receiver(),
            withdrawal.amount(),
            withdrawal.signatures().setElement(nodeIndex, signature));
    return new ByocOutgoingContractState(
        state.byocContract(),
        state.withdrawNonce(),
        oracleNonce,
        state.currentEpoch(),
        state
            .epochs()
            .set(
                oracleNonce,
                new Epoch(
                    epoch.withdrawals().set(nonce, newWithdrawal),
                    epoch.disputes(),
                    epoch.oracles())));
  }

  /**
   * Set a new active dispute.
   *
   * @param state state
   * @param withdrawId the disputed withdrawal
   * @param dispute the new active dispute
   * @return a state with an active dispute
   */
  public static ByocOutgoingContractState createDispute(
      ByocOutgoingContractState state, WithdrawId withdrawId, Dispute dispute) {
    Epoch epoch = state.epochs().getValue(withdrawId.oracleNonce());
    return new ByocOutgoingContractState(
        state.byocContract(),
        state.withdrawNonce(),
        state.oracleNonce(),
        state.currentEpoch(),
        state
            .epochs()
            .set(
                withdrawId.oracleNonce(),
                new Epoch(
                    epoch.withdrawals(),
                    epoch.disputes().set(withdrawId.withdrawalNonce(), dispute),
                    epoch.oracles())));
  }

  /**
   * End the current epoch. Disable oracles, until it has been updated by the large oracle.
   *
   * @param state state
   * @return a state with a new epoch and no current oracle nodes
   */
  public static ByocOutgoingContractState endCurrentEpoch(ByocOutgoingContractState state) {
    long nextEpoch = state.currentEpoch() + 1;
    return new ByocOutgoingContractState(
        state.byocContract(),
        0,
        state.oracleNonce(),
        nextEpoch,
        state
            .epochs()
            .set(state.currentEpoch(), state.epochs().getValue(state.currentEpoch()))
            .set(nextEpoch, new Epoch(AvlTree.create(), AvlTree.create(), null)));
  }
}
