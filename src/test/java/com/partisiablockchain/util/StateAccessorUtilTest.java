package com.partisiablockchain.util;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.contract.StateSerializableEquality;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.KeyPair;
import com.partisiablockchain.crypto.Signature;
import com.partisiablockchain.demo.byoc.governance.bporchestration.Bitmap;
import com.partisiablockchain.demo.byoc.governance.bporchestration.BlockProducer;
import com.partisiablockchain.demo.byoc.governance.bporchestration.BpOrchestrationContractState;
import com.partisiablockchain.demo.byoc.governance.bporchestration.BroadcastTracker;
import com.partisiablockchain.demo.byoc.governance.bporchestration.CandidateKey;
import com.partisiablockchain.demo.byoc.governance.bporchestration.LargeOracleUpdate;
import com.partisiablockchain.demo.byoc.governance.bporchestration.ThresholdKey;
import com.partisiablockchain.demo.byoc.governance.byocincoming.ByocIncomingContractState;
import com.partisiablockchain.demo.byoc.governance.byocincoming.Deposit;
import com.partisiablockchain.demo.byoc.governance.byocincoming.Dispute;
import com.partisiablockchain.demo.byoc.governance.byocincoming.Epoch;
import com.partisiablockchain.demo.byoc.governance.byocoutgoing.ByocOutgoingContractState;
import com.partisiablockchain.demo.byoc.governance.byocoutgoing.EthereumAddress;
import com.partisiablockchain.demo.byoc.governance.byocoutgoing.Withdrawal;
import com.partisiablockchain.demo.byoc.governance.largeoracle.LargeOracleContractState;
import com.partisiablockchain.demo.byoc.governance.largeoracle.OracleDisputeId;
import com.partisiablockchain.demo.byoc.governance.largeoracle.OracleMember;
import com.partisiablockchain.demo.byoc.governance.largeoracle.SigningRequest;
import com.partisiablockchain.demo.byoc.governance.largeoracle.StakedTokens;
import com.partisiablockchain.demo.byoc.governance.priceoracle.ChallengePeriod;
import com.partisiablockchain.demo.byoc.governance.priceoracle.DisputeInfo;
import com.partisiablockchain.demo.byoc.governance.priceoracle.PriceOracleContractState;
import com.partisiablockchain.demo.byoc.governance.priceoracle.PriceOracleNodes;
import com.partisiablockchain.demo.byoc.governance.priceoracle.PriceUpdates;
import com.partisiablockchain.demo.byoc.governance.priceoracle.RegistrationStatus;
import com.partisiablockchain.demo.byoc.governance.priceoracle.RoundPriceUpdates;
import com.partisiablockchain.demo.byoc.monitor.ContractStateHandler;
import com.partisiablockchain.math.Unsigned256;
import com.partisiablockchain.serialization.LargeByteArray;
import com.partisiablockchain.serialization.StateSerializable;
import com.partisiablockchain.tree.AvlTree;
import com.secata.tools.immutable.FixedList;
import java.math.BigInteger;
import java.util.List;
import java.util.Map;
import org.junit.jupiter.api.Test;

/** Test. */
final class StateAccessorUtilTest {
  private final Hash hash = Hash.create(h -> h.writeInt(123));
  private final BlockchainAddress blockchainAddress =
      BlockchainAddress.fromHash(BlockchainAddress.Type.ACCOUNT, hash);

  final ContractStateHandler contractStateHandler =
      new ContractStateHandler(List.of(blockchainAddress));

  @Test
  void bpOrchestrationContract() {
    final KeyPair keyPair = new KeyPair(BigInteger.ONE);
    final ThresholdKey thresholdKey = new ThresholdKey(keyPair.getPublic(), "id");
    final FixedList<BlockProducer> committee =
        FixedList.create(List.of(new BlockProducer(blockchainAddress, keyPair.getPublic())));
    final FixedList<CandidateKey> candidates =
        FixedList.create(List.of(new CandidateKey(thresholdKey, 2)));
    final Bitmap voters = Bitmap.create(3);
    final AvlTree<BlockchainAddress, Bitmap> heartbeats =
        AvlTree.create(Map.of(blockchainAddress, voters));
    final Bitmap producers = Bitmap.create(2);
    final AvlTree<BlockchainAddress, Hash> messages =
        AvlTree.create(Map.of(blockchainAddress, hash));
    final FixedList<BroadcastTracker> broadcastTrackers =
        FixedList.create(List.of(new BroadcastTracker(messages, heartbeats, voters)));
    final LargeOracleUpdate largeOracleUpdate =
        new LargeOracleUpdate(
            committee,
            thresholdKey,
            candidates,
            voters,
            heartbeats,
            producers,
            2,
            broadcastTrackers,
            hash);
    final AvlTree<BlockchainAddress, BlockProducer> blockProducers =
        AvlTree.create(
            Map.of(blockchainAddress, new BlockProducer(blockchainAddress, keyPair.getPublic())));

    BpOrchestrationContractState state =
        new BpOrchestrationContractState(
            9, 3, thresholdKey, largeOracleUpdate, blockProducers, committee, hash);

    contractStateHandler.createContract(null, null, blockchainAddress, null, state);

    BpOrchestrationContractState fromStateAccessor =
        StateAccessorUtil.createContractStateRecord(
            BpOrchestrationContractState.class,
            contractStateHandler.contractStates.get(blockchainAddress));

    StateSerializableEquality.assertStatesEqual(fromStateAccessor, state);
  }

  @Test
  void byocIncomingContract() {
    final Deposit deposit = new Deposit(blockchainAddress, Unsigned256.TEN, 2);
    final AvlTree<Long, Deposit> deposits = AvlTree.create(Map.of(2L, deposit));
    final Dispute dispute =
        new Dispute(
            FixedList.create(
                List.of(new Dispute.Transaction(1L, blockchainAddress, Unsigned256.ONE))));
    final Epoch epoch = new Epoch(1L, 2L);
    final AvlTree<Long, Dispute> disputes = AvlTree.create(Map.of(3L, dispute));
    final AvlTree<Long, Epoch> epochs = AvlTree.create(Map.of(2L, epoch));

    ByocIncomingContractState state =
        new ByocIncomingContractState(
            5,
            deposits,
            FixedList.create(List.of(blockchainAddress)),
            deposit,
            disputes,
            3,
            epochs);

    contractStateHandler.createContract(null, null, blockchainAddress, null, state);

    ByocIncomingContractState fromStateAccessor =
        StateAccessorUtil.createContractStateRecord(
            ByocIncomingContractState.class,
            contractStateHandler.contractStates.get(blockchainAddress));

    StateSerializableEquality.assertStatesEqual(fromStateAccessor, state);
  }

  @Test
  void byocOutgoingContract() {
    final EthereumAddress ethereumAddress = new EthereumAddress(new LargeByteArray(new byte[5]));
    final AvlTree<Long, Withdrawal> withdrawals =
        AvlTree.create(
            Map.of(
                1L,
                new Withdrawal(
                    ethereumAddress,
                    Unsigned256.ONE,
                    FixedList.create(List.of(new Signature(3, BigInteger.ONE, BigInteger.TWO))))));
    final AvlTree<Long, com.partisiablockchain.demo.byoc.governance.byocoutgoing.Epoch> epochs =
        AvlTree.create(
            Map.of(
                1L,
                new com.partisiablockchain.demo.byoc.governance.byocoutgoing.Epoch(
                    withdrawals, AvlTree.create(), FixedList.create())));

    ByocOutgoingContractState state =
        new ByocOutgoingContractState(ethereumAddress, 1, 1, 1, epochs);

    contractStateHandler.createContract(null, null, blockchainAddress, null, state);

    ByocOutgoingContractState fromStateAccessor =
        StateAccessorUtil.createContractStateRecord(
            ByocOutgoingContractState.class,
            contractStateHandler.contractStates.get(blockchainAddress));

    StateSerializableEquality.assertStatesEqual(fromStateAccessor, state);
  }

  @Test
  void largeOracleContract() {
    final AvlTree<BlockchainAddress, StakedTokens> stakedTokens =
        AvlTree.create(Map.of(blockchainAddress, new StakedTokens(3, 0, null)));
    final SigningRequest signingRequest = new SigningRequest(2, hash, hash);
    final FixedList<SigningRequest> pendingMessages = FixedList.create(List.of(signingRequest));
    final AvlTree<SigningRequest, Signature> signedMessages =
        AvlTree.create(Map.of(signingRequest, new Signature(3, BigInteger.ONE, BigInteger.TWO)));
    final com.partisiablockchain.demo.byoc.governance.largeoracle.Dispute dispute =
        new com.partisiablockchain.demo.byoc.governance.largeoracle.Dispute(
            FixedList.create(List.of(1, 2)));
    final AvlTree<OracleDisputeId, com.partisiablockchain.demo.byoc.governance.largeoracle.Dispute>
        activeDisputes =
            AvlTree.create(Map.of(new OracleDisputeId(blockchainAddress, 2L, 3L), dispute));

    LargeOracleContractState state =
        new LargeOracleContractState(
            FixedList.create(List.of(new OracleMember(blockchainAddress))),
            stakedTokens,
            pendingMessages,
            3,
            5,
            signedMessages,
            activeDisputes);

    contractStateHandler.createContract(null, null, blockchainAddress, null, state);

    LargeOracleContractState fromStateAccessor =
        StateAccessorUtil.createContractStateRecord(
            LargeOracleContractState.class,
            contractStateHandler.contractStates.get(blockchainAddress));

    StateSerializableEquality.assertStatesEqual(fromStateAccessor, state);
  }

  @Test
  void priceOracleContract() {
    final AvlTree<Unsigned256, PriceUpdates> roundPriceUpdates =
        AvlTree.create(Map.of(Unsigned256.ONE, new PriceUpdates(AvlTree.create())));

    ChallengePeriod challengePeriod = new ChallengePeriod(Unsigned256.ONE, 0);

    PriceOracleContractState state =
        new PriceOracleContractState(
            blockchainAddress,
            null,
            new RoundPriceUpdates(roundPriceUpdates),
            Unsigned256.ONE,
            "test",
            new PriceOracleNodes(
                AvlTree.create(Map.of(blockchainAddress, RegistrationStatus.PENDING))),
            challengePeriod,
            new DisputeInfo(null, false, null),
            "ChainId");

    contractStateHandler.createContract(null, null, blockchainAddress, null, state);

    PriceOracleContractState fromStateAccessor =
        StateAccessorUtil.createContractStateRecord(
            PriceOracleContractState.class,
            contractStateHandler.contractStates.get(blockchainAddress));

    StateSerializableEquality.assertStatesEqual(fromStateAccessor, state);
  }

  @Test
  void booleanSupport() {
    BooleanState state = new BooleanState(true);
    BooleanState fromStateAccessor =
        StateAccessorUtil.createContractStateRecord(BooleanState.class, state);
    StateSerializableEquality.assertStatesEqual(fromStateAccessor, state);
  }

  record BooleanState(boolean b) implements StateSerializable {}

  @Test
  void enumSupport() {
    EnumState state = new EnumState(TestEnum.B);
    EnumState fromStateAccessor =
        StateAccessorUtil.createContractStateRecord(EnumState.class, state);
    StateSerializableEquality.assertStatesEqual(fromStateAccessor, state);

    AnotherEnumState anotherFromStateAccessor =
        StateAccessorUtil.createContractStateRecord(AnotherEnumState.class, state);
    assertThat(anotherFromStateAccessor).isNotNull();
    assertThat(anotherFromStateAccessor.e).isEqualTo(AnotherTestEnum.B);
  }

  record EnumState(TestEnum e) implements StateSerializable {}

  record AnotherEnumState(AnotherTestEnum e) implements StateSerializable {}

  enum TestEnum {
    A,
    B
  }

  enum AnotherTestEnum {
    A,
    B
  }
}
