package com.partisiablockchain.crypto;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.secata.tools.coverage.ExceptionConverter;
import java.math.BigInteger;
import java.util.Arrays;
import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

/** Helper for creating objects for encrypting and decrypting. */
public final class AesHelper {

  private AesHelper() {}

  /**
   * Create AES for encrypt.
   *
   * @param privateKey private key
   * @param publicKey public key
   * @param extraData any extra data
   * @return AES for encrypt
   */
  public static Cipher createAesForEncrypt(
      BigInteger privateKey, BlockchainPublicKey publicKey, byte[] extraData) {
    return createAes(privateKey, publicKey, false, extraData);
  }

  /**
   * Create AES for decrypt.
   *
   * @param privateKey private key
   * @param publicKey public key
   * @param extraData any extra data
   * @return AES for decrypt
   */
  public static Cipher createAesForDecrypt(
      BigInteger privateKey, BlockchainPublicKey publicKey, byte[] extraData) {
    return createAes(privateKey, publicKey, true, extraData);
  }

  private static Cipher createAes(
      BigInteger privateKey, BlockchainPublicKey publicKey, boolean decrypt, byte[] extraData) {
    Hash shared = publicKey.deriveSharedKey(privateKey, extraData);
    return ExceptionConverter.call(
        () -> {
          Cipher aes = Cipher.getInstance("AES/CBC/PKCS5Padding");
          byte[] sharedBytes = shared.getBytes();
          IvParameterSpec ivSpec = new IvParameterSpec(Arrays.copyOfRange(sharedBytes, 0, 16));
          SecretKeySpec skSpec = new SecretKeySpec(Arrays.copyOfRange(sharedBytes, 16, 32), "AES");
          aes.init(decrypt ? Cipher.DECRYPT_MODE : Cipher.ENCRYPT_MODE, skSpec, ivSpec);
          return aes;
        },
        "Unable to initialize aes");
  }
}
