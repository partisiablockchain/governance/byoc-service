package com.partisiablockchain.util;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.serialization.StateAccessor;
import com.partisiablockchain.serialization.StateAccessorAvlLeafNode;
import com.partisiablockchain.serialization.StateSerializable;
import com.partisiablockchain.tree.AvlTree;
import com.secata.tools.coverage.ExceptionConverter;
import com.secata.tools.immutable.FixedList;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.RecordComponent;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * Utility class for creating a state of a given class or accessing states from a state accessor.
 */
public final class StateAccessorUtil {

  private StateAccessorUtil() {}

  /**
   * Create an instance of a contract state class (i.e. a Record) from a contract state class and
   * the corresponding StateSerializable. If no contract state is found, i.e. the StateSerializable
   * is empty, null is returned
   *
   * @param stateClass the class of the contract state
   * @param contractState the StateSerializable the new contract state is created from
   * @param <S> the type of the contract state
   * @return an instance of the contract state class or null if contractState is an empty
   *     StateSerializable
   */
  public static <S> S createContractStateRecord(
      Class<S> stateClass, StateSerializable contractState) {
    StateAccessor accessor = StateAccessor.create(contractState);
    if (accessor.isNull()) { // contract was not registered in the ContractStateHandler
      return null;
    }
    return createRecordStateFromStateAccessor(accessor, stateClass);
  }

  /**
   * Create an instance of a state class (i.e. a Record) from a corresponding state accessor.
   *
   * @param accessor state accessor used to get values
   * @param stateClass the class of the state to create an instance of. Must be a Record
   * @param <S> state class type
   * @return instance of state class populated from state accessor
   */
  private static <S> S createRecordStateFromStateAccessor(
      StateAccessor accessor, Class<S> stateClass) {
    List<Object> initArgs = new ArrayList<>();
    List<Class<?>> parameterTypes = new ArrayList<>();
    // Iterate over record fields
    for (RecordComponent recordComponent : stateClass.getRecordComponents()) {
      // Get the type of the field
      Class<?> type = recordComponent.getType();
      parameterTypes.add(type);

      // If the field is a parameterized type (i.e. AvlTree or FixedList) get the type arguments
      Type[] additionalTypeArguments = new Type[0];
      if (recordComponent.getGenericType() instanceof ParameterizedType) {
        additionalTypeArguments =
            ((ParameterizedType) recordComponent.getGenericType()).getActualTypeArguments();
      }

      // Get value of field from state accessor
      StateAccessor fieldAccessor = accessor.get(recordComponent.getName());
      Object value = getValueOfType(fieldAccessor, type, additionalTypeArguments);
      initArgs.add(value);
    }
    // Create instance of state class
    return ExceptionConverter.call(
        () ->
            stateClass
                .getDeclaredConstructor(parameterTypes.toArray(new Class<?>[0]))
                .newInstance(initArgs.toArray()),
        "Could not create instance of state class from state accessor");
  }

  @SuppressWarnings({"unchecked", "rawtypes", "Immutable", "EnumOrdinal"})
  private static Object getValueOfType(
      StateAccessor accessor, Class<?> type, Type... additionalTypes) {
    if (accessor.isNull()) {
      return null;
    } else if (type.equals(FixedList.class)) {
      Class<?> typeOfFixedList = (Class<?>) additionalTypes[0];
      if (typeOfFixedList.isRecord()) {
        return FixedList.create(
            accessor.getListElements().stream().map(l -> getValueOfType(l, typeOfFixedList)));
      } else {
        return accessor.typedFixedList(typeOfFixedList);
      }
    } else if (type.equals(AvlTree.class)) {
      Type keyType = additionalTypes[0];
      Type valueType = additionalTypes[1];
      AvlTree tree = AvlTree.create();
      for (StateAccessorAvlLeafNode leaf : accessor.getTreeLeaves()) {
        Comparable key = (Comparable) getValueOfType(leaf.getKey(), (Class<?>) keyType);
        Object value = getValueOfType(leaf.getValue(), (Class<?>) valueType);
        tree = tree.set(key, value);
      }
      return tree;
    } else if (type.isRecord()) {
      return createRecordStateFromStateAccessor(accessor, type);
    } else if (type.isEnum()) {
      Enum<?> enumValue = accessor.cast(Enum.class);
      return type.getEnumConstants()[enumValue.ordinal()];
    } else if (type.equals(int.class)) {
      return accessor.intValue();
    } else if (type.equals(long.class)) {
      return accessor.longValue();
    } else if (type.equals(boolean.class)) {
      return accessor.booleanValue();
    } else {
      return accessor.cast(type);
    }
  }
}
