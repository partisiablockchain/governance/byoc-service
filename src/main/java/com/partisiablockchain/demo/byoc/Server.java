package com.partisiablockchain.demo.byoc;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.crypto.KeyPair;
import com.partisiablockchain.demo.byoc.monitor.ByocProcess;
import com.partisiablockchain.demo.byoc.monitor.ByocProcessRunner;
import com.partisiablockchain.demo.byoc.monitor.CpuInformationProvider;
import com.partisiablockchain.demo.byoc.monitor.CpuInformationProviderImpl;
import com.partisiablockchain.demo.byoc.monitor.DepositProcessor;
import com.partisiablockchain.demo.byoc.monitor.DisputeProcess;
import com.partisiablockchain.demo.byoc.monitor.Ethereum;
import com.partisiablockchain.demo.byoc.monitor.InMemoryMessageStorage;
import com.partisiablockchain.demo.byoc.monitor.LargeOracleProcess;
import com.partisiablockchain.demo.byoc.monitor.LargeOracleResource;
import com.partisiablockchain.demo.byoc.monitor.LargeOracleStatusInformation;
import com.partisiablockchain.demo.byoc.monitor.MemoryUsageProvider;
import com.partisiablockchain.demo.byoc.monitor.MemoryUsageProviderImpl;
import com.partisiablockchain.demo.byoc.monitor.PeerDatabase;
import com.partisiablockchain.demo.byoc.monitor.PeerDiscoveryProcess;
import com.partisiablockchain.demo.byoc.monitor.PeerDiscoveryResource;
import com.partisiablockchain.demo.byoc.monitor.PriceOracleChallengePeriodProcess;
import com.partisiablockchain.demo.byoc.monitor.PriceOracleDisputeProcess;
import com.partisiablockchain.demo.byoc.monitor.PriceOracleNotifyProcess;
import com.partisiablockchain.demo.byoc.monitor.PublicProvider;
import com.partisiablockchain.demo.byoc.monitor.StatusResource;
import com.partisiablockchain.demo.byoc.monitor.SystemPublicProvider;
import com.partisiablockchain.demo.byoc.monitor.SystemTimeProvider;
import com.partisiablockchain.demo.byoc.monitor.SystemTimeProviderImpl;
import com.partisiablockchain.demo.byoc.monitor.SystemUptimeProvider;
import com.partisiablockchain.demo.byoc.monitor.UptimeProvider;
import com.partisiablockchain.demo.byoc.monitor.VersionInformation;
import com.partisiablockchain.demo.byoc.monitor.WithdrawalProcess;
import com.partisiablockchain.demo.byoc.settings.DepositSettings;
import com.partisiablockchain.demo.byoc.settings.DisputeSettings;
import com.partisiablockchain.demo.byoc.settings.EthereumSettings;
import com.partisiablockchain.demo.byoc.settings.LargeOracleSettings;
import com.partisiablockchain.demo.byoc.settings.PriceOracleSettings;
import com.partisiablockchain.demo.byoc.settings.Settings;
import com.partisiablockchain.demo.byoc.settings.WithdrawalSettings;
import com.partisiablockchain.governance.MessageStorage;
import com.partisiablockchain.providers.AdditionalStatusProviders;
import com.secata.tools.rest.RestServer;
import com.secata.tools.thread.ShutdownHook;
import java.util.function.Function;
import org.glassfish.hk2.utilities.binding.AbstractBinder;
import org.glassfish.jersey.server.ResourceConfig;

/** Server. */
public final class Server {

  private Server() {}

  /**
   * Default entry method for running a BYOC service.
   *
   * @param configuration the configuration of the service
   * @param shutdownHook shutdown hook for closing processes
   */
  public static void run(Settings configuration, ShutdownHook shutdownHook) {
    if (configuration instanceof DepositSettings settings) {
      runProcess(shutdownHook, settings, DepositProcessor::new);
    } else if (configuration instanceof DisputeSettings settings) {
      runProcess(shutdownHook, settings, DisputeProcess::new);
    } else if (configuration instanceof LargeOracleSettings settings) {
      runLargeOracleProcesses(shutdownHook, settings);
    } else if (configuration instanceof WithdrawalSettings settings) {
      runProcess(shutdownHook, settings, WithdrawalProcess::new);
    } else if (configuration instanceof EthereumSettings settings) {
      runProcess(shutdownHook, settings, Ethereum::create);
    } else if (configuration instanceof PriceOracleSettings settings) {
      runProcess(shutdownHook, settings, PriceOracleNotifyProcess::create);
      runProcess(shutdownHook, settings, PriceOracleDisputeProcess::create);
      runProcess(shutdownHook, settings, PriceOracleChallengePeriodProcess::create);
    } else {
      throw new RuntimeException("Unknown service settings");
    }
  }

  private static <T extends Settings> void runProcess(
      ShutdownHook shutdownHook, T settings, Function<T, ByocProcess> processFactory) {
    ByocProcess process = processFactory.apply(settings);
    ByocProcessRunner processRunner =
        new ByocProcessRunner(process, settings.getDelayOnError(), settings.getRequestInterval());
    shutdownHook.register(processRunner);
  }

  private static void runLargeOracleProcesses(
      ShutdownHook shutdownHook, LargeOracleSettings settings) {
    VersionInformation versionInformation = settings.getVersionInformation();
    InMemoryMessageStorage storage = new InMemoryMessageStorage();
    SystemUptimeProvider systemUptimeProvider = new SystemUptimeProvider();
    SystemTimeProviderImpl systemTimeProvider = new SystemTimeProviderImpl();
    CpuInformationProviderImpl cpuInformationProvider = new CpuInformationProviderImpl();
    MemoryUsageProvider memoryUsageProvider = new MemoryUsageProviderImpl();
    SystemPublicProvider publicProvider =
        new SystemPublicProvider(
            settings.getSigningKey().getPublic(),
            settings.getPbcAccount(),
            settings.getFinalizationKey());
    PeerDatabase peerDatabase = settings.getBlockProducerPeerDatabase();
    LargeOracleProcess process = new LargeOracleProcess(settings, storage);
    AdditionalStatusProviders additionalStatusProviders = settings.getAdditionalStatusProviders();
    ResourceConfig config =
        new ResourceConfig(
                LargeOracleResource.class, StatusResource.class, PeerDiscoveryResource.class)
            .register(
                new AbstractBinder() {
                  @Override
                  protected void configure() {
                    // Threshold resource
                    bind(storage).to(MessageStorage.class);
                    bind(settings.getSigningKey()).to(KeyPair.class);
                    bind(process).to(LargeOracleStatusInformation.class);
                    // Status resource
                    bind(systemUptimeProvider).to(UptimeProvider.class);
                    bind(systemTimeProvider).to(SystemTimeProvider.class);
                    bind(cpuInformationProvider).to(CpuInformationProvider.class);
                    bind(memoryUsageProvider).to(MemoryUsageProvider.class);
                    bind(versionInformation).to(VersionInformation.class);
                    bind(publicProvider).to(PublicProvider.class);
                    bind(peerDatabase).to(PeerDatabase.class);
                    bind(additionalStatusProviders).to(AdditionalStatusProviders.class);
                    // PeerDiscoveryResource dependencies are specified above
                  }
                });
    ByocProcessRunner largeOracleRunner =
        new ByocProcessRunner(process, settings.getDelayOnError(), settings.getRequestInterval());
    RestServer server = new RestServer(settings.getLargeOraclePort(), config);
    shutdownHook.register(server);
    shutdownHook.register(largeOracleRunner);
    server.start();

    ByocProcessRunner discoveryRunner =
        new ByocProcessRunner(
            new PeerDiscoveryProcess(settings),
            settings.getDelayOnError(),
            settings.getRequestInterval());
    shutdownHook.register(discoveryRunner);
  }
}
