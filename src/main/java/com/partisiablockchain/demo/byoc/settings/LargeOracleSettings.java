package com.partisiablockchain.demo.byoc.settings;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.CanIgnoreReturnValue;
import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.BlsPublicKey;
import com.partisiablockchain.crypto.KeyPair;
import com.partisiablockchain.demo.byoc.Address;
import com.partisiablockchain.demo.byoc.monitor.PeerDatabase;
import com.partisiablockchain.demo.byoc.monitor.VersionInformation;
import com.partisiablockchain.providers.AdditionalStatusProviders;
import java.util.Objects;

/** Settings for running the large oracle process. */
public final class LargeOracleSettings extends Settings {

  private final VersionInformation versionInformation;
  private final KeyPair signingKey;
  private final BlockchainAddress bpOrchestrationContract;
  private final BlockchainAddress largeOracleContract;
  private final int preSignatureCount;
  private final String largeOracleDatabaseName;
  private final String largeOracleDatabaseBackupName;
  private final byte[] largeOracleDatabaseKey;
  private final PeerDatabase blockProducerPeerDatabase;
  private final int largeOraclePort;
  private final Address myEndpoint;
  private final BlsPublicKey finalizationKey;

  private final AdditionalStatusProviders additionalStatusProviders;

  LargeOracleSettings(Builder builder) {
    super(builder);
    this.versionInformation = Objects.requireNonNull(builder.versionInformation);
    this.signingKey = Objects.requireNonNull(builder.signingKey);
    this.bpOrchestrationContract = Objects.requireNonNull(builder.bpOrchestrationContract);
    this.largeOracleContract = Objects.requireNonNull(builder.largeOracleContract);
    this.preSignatureCount = builder.preSignatureCount;
    this.largeOracleDatabaseName = Objects.requireNonNull(builder.largeOracleDatabaseName);
    this.finalizationKey = Objects.requireNonNull(builder.finalizationKey);
    this.largeOracleDatabaseBackupName =
        Objects.requireNonNull(builder.largeOracleDatabaseBackupName);
    this.largeOracleDatabaseKey = Objects.requireNonNull(builder.largeOracleDatabaseKey);
    this.blockProducerPeerDatabase = Objects.requireNonNull(builder.peerDatabase);
    this.largeOraclePort = builder.largeOraclePort;
    this.myEndpoint = Objects.requireNonNull(builder.myEndpoint);
    this.additionalStatusProviders = Objects.requireNonNull(builder.additionalStatusProviders);
  }

  /**
   * Create new builder for these settings.
   *
   * @return new builder
   */
  public static Builder builder() {
    return new Builder();
  }

  /**
   * Get version information.
   *
   * @return version information
   */
  public VersionInformation getVersionInformation() {
    return versionInformation;
  }

  /**
   * Get signing key.
   *
   * @return signing key
   */
  public KeyPair getSigningKey() {
    return signingKey;
  }

  /**
   * Get address for the bp orchestration contract.
   *
   * @return contract address
   */
  public BlockchainAddress getBpOrchestrationContract() {
    return bpOrchestrationContract;
  }

  /**
   * Get address for the large oracle contract.
   *
   * @return contract address
   */
  public BlockchainAddress getLargeOracleContract() {
    return largeOracleContract;
  }

  /**
   * Get the pre signature count.
   *
   * @return pre signature count
   */
  public int getPreSignatureCount() {
    return preSignatureCount;
  }

  /**
   * Get the name of the database to use.
   *
   * @return database name.
   */
  public String getLargeOracleDatabaseName() {
    return largeOracleDatabaseName;
  }

  /**
   * Get the BLS key for finalization.
   *
   * @return BLS finalization key.
   */
  public BlsPublicKey getFinalizationKey() {
    return finalizationKey;
  }

  /**
   * Get the name of the database backup.
   *
   * @return database backup name.
   */
  public String getLargeOracleDatabaseBackupName() {
    return largeOracleDatabaseBackupName;
  }

  /**
   * Get key to the large oracle database.
   *
   * @return database key
   */
  public byte[] getLargeOracleDatabaseKey() {
    return largeOracleDatabaseKey;
  }

  /**
   * Get the block producer peer database.
   *
   * @return block producer peer database
   */
  public PeerDatabase getBlockProducerPeerDatabase() {
    return blockProducerPeerDatabase;
  }

  /**
   * Get large oracle port.
   *
   * @return large oracle port
   */
  public int getLargeOraclePort() {
    return largeOraclePort;
  }

  /**
   * Get my endpoint.
   *
   * @return my endpoint
   */
  public Address getMyEndpoint() {
    return myEndpoint;
  }

  /**
   * Returns the additional status provider information.
   *
   * @return additional status provider
   */
  public AdditionalStatusProviders getAdditionalStatusProviders() {
    return additionalStatusProviders;
  }

  /** Get builder for these settings. */
  public static final class Builder extends Settings.Builder<Builder> {

    private VersionInformation versionInformation;
    private KeyPair signingKey;
    private BlockchainAddress bpOrchestrationContract;
    private BlockchainAddress largeOracleContract;
    private int preSignatureCount = 1000;
    private String largeOracleDatabaseName;
    private BlsPublicKey finalizationKey;
    private String largeOracleDatabaseBackupName;
    private byte[] largeOracleDatabaseKey;
    private PeerDatabase peerDatabase;
    private int largeOraclePort;
    private Address myEndpoint;

    private AdditionalStatusProviders additionalStatusProviders;

    /**
     * Set additional status provider.
     *
     * @param additionalStatusProviders additional status providers
     * @return updated builder
     */
    @CanIgnoreReturnValue
    public Builder setAdditionalStatusProvider(
        AdditionalStatusProviders additionalStatusProviders) {
      this.additionalStatusProviders = additionalStatusProviders;
      return self();
    }

    /**
     * Set version information.
     *
     * @param versionInformation version information
     * @return updated builder
     */
    @CanIgnoreReturnValue
    public Builder setVersionInformation(VersionInformation versionInformation) {
      this.versionInformation = versionInformation;
      return self();
    }

    /**
     * Set signing key.
     *
     * @param signingKey signing key
     * @return updated builder
     */
    @CanIgnoreReturnValue
    public Builder setSigningKey(KeyPair signingKey) {
      this.signingKey = signingKey;
      return self();
    }

    /**
     * Set address for the bp orchestration contract.
     *
     * @param bpOrchestrationContract contract address
     * @return updated builder
     */
    @CanIgnoreReturnValue
    public Builder setBpOrchestrationContract(BlockchainAddress bpOrchestrationContract) {
      this.bpOrchestrationContract = bpOrchestrationContract;
      return self();
    }

    /**
     * Set address for the large oracle contract.
     *
     * @param largeOracleContract contract address
     * @return updated builder
     */
    @CanIgnoreReturnValue
    public Builder setLargeOracleContract(BlockchainAddress largeOracleContract) {
      this.largeOracleContract = largeOracleContract;
      return self();
    }

    /**
     * Set the name of the database to use.
     *
     * @param largeOracleDatabaseName the name
     * @return updated builder.
     */
    @CanIgnoreReturnValue
    public Builder setLargeOracleDatabaseName(String largeOracleDatabaseName) {
      this.largeOracleDatabaseName = largeOracleDatabaseName;
      return this;
    }

    /**
     * Set the BLS key for finalization.
     *
     * @param finalizationKey the key
     * @return updated builder.
     */
    @CanIgnoreReturnValue
    public Builder setFinalizationKey(BlsPublicKey finalizationKey) {
      this.finalizationKey = finalizationKey;
      return this;
    }

    /**
     * Set the name of database backup to use.
     *
     * @param largeOracleDatabaseBackupName the name
     * @return updated builder.
     */
    @CanIgnoreReturnValue
    public Builder setLargeOracleDatabaseBackupName(String largeOracleDatabaseBackupName) {
      this.largeOracleDatabaseBackupName = largeOracleDatabaseBackupName;
      return this;
    }

    /**
     * Set key to the large oracle database.
     *
     * @param largeOracleDatabaseKey key
     * @return updated builder
     */
    @CanIgnoreReturnValue
    public Builder setLargeOracleDatabaseKey(byte[] largeOracleDatabaseKey) {
      this.largeOracleDatabaseKey = largeOracleDatabaseKey;
      return self();
    }

    /**
     * Set block producer peer database.
     *
     * @param peerDatabase database
     * @return updated builder
     */
    @CanIgnoreReturnValue
    public Builder setPeerDatabase(PeerDatabase peerDatabase) {
      this.peerDatabase = peerDatabase;
      return self();
    }

    /**
     * Set port to the large oracle.
     *
     * @param largeOraclePort port
     * @return updated builder
     */
    @CanIgnoreReturnValue
    public Builder setLargeOraclePort(int largeOraclePort) {
      this.largeOraclePort = largeOraclePort;
      return self();
    }

    /**
     * Set endpoint to this process.
     *
     * @param myEndpoint endpoint
     * @return updated builder
     */
    @CanIgnoreReturnValue
    public Builder setMyEndpoint(Address myEndpoint) {
      this.myEndpoint = myEndpoint;
      return self();
    }

    /**
     * Set pre signature count.
     *
     * @param count count
     * @return updated builder
     */
    @CanIgnoreReturnValue
    public Builder setPreSignatureCount(int count) {
      this.preSignatureCount = count;
      return self();
    }

    @Override
    public LargeOracleSettings build() {
      return new LargeOracleSettings(this);
    }

    @Override
    Builder self() {
      return this;
    }
  }
}
