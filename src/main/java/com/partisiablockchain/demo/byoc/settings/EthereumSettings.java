package com.partisiablockchain.demo.byoc.settings;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.CanIgnoreReturnValue;
import com.partisiablockchain.demo.byoc.monitor.DefaultEthereumEventsCache;
import com.partisiablockchain.demo.byoc.monitor.EthereumEventsCache;
import java.math.BigInteger;

/** Configuration for the process reading the BYOC events on Ethereum. */
public final class EthereumSettings extends Settings {

  private final String ethereumEndpoint;
  private final BigInteger ethereumBlockValidityDepth;
  private final String ethereumByocContract;
  private final EthereumEventsCache cache;
  private final BigInteger maxBlockRange;

  EthereumSettings(Builder builder) {
    super(builder);
    this.ethereumEndpoint = builder.ethereumEndpoint;
    this.ethereumBlockValidityDepth = builder.ethereumBlockValidityDepth;
    this.ethereumByocContract = builder.ethereumByocContract;
    this.cache = builder.cache;
    this.maxBlockRange = builder.maxBlockRange;
  }

  /**
   * Create new builder for these settings.
   *
   * @return new builder
   */
  public static Builder builder() {
    return new Builder();
  }

  /**
   * Get the endpoint to connect to the ethereum chain.
   *
   * @return endpoint
   */
  public String getEthereumEndpoint() {
    return ethereumEndpoint;
  }

  /**
   * The depth for Ethereum block validity.
   *
   * @return block validity depth
   */
  public BigInteger getEthereumBlockValidityDepth() {
    return ethereumBlockValidityDepth;
  }

  /**
   * Get the address to the Ethereum BYOC contract.
   *
   * @return contract address
   */
  public String getEthereumByocContract() {
    return ethereumByocContract;
  }

  /**
   * Get the event cache for the Ethereum process.
   *
   * @return event cache
   */
  public EthereumEventsCache getCache() {
    return cache;
  }

  /**
   * Get the maximum number of blocks to read per request.
   *
   * @return max block range
   */
  public BigInteger getMaxBlockRange() {
    return maxBlockRange;
  }

  /** Builder for these settings. */
  public static final class Builder extends Settings.Builder<Builder> {

    private String ethereumEndpoint;
    private BigInteger ethereumBlockValidityDepth = BigInteger.TEN;
    private String ethereumByocContract;
    private EthereumEventsCache cache = new DefaultEthereumEventsCache();
    private BigInteger maxBlockRange = BigInteger.valueOf(5_000L);

    /**
     * Set the endpoint to connect to the Ethereum chain.
     *
     * @param ethereumEndpoint endpoint
     * @return updated builder
     */
    @CanIgnoreReturnValue
    public Builder setEthereumEndpoint(String ethereumEndpoint) {
      this.ethereumEndpoint = ethereumEndpoint;
      return self();
    }

    /**
     * Set the block validity depth. Must be non-negative. Default is 10.
     *
     * @param ethereumBlockValidityDepth block validity depth
     * @return updated builder
     */
    @CanIgnoreReturnValue
    public Builder setEthereumBlockValidityDepth(BigInteger ethereumBlockValidityDepth) {
      if (ethereumBlockValidityDepth.signum() < 0) {
        throw new RuntimeException("Ethereum block validity depth must be non-negative");
      }
      this.ethereumBlockValidityDepth = ethereumBlockValidityDepth;
      return self();
    }

    /**
     * Set the address to the Ethereum BYOC contract.
     *
     * @param ethereumByocContract contract address
     * @return updated builder
     */
    @CanIgnoreReturnValue
    public Builder setEthereumByocContract(String ethereumByocContract) {
      this.ethereumByocContract = ethereumByocContract;
      return self();
    }

    /**
     * Set the event cache for the Ethereum process.
     *
     * @param cache the ethereum event cache
     * @return updated builder
     */
    @CanIgnoreReturnValue
    public Builder setEthereumEventsCache(EthereumEventsCache cache) {
      this.cache = cache;
      return self();
    }

    /**
     * Set the maximum number of block to read per Eth request.
     *
     * @param maxBlockRange maximum number of blocks to read
     * @return updated builder
     */
    @CanIgnoreReturnValue
    public Builder setMaxBlockRange(BigInteger maxBlockRange) {
      this.maxBlockRange = maxBlockRange;
      return self();
    }

    @Override
    public EthereumSettings build() {
      return new EthereumSettings(this);
    }

    @Override
    Builder self() {
      return this;
    }
  }
}
