package com.partisiablockchain.demo.byoc.settings;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.CanIgnoreReturnValue;
import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.demo.byoc.monitor.ByocTransactionSender;
import com.partisiablockchain.demo.byoc.monitor.ContractStateHandler;
import java.util.List;
import java.util.Objects;

/** Base settings for BYOC service. */
public abstract class Settings {

  private final String pbcEndpoint;
  private final List<String> shards;
  private final long transactionCost;

  private final ByocTransactionSender transactionSender;
  private final BlockchainAddress pbcAccount;

  private final int delayOnError;
  private final int transactionTimeout;
  private final int requestInterval;
  private final ContractStateHandler contractStateHandler;

  Settings(Builder<?> builder) {
    this.pbcEndpoint = Objects.requireNonNull(builder.pbcEndpoint);
    this.shards = Objects.requireNonNull(builder.shards);
    this.transactionCost = builder.transactionCost;
    this.transactionSender = Objects.requireNonNull(builder.transactionSender);
    this.pbcAccount = Objects.requireNonNull(builder.pbcAccount);
    this.delayOnError = builder.delayOnError;
    this.transactionTimeout = builder.transactionTimeout;
    this.requestInterval = builder.requestInterval;
    this.contractStateHandler = builder.contractStateHandler;
  }

  /**
   * Get endpoint to pbc.
   *
   * @return pbc endpoint
   */
  public String getPbcEndpoint() {
    return pbcEndpoint;
  }

  /**
   * Get list of known shards.
   *
   * @return list of shards
   */
  public List<String> getShards() {
    return shards;
  }

  /**
   * Get the cost of sending a transaction.
   *
   * @return transaction cost
   */
  public long getTransactionCost() {
    return transactionCost;
  }

  /**
   * Get proxy for sending transactions to PBC.
   *
   * @return transaction sender
   */
  public ByocTransactionSender getTransactionSender() {
    return transactionSender;
  }

  /**
   * Get pbc account.
   *
   * @return pbc account
   */
  public BlockchainAddress getPbcAccount() {
    return pbcAccount;
  }

  /**
   * Get amount of time to wait on error in milliseconds.
   *
   * @return delay on error
   */
  public int getDelayOnError() {
    return delayOnError;
  }

  /**
   * Get the amount of time to wait when sending a transaction in milliseconds.
   *
   * @return transaction timeout
   */
  public long getTransactionTimeout() {
    return transactionTimeout;
  }

  /**
   * Get the amount of time to wait between running service actions in milliseconds.
   *
   * @return request interval
   */
  public int getRequestInterval() {
    return requestInterval;
  }

  /**
   * Get the contract state handler for this process.
   *
   * @return the contract state handler
   */
  public ContractStateHandler getContractStateHandler() {
    return contractStateHandler;
  }

  abstract static class Builder<T extends Builder<T>> {
    private String pbcEndpoint;
    private List<String> shards;
    private long transactionCost = 10_000;

    private ByocTransactionSender transactionSender;
    private BlockchainAddress pbcAccount;

    private int delayOnError = 500;
    private int transactionTimeout = 25_000;
    private int requestInterval = 30_000;
    private ContractStateHandler contractStateHandler;

    /**
     * Set endpoint to PBC.
     *
     * @param endpoint endpoint
     * @return updated builder
     */
    @CanIgnoreReturnValue
    public T setPbcEndpoint(String endpoint) {
      this.pbcEndpoint = endpoint;
      return self();
    }

    /**
     * Set shards.
     *
     * @param shards shards
     * @return updated builder
     */
    @CanIgnoreReturnValue
    public T setShards(List<String> shards) {
      this.shards = shards;
      return self();
    }

    /**
     * Set the gas cost for sending a transaction.
     *
     * @param transactionCost transaction cost
     * @return updated builder
     */
    @CanIgnoreReturnValue
    public T setTransactionCost(long transactionCost) {
      this.transactionCost = transactionCost;
      return self();
    }

    /**
     * Set the proxy for sending transactions to PBC.
     *
     * @param sender transaction sender proxy
     * @return updated builder
     */
    @CanIgnoreReturnValue
    public T setTransactionSender(ByocTransactionSender sender) {
      this.transactionSender = sender;
      return self();
    }

    /**
     * Set address for the PBC account running the service.
     *
     * @param account account address
     * @return updated builder
     */
    @CanIgnoreReturnValue
    public T setPbcAccount(BlockchainAddress account) {
      this.pbcAccount = account;
      return self();
    }

    /**
     * Set time to wait after an error.
     *
     * @param delay time to wait after error
     * @return updated builder
     */
    @CanIgnoreReturnValue
    public T setDelayOnError(int delay) {
      this.delayOnError = delay;
      return self();
    }

    /**
     * Set time to wait for a transaction to be sent.
     *
     * @param timeout transaction timeout
     * @return updated builder
     */
    @CanIgnoreReturnValue
    public T setTransactionTimeout(int timeout) {
      this.transactionTimeout = timeout;
      return self();
    }

    /**
     * Set time to wait between running the service process.
     *
     * @param interval time to wait between requests
     * @return updated builder
     */
    @CanIgnoreReturnValue
    public T setRequestInterval(int interval) {
      this.requestInterval = interval;
      return self();
    }

    /**
     * Sets the handler for contract states to be used by the process.
     *
     * @param contractStateHandler the state handler
     * @return updated builder
     */
    @CanIgnoreReturnValue
    public T setContractStateHandler(ContractStateHandler contractStateHandler) {
      this.contractStateHandler = contractStateHandler;
      return self();
    }

    /**
     * Build settings.
     *
     * @return settings
     */
    public abstract Settings build();

    // Subclasses must override this method to return "this"
    abstract T self();
  }
}
