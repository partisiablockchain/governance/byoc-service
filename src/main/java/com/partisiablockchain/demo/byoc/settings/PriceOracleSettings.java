package com.partisiablockchain.demo.byoc.settings;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.CanIgnoreReturnValue;
import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.demo.byoc.monitor.chainlink.ChainlinkClient;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.http.HttpService;

/** Configuration for the process querying data feeds for answer updates on Ethereum. */
public final class PriceOracleSettings extends Settings {

  private final List<EvmPriceOracleChainConfig> chainConfigs;
  private final List<BlockchainAddress> priceOracleContracts;
  private final BlockchainAddress largeOracleContract;

  PriceOracleSettings(PriceOracleSettings.Builder builder) {
    super(builder);
    this.chainConfigs = builder.chainConfigs;
    this.priceOracleContracts = builder.priceOracleContracts;
    this.largeOracleContract = builder.largeOracleContract;
  }

  /**
   * Create new builder for these settings.
   *
   * @return new builder
   */
  public static PriceOracleSettings.Builder builder() {
    return new PriceOracleSettings.Builder();
  }

  /**
   * Create Chainlink clients for each external chain configured.
   *
   * @return map of chain identifiers to Chainlink client
   */
  public Map<String, ChainlinkClient> createChainlinkClients() {
    Map<String, ChainlinkClient> chainlinkClients = new HashMap<>();
    for (EvmPriceOracleChainConfig chainConfig : getChainConfigs()) {
      chainlinkClients.put(
          chainConfig.chainId,
          new ChainlinkClient(
              Web3j.build(new HttpService(chainConfig.endpointUrl)),
              chainConfig.blockValidityDepth));
    }
    return chainlinkClients;
  }

  /**
   * Returns the blockchain address for the large oracle contract.
   *
   * @return blockchain address for the large oracle contract
   */
  public BlockchainAddress getLargeOracleContract() {
    return largeOracleContract;
  }

  /**
   * Get the address to the PBC price oracle contract.
   *
   * @return contract address
   */
  public List<BlockchainAddress> getPriceOracleContracts() {
    return priceOracleContracts;
  }

  /**
   * Returns the configurations for external chains.
   *
   * @return configurations for external chains.
   */
  public List<EvmPriceOracleChainConfig> getChainConfigs() {
    return chainConfigs;
  }

  /** Builder for these settings. */
  public static final class Builder extends Settings.Builder<PriceOracleSettings.Builder> {

    private List<EvmPriceOracleChainConfig> chainConfigs;
    private List<BlockchainAddress> priceOracleContracts;
    private BlockchainAddress largeOracleContract;

    /**
     * Sets the configurations for external chains.
     *
     * @param chainConfigs external chain configurations
     * @return updated builder
     */
    public PriceOracleSettings.Builder setChainConfigs(
        List<EvmPriceOracleChainConfig> chainConfigs) {
      this.chainConfigs = chainConfigs;
      return self();
    }

    /**
     * Sets the blockchain address for the large oracle contract.
     *
     * @param largeOracleContract blockchain address for the large oracle contract
     * @return updated builder
     */
    @CanIgnoreReturnValue
    public PriceOracleSettings.Builder setLargeOracleContract(
        BlockchainAddress largeOracleContract) {
      this.largeOracleContract = largeOracleContract;
      return self();
    }

    /**
     * Set the addresses to the PBC price oracle contracts.
     *
     * @param priceOracleContracts contract addresses
     * @return updated builder
     */
    @CanIgnoreReturnValue
    public PriceOracleSettings.Builder setPriceOracleContracts(
        List<BlockchainAddress> priceOracleContracts) {
      this.priceOracleContracts = priceOracleContracts;
      return self();
    }

    @Override
    public PriceOracleSettings build() {
      return new PriceOracleSettings(this);
    }

    @Override
    PriceOracleSettings.Builder self() {
      return this;
    }
  }
}
