package com.partisiablockchain.demo.byoc.settings;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.CanIgnoreReturnValue;
import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.demo.byoc.monitor.DataSource;
import java.util.Objects;

/** Settings for running the deposit process. */
public final class DepositSettings extends Settings {

  private final BlockchainAddress depositContractAddress;
  private final DataSource depositDataSource;

  DepositSettings(Builder builder) {
    super(builder);
    this.depositContractAddress = Objects.requireNonNull(builder.depositContractAddress);
    this.depositDataSource = Objects.requireNonNull(builder.depositDataSource);
  }

  /**
   * Create builder for these settings.
   *
   * @return new builder
   */
  public static Builder builder() {
    return new Builder();
  }

  /**
   * Get address for the PBC deposit contract.
   *
   * @return deposit contract address
   */
  public BlockchainAddress getDepositContractAddress() {
    return depositContractAddress;
  }

  /**
   * Get the data source for new deposits.
   *
   * @return deposit data source
   */
  public DataSource getDepositDataSource() {
    return depositDataSource;
  }

  /** Builder for these settings. */
  public static final class Builder extends Settings.Builder<Builder> {

    private BlockchainAddress depositContractAddress;
    private DataSource depositDataSource;

    /**
     * Set the address for the PBC deposit contract.
     *
     * @param depositContractAddress the contract address
     * @return updated builder
     */
    @CanIgnoreReturnValue
    public Builder setDepositContractAddress(BlockchainAddress depositContractAddress) {
      this.depositContractAddress = depositContractAddress;
      return self();
    }

    /**
     * Set the data source for new deposits.
     *
     * @param depositDataSource the deposit data source
     * @return updated builder
     */
    @CanIgnoreReturnValue
    public Builder setDepositDataSource(DataSource depositDataSource) {
      this.depositDataSource = depositDataSource;
      return self();
    }

    @Override
    public DepositSettings build() {
      return new DepositSettings(this);
    }

    @Override
    Builder self() {
      return this;
    }
  }
}
