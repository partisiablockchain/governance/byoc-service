package com.partisiablockchain.demo.byoc.settings;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import java.math.BigInteger;

/** Configuration for an external EVM chain related to price oracles. */
public final class EvmPriceOracleChainConfig {

  /** Chain identifier. */
  public String chainId;

  /** URL endpoint for interacting with the external chain. */
  public String endpointUrl;

  /** Block validity depth used to determine the depth at which a block becomes valid. */
  public BigInteger blockValidityDepth;

  /**
   * Default constructor.
   *
   * @param chainId identifier of the external chain
   * @param endpointUrl URL endpoint for interacting with the external chain
   * @param blockValidityDepth block validity depth used to determine the depth at which a block
   *     becomes valid.
   */
  public EvmPriceOracleChainConfig(
      String chainId, String endpointUrl, BigInteger blockValidityDepth) {
    if (blockValidityDepth.signum() < 0) {
      throw new RuntimeException("Block validity depth must be non-negative");
    }

    this.chainId = chainId;
    this.endpointUrl = endpointUrl;
    this.blockValidityDepth = blockValidityDepth;
  }
}
