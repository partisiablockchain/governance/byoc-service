package com.partisiablockchain.demo.byoc.settings;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.CanIgnoreReturnValue;
import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.KeyPair;

/** Settings for running the withdrawal process. */
public final class WithdrawalSettings extends Settings {

  private final BlockchainAddress withdrawalContractAddress;
  private final KeyPair oracleSigningKey;

  WithdrawalSettings(Builder builder) {
    super(builder);
    this.withdrawalContractAddress = builder.withdrawalContractAddress;
    this.oracleSigningKey = builder.oracleSigningKey;
  }

  /**
   * Create a new builder for these settings.
   *
   * @return new builder
   */
  public static Builder builder() {
    return new Builder();
  }

  /**
   * Get the address to the BYOC withdrawal contract.
   *
   * @return contract address
   */
  public BlockchainAddress getWithdrawalContractAddress() {
    return withdrawalContractAddress;
  }

  /**
   * Get the key used to sign withdrawal requests.
   *
   * @return signing key
   */
  public KeyPair getOracleSigningKey() {
    return oracleSigningKey;
  }

  /** Builder for these settings. */
  public static final class Builder extends Settings.Builder<Builder> {

    private BlockchainAddress withdrawalContractAddress;
    private KeyPair oracleSigningKey;

    /**
     * Set address to withdrawal contract.
     *
     * @param withdrawalContractAddress contract address
     * @return updated builder
     */
    @CanIgnoreReturnValue
    public Builder setWithdrawalContractAddress(BlockchainAddress withdrawalContractAddress) {
      this.withdrawalContractAddress = withdrawalContractAddress;
      return self();
    }

    /**
     * Set key used to sign withdrawals.
     *
     * @param oracleSigningKey signing key
     * @return updated builder
     */
    @CanIgnoreReturnValue
    public Builder setOracleSigningKey(KeyPair oracleSigningKey) {
      this.oracleSigningKey = oracleSigningKey;
      return self();
    }

    @Override
    public WithdrawalSettings build() {
      return new WithdrawalSettings(this);
    }

    @Override
    Builder self() {
      return this;
    }
  }
}
