package com.partisiablockchain.demo.byoc.settings;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.CanIgnoreReturnValue;
import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.demo.byoc.monitor.DataSource;
import java.util.Objects;

/** Settings for running the dispute process. */
public final class DisputeSettings extends Settings {

  private final DataSource dataSource;

  private final BlockchainAddress largeOracleContract;
  private final BlockchainAddress depositContract;
  private final BlockchainAddress withdrawContract;

  DisputeSettings(Builder builder) {
    super(builder);
    this.dataSource = Objects.requireNonNull(builder.dataSource);
    this.largeOracleContract = Objects.requireNonNull(builder.largeOracleContract);
    this.depositContract = Objects.requireNonNull(builder.depositContract);
    this.withdrawContract = Objects.requireNonNull(builder.withdrawContract);
  }

  /**
   * Create new builder for these settings.
   *
   * @return new builder
   */
  public static Builder builder() {
    return new Builder();
  }

  /**
   * Get the datasource for external deposits and withdrawal events.
   *
   * @return transaction datasource
   */
  public DataSource getDataSource() {
    return dataSource;
  }

  /**
   * Get address for the large oracle contract.
   *
   * @return large oracle contract address
   */
  public BlockchainAddress getLargeOracleContract() {
    return largeOracleContract;
  }

  /**
   * Get the address for the deposit contract.
   *
   * @return deposit contract address
   */
  public BlockchainAddress getDepositContract() {
    return depositContract;
  }

  /**
   * Get the address for the withdrawal contract.
   *
   * @return withdrawal contract address
   */
  public BlockchainAddress getWithdrawContract() {
    return withdrawContract;
  }

  /** Builder for these settings. */
  public static final class Builder extends Settings.Builder<Builder> {

    private DataSource dataSource;

    private BlockchainAddress largeOracleContract;
    private BlockchainAddress depositContract;
    private BlockchainAddress withdrawContract;

    /**
     * Set the datasource for external transactions.
     *
     * @param dataSource the datasource
     * @return updated builder
     */
    @CanIgnoreReturnValue
    public Builder setDataSource(DataSource dataSource) {
      this.dataSource = dataSource;
      return self();
    }

    /**
     * Set the address for the large oracle contract.
     *
     * @param largeOracleContract the contract address
     * @return updated builder
     */
    @CanIgnoreReturnValue
    public Builder setLargeOracleContract(BlockchainAddress largeOracleContract) {
      this.largeOracleContract = largeOracleContract;
      return self();
    }

    /**
     * Set the address fot the deposit contract.
     *
     * @param depositContract the contract address
     * @return updated builder
     */
    @CanIgnoreReturnValue
    public Builder setDepositContract(BlockchainAddress depositContract) {
      this.depositContract = depositContract;
      return self();
    }

    /**
     * Set the address for the withdrawal contract.
     *
     * @param withdrawContract the contract address
     * @return updated builder
     */
    @CanIgnoreReturnValue
    public Builder setWithdrawContract(BlockchainAddress withdrawContract) {
      this.withdrawContract = withdrawContract;
      return self();
    }

    @Override
    public DisputeSettings build() {
      return new DisputeSettings(this);
    }

    @Override
    Builder self() {
      return this;
    }
  }
}
