package com.partisiablockchain.demo.byoc;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.secata.stream.DataStreamSerializable;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import java.util.Objects;

/** An address, points to a host and a port. */
public final class Address implements DataStreamSerializable {

  private final String host;
  private final int port;

  @SuppressWarnings("unused")
  Address() {
    this.host = null;
    this.port = 0;
  }

  /**
   * Creates address object for network node.
   *
   * @param host network host
   * @param port network port
   */
  public Address(String host, int port) {
    this.host = host;
    this.port = port;
  }

  /**
   * Splits the input into two different components used in the constructor.
   *
   * @param address the address to parse
   * @return the parsed address
   */
  public static Address parseAddress(String address) {
    String[] addressSplit = address.split(":", -1);
    return new Address(addressSplit[0], Integer.parseInt(addressSplit[1]));
  }

  /**
   * Get the host.
   *
   * @return host
   */
  public String getHost() {
    return host;
  }

  /**
   * Get the port.
   *
   * @return port
   */
  public int getPort() {
    return port;
  }

  /**
   * Get the endpoint.
   *
   * @return the endpoint
   */
  public String getEndpoint() {
    return "http://" + host + ":" + port;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Address address = (Address) o;
    return port == address.port && Objects.equals(host, address.host);
  }

  @Override
  public int hashCode() {
    return Objects.hash(host, port);
  }

  @Override
  public String toString() {
    return host + ':' + port;
  }

  @Override
  public void write(SafeDataOutputStream stream) {
    stream.writeInt(port);
    stream.writeString(host);
  }

  /**
   * Returns an Address from the given stream.
   *
   * @param stream stream to be read
   * @return Address from stream
   */
  public static Address read(SafeDataInputStream stream) {
    int port = stream.readInt();
    String host = stream.readString();
    return new Address(host, port);
  }
}
