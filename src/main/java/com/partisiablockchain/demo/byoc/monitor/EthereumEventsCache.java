package com.partisiablockchain.demo.byoc.monitor;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.demo.byoc.governance.byocincoming.DepositTransaction;
import com.partisiablockchain.demo.byoc.governance.byocoutgoing.WithdrawalTransaction;
import com.partisiablockchain.math.Unsigned256;
import java.util.List;

/**
 * Cache for BYOC deposits and withdrawal events.
 *
 * <p>The Ethereum event cache exposes methods to load and save deposit and withdrawal events to a
 * cache. Additionally, the number of the latest checked block can also be stored in the cache.
 */
public interface EthereumEventsCache {

  /**
   * Load a deposit transaction from the cache. Returns null, if the deposit does not exist in the
   * cache.
   *
   * @param depositNonce id of the deposit to load
   * @return the deposit transaction or null
   */
  DepositTransaction loadDeposit(long depositNonce);

  /**
   * Load all deposits transactions from the cache.
   *
   * @return all deposits transactions in cache
   */
  List<DepositTransaction> loadAllDeposits();

  /**
   * Load a withdrawal transaction from the cache. Returns null, if the withdrawal does not exist in
   * the cache.
   *
   * @param withdrawalId id of the withdrawal to load
   * @return the withdrawal transaction or null
   */
  WithdrawalTransaction loadWithdrawal(DataSource.WithdrawalId withdrawalId);

  /**
   * Load all withdrawals transactions from the cache.
   *
   * @return all withdrawal transactions in cache
   */
  List<WithdrawalTransaction> loadAllWithdrawals();

  /**
   * Load the number of the latest checked block. May return null if no blocks have been read yet.
   *
   * @return block number of latest checked block
   */
  Unsigned256 loadLatestCheckedBlock();

  /**
   * Save the deposits to the cache.
   *
   * @param deposits to save
   */
  void saveDeposits(List<DepositTransaction> deposits);

  /**
   * Save the withdrawals to the cache.
   *
   * @param withdrawals to save
   */
  void saveWithdrawals(List<WithdrawalTransaction> withdrawals);

  /**
   * Save the block number of the latest checked block.
   *
   * @param blockNumber to save
   */
  void saveLatestCheckedBlock(Unsigned256 blockNumber);
}
