package com.partisiablockchain.demo.byoc.monitor;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.client.transaction.Transaction;
import com.partisiablockchain.client.web.JerseyWebClient;
import com.partisiablockchain.client.web.WebClient;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.KeyPair;
import com.partisiablockchain.crypto.Signature;
import com.partisiablockchain.demo.byoc.Address;
import com.partisiablockchain.demo.byoc.governance.bporchestration.Bitmap;
import com.partisiablockchain.demo.byoc.governance.bporchestration.BlockProducer;
import com.partisiablockchain.demo.byoc.governance.bporchestration.BpOrchestrationContractState;
import com.partisiablockchain.demo.byoc.governance.bporchestration.BroadcastTracker;
import com.partisiablockchain.demo.byoc.governance.bporchestration.LargeOracleUpdate;
import com.partisiablockchain.demo.byoc.governance.bporchestration.ThresholdKey;
import com.partisiablockchain.demo.byoc.governance.largeoracle.LargeOracleContractState;
import com.partisiablockchain.demo.byoc.governance.largeoracle.SigningRequest;
import com.partisiablockchain.demo.byoc.settings.LargeOracleSettings;
import com.partisiablockchain.governance.KeyGenMessageRetriever;
import com.partisiablockchain.governance.PbctsKeyGen;
import com.partisiablockchain.governance.PbctsSigner;
import com.partisiablockchain.governance.SignatureAndContributors;
import com.partisiablockchain.governance.ThresholdKeyGenProtocol;
import com.partisiablockchain.governance.ThresholdPublicKey;
import com.partisiablockchain.governance.ThresholdSessionId;
import com.partisiablockchain.governance.ThresholdSignatureProvider;
import com.secata.stream.SafeDataOutputStream;
import com.secata.tools.coverage.ExceptionConverter;
import com.secata.tools.immutable.FixedList;
import jakarta.ws.rs.client.ClientBuilder;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;
import java.util.function.Supplier;
import java.util.stream.LongStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** BYOC service running a threshold signature protocol as part of the large oracle. */
public final class LargeOracleProcess implements ByocProcess, LargeOracleStatusInformation {

  private static final Logger logger = LoggerFactory.getLogger(LargeOracleProcess.class);

  static final byte BPO_ADD_CANDIDATE_KEY = 2;
  static final byte BPO_AUTHORIZE_NEW_ORACLE = 3;
  static final byte BPO_ADD_HEARTBEAT = 4;
  static final byte BPO_ADD_BROADCAST_HASH = 6;
  static final byte BPO_ADD_BROADCAST_BITS = 12;

  static final byte LO_ADD_SIGNATURE = 6;

  private final InMemoryMessageStorage messageStorage;
  private final Supplier<KeyPair> ephemeralKeySupplier;
  private final VersionInformation versionInformation;
  private final BlockchainAddress bpOrchestrationContract;
  private final BlockchainAddress largeOracleContract;
  private final BlockchainAddress myAddress;
  private final KeyPair restServerSigningKey;
  private final ByocTransactionSender transactionSender;
  private final long transactionCost;
  private final PeerDatabase peerDatabase;
  private final int preSignatureCount;
  private final String databaseName;
  private final String databaseBackupName;
  private final byte[] databaseKey;
  private final WebClient webClient;

  // Messages are queried based on a producer's rest-signing key, but producers are identified by
  // the PBC address elsewhere. myProtocolId is the address derived from a producer's rest signing
  // key and is used by ThresholdSignatureProvider and ThresholdKeyGenProtocol.
  private final BlockchainPublicKey myProtocolId;
  private final Map<ThresholdSessionId, ThresholdKeyGenProtocol.Step> keyGenProtocols;
  private final Map<BlockchainPublicKey, byte[]> unicastMessages;
  private final Map<BlockchainPublicKey, Hash> broadcastEchos;
  private final Map<ThresholdSessionId, ThresholdSignatureProvider> signers;
  private final Map<String, Integer> signatureIdToNonce;
  private final Map<String, Hash> signatureIdToMessage;
  private boolean shouldPublishBitmap;
  private final ContractStateHandler contractStateHandler;

  /**
   * Create a new Large Oracle BYOC process.
   *
   * @param settings settings for the process
   * @param messageStorage object for passing messages between the process and REST resource
   */
  public LargeOracleProcess(LargeOracleSettings settings, InMemoryMessageStorage messageStorage) {
    this(
        messageStorage,
        KeyPair::new,
        settings.getVersionInformation(),
        settings.getBpOrchestrationContract(),
        settings.getLargeOracleContract(),
        settings.getPbcAccount(),
        settings.getSigningKey(),
        settings.getTransactionSender(),
        settings.getTransactionCost(),
        settings.getBlockProducerPeerDatabase(),
        settings.getPreSignatureCount(),
        settings.getLargeOracleDatabaseName(),
        settings.getLargeOracleDatabaseBackupName(),
        settings.getLargeOracleDatabaseKey(),
        createJerseyWebClient(),
        settings.getContractStateHandler());
  }

  LargeOracleProcess(
      InMemoryMessageStorage messageStorage,
      Supplier<KeyPair> ephemeralKeySupplier,
      VersionInformation versionInformation,
      BlockchainAddress bpOrchestrationContract,
      BlockchainAddress largeOracleContract,
      BlockchainAddress myAddress,
      KeyPair restServerSigningKey,
      ByocTransactionSender transactionSender,
      long transactionCost,
      PeerDatabase peerDatabase,
      int preSignatureCount,
      String databaseName,
      String databaseBackupName,
      byte[] databaseKey,
      WebClient webClient,
      ContractStateHandler contractStateHandler) {
    this.versionInformation = versionInformation;
    this.ephemeralKeySupplier = ephemeralKeySupplier;
    this.bpOrchestrationContract = bpOrchestrationContract;
    this.largeOracleContract = largeOracleContract;
    this.myAddress = myAddress;
    this.restServerSigningKey = restServerSigningKey;
    this.transactionSender = transactionSender;
    this.transactionCost = transactionCost;
    this.messageStorage = messageStorage;
    this.peerDatabase = peerDatabase;
    this.preSignatureCount = preSignatureCount;
    this.databaseName = databaseName;
    this.databaseBackupName = databaseBackupName;
    this.databaseKey = databaseKey;
    this.webClient = webClient;

    this.myProtocolId = restServerSigningKey.getPublic();
    this.keyGenProtocols = new HashMap<>();
    this.unicastMessages = new HashMap<>();
    this.broadcastEchos = new HashMap<>();
    this.signers = new HashMap<>();
    this.signatureIdToNonce = new HashMap<>();
    this.signatureIdToMessage = new HashMap<>();
    this.shouldPublishBitmap = false;

    this.contractStateHandler = contractStateHandler;
  }

  static WebClient createJerseyWebClient() {
    return new JerseyWebClient(
        ClientBuilder.newBuilder()
            .connectTimeout(5, TimeUnit.SECONDS)
            .readTimeout(30, TimeUnit.SECONDS)
            .build());
  }

  @Override
  public boolean run() {
    BpOrchestrationContractState bpoState =
        contractStateHandler.getState(bpOrchestrationContract, BpOrchestrationContractState.class);
    LargeOracleContractState locState =
        contractStateHandler.getLargeOracleState(largeOracleContract);
    if (bpoState == null || locState == null) {
      return false;
    }

    boolean runEagerly = false;
    FixedList<BlockProducer> registeredProducers =
        PeerDiscoveryProcess.includeCommitteeMembersInDiscovery(
            bpoState.committee(), bpoState.getConfirmedBlockProducers());
    registerPeersInPeerDatabase(registeredProducers);
    ThresholdSessionId currentSessionId =
        getCurrentSessionId(bpoState.sessionId(), bpoState.retryNonce());
    LargeOracleUpdate oracleUpdate = bpoState.oracleUpdate();

    if (oracleUpdate != null) {
      ThresholdSessionId nextSessionId =
          getNextSessionIdFromCurrent(currentSessionId, oracleUpdate);
      collectHeartbeats(oracleUpdate);
      runEagerly = runIncoming(nextSessionId, oracleUpdate);
      if (signers.containsKey(nextSessionId)) {
        ThresholdSignatureProvider provider = signers.get(nextSessionId);
        publishCandidateKey(oracleUpdate, provider.getPublicKey(), provider.getHonestParties());
      }
    }

    runCurrent(bpoState, currentSessionId, oracleUpdate, locState);
    return runEagerly;
  }

  private void registerPeersInPeerDatabase(FixedList<BlockProducer> blockProducers) {
    List<BlockchainPublicKey> producers = new ArrayList<>();
    for (BlockProducer producer : blockProducers) {
      producers.add(producer.publicKey());
    }
    peerDatabase.setRegisteredBlockProducers(producers);
  }

  @Override
  public String name() {
    return LargeOracleProcess.class.getSimpleName();
  }

  @Override
  public int getCurrentRoundNumber(ThresholdSessionId thresholdSessionId) {
    ThresholdKeyGenProtocol.Step protocol = keyGenProtocols.get(thresholdSessionId);
    if (protocol != null) {
      return protocol.roundNumber();
    } else {
      return -1;
    }
  }

  @Override
  public int getSignatureIdsLeft(ThresholdSessionId thresholdSessionId) {
    ThresholdSignatureProvider signer = signers.get(thresholdSessionId);
    if (signer != null) {
      // one signature is reserved for signing the next oracle
      return signer.getRemainingSignatures() - 1;
    } else {
      return -1;
    }
  }

  @Override
  public List<BlockchainPublicKey> getHonestParties(ThresholdSessionId thresholdSessionId) {
    ThresholdSignatureProvider signer = signers.get(thresholdSessionId);
    if (signer != null) {
      return signer.getHonestParties();
    } else {
      return null;
    }
  }

  @Override
  public List<BlockchainPublicKey> getUnicastResponders(ThresholdSessionId thresholdSessionId) {
    return new ArrayList<>(unicastMessages.keySet());
  }

  private boolean runIncoming(ThresholdSessionId sessionId, LargeOracleUpdate update) {

    boolean activeParticipant = isActiveParticipant(update);
    if (activeParticipant && !signers.containsKey(sessionId)) {
      ThresholdKeyGenProtocol.Step current = keyGenProtocols.get(sessionId);
      int currentBroadcastRound = update.getCurrentBroadcastRound();
      if (current == null) {
        keyGenProtocols.put(sessionId, initializeOrLoadKeyGenSession(sessionId, update));
        sendBroadcast(sessionId, update, currentBroadcastRound + 1);
      } else {

        int roundNumber = current.roundNumber();

        if (roundNumber == 2) {
          // We only need to collect unicast messages in round 2.
          collectUnicastMessages(sessionId, update);
        }

        // We cannot proceed so long as we are in a protocol round which is larger than the current
        // broadcast round.
        if (roundNumber <= currentBroadcastRound) {
          shouldPublishBitmap = false;
          if (hasAllBroadcastMessages(roundNumber, update)) {
            logger.info("Ready to advance from round=" + roundNumber);
            ThresholdKeyGenProtocol.Step next = executeProtocolRound(current, update);
            keyGenProtocols.put(sessionId, next);
            if (isFinalRound(next)) {
              ThresholdSignatureProvider provider = finalizeProtocol(next);
              ThresholdPublicKey publicKey = provider.getPublicKey();
              logger.info(
                  "Finalized key-gen with key="
                      + publicKey
                      + ", honest="
                      + provider.getHonestParties());
              putSigner(sessionId, provider);
            } else {
              Hash hash = messageStorage.getMyBroadcastMessageHash(sessionId, next.roundNumber());
              broadcastEchos.put(myProtocolId, hash);
              sendBroadcast(sessionId, update, next.roundNumber());
            }
          } else {
            // we do not want to publish a bitmap at this point because BPO has already moved onto
            // the next round.
            collectBroadcastMessages(roundNumber, update);
          }
        } else {
          sendBroadcast(sessionId, update, roundNumber);
          collectBroadcastMessages(roundNumber, update);
          if (shouldPublishBitmap) {
            publishBroadcastBitmap(sessionId, roundNumber, update);
          }
          // By first setting shouldPublishBitmap here, we can ensure that we execute eagerly until
          // we have enough messages. At that point, we will run one round non-eagerly before the
          // bitmap is actually sent. This should give slow producers a bit of time to publish their
          // messages, so hopefully they will be included as well.
          shouldPublishBitmap = shouldPublishBroadcastBitmap(roundNumber, update);
          return !shouldPublishBitmap;
        }
      }
      // run eagerly if key-generation is ongoing.
      return true;
    }
    // otherwise, run non-eagerly (e.g., if the heartbeat protocol hasn't finished yet).
    return false;
  }

  private void publishBroadcastBitmap(
      ThresholdSessionId sessionId, int roundNumber, LargeOracleUpdate update) {
    List<BlockProducer> activeProducers = update.getActiveProducers();
    Bitmap messageBitmap = Bitmap.create(activeProducers.size());
    for (int i = 0; i < activeProducers.size(); i++) {
      BlockchainPublicKey protocolId = activeProducers.get(i).publicKey();
      if (broadcastEchos.containsKey(protocolId)) {
        messageBitmap = messageBitmap.setBit(i);
      }
    }
    int broadcastRound = roundNumber - 1;
    BroadcastTracker broadcastTracker = update.getBroadcastTracker(broadcastRound);
    Bitmap finalMessageBitmap = messageBitmap;
    if (bitMapShouldBeBroadcast(activeProducers, finalMessageBitmap, broadcastTracker)) {
      logger.info(
          "Publishing broadcast bitmap="
              + finalMessageBitmap
              + ", broadcastRound="
              + broadcastRound);
      sendTransaction(
          bpOrchestrationContract,
          rpc -> {
            rpc.writeByte(BPO_ADD_BROADCAST_BITS);
            rpc.writeInt(sessionId.getSessionId());
            rpc.writeInt(sessionId.getRetryNonce());
            rpc.writeInt(broadcastRound);
            rpc.writeDynamicBytes(finalMessageBitmap.asBytes());
          });
    }
  }

  private boolean bitMapShouldBeBroadcast(
      List<BlockProducer> broadcasters, Bitmap bitmap, BroadcastTracker tracker) {
    int broadcasterIndex = 0;
    List<BlockchainAddress> broadCasterAddresses =
        broadcasters.stream().map(BlockProducer::identity).toList();
    int myIndex = broadCasterAddresses.indexOf(myAddress);
    for (BlockProducer broadcaster : broadcasters) {
      if (bitmap.testBit(broadcasterIndex)) {
        Bitmap otherPartyBitmap = tracker.getMessageBitmap(broadcaster.identity());
        if (!otherPartyBitmap.testBit(myIndex)) {
          return true;
        }
      }
      broadcasterIndex++;
    }
    return false;
  }

  static int computeThreshold(int numberOfProducers) {
    return (numberOfProducers - 1) / 3;
  }

  private boolean shouldPublishBroadcastBitmap(int roundNumber, LargeOracleUpdate update) {
    int threshold = computeThreshold(update.getActiveProducers().size());
    boolean enoughBroadcasts = broadcastEchos.size() > 2 * threshold;
    if (roundNumber == 2) {
      // If we're in round two, then we only wish to start publishing our broadcast bitmap if we
      // have collected at least 2t+1 unicast messages. This hopefully ensures that we get as many
      // slow people included as possible.
      return enoughBroadcasts && unicastMessages.size() > 2 * threshold;
    } else {
      // Otherwise, in other rounds, we will publish our bitmap if we have collected 2t+1 broadcast
      // messages.
      return enoughBroadcasts;
    }
  }

  private ThresholdKeyGenProtocol.Step executeProtocolRound(
      ThresholdKeyGenProtocol.Step current, LargeOracleUpdate update) {
    KeyGenMessageRetriever messageRetriever = getMessageRetriever(update);
    ThresholdKeyGenProtocol.Step next =
        ((ThresholdKeyGenProtocol.InternalStep) current).advance(messageRetriever);
    broadcastEchos.clear();
    unicastMessages.clear();
    return next;
  }

  Map<BlockchainPublicKey, byte[]> getUnicastMessages() {
    return unicastMessages;
  }

  Map<BlockchainPublicKey, Hash> getBroadcastEchos() {
    return broadcastEchos;
  }

  boolean getShouldPublishBitmap() {
    return shouldPublishBitmap;
  }

  private boolean hasAllBroadcastMessages(int roundNumber, LargeOracleUpdate update) {
    // BPO just advanced to the next broadcast round, so the round we want to check is the previous
    // one.
    int broadcastRound = roundNumber - 1;
    List<BlockProducer> broadcasters = update.getBroadcasters(broadcastRound);
    BroadcastTracker tracker = update.getBroadcastTracker(broadcastRound);
    for (BlockProducer broadcaster : broadcasters) {
      BlockchainAddress identity = broadcaster.identity();
      Hash message = tracker.getMessage(identity);
      // We're missing a message, so we have to wait until we get collect it.
      if (messageStorage.getBroadcastMessage(message) == null) {
        logger.info(
            "Missing broadcast="
                + message
                + ", broadcaster="
                + identity
                + ", round="
                + roundNumber);
        return false;
      }
    }
    return true;
  }

  private void sendBroadcast(
      ThresholdSessionId sessionId, LargeOracleUpdate update, int roundNumber) {
    Hash myBroadcastMessageHash = messageStorage.getMyBroadcastMessageHash(sessionId, roundNumber);
    int broadcastRound = roundNumber - 1;
    BroadcastTracker broadcastTracker = update.getBroadcastTracker(broadcastRound);
    if (broadcastTracker.getMessage(myAddress) == null && myBroadcastMessageHash != null) {
      logger.info("Sending broadcast=" + myBroadcastMessageHash + ", round=" + roundNumber);
      sendTransaction(
          bpOrchestrationContract,
          rpc -> {
            rpc.writeByte(BPO_ADD_BROADCAST_HASH);
            rpc.writeInt(sessionId.getSessionId());
            rpc.writeInt(sessionId.getRetryNonce());
            rpc.writeInt(broadcastRound);
            myBroadcastMessageHash.write(rpc);
          });
    }
  }

  private void collectUnicastMessages(ThresholdSessionId sessionId, LargeOracleUpdate update) {
    List<BlockProducer> activeProducers = update.getActiveProducers();
    for (BlockProducer activeProducer : activeProducers) {
      BlockchainPublicKey protocolId = activeProducer.publicKey();
      unicastMessages.computeIfAbsent(
          protocolId,
          pid -> {
            Address endpoint = peerDatabase.getBlockProducerEndpoint(pid);
            if (endpoint != null) {
              LargeOracleClient restClient = createRestClient(endpoint, pid);
              return restClient.getUnicastMessage(sessionId, 2);
            }
            return null;
          });
    }
  }

  private void collectBroadcastMessages(int roundNumber, LargeOracleUpdate update) {
    List<BlockProducer> activeProducers = update.getActiveProducers();
    BroadcastTracker tracker = update.getBroadcastTracker(roundNumber - 1);
    for (BlockProducer activeProducer : activeProducers) {
      BlockchainAddress id = activeProducer.identity();
      BlockchainPublicKey protocolId = activeProducer.publicKey();
      Hash messageHash = tracker.getMessage(id);
      if (messageHash != null) {
        broadcastEchos.computeIfAbsent(
            protocolId,
            pid -> {
              // check local storage for the message first -- it might happen that we are
              // broadcasting the same message, and in that case there's no need to fetch it
              // remotely.
              byte[] message = messageStorage.getBroadcastMessage(messageHash);
              if (message == null) {
                message = fetchBroadcastMessage(messageHash, update);
              }
              if (message != null) {
                messageStorage.setBroadcastMessage(messageHash, message);
                return messageHash;
              } else {
                return null;
              }
            });
      }
    }
  }

  private byte[] fetchBroadcastMessage(Hash messageHash, LargeOracleUpdate update) {
    List<BlockProducer> activeProducers = update.getActiveProducers();
    for (BlockProducer activeProducer : activeProducers) {
      BlockchainPublicKey protocolId = activeProducer.publicKey();
      Address endpoint = peerDatabase.getBlockProducerEndpoint(protocolId);
      if (endpoint != null) {
        LargeOracleClient restClient = createRestClient(endpoint, protocolId);
        byte[] broadcastMessage = restClient.getBroadcastMessage(messageHash);
        if (messageValid(broadcastMessage, messageHash)) {
          return broadcastMessage;
        }
      }
    }
    return null;
  }

  static boolean messageValid(byte[] message, Hash messageHash) {
    if (message != null) {
      Hash actual = Hash.create(s -> s.write(message));
      return actual.equals(messageHash);
    } else {
      return false;
    }
  }

  private ThresholdKeyGenProtocol.Step initializeOrLoadKeyGenSession(
      ThresholdSessionId sessionId, LargeOracleUpdate update) {
    List<BlockchainPublicKey> parties =
        update.getActiveProducers().stream().map(BlockProducer::publicKey).toList();
    logger.info(
        "Initializing key generation protocol with database="
            + databaseName
            + ", session="
            + sessionId
            + ", protocolId="
            + myProtocolId
            + ", id="
            + myAddress
            + ", index="
            + parties.indexOf(myProtocolId));
    final PbctsKeyGen keyGen = createKeyGenProtocol(sessionId, parties);
    broadcastEchos.clear();
    unicastMessages.clear();
    shouldPublishBitmap = false;
    try {
      logger.info("Attempting to resume key-gen session");
      return keyGen.resume(update.getCurrentBroadcastRound() + 1);
    } catch (RuntimeException exception) {
      logger.info("Resumption failed; starting new session");
      ThresholdKeyGenProtocol.Step init = keyGen.init();
      unicastMessages.put(
          myProtocolId, messageStorage.getRoundUnicastMessage(sessionId, 2, myProtocolId));
      Hash myBroadcastMessage = messageStorage.getMyBroadcastMessageHash(sessionId, 2);
      broadcastEchos.put(myProtocolId, myBroadcastMessage);
      return init;
    }
  }

  private boolean isFinalRound(ThresholdKeyGenProtocol.Step next) {
    return next instanceof ThresholdKeyGenProtocol.FinalStep;
  }

  private ThresholdSignatureProvider finalizeProtocol(ThresholdKeyGenProtocol.Step step) {
    ThresholdKeyGenProtocol.FinalStep lastStep = (ThresholdKeyGenProtocol.FinalStep) step;
    return lastStep.finish();
  }

  private void publishCandidateKey(
      LargeOracleUpdate update,
      ThresholdPublicKey publicKey,
      List<BlockchainPublicKey> honestParties) {
    Bitmap voters = update.voters();
    List<BlockchainAddress> producersAddresses =
        update.getActiveProducers().stream().map(BlockProducer::identity).toList();
    int myIndex = producersAddresses.indexOf(myAddress);
    if (update.thresholdKey() == null && !voters.testBit(myIndex)) {
      Bitmap honestPartiesBitmap = protocolIdsToBitmap(update.getActiveProducers(), honestParties);
      sendTransaction(
          bpOrchestrationContract,
          rpc -> {
            rpc.writeByte(BPO_ADD_CANDIDATE_KEY);
            publicKey.getPublicKey().write(rpc);
            rpc.writeString(publicKey.getPublicKeyId());
            rpc.writeDynamicBytes(honestPartiesBitmap.asBytes());
          });
    }
  }

  static Bitmap protocolIdsToBitmap(
      List<BlockProducer> producers, List<BlockchainPublicKey> protocolIds) {
    Bitmap bp = Bitmap.create(producers.size());
    for (int i = 0; i < producers.size(); i++) {
      BlockProducer producer = producers.get(i);
      for (BlockchainPublicKey protocolId : protocolIds) {
        if (producer.publicKey().equals(protocolId)) {
          bp = bp.setBit(i);
          break;
        }
      }
    }
    return bp;
  }

  KeyGenMessageRetriever getMessageRetriever(LargeOracleUpdate update) {
    return new KeyGenMessageRetriever() {
      @Override
      public byte[] retrieveUnicastMessage(BlockchainPublicKey sender) {
        return unicastMessages.get(sender);
      }

      @Override
      public byte[] retrieveBroadcastMessage(BlockchainPublicKey sender) {
        int broadcastRound = update.getCurrentBroadcastRound() - 1;
        BroadcastTracker tracker = update.getBroadcastTracker(broadcastRound);
        List<BlockProducer> broadcasters = update.getBroadcasters(broadcastRound);
        BlockProducer producer = getBlockProducer(broadcasters, sender);
        if (producer != null) {
          Hash hash = tracker.getMessage(producer.identity());
          return messageStorage.getBroadcastMessage(hash);
        } else {
          return null;
        }
      }
    };
  }

  private BlockProducer getBlockProducer(
      List<BlockProducer> producers, BlockchainPublicKey protocolId) {
    return producers.stream()
        .filter(bp -> bp.publicKey().equals(protocolId))
        .findFirst()
        .orElse(null);
  }

  private LargeOracleClient createRestClient(
      Address endpoint, BlockchainPublicKey serverPublicKey) {
    return new LargeOracleClient(
        endpoint::getEndpoint,
        serverPublicKey,
        restServerSigningKey,
        ephemeralKeySupplier.get(),
        webClient);
  }

  private void runCurrent(
      BpOrchestrationContractState bpoState,
      ThresholdSessionId sessionId,
      LargeOracleUpdate incoming,
      LargeOracleContractState locState) {

    boolean partOfCurrentOracle = isPartOfCurrentOracle(bpoState.committee());
    if (partOfCurrentOracle) {

      if (!signers.containsKey(sessionId)) {
        PbctsSigner signer =
            loadSignerFromDatabase(
                bpoState.committee(),
                bpoState.sessionId(),
                bpoState.retryNonce(),
                bpoState.thresholdKey());
        if (signer != null) {
          putSigner(sessionId, signer);
        }
      }

      if (signers.containsKey(sessionId)) {
        if (incoming == null) {
          signMessages(sessionId, locState.initialMessageNonce(), locState.pendingMessages());
          combineAndPublishSignatures(sessionId, locState.getSignedMessages());
        } else {
          signIncomingOracle(sessionId, incoming, bpoState.domainSeparator());
          combineNewOracleSignature(sessionId);
        }
      }
    }
  }

  private void signIncomingOracle(
      ThresholdSessionId sessionId, LargeOracleUpdate incoming, Hash domainSeparator) {
    if (incoming.thresholdKey() != null) {
      ThresholdSignatureProvider provider = signers.get(sessionId);
      String signatureId = getNextOracleSignatureId(sessionId);
      if (messageStorage.getPartialSignature(sessionId, signatureId) == null) {
        Hash messageToBeSigned =
            incoming.getMessageToBeSigned(sessionId.getSessionId() + 1, domainSeparator);
        logger.info("Signing incoming oracle");
        provider.preSignAndStore(
            messageToBeSigned, signatureId, incoming.signatureRandomization().getBytes());
      }
    }
  }

  private String getNextOracleSignatureId(ThresholdSessionId sessionId) {
    return signers.get(sessionId).getSignatureIds().get(0);
  }

  private void signMessages(
      ThresholdSessionId sessionId,
      Integer initialMessageNonce,
      FixedList<SigningRequest> pendingMessages) {
    ThresholdSignatureProvider provider = signers.get(sessionId);
    for (SigningRequest message : pendingMessages) {
      String signatureId = getSignatureId(provider, message.nonce() - initialMessageNonce);
      // only sign messages that we haven't signed before
      if (signatureId != null
          && messageStorage.getPartialSignature(sessionId, signatureId) == null) {
        logger.info(
            "Signing message="
                + message.messageHash()
                + ", nonce="
                + message.nonce()
                + ", signatureId="
                + signatureId);
        provider.preSignAndStore(
            message.messageHash(), signatureId, message.transactionHash().getBytes());
        signatureIdToNonce.put(signatureId, message.nonce());
        signatureIdToMessage.put(signatureId, message.messageHash());
      }
    }
  }

  private void combineNewOracleSignature(ThresholdSessionId sessionId) {
    List<SignatureAndContributors> fresh =
        signers.get(sessionId).collectNewSignatures(this::fetchPreSignatures);
    for (SignatureAndContributors freshSignature : fresh) {
      String signatureId = freshSignature.signatureId;
      if (isNextOracleSignatureId(sessionId, signatureId)) {
        logger.info(
            "Publishing signature on new oracle: " + freshSignature.signature.writeAsString());
        sendTransaction(
            bpOrchestrationContract,
            rpc -> {
              rpc.writeByte(BPO_AUTHORIZE_NEW_ORACLE);
              freshSignature.signature.write(rpc);
            });
      }
    }
  }

  private boolean isNextOracleSignatureId(ThresholdSessionId sessionId, String signatureId) {
    return signatureId.equals(getNextOracleSignatureId(sessionId));
  }

  private void combineAndPublishSignatures(
      ThresholdSessionId sessionId, List<SigningRequest> signedMessages) {
    List<SignatureAndContributors> fresh =
        signers.get(sessionId).collectNewSignatures(this::fetchPreSignatures);
    for (SignatureAndContributors freshSignature : fresh) {
      String signatureId = freshSignature.signatureId;
      if (!publishedOnChain(signatureId, signedMessages)) {
        Signature signature = freshSignature.signature;
        int signatureNonce = getNonceForSignatureId(signatureId);
        Hash signedMessage = getMessageForSignatureId(signatureId);
        logger.info(
            "Publishing signature="
                + signature.writeAsString()
                + ", signatureId="
                + signatureId
                + ", nonce="
                + signatureNonce);
        sendTransaction(
            largeOracleContract,
            rpc -> {
              rpc.writeByte(LO_ADD_SIGNATURE);
              signedMessage.write(rpc);
              rpc.writeInt(signatureNonce);
              signature.write(rpc);
            });
      }
    }
  }

  private Hash getMessageForSignatureId(String signatureId) {
    return signatureIdToMessage.get(signatureId);
  }

  private int getNonceForSignatureId(String signatureId) {
    return signatureIdToNonce.get(signatureId);
  }

  private boolean publishedOnChain(String signatureId, List<SigningRequest> signedMessages) {

    int nonceForSignatureId = getNonceForSignatureId(signatureId);
    return signedMessages.stream().anyMatch(m -> m.nonce().equals(nonceForSignatureId));
  }

  private byte[] fetchPreSignatures(
      ThresholdSessionId sessionId, String signatureId, BlockchainPublicKey from) {
    Address endpoint = peerDatabase.getBlockProducerEndpoint(from);
    if (endpoint != null) {
      LargeOracleClient client = createRestClient(endpoint, from);
      return client.getPartialSignature(sessionId, signatureId);
    }
    return null;
  }

  private String getSignatureId(ThresholdSignatureProvider provider, int offsetNonce) {
    List<String> signatureIds = provider.getSignatureIds();
    // first signatureID is reserved for signing next oracle.
    int actualOffset = 1 + offsetNonce;
    if (signatureIds.size() <= actualOffset) {
      return null;
    } else {
      return signatureIds.get(actualOffset);
    }
  }

  private PbctsSigner loadSignerFromDatabase(
      FixedList<BlockProducer> committee,
      int sessionId,
      int retryNonce,
      ThresholdKey thresholdKey) {
    List<BlockchainPublicKey> parties = committee.stream().map(BlockProducer::publicKey).toList();
    ThresholdSessionId thresholdSessionId = getCurrentSessionId(sessionId, retryNonce);
    String keyId = thresholdKey.keyId();
    try {
      PbctsSigner signer =
          PbctsSigner.load(
              thresholdSessionId,
              keyId,
              parties,
              myProtocolId,
              messageStorage,
              LongStream.range(0, parties.size()).boxed().toList(),
              databaseName,
              databaseBackupName,
              databaseKey);
      // if the call to load above succeeds, this call will check that what we managed to load
      // actually contains a public key.
      ThresholdPublicKey publicKey = signer.getPublicKey();
      logger.info("Loaded a signer from " + databaseName + ", key=" + publicKey);
      return signer;
    } catch (Exception error) {
      logger.warn("Could not load signer from database");
      return null;
    }
  }

  private ThresholdSessionId getCurrentSessionId(int sessionId, int retryNonce) {
    return new ThresholdSessionId(sessionId, retryNonce);
  }

  private PbctsKeyGen createKeyGenProtocol(
      ThresholdSessionId sessionId, List<BlockchainPublicKey> parties) {
    // parties have to be identified via their network keys in the protocol
    if (databaseExists(databaseName)) {
      return loadKeyGenProtocol(sessionId, parties);
    } else {
      return PbctsKeyGen.create(
          sessionId,
          parties,
          myProtocolId,
          messageStorage,
          preSignatureCount,
          databaseName,
          databaseBackupName,
          databaseKey);
    }
  }

  private PbctsKeyGen loadKeyGenProtocol(
      ThresholdSessionId sessionId, List<BlockchainPublicKey> parties) {
    return PbctsKeyGen.load(
        sessionId,
        parties,
        myProtocolId,
        messageStorage,
        preSignatureCount,
        databaseName,
        databaseBackupName,
        databaseKey);
  }

  static boolean databaseExists(String databaseName) {
    // For testing purposes only
    if (databaseName.equals(":memory:")) {
      return false;
    } else {
      return ExceptionConverter.call(
          () -> Files.exists(Path.of(databaseName)), "Could not stat file");
    }
  }

  boolean isPartOfCurrentOracle(FixedList<BlockProducer> committee) {
    return committee.stream().anyMatch(p -> p.identity().equals(myAddress));
  }

  boolean isActiveParticipant(LargeOracleUpdate oracle) {
    List<BlockProducer> activeProducers = oracle.getActiveProducers();
    if (activeProducers == null) {
      return false;
    } else {
      return activeProducers.stream().anyMatch(p -> p.identity().equals(myAddress));
    }
  }

  boolean isParticipant(LargeOracleUpdate oracle) {
    return oracle.producers().stream().anyMatch(p -> p.identity().equals(myAddress));
  }

  private void collectHeartbeats(LargeOracleUpdate update) {
    // we will only collect heartbeats if (1) no active producer set have been selected yet, (2) we
    // are part of this oracle, (3) we haven't collected heartbeats yet.
    if (update.getActiveProducers() != null
        || !isParticipant(update)
        || update.getHeartbeat(myAddress) != null) {
      return;
    }

    FixedList<BlockProducer> parties = update.producers();
    int n = parties.size();
    Bitmap heartbeatBitmap = Bitmap.create(n);
    for (int i = 0; i < n; i++) {
      BlockProducer producer = parties.get(i);
      if (producer.identity().equals(myAddress)) {
        heartbeatBitmap = heartbeatBitmap.setBit(i);
      } else {
        logger.info("Checking if " + producer.identity() + " is alive");
        if (isAlive(producer)) {
          heartbeatBitmap = heartbeatBitmap.setBit(i);
        }
      }
    }

    int threshold = computeThreshold(n);
    if (heartbeatBitmap.popCount() > 2 * threshold) {
      Bitmap finalHeartbeatBitmap = heartbeatBitmap;
      sendTransaction(
          bpOrchestrationContract,
          rpc -> {
            rpc.writeByte(BPO_ADD_HEARTBEAT);
            rpc.writeDynamicBytes(finalHeartbeatBitmap.asBytes());
          });
    }
  }

  private boolean isAlive(BlockProducer producer) {
    PeerDiscoveryClient client =
        PeerDiscoveryProcess.createRestClient(
            versionInformation,
            peerDatabase.getBlockProducerEndpoint(producer.publicKey()),
            producer.publicKey(),
            producer.identity(),
            myAddress,
            restServerSigningKey,
            peerDatabase.getBlockProducerEndpoint(restServerSigningKey.getPublic()),
            webClient,
            ephemeralKeySupplier.get());
    return PeerDiscoveryProcess.handshake(client);
  }

  private void sendTransaction(BlockchainAddress address, Consumer<SafeDataOutputStream> rpc) {
    Transaction transaction = Transaction.create(address, SafeDataOutputStream.serialize(rpc));
    transactionSender.sendTransactionAndWaitForEvents(transaction, transactionCost);
  }

  private ThresholdSessionId getNextSessionIdFromCurrent(
      ThresholdSessionId currentSessionId, LargeOracleUpdate update) {
    return new ThresholdSessionId(currentSessionId.getSessionId() + 1, update.retryNonce());
  }

  ThresholdSignatureProvider getSigner(ThresholdSessionId sessionId) {
    return signers.get(sessionId);
  }

  ThresholdKeyGenProtocol.Step getCurrentStep(ThresholdSessionId sessionId) {
    return keyGenProtocols.get(sessionId);
  }

  void putSigner(ThresholdSessionId sessionId, ThresholdSignatureProvider signer) {
    signers.put(sessionId, signer);
  }
}
