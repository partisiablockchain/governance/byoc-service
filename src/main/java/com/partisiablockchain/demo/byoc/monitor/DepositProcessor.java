package com.partisiablockchain.demo.byoc.monitor;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.client.transaction.Transaction;
import com.partisiablockchain.demo.byoc.governance.byocincoming.ByocIncomingContractState;
import com.partisiablockchain.demo.byoc.governance.byocincoming.Deposit;
import com.partisiablockchain.demo.byoc.rest.dto.DepositDto;
import com.partisiablockchain.demo.byoc.settings.DepositSettings;
import com.partisiablockchain.math.Unsigned256;
import com.secata.stream.SafeDataOutputStream;

/**
 * Implementation of BYOC deposit requests.
 *
 * <p>Receives deposit requests and registers these on PBC.
 */
public final class DepositProcessor implements ByocProcess {
  /**
   * The maximum number of times to wait for a state update when processing a deposit that has
   * already been signed. The default time to sleep is 5 seconds, so sleeping 6 times corresponds to
   * waiting ~30 seconds before sending a new transaction.
   */
  private static final int MAX_SLEEPS_FOR_STATE_UPDATE = 6;

  private final BlockchainAddress byocAddress;
  private final BlockchainAddress pbcAccount;
  private final long transactionCost;
  private final DataSource dataSource;
  private final ByocTransactionSender sender;
  private final ContractStateHandler contractStateHandler;
  private long latestSignedDepositNonce;
  private int sleepsWaitingForStateUpdate;

  /**
   * Default constructor setting up a deposit processor depending on the settings.
   *
   * @param settings configuration object
   */
  public DepositProcessor(DepositSettings settings) {
    this(
        settings.getDepositContractAddress(),
        settings.getPbcAccount(),
        settings.getTransactionCost(),
        settings.getTransactionSender(),
        settings.getDepositDataSource(),
        settings.getContractStateHandler());
  }

  DepositProcessor(
      BlockchainAddress byocAddress,
      BlockchainAddress pbcAccount,
      long transactionCost,
      ByocTransactionSender sender,
      DataSource dataSource,
      ContractStateHandler contractStateHandler) {
    this.byocAddress = byocAddress;
    this.pbcAccount = pbcAccount;
    this.transactionCost = transactionCost;
    this.dataSource = dataSource;
    this.sender = sender;
    this.contractStateHandler = contractStateHandler;
    latestSignedDepositNonce = -1;
    sleepsWaitingForStateUpdate = 0;
  }

  @Override
  public boolean run() {
    ByocIncomingContractState byocState =
        contractStateHandler.getState(byocAddress, ByocIncomingContractState.class);
    if (byocState == null) {
      return false;
    }

    int processorIndexInOracle = byocState.oracleNodes().indexOf(pbcAccount);
    boolean isProcessorNotPartOfOracle = processorIndexInOracle == -1;
    if (isProcessorNotPartOfOracle) {
      return false;
    }

    Deposit deposit = byocState.deposit();
    boolean hasContractRegisteredSignatureFromProcessor =
        deposit != null && deposit.hasSigned(processorIndexInOracle);
    if (hasContractRegisteredSignatureFromProcessor) {
      return false;
    }

    long depositNonce = byocState.depositNonce();
    // If this deposit has already been signed, but the contract has not registered this, we must
    // determine whether to retry the transaction or wait for the contract to update
    boolean hasSignedDeposit = depositNonce == latestSignedDepositNonce;
    if (hasSignedDeposit && shouldWaitForContractStateChange()) {
      return false;
    }

    DepositDto depositDto = dataSource.waitForDeposit(depositNonce);
    // If the incoming contract has moved on to a new deposit while we waited for the external
    // deposit, move on to the new deposit immediately.
    long latestDepositNonce =
        contractStateHandler.getState(byocAddress, ByocIncomingContractState.class).depositNonce();
    if (depositNonce != latestDepositNonce) {
      return true;
    }

    Transaction tx = createTransaction(depositNonce, depositDto);
    sender.sendTransactionAndWaitForEvents(tx, transactionCost);
    latestSignedDepositNonce = depositNonce;
    return true;
  }

  /**
   * Determines whether this processor should wait for a contract state change that reflects a
   * previously sent signature, or send the transaction again.
   *
   * @return true if this processor should wait for the incoming contract to update its state
   *     change, or false if this processor should continue with the deposit.
   */
  private boolean shouldWaitForContractStateChange() {
    if (sleepsWaitingForStateUpdate < MAX_SLEEPS_FOR_STATE_UPDATE) {
      sleepsWaitingForStateUpdate += 1;
      return true;
    } else {
      sleepsWaitingForStateUpdate = 0;
      return false;
    }
  }

  @Override
  public String name() {
    return DepositProcessor.class.getSimpleName();
  }

  private Transaction createTransaction(long depositNonce, DepositDto depositDto) {
    return Transaction.create(
        byocAddress,
        SafeDataOutputStream.serialize(
            stream -> {
              stream.writeByte(0); // DEPOSIT
              stream.writeLong(depositNonce);
              BlockchainAddress.fromString(depositDto.pbcAddress).write(stream);
              Unsigned256.create(depositDto.amount).write(stream);
            }));
  }

  /**
   * Get data source.
   *
   * @return data source
   */
  public DataSource getDataSource() {
    return dataSource;
  }
}
