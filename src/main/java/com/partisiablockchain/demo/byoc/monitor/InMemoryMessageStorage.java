package com.partisiablockchain.demo.byoc.monitor;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.governance.MessageStorage;
import com.partisiablockchain.governance.ThresholdSessionId;
import java.util.HashMap;
import java.util.Map;

/** In-memory message storage. */
public final class InMemoryMessageStorage implements MessageStorage {

  private final Map<ThresholdSessionId, Map<String, byte[]>> partialSignatures;
  private final Map<ThresholdSessionId, Map<BlockchainPublicKey, byte[]>> unicastMessages;
  private final Map<ThresholdSessionId, Map<Integer, Hash>> myBroadcastMessages;
  private final Map<Hash, byte[]> broadcastMessages;

  /** Create a new message storage object for storing messages in-memory. */
  public InMemoryMessageStorage() {
    this.partialSignatures = new HashMap<>();
    this.unicastMessages = new HashMap<>();
    this.broadcastMessages = new HashMap<>();
    this.myBroadcastMessages = new HashMap<>();
  }

  @Override
  public byte[] getPartialSignature(ThresholdSessionId sessionId, String signatureId) {
    Map<String, byte[]> inner = partialSignatures.get(sessionId);
    if (inner != null) {
      return inner.get(signatureId);
    }
    return null;
  }

  @Override
  public void setPartialSignature(ThresholdSessionId sessionId, String signatureId, byte[] data) {
    if (!partialSignatures.containsKey(sessionId)) {
      partialSignatures.put(sessionId, new HashMap<>());
    }
    partialSignatures.get(sessionId).put(signatureId, data);
  }

  @Override
  public byte[] getRoundUnicastMessage(
      ThresholdSessionId sessionId, int roundNumber, BlockchainPublicKey to) {
    // only round 2 has unicast messages
    Map<BlockchainPublicKey, byte[]> inner = unicastMessages.get(sessionId);
    if (inner != null) {
      return inner.get(to);
    }
    return null;
  }

  @Override
  public void setRoundUnicastMessage(
      ThresholdSessionId sessionId, int roundNumber, BlockchainPublicKey to, byte[] data) {
    if (!unicastMessages.containsKey(sessionId)) {
      unicastMessages.put(sessionId, new HashMap<>());
    }
    unicastMessages.get(sessionId).put(to, data);
  }

  @Override
  public byte[] getBroadcastMessage(Hash messageHash) {
    return broadcastMessages.get(messageHash);
  }

  @Override
  public void setBroadcastMessage(ThresholdSessionId sessionId, int roundNumber, byte[] message) {
    Hash messageHash = Hash.create(s -> s.write(message));
    setBroadcastMessage(messageHash, message);
    if (!myBroadcastMessages.containsKey(sessionId)) {
      myBroadcastMessages.put(sessionId, new HashMap<>());
    }
    myBroadcastMessages.get(sessionId).put(roundNumber, messageHash);
  }

  void setBroadcastMessage(Hash messageHash, byte[] message) {
    broadcastMessages.putIfAbsent(messageHash, message);
  }

  Hash getMyBroadcastMessageHash(ThresholdSessionId sessionId, int roundNumber) {
    Map<Integer, Hash> inner = myBroadcastMessages.get(sessionId);
    if (inner != null) {
      return inner.get(roundNumber);
    } else {
      return null;
    }
  }
}
