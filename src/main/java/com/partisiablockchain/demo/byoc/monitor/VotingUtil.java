package com.partisiablockchain.demo.byoc.monitor;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.client.transaction.Transaction;
import com.partisiablockchain.demo.byoc.governance.largeoracle.OracleDisputeId;
import com.secata.stream.SafeDataOutputStream;

/** A utility class for executing voting invocations on the large oracle contract. */
public final class VotingUtil {

  private static final byte ADD_COUNTER_CLAIM = 1;
  private static final byte VOTE_ON_DISPUTE = 2;

  ByocTransactionSender sender;
  BlockchainAddress largeOracleContract;

  private VotingUtil(ByocTransactionSender sender, BlockchainAddress largeOracleContract) {
    this.sender = sender;
    this.largeOracleContract = largeOracleContract;
  }

  /**
   * Creates a new voting util for disputes.
   *
   * @param sender transaction sender proxy
   * @param largeOracleContract the contract for the large oracle
   * @return a new Voting Util object
   */
  public static VotingUtil create(
      ByocTransactionSender sender, BlockchainAddress largeOracleContract) {
    return new VotingUtil(sender, largeOracleContract);
  }

  /**
   * Casts a vote in a dispute.
   *
   * @param id the id object for the dispute
   * @param vote the vote that was cast in the dispute
   */
  public void castVote(OracleDisputeId id, int vote) {
    Transaction tx =
        Transaction.create(
            largeOracleContract,
            SafeDataOutputStream.serialize(
                stream -> {
                  stream.writeByte(VOTE_ON_DISPUTE);
                  id.smallOracle().write(stream);
                  stream.writeLong(id.oracleId());
                  stream.writeLong(id.disputeId());
                  stream.writeInt(vote);
                }));
    // Cost is always 0 since it is only members of the large oracle that are going to vote,
    // and they have sufficient stakes to be allowed to send free transactions.
    sender.sendTransactionAndWaitForEvents(tx, 0);
  }

  /**
   * Adds a counter claim to a dispute.
   *
   * @param id the id object for the dispute
   * @param counterClaim another claim to the dispute
   */
  public void addCounterClaim(OracleDisputeId id, byte[] counterClaim) {
    Transaction tx =
        Transaction.create(
            largeOracleContract,
            SafeDataOutputStream.serialize(
                stream -> {
                  stream.writeByte(ADD_COUNTER_CLAIM);
                  id.smallOracle().write(stream);
                  stream.writeLong(id.oracleId());
                  stream.writeLong(id.disputeId());
                  stream.writeDynamicBytes(counterClaim);
                }));
    // Cost is always 0 since it is only members of the large oracle that are going to vote,
    // and they have sufficient stakes to be allowed to send free transactions.
    sender.sendTransactionAndWaitForEvents(tx, 0);
  }
}
