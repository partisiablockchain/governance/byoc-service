package com.partisiablockchain.demo.byoc.monitor;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/** JVM memory usage information provider. */
public interface MemoryUsageProvider {
  /**
   * Returns the amount of free memory in the Java Virtual Machine.
   *
   * @return an approximation to the total amount of memory currently available for future allocated
   *     objects, measured in bytes.
   */
  long getFreeMemory();

  /**
   * Returns the total amount of memory in the Java Virtual Machine.
   *
   * @return the total amount of memory currently available for current and future objects, measured
   *     in bytes.
   */
  long getTotalMemory();

  /**
   * Returns the maximum amount of memory that the Java Virtual Machine will attempt to use.
   *
   * @return the maximum amount of memory that the virtual machine will attempt to use, measured in
   *     bytes
   */
  long getMaxMemory();
}
