package com.partisiablockchain.demo.byoc.monitor;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.demo.byoc.governance.byocincoming.DepositTransaction;
import com.partisiablockchain.demo.byoc.governance.byocoutgoing.WithdrawalTransaction;
import com.partisiablockchain.demo.byoc.rest.dto.DepositDto;
import com.partisiablockchain.demo.byoc.settings.EthereumSettings;
import com.partisiablockchain.math.Unsigned256;
import com.secata.stream.SafeDataInputStream;
import com.secata.tools.coverage.ExceptionConverter;
import java.math.BigInteger;
import java.util.List;
import java.util.concurrent.TimeUnit;
import okhttp3.OkHttpClient;
import org.web3j.abi.EventEncoder;
import org.web3j.abi.FunctionReturnDecoder;
import org.web3j.abi.TypeReference;
import org.web3j.abi.datatypes.Address;
import org.web3j.abi.datatypes.Event;
import org.web3j.abi.datatypes.Type;
import org.web3j.abi.datatypes.generated.Bytes21;
import org.web3j.abi.datatypes.generated.Uint256;
import org.web3j.abi.datatypes.generated.Uint32;
import org.web3j.abi.datatypes.generated.Uint64;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.core.DefaultBlockParameter;
import org.web3j.protocol.core.DefaultBlockParameterName;
import org.web3j.protocol.core.methods.request.EthFilter;
import org.web3j.protocol.core.methods.response.EthLog;
import org.web3j.protocol.core.methods.response.Log;
import org.web3j.protocol.http.HttpService;

/** Datasource listening on the Ethereum blockchain's event log for new BYOC events. */
public final class Ethereum implements DataSource, ByocProcess {

  /** Ethereum deposit event definition. */
  public static final Event DEPOSIT_EVENT =
      new Event(
          "Deposit",
          List.of(
              new TypeReference<Uint64>(false) {}, // nonce
              new TypeReference<Bytes21>(false) {}, // destination
              new TypeReference<Uint256>(false) {} // amount
              ));

  /** Encoded ethereum deposit event definition. */
  public static final String ENCODED_DEPOSIT_EVENT = EventEncoder.encode(DEPOSIT_EVENT);

  /** Withdrawal event definition. */
  public static final Event WITHDRAWAL_EVENT =
      new Event(
          "Withdrawal",
          List.of(
              new TypeReference<Uint64>(false) {}, // withdrawalNonce
              new TypeReference<Address>(false) {}, // destination
              new TypeReference<Uint256>(false) {}, // amount
              new TypeReference<Uint64>(false) {}, // oracleNonce
              new TypeReference<Uint32>(false) {} // bitmask
              ));

  /** Encoded withdrawal event definition. */
  public static final String ENCODED_WITHDRAWAL_EVENT = EventEncoder.encode(WITHDRAWAL_EVENT);

  private final Web3j web3j;
  private final String contractAddress;
  private final EthereumEventsCache cache;
  private final BlockPagination blockPagination;
  private final ContractInformation contractInformation;

  Ethereum(
      Web3j web3j,
      String contractAddress,
      EthereumEventsCache cache,
      BlockPagination blockPagination,
      ContractInformation contractInformation) {
    this.web3j = web3j;
    this.contractAddress = contractAddress;
    this.cache = cache;
    this.blockPagination = blockPagination;
    this.contractInformation = contractInformation;
  }

  /**
   * Create new Ethereum process.
   *
   * @param settings configuration for the process
   * @return new ethereum process
   */
  public static Ethereum create(EthereumSettings settings) {
    OkHttpClient httpClient = createHttpClient();
    Web3j client = Web3j.build(new HttpService(settings.getEthereumEndpoint(), httpClient));
    BlockPagination blockPagination =
        new DefaultBlockPagination(
            client, settings.getEthereumBlockValidityDepth(), settings.getMaxBlockRange());
    ContractInformation contractLookup =
        new BinarySearchDeploymentBlock(client, settings.getEthereumByocContract());
    return new Ethereum(
        client,
        settings.getEthereumByocContract(),
        settings.getCache(),
        blockPagination,
        contractLookup);
  }

  static OkHttpClient createHttpClient() {
    return new OkHttpClient.Builder()
        .connectTimeout(30, TimeUnit.SECONDS)
        .readTimeout(30, TimeUnit.SECONDS)
        .build();
  }

  @SuppressWarnings("rawtypes")
  @Override
  public boolean run() {
    if (cache.loadLatestCheckedBlock() == null) {
      BigInteger initialBlock = contractInformation.deploymentBlock();
      cache.saveLatestCheckedBlock(Unsigned256.create(initialBlock));
    }

    BigInteger latestCheckedBlock = new BigInteger(cache.loadLatestCheckedBlock().serialize());
    BlockRange nextRange = blockPagination.nextRange(latestCheckedBlock);

    List<EthLog.LogResult> eventLogs = getLogs(nextRange.fromBlock(), nextRange.toBlock);
    List<DepositTransaction> depositLogs = getDepositLogs(eventLogs);
    List<WithdrawalTransaction> withdrawalLogs = getWithdrawalLogs(eventLogs);
    cacheEventLogs(depositLogs, withdrawalLogs, Unsigned256.create(nextRange.toBlock));

    return nextRange.moreBlocksAvailable();
  }

  private synchronized void cacheEventLogs(
      List<DepositTransaction> depositLogs,
      List<WithdrawalTransaction> withdrawalLogs,
      Unsigned256 latestFinalBlock) {
    cache.saveDeposits(depositLogs);
    cache.saveWithdrawals(withdrawalLogs);
    cache.saveLatestCheckedBlock(latestFinalBlock);
    this.notifyAll();
  }

  @Override
  public String name() {
    return Ethereum.class.getSimpleName();
  }

  @Override
  public synchronized DepositDto waitForDeposit(long depositNonce) {
    while (cache.loadDeposit(depositNonce) == null) {
      ExceptionConverter.run(this::wait, "Wait interrupted");
    }
    DepositTransaction deposit = cache.loadDeposit(depositNonce);
    DepositDto dto = new DepositDto();
    dto.pbcAddress = deposit.receiver().writeAsString();
    dto.amount = deposit.amount().toString();
    return dto;
  }

  @Override
  public synchronized DepositTransaction getDeposit(long depositNonce) {
    return cache.loadDeposit(depositNonce);
  }

  @Override
  public synchronized WithdrawalTransaction getWithdrawal(WithdrawalId id) {
    return cache.loadWithdrawal(id);
  }

  @Override
  public List<WithdrawalTransaction> getAllWithdrawalsForOracleNonce(long oracleNonce) {
    return cache.loadAllWithdrawals().stream()
        .filter(withdrawal -> withdrawal.oracleNonce() == oracleNonce)
        .toList();
  }

  @SuppressWarnings("rawtypes")
  private List<DepositTransaction> getDepositLogs(List<EthLog.LogResult> eventLogs) {
    return eventLogs.stream()
        .filter(this::isLogPresent)
        .filter(this::isDepositEventLog)
        .map(this::parseDepositEvent)
        .toList();
  }

  @SuppressWarnings("rawtypes")
  private List<WithdrawalTransaction> getWithdrawalLogs(List<EthLog.LogResult> eventLogs) {
    return eventLogs.stream()
        .filter(this::isLogPresent)
        .filter(this::isWithdrawalEventLog)
        .map(this::parseWithdrawalEvent)
        .toList();
  }

  @SuppressWarnings("rawtypes")
  private List<EthLog.LogResult> getLogs(BigInteger from, BigInteger to) {
    DefaultBlockParameter fromBlock =
        from.compareTo(BigInteger.ZERO) > 0
            ? DefaultBlockParameter.valueOf(from)
            : DefaultBlockParameterName.EARLIEST;

    DefaultBlockParameter toBlock = DefaultBlockParameter.valueOf(to);
    EthFilter logFilter =
        new EthFilter(fromBlock, toBlock, contractAddress)
            .addOptionalTopics(ENCODED_DEPOSIT_EVENT, ENCODED_WITHDRAWAL_EVENT);
    String errorMessage =
        "Failed to fetch event logs [fromBlock='%s', toBlock='%s', contract='%s']"
            .formatted(from, to, contractAddress);
    return ExceptionConverter.call(
        () -> web3j.ethGetLogs(logFilter).send().getLogs(), errorMessage);
  }

  @SuppressWarnings("rawtypes")
  private boolean isLogPresent(EthLog.LogResult logResult) {
    Log log = (Log) logResult.get();
    return !log.isRemoved();
  }

  @SuppressWarnings("rawtypes")
  private boolean isDepositEventLog(EthLog.LogResult logResult) {
    Log log = (Log) logResult.get();
    return ENCODED_DEPOSIT_EVENT.equals(log.getTopics().get(0));
  }

  @SuppressWarnings("rawtypes")
  private boolean isWithdrawalEventLog(EthLog.LogResult logResult) {
    Log log = (Log) logResult.get();
    return ENCODED_WITHDRAWAL_EVENT.equals(log.getTopics().get(0));
  }

  @SuppressWarnings("rawtypes")
  private WithdrawalTransaction parseWithdrawalEvent(EthLog.LogResult logResult) {
    Log log = (Log) logResult.get();
    List<Type> decoded =
        FunctionReturnDecoder.decode(log.getData(), WITHDRAWAL_EVENT.getNonIndexedParameters());
    long withdrawalNonce = ((Uint64) decoded.get(0)).getValue().longValue();
    String destination = ((Address) decoded.get(1)).getValue().substring(2); // Remove 0x prefix
    String amount = ((Uint256) decoded.get(2)).getValue().toString();
    long oracleNonce = ((Uint64) decoded.get(3)).getValue().longValue();
    int bitmask = ((Uint32) decoded.get(4)).getValue().intValue();
    return new WithdrawalTransaction(
        withdrawalNonce, destination, Unsigned256.create(amount), oracleNonce, bitmask);
  }

  @SuppressWarnings("rawtypes")
  private DepositTransaction parseDepositEvent(EthLog.LogResult logResult) {
    Log log = (Log) logResult.get();
    List<Type> decoded =
        FunctionReturnDecoder.decode(log.getData(), DEPOSIT_EVENT.getNonIndexedParameters());
    long nonce = ((Uint64) decoded.get(0)).getValue().longValue();
    byte[] pbcAddress = ((Bytes21) decoded.get(1)).getValue();
    String amount = ((Uint256) decoded.get(2)).getValue().toString();
    BlockchainAddress address =
        BlockchainAddress.read(SafeDataInputStream.createFromBytes(pbcAddress));
    return new DepositTransaction(nonce, address, Unsigned256.create(amount));
  }

  record BlockRange(BigInteger fromBlock, BigInteger toBlock, boolean moreBlocksAvailable) {}

  /** Strategy for iterating over the blocks of the current chain. */
  interface BlockPagination {
    /**
     * Get the next range of block to read events from.
     *
     * @param latestCheckedBlock the last block that was read
     * @return next block range
     */
    BlockRange nextRange(BigInteger latestCheckedBlock);
  }

  /** Information about the current ethereum contract. */
  interface ContractInformation {

    /**
     * Get the number of the block that the contract was deployed in.
     *
     * @return deployment block number
     */
    BigInteger deploymentBlock();
  }

  static final class DefaultBlockPagination implements BlockPagination {
    private final Web3j web3j;
    private final BigInteger blockValidityDepth;
    private final BigInteger maxBlockRange;
    private BigInteger latestFinal;

    DefaultBlockPagination(Web3j web3j, BigInteger blockValidityDepth, BigInteger maxBlockRange) {
      this.web3j = web3j;
      this.blockValidityDepth = blockValidityDepth;
      this.maxBlockRange = maxBlockRange;
      this.latestFinal = BigInteger.ZERO;
    }

    @Override
    public BlockRange nextRange(BigInteger latestCheckedBlock) {
      if (latestCheckedBlock.compareTo(latestFinal) >= 0) {
        latestFinal = getLatestFinalBlockNumber();
      }

      if (latestFinal.subtract(latestCheckedBlock).compareTo(maxBlockRange) <= 0) {
        return new BlockRange(latestCheckedBlock, latestFinal, false);
      } else {
        return new BlockRange(latestCheckedBlock, latestCheckedBlock.add(maxBlockRange), true);
      }
    }

    private BigInteger getLatestFinalBlockNumber() {
      return ExceptionConverter.call(
          () -> web3j.ethBlockNumber().send().getBlockNumber().subtract(blockValidityDepth),
          "Failed to get the number of the latest final block");
    }
  }

  static final class BinarySearchDeploymentBlock implements ContractInformation {

    private final Web3j web3j;
    private final String contractAddress;
    private BigInteger lowBlock;
    private BigInteger highBlock;
    private BigInteger deploymentBlock;

    BinarySearchDeploymentBlock(Web3j web3j, String contractAddress) {
      this.web3j = web3j;
      this.contractAddress = contractAddress;
      this.lowBlock = BigInteger.ZERO;
    }

    @Override
    public BigInteger deploymentBlock() {
      if (deploymentBlock == null) {
        deploymentBlock = searchForDeploymentBlock();
      }
      return deploymentBlock;
    }

    private BigInteger searchForDeploymentBlock() {
      if (highBlock == null) {
        highBlock =
            ExceptionConverter.call(
                () -> web3j.ethBlockNumber().send().getBlockNumber(),
                "Failed to get latest block number");
      }

      while (lowBlock.compareTo(highBlock) <= 0) {
        BigInteger difference = highBlock.subtract(lowBlock);
        BigInteger middle = lowBlock.add(difference.divide(BigInteger.valueOf(2)));

        String errorMessage =
            String.format(
                "Failed to get contract code "
                    + "[contract='%s', lowBlock='%s', middleBlock='%s', highBlock='%s']",
                contractAddress,
                lowBlock.toString(10),
                middle.toString(10),
                highBlock.toString(10));
        String code =
            ExceptionConverter.call(
                () ->
                    web3j
                        .ethGetCode(contractAddress, DefaultBlockParameter.valueOf(middle))
                        .send()
                        .getCode(),
                errorMessage);

        boolean isEmptyCodeResult = "0x".equals(code);
        if (isEmptyCodeResult) {
          // Empty code return, so we know that it was deployed after the current "middle" block.
          lowBlock = middle.add(BigInteger.ONE);
        } else {
          // Non-empty so we know that it exists at this point.
          // If current "low" is equal to current "middle" we cannot go further back.
          // Else if we can set the current "high" to "middle"
          if (lowBlock.equals(middle)) {
            return middle;
          } else {
            highBlock = middle;
          }
        }
      }
      throw new RuntimeException(
          "Failed to find deployment block for '%s'".formatted(contractAddress));
    }
  }
}
