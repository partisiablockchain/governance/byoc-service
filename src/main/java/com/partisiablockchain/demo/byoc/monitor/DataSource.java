package com.partisiablockchain.demo.byoc.monitor;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.demo.byoc.governance.byocincoming.DepositTransaction;
import com.partisiablockchain.demo.byoc.governance.byocoutgoing.WithdrawalTransaction;
import com.partisiablockchain.demo.byoc.rest.dto.DepositDto;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import org.jetbrains.annotations.NotNull;

/** External data source containing BYOC requests. */
public interface DataSource {

  /**
   * Wait for and get deposit.
   *
   * @param depositNonce of deposit to wait for
   * @return deposit dto
   */
  DepositDto waitForDeposit(long depositNonce);

  /**
   * Get deposit if it exists, null otherwise.
   *
   * @param depositNonce deposit to get
   * @return deposit or null
   */
  DepositTransaction getDeposit(long depositNonce);

  /**
   * Get withdrawal for given withdrawal and oracle nonces.
   *
   * @param id withdrawal id
   * @return withdrawal or null
   */
  WithdrawalTransaction getWithdrawal(WithdrawalId id);

  /**
   * Returns a list of all withdrawal transactions for the given oracle nonce.
   *
   * @param oracleNonce a oracle nonce.
   * @return list of withdrawal transactions.
   */
  List<WithdrawalTransaction> getAllWithdrawalsForOracleNonce(long oracleNonce);

  /** Identifier for withdrawals. */
  final class WithdrawalId implements Comparable<WithdrawalId> {

    private final long withdrawalNonce;
    private final long oracleNonce;

    WithdrawalId(long withdrawalNonce, long oracleNonce) {
      this.withdrawalNonce = withdrawalNonce;
      this.oracleNonce = oracleNonce;
    }

    @Override
    public boolean equals(Object o) {
      if (this == o) {
        return true;
      }
      if (o == null || getClass() != o.getClass()) {
        return false;
      }
      WithdrawalId that = (WithdrawalId) o;
      return withdrawalNonce == that.withdrawalNonce && oracleNonce == that.oracleNonce;
    }

    @Override
    public int hashCode() {
      return Objects.hash(withdrawalNonce, oracleNonce);
    }

    @Override
    public int compareTo(@NotNull WithdrawalId o) {
      return Comparator.comparingLong(WithdrawalId::getWithdrawalNonce)
          .thenComparing(WithdrawalId::getOracleNonce)
          .compare(this, o);
    }

    /**
     * Get the withdrawal nonce.
     *
     * @return withdrawal nonce for the withdrawal id
     */
    public long getWithdrawalNonce() {
      return withdrawalNonce;
    }

    /**
     * Get the oracle nonce.
     *
     * @return oracle nonce for the withdrawal id.
     */
    public long getOracleNonce() {
      return oracleNonce;
    }
  }
}
