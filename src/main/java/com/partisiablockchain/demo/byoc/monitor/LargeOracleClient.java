package com.partisiablockchain.demo.byoc.monitor;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.client.web.WebClient;
import com.partisiablockchain.crypto.AesHelper;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.KeyPair;
import com.partisiablockchain.demo.byoc.rest.dto.BroadcastMessageDto;
import com.partisiablockchain.demo.byoc.rest.dto.PartialSignatureDto;
import com.partisiablockchain.demo.byoc.rest.dto.SignatureAndKeyDto;
import com.partisiablockchain.demo.byoc.rest.dto.UnicastMessageDto;
import com.partisiablockchain.governance.ThresholdSessionId;
import com.secata.tools.coverage.ExceptionConverter;
import jakarta.ws.rs.WebApplicationException;
import jakarta.ws.rs.core.Response;
import java.util.function.Supplier;
import javax.crypto.Cipher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** Threshold client that queries some REST server for messages. */
public final class LargeOracleClient {

  private static final Logger logger = LoggerFactory.getLogger(LargeOracleClient.class);

  private final Supplier<String> endpointSupplier;
  private final BlockchainPublicKey server;
  private final KeyPair signingKey;
  private final WebClient client;
  private final KeyPair ephemeralKey;

  /**
   * Create a new threshold protocol client that interacts with a REST server.
   *
   * @param endpointSupplier endpoint supplier for the server URL
   * @param server the address of the server
   * @param signingKey the key used by this client to sign requests
   * @param ephemeralKey an ephemeral key used for decrypting round messages
   * @param client a web client for making POST and GET requests
   */
  public LargeOracleClient(
      Supplier<String> endpointSupplier,
      BlockchainPublicKey server,
      KeyPair signingKey,
      KeyPair ephemeralKey,
      WebClient client) {
    this.signingKey = signingKey;
    this.server = server;
    this.endpointSupplier = endpointSupplier;
    this.client = client;
    this.ephemeralKey = ephemeralKey;
  }

  static Hash computeHash(
      BlockchainPublicKey server,
      BlockchainPublicKey myKey,
      int sessionId,
      int retryNonce,
      int roundNumber) {
    return Hash.create(
        s -> {
          server.write(s);
          myKey.write(s);
          s.writeInt(sessionId);
          s.writeInt(retryNonce);
          s.writeInt(roundNumber);
        });
  }

  /**
   * Acquire a unicast message.
   *
   * @param sessionId the session ID
   * @param roundNumber the round number of the message
   * @return the message or null if the remote end returned an error.
   */
  public byte[] getUnicastMessage(ThresholdSessionId sessionId, int roundNumber) {
    String endpoint = endpointSupplier.get();
    BlockchainPublicKey publicKey = ephemeralKey.getPublic();
    Hash message =
        computeHash(
            server, publicKey, sessionId.getSessionId(), sessionId.getRetryNonce(), roundNumber);
    SignatureAndKeyDto sign = SignatureAndKeyDto.create(signingKey.sign(message), publicKey);

    try {
      UnicastMessageDto post =
          client.post(
              unicastMessageUrl(sessionId, roundNumber, endpoint), sign, UnicastMessageDto.class);
      // the message we signed above is used in the key derivation to ensure that we don't
      // accidentally reuse keys.
      return decrypt(post.data, message.getBytes());
    } catch (WebApplicationException error) {
      if (error.getResponse().getStatusInfo().equals(Response.Status.NOT_FOUND)) {
        logger.warn(
            "No messages from "
                + server
                + " ("
                + endpoint
                + ")"
                + ", sessionId="
                + sessionId
                + ", roundNumber="
                + roundNumber);
        return null;
      } else {
        logger.error("Got " + error);
        throw error;
      }
    }
  }

  static String unicastMessageUrl(ThresholdSessionId sessionId, int roundNumber, String endpoint) {
    return endpoint
        + "/threshold/getUnicastMessage/"
        + sessionId.getSessionId()
        + "/"
        + sessionId.getRetryNonce()
        + "/"
        + roundNumber;
  }

  /**
   * Acquire a broadcast message.
   *
   * @param messageHash the hash of the message
   * @return the message or null if the remote end returned an error.
   */
  public byte[] getBroadcastMessage(Hash messageHash) {
    String endpoint = endpointSupplier.get();
    logger.debug("Fetching broadcast message " + messageHash);

    String url = broadcastMessageUrl(endpoint, messageHash);
    try {
      return client.get(url, BroadcastMessageDto.class).message;
    } catch (Exception error) {
      logger.info("No broadcast message for " + messageHash);
      return null;
    }
  }

  static String broadcastMessageUrl(String endpoint, Hash messageHash) {
    return endpoint + "/threshold/getBroadcastMessage/" + messageHash;
  }

  /**
   * Acquire a partial signature.
   *
   * @param sessionId the session ID
   * @param signatureId the signature ID
   * @return a partial signature or null if the remote returned an error.
   */
  public byte[] getPartialSignature(ThresholdSessionId sessionId, String signatureId) {
    String endpoint = endpointSupplier.get();
    try {
      return client.get(
              partialSignatureUrl(sessionId, signatureId, endpoint), PartialSignatureDto.class)
          .partialSignature;
    } catch (Exception error) {
      logger.warn(
          "No partial signature data from "
              + server
              + " ("
              + endpoint
              + ")"
              + ", sessionId="
              + sessionId.getSessionId()
              + ", retryNonce="
              + sessionId.getRetryNonce()
              + ", signatureId="
              + signatureId
              + " because of "
              + error);
      return null;
    }
  }

  static String partialSignatureUrl(
      ThresholdSessionId sessionId, String signatureId, String endpoint) {
    return endpoint
        + "/threshold/getPartialSignature/"
        + sessionId.getSessionId()
        + "/"
        + sessionId.getRetryNonce()
        + "/"
        + signatureId;
  }

  private byte[] decrypt(byte[] cipherText, byte[] extraData) {
    Cipher aesForDecrypt =
        AesHelper.createAesForDecrypt(ephemeralKey.getPrivateKey(), server, extraData);
    return ExceptionConverter.call(() -> aesForDecrypt.doFinal(cipherText), "Could not decrypt");
  }
}
