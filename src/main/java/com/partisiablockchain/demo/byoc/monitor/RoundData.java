package com.partisiablockchain.demo.byoc.monitor;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.demo.byoc.governance.largeoracle.OracleDisputeId;
import com.partisiablockchain.math.Unsigned256;
import com.partisiablockchain.serialization.StateSerializable;
import com.secata.stream.DataStreamSerializable;
import com.secata.stream.SafeDataOutputStream;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.List;
import org.web3j.abi.TypeReference;
import org.web3j.abi.datatypes.Type;
import org.web3j.abi.datatypes.generated.Uint256;
import org.web3j.abi.datatypes.generated.Uint80;

/**
 * Price data for a round.
 *
 * <p>See <a href="https://docs.chain.link/docs/price-feeds-api-reference/">Data Feeds API
 * Reference</a>.
 */
@Immutable
public final class RoundData implements DataStreamSerializable, StateSerializable {

  /** The output types associated with a RoundData object. */
  public static final List<TypeReference<?>> OUTPUT_TYPES =
      List.of(
          new TypeReference<Uint80>() {}, // roundId
          new TypeReference<Uint256>() {}, // answer
          new TypeReference<Uint256>() {}, // startedAt
          new TypeReference<Uint256>() {}, // updatedAt
          new TypeReference<Uint80>() {} // answeredInRound
          );

  /** The identifier of the round. Round identifiers are strictly increasing. */
  private final Unsigned256 roundId;

  /** The price of the round. */
  private final Unsigned256 answer;

  /** The Unix timestamp in seconds for when the round was started. */
  private final Unsigned256 startedAt;

  /** The Unix timestamp in seconds for when the round was last updated. */
  private final Unsigned256 updatedAt;

  /** Deprecated. Previously used when answers could take multiple rounds to be computed. */
  private final Unsigned256 answeredInRound;

  static final int UNSIGNED_256_BYTE_LENGTH = 32;
  static final int ROUND_ID_BYTE_LENGTH = 10;

  /**
   * Constructor for a new RoundData object.
   *
   * @param roundId identifies which round the data belongs to
   * @param answer the price given as an answer
   * @param startedAt when it was started
   * @param updatedAt when it was updated
   * @param answeredInRound which round the answer was given in
   */
  public RoundData(
      Unsigned256 roundId,
      Unsigned256 answer,
      Unsigned256 startedAt,
      Unsigned256 updatedAt,
      Unsigned256 answeredInRound) {
    this.roundId = roundId;
    this.answer = answer;
    this.startedAt = startedAt;
    this.updatedAt = updatedAt;
    this.answeredInRound = answeredInRound;
  }

  /**
   * Constructor for a new RoundData object, using String parameters.
   *
   * @param roundId identifies which round the data belongs to
   * @param answer the price given as an answer
   * @param startedAt when it was started
   * @param updatedAt when it was updated
   * @param answeredInRound which round the answer was given in
   */
  public RoundData(
      String roundId, String answer, String startedAt, String updatedAt, String answeredInRound) {
    this(
        Unsigned256.create(roundId),
        Unsigned256.create(answer),
        Unsigned256.create(startedAt),
        Unsigned256.create(updatedAt),
        Unsigned256.create(answeredInRound));
  }

  /**
   * Parse decoded return values to a round data object.
   *
   * @param decoded the decoded values
   * @return parsed round data object
   */
  @SuppressWarnings("rawtypes")
  public static RoundData parseDecoded(List<Type> decoded) {
    String roundId = ((Uint80) decoded.get(0)).getValue().toString();
    String answer = ((Uint256) decoded.get(1)).getValue().toString();
    String startedAt = ((Uint256) decoded.get(2)).getValue().toString();
    String updatedAt = ((Uint256) decoded.get(3)).getValue().toString();
    String answeredInRound = ((Uint80) decoded.get(4)).getValue().toString();

    return new RoundData(roundId, answer, startedAt, updatedAt, answeredInRound);
  }

  /**
   * Checks if the round data is for a finished round. Both {@link RoundData#startedAt()} and {@link
   * RoundData#updatedAt()} has to be non-zero, otherwise the round has not finished.
   *
   * @return true if the round is finished, otherwise false
   */
  public boolean isFinished() {
    return !startedAt().equals(Unsigned256.ZERO) && !updatedAt().equals(Unsigned256.ZERO);
  }

  /**
   * Reconstructs the {@code Unsigned256} round identifier from the {@link OracleDisputeId}.
   *
   * <p>To use a price oracle round id as the dispute id on the large oracle contract the round id
   * has been split into two longs on the Price Oracle contract. This is possible since a round id
   * is an uint80 stored as an Unsigned256. See <a
   * href="https://gitlab.com/partisiablockchain/governance/price-oracle-contract/-/blob/main/src/main/java/com/partisiablockchain/governance/priceoracle/LargeOracleDisputeId.java?ref_type=heads">LargeOracleDisputeId</a>
   * from the price-oracle-contract repository.
   *
   * @param id identifier for an oracle dispute
   * @return the round ID that was used to create the OracleDisputeId.
   */
  static Unsigned256 reconstructRoundIdFromOracleDisputeId(OracleDisputeId id) {
    ByteBuffer byteBuffer = ByteBuffer.allocate(Long.BYTES * 2);
    byteBuffer.putLong(Long.BYTES, id.disputeId());
    byteBuffer.putLong(0, id.oracleId());
    return Unsigned256.create(byteBuffer.array());
  }

  /**
   * Write round data to stream.
   *
   * @param stream output stream
   */
  @Override
  public void write(SafeDataOutputStream stream) {
    stream.write(
        Arrays.copyOfRange(
            roundId.serialize(),
            UNSIGNED_256_BYTE_LENGTH - ROUND_ID_BYTE_LENGTH,
            UNSIGNED_256_BYTE_LENGTH));
    answer.write(stream);
    startedAt.write(stream);
    updatedAt.write(stream);
    answeredInRound.write(stream);
  }

  /**
   * Get the round ID.
   *
   * @return round id
   */
  public Unsigned256 roundId() {
    return roundId;
  }

  /**
   * Get the answer for this round.
   *
   * @return answer
   */
  public Unsigned256 answer() {
    return answer;
  }

  /**
   * Get the timestamp of when the round started.
   *
   * @return timestamp of when the round started
   */
  public Unsigned256 startedAt() {
    return startedAt;
  }

  /**
   * Get timestamp of when the round was updated.
   *
   * @return timestamp of when the round was updated
   */
  public Unsigned256 updatedAt() {
    return updatedAt;
  }

  /**
   * Get the round ID in which the answer was computed.
   *
   * @return the round ID in which the answer was computed
   */
  public Unsigned256 answeredInRound() {
    return answeredInRound;
  }
}
