package com.partisiablockchain.demo.byoc.monitor.chainlink;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.math.Unsigned256;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * Keeps track of retry attempts for calls to different Chainlink data feed contracts on an external
 * chain. Each contract can be called for different rounds and separate retry attempts for each
 * round is necessary.
 */
public final class ChainlinkRetryHandler {

  /** The maximum number of times to retry before deeming a round malicious. */
  static final int MAX_RETRIES = 2;

  /** Retry attempts for each contract and its rounds. */
  private final Map<String, Map<Unsigned256, Integer>> retries;

  private ChainlinkRetryHandler(Map<String, Map<Unsigned256, Integer>> retries) {
    this.retries = retries;
  }

  /**
   * Create a new Chainlink retry handler.
   *
   * @return Chainlink retry handler
   */
  public static ChainlinkRetryHandler create() {
    return new ChainlinkRetryHandler(new HashMap<>());
  }

  /**
   * Remove retry attempts for a contract where rounds have a round ID that is older (less) than the
   * specified round ID.
   *
   * <p>If all round retry attempts for a contract has been removed the contract is removed from the
   * map as well.
   *
   * @param contractAddress address of the contract
   * @param roundId identifier of the round
   */
  public void removeAttemptsForRoundsOlderThan(String contractAddress, Unsigned256 roundId) {
    if (retries.containsKey(contractAddress)) {
      retries.get(contractAddress).entrySet().removeIf(e -> e.getKey().compareTo(roundId) <= 0);
    }
    retries.entrySet().removeIf(e -> e.getValue().isEmpty());
  }

  /**
   * Increment the number of used retry attempts for a round on a contract.
   *
   * @param contractAddress address of the contract
   * @param roundId identifier of the round
   */
  public void incrementAttempts(String contractAddress, Unsigned256 roundId) {
    retries.compute(
        contractAddress,
        (k, v) -> {
          Map<Unsigned256, Integer> roundAttempts = Objects.requireNonNullElse(v, new HashMap<>());
          roundAttempts.merge(roundId, 1, Integer::sum);
          return roundAttempts;
        });
  }

  /**
   * Gets the number of remaining attempts for a specific round on a contract.
   *
   * @param contractAddress contract address
   * @param roundId round identifier
   * @return number of remaining attempts
   */
  public int getRemainingAttempts(String contractAddress, Unsigned256 roundId) {
    return Math.max(0, MAX_RETRIES - getUsedAttempts(contractAddress, roundId));
  }

  private int getUsedAttempts(String contractAddress, Unsigned256 roundId) {
    return retries.getOrDefault(contractAddress, Map.of()).getOrDefault(roundId, 0);
  }

  Map<String, Map<Unsigned256, Integer>> getRetries() {
    return retries;
  }
}
