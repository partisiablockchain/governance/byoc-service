package com.partisiablockchain.demo.byoc.monitor;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.BlsPublicKey;

/** Public information for the service. */
public interface PublicProvider {

  /**
   * The network key for this service.
   *
   * @return network key for this service
   */
  BlockchainPublicKey getNetworkKey();

  /**
   * The blockchain address for this service.
   *
   * @return blockchain address for this service.
   */
  BlockchainAddress getAccountBlockchainAddress();

  /**
   * The finalization key for this service.
   *
   * @return finalization key for this service.
   */
  BlsPublicKey getFinalizationKey();
}
