package com.partisiablockchain.demo.byoc.monitor;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.client.transaction.Transaction;
import com.secata.stream.SafeDataOutputStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Notifies price updates to the Price Oracle contract by sending 'NOTIFY_PRICE_UPDATE'
 * transactions.
 */
final class PriceOracleNotifyPriceUpdateSender {

  private static final Logger logger =
      LoggerFactory.getLogger(PriceOracleNotifyPriceUpdateSender.class);

  /** PriceOracleContract.Invocation#NOTIFY_PRICE_UPDATE. */
  static final byte NOTIFY_PRICE_UPDATE_INVOCATION_BYTE = 1;

  private PriceOracleNotifyPriceUpdateSender() {}

  /**
   * Notifies a price update on the price oracle contract.
   *
   * @param priceOracleContractAddress the address of the price oracle contract
   * @param latestRoundData the round data to send
   * @param transactionSender the sender of the transaction
   * @param transactionCost the cost of the transaction
   */
  static void notifyPriceUpdate(
      BlockchainAddress priceOracleContractAddress,
      RoundData latestRoundData,
      ByocTransactionSender transactionSender,
      long transactionCost) {
    logger.info("Sending latest round data {}", latestRoundData);
    Transaction tx =
        Transaction.create(
            priceOracleContractAddress,
            SafeDataOutputStream.serialize(
                stream -> {
                  stream.writeByte(NOTIFY_PRICE_UPDATE_INVOCATION_BYTE);
                  latestRoundData.write(stream);
                }));
    transactionSender.sendTransactionAndWaitForEvents(tx, transactionCost);
  }
}
