package com.partisiablockchain.demo.byoc.monitor;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.demo.byoc.rest.dto.AdditionalStatusProviderDto;
import com.partisiablockchain.demo.byoc.rest.dto.ResourceStatusDto;
import com.partisiablockchain.providers.AdditionalStatusProvider;
import com.partisiablockchain.providers.AdditionalStatusProviders;
import jakarta.inject.Inject;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.List;

/** A REST resource for server/node status. */
@Path("/status")
public final class StatusResource {

  private final UptimeProvider uptimeProvider;
  private final SystemTimeProvider systemTimeProvider;
  private final VersionInformation versionInformation;
  private final PeerDatabase peerDatabase;
  private final PublicProvider publicProvider;
  private final CpuInformationProvider cpuInformationProvider;
  private final MemoryUsageProvider memoryUsageProvider;
  private final AdditionalStatusProviders additionalStatusProviders;

  /**
   * Constructor.
   *
   * @param uptimeProvider a provider for the uptime of a service
   * @param systemTimeProvider a provider for the system time of a service
   * @param versionInformation version information
   * @param peerDatabase the peer database
   * @param publicProvider public information provider
   * @param cpuInformationProvider system cpu information provider
   * @param memoryUsageProvider memory usage information provider
   * @param additionalStatusProviders additional status providers
   */
  @Inject
  public StatusResource(
      UptimeProvider uptimeProvider,
      SystemTimeProvider systemTimeProvider,
      VersionInformation versionInformation,
      PeerDatabase peerDatabase,
      PublicProvider publicProvider,
      CpuInformationProvider cpuInformationProvider,
      MemoryUsageProvider memoryUsageProvider,
      AdditionalStatusProviders additionalStatusProviders) {
    this.uptimeProvider = uptimeProvider;
    this.systemTimeProvider = systemTimeProvider;
    this.versionInformation = versionInformation;
    this.peerDatabase = peerDatabase;
    this.publicProvider = publicProvider;
    this.cpuInformationProvider = cpuInformationProvider;
    this.memoryUsageProvider = memoryUsageProvider;
    this.additionalStatusProviders = additionalStatusProviders;
  }

  static List<AdditionalStatusProviderDto> createAdditionalStatusProviderDtos(
      List<AdditionalStatusProvider> statusProviders) {
    List<AdditionalStatusProviderDto> dtos = new ArrayList<>();
    for (AdditionalStatusProvider provider : statusProviders) {
      AdditionalStatusProviderDto dto = new AdditionalStatusProviderDto();
      dto.name = provider.getName();
      dto.status = provider.getStatus();
      dtos.add(dto);
    }
    return dtos;
  }

  /**
   * Endpoint that reveals the uptime and version of this service.
   *
   * @return a status dto.
   */
  @GET
  @Produces(MediaType.APPLICATION_JSON)
  public ResourceStatusDto status() {
    ResourceStatusDto dto = new ResourceStatusDto();
    dto.uptime = uptimeProvider.getUptime();
    dto.systemTime = systemTimeProvider.getSystemTime();
    dto.versionIdentifier = versionInformation.toString();
    List<byte[]> knownKeys = new ArrayList<>();
    for (BlockchainPublicKey key : peerDatabase.getBlockProducersWithEndpoint()) {
      knownKeys.add(key.asBytes());
    }
    dto.knownPeersNetworkKeys = knownKeys;
    dto.networkKey = publicProvider.getNetworkKey().asBytes();
    dto.blockchainAddress = publicProvider.getAccountBlockchainAddress().writeAsString();
    dto.finalizationKey = publicProvider.getFinalizationKey().getPublicKeyValue().serialize(true);
    dto.numberOfProcessors = cpuInformationProvider.getNumberOfProcessors();
    dto.systemLoad = cpuInformationProvider.getSystemLoad();
    dto.freeMemory = memoryUsageProvider.getFreeMemory();
    dto.totalMemory = memoryUsageProvider.getTotalMemory();
    dto.maxMemory = memoryUsageProvider.getMaxMemory();
    dto.additionalStatus =
        createAdditionalStatusProviderDtos(additionalStatusProviders.getProviders());
    return dto;
  }
}
