package com.partisiablockchain.demo.byoc.monitor;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.demo.byoc.Address;
import java.util.List;

/** Provides a mapping between PBC addresses and IP addresses. */
public interface PeerDatabase {

  /**
   * Set the current list of registered block producers.
   *
   * @param addresses the addresses to set as registered
   */
  void setRegisteredBlockProducers(List<BlockchainPublicKey> addresses);

  /**
   * Returns true if the address corresponds to a peer that is registered with the BPO contract.
   *
   * @param address the address
   * @return true if address is registered on BPO and false otherwise.
   */
  boolean isRegisteredBlockProducer(BlockchainPublicKey address);

  /**
   * Get the endpoint pointed to by an address, or null if it does not exist.
   *
   * @param address the address
   * @return the endpoint that address points to or null if it does not exist.
   */
  Address getBlockProducerEndpoint(BlockchainPublicKey address);

  /**
   * Returns the current list of entries of the peer database.
   *
   * @return addresses of all known peers.
   */
  List<BlockchainPublicKey> getBlockProducersWithEndpoint();

  /**
   * Indicate whether the database has a (possibly null) entry.
   *
   * @param address the address of the entry
   * @return true if an entry exists, and false otherwise.
   */
  boolean containsBlockProducer(BlockchainPublicKey address);

  /**
   * Update a mapping.
   *
   * @param address the address
   * @param endpoint the endpoint
   */
  void updateBlockProducerEndpoint(BlockchainPublicKey address, Address endpoint);
}
