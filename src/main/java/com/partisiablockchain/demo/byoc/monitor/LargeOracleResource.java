package com.partisiablockchain.demo.byoc.monitor;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.crypto.AesHelper;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.KeyPair;
import com.partisiablockchain.crypto.Signature;
import com.partisiablockchain.demo.byoc.rest.dto.BroadcastMessageDto;
import com.partisiablockchain.demo.byoc.rest.dto.LargeOracleStatusDto;
import com.partisiablockchain.demo.byoc.rest.dto.PartialSignatureDto;
import com.partisiablockchain.demo.byoc.rest.dto.SignatureAndKeyDto;
import com.partisiablockchain.demo.byoc.rest.dto.UnicastMessageDto;
import com.partisiablockchain.governance.MessageStorage;
import com.partisiablockchain.governance.ThresholdSessionId;
import com.secata.stream.SafeDataInputStream;
import com.secata.tools.coverage.ExceptionConverter;
import jakarta.inject.Inject;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.WebApplicationException;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import java.util.List;
import javax.crypto.Cipher;
import org.bouncycastle.util.encoders.Hex;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** Resource for threshold interactions. */
@Path("/threshold")
public final class LargeOracleResource {

  private static final Logger logger = LoggerFactory.getLogger(LargeOracleResource.class);

  private final MessageStorage storage;
  private final LargeOracleStatusInformation resource;
  private final KeyPair key;

  /**
   * Create threshold resource.
   *
   * @param storage storage object for messages that should be available to other parties
   * @param resource resource object for information available doing sessions
   * @param key signature key pair
   */
  @Inject
  public LargeOracleResource(
      MessageStorage storage, LargeOracleStatusInformation resource, KeyPair key) {
    this.storage = storage;
    this.resource = resource;
    this.key = key;
  }

  /**
   * Obtain a bit of information about the current state of a threshold signing session.
   *
   * @param sessionId the session ID
   * @param retryNonce the retry nonce
   * @return a status dto.
   */
  @GET
  @Path("/status/{sessionId}/{retryNonce}")
  @Produces(MediaType.APPLICATION_JSON)
  public LargeOracleStatusDto status(
      @PathParam("sessionId") int sessionId, @PathParam("retryNonce") int retryNonce) {
    ThresholdSessionId thresholdSessionId = new ThresholdSessionId(sessionId, retryNonce);
    int currentRoundNumber = resource.getCurrentRoundNumber(thresholdSessionId);
    int unusedSignatureIds = resource.getSignatureIdsLeft(thresholdSessionId);
    List<BlockchainPublicKey> honestParties = resource.getHonestParties(thresholdSessionId);
    List<BlockchainPublicKey> unicastResponders = resource.getUnicastResponders(thresholdSessionId);
    return LargeOracleStatusDto.create(
        currentRoundNumber, unusedSignatureIds, honestParties, unicastResponders);
  }

  /**
   * Return a round message if authorized to do so.
   *
   * @param sessionId the session identifier
   * @param roundNumber the round number
   * @param retryNonce a counter indicating the retry count for this session
   * @param signatureAndKey a key and signature dto
   * @return a round message if the signature was valid and 404 otherwise.
   */
  @POST
  @Path("/getUnicastMessage/{sessionId}/{retryNonce}/{roundNumber}")
  @Consumes(MediaType.APPLICATION_JSON)
  @Produces(MediaType.APPLICATION_JSON)
  public UnicastMessageDto getUnicastMessage(
      @PathParam("sessionId") int sessionId,
      @PathParam("retryNonce") int retryNonce,
      @PathParam("roundNumber") int roundNumber,
      SignatureAndKeyDto signatureAndKey) {
    Signature read = Signature.read(SafeDataInputStream.createFromBytes(signatureAndKey.signature));
    BlockchainPublicKey clientPublicKey =
        BlockchainPublicKey.fromEncodedEcPoint(signatureAndKey.ephemeralKey);
    Hash message = getMessage(clientPublicKey, sessionId, retryNonce, roundNumber);
    BlockchainPublicKey sender = read.recoverPublicKey(message);
    logger.debug(
        Hex.toHexString(sender.asBytes())
            + " requested round message. roundNumber="
            + roundNumber
            + ", sessionId="
            + sessionId
            + ", retryNonce="
            + retryNonce);
    // clientPublicKey is an ephemeral key, so not the same as sender.
    ThresholdSessionId thresholdSessionId = new ThresholdSessionId(sessionId, retryNonce);
    byte[] messagesToReturn =
        storage.getRoundUnicastMessage(thresholdSessionId, roundNumber, sender);
    if (messagesToReturn == null) {
      throw new WebApplicationException(Response.Status.NOT_FOUND);
    } else {
      Cipher aesForEncrypt =
          AesHelper.createAesForEncrypt(key.getPrivateKey(), clientPublicKey, message.getBytes());
      return ExceptionConverter.call(
          () -> UnicastMessageDto.create(aesForEncrypt.doFinal(messagesToReturn)),
          "Could not encrypt message");
    }
  }

  /**
   * Get a broadcast message from this node. The message is either the one sent by this node, or a
   * message sent by another node and echoed by this node.
   *
   * @param messageHashAsString hash of the broadcast message
   * @return a broadcast message.
   */
  @GET
  @Path("/getBroadcastMessage/{messageHash}")
  @Produces(MediaType.APPLICATION_JSON)
  public BroadcastMessageDto getBroadcastMessage(
      @PathParam("messageHash") String messageHashAsString) {
    Hash messageHash = Hash.fromString(messageHashAsString);
    byte[] message = storage.getBroadcastMessage(messageHash);
    if (message == null) {
      throw new WebApplicationException(Response.Status.NOT_FOUND);
    } else {
      return BroadcastMessageDto.create(message);
    }
  }

  /**
   * Gets a partial signature from a client.
   *
   * @param sessionId the identifier of the session
   * @param retryNonce the retry nonce
   * @param signatureId the identifier of the signature
   * @return The partial signature
   */
  @GET
  @Path("/getPartialSignature/{sessionId}/{retryNonce}/{signatureId}")
  @Produces(MediaType.APPLICATION_JSON)
  public PartialSignatureDto getPartialSignature(
      @PathParam("sessionId") int sessionId,
      @PathParam("retryNonce") int retryNonce,
      @PathParam("signatureId") String signatureId) {
    // no need to sign this request, since everyone should be allowed to reconstruct the signature.
    ThresholdSessionId thresholdSessionId = new ThresholdSessionId(sessionId, retryNonce);
    byte[] partialSignatureData = storage.getPartialSignature(thresholdSessionId, signatureId);
    if (partialSignatureData == null) {
      throw new WebApplicationException(Response.Status.NOT_FOUND);
    } else {
      return PartialSignatureDto.create(partialSignatureData);
    }
  }

  Hash getMessage(
      BlockchainPublicKey clientPublicKey, int sessionId, int retryNonce, int roundNumber) {
    BlockchainPublicKey serverPublicKey = key.getPublic();
    return Hash.create(
        s -> {
          serverPublicKey.write(s);
          clientPublicKey.write(s);
          s.writeInt(sessionId);
          s.writeInt(retryNonce);
          s.writeInt(roundNumber);
        });
  }
}
