package com.partisiablockchain.demo.byoc.monitor;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.client.web.WebClient;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.KeyPair;
import com.partisiablockchain.demo.byoc.Address;
import com.partisiablockchain.demo.byoc.governance.bporchestration.BlockProducer;
import com.partisiablockchain.demo.byoc.governance.bporchestration.BpOrchestrationContractState;
import com.partisiablockchain.demo.byoc.settings.LargeOracleSettings;
import com.secata.tools.immutable.FixedList;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import java.util.function.Supplier;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** Service for discovering the addresses of other peers in PBC. */
public final class PeerDiscoveryProcess implements ByocProcess {

  private static final Logger logger = LoggerFactory.getLogger(PeerDiscoveryProcess.class);

  private final Supplier<KeyPair> ephemeralKeySupplier;
  private final WebClient webClient;
  private final KeyPair restServerSigningKey;
  private final PeerDatabase peerDatabase;
  private final BlockchainAddress bpOrchestrationContract;
  private final VersionInformation versionInformation;
  private final Address myEndpoint;
  private final BlockchainAddress clientPbcAddress;
  private boolean justStarted;
  private final ContractStateHandler contractStateHandler;
  private final Waiter waiter;

  /**
   * Default constructor for this process.
   *
   * @param settings settings for this process
   */
  public PeerDiscoveryProcess(LargeOracleSettings settings) {
    this(
        KeyPair::new,
        LargeOracleProcess.createJerseyWebClient(),
        settings.getSigningKey(),
        settings.getBlockProducerPeerDatabase(),
        settings.getBpOrchestrationContract(),
        settings.getVersionInformation(),
        settings.getMyEndpoint(),
        settings.getPbcAccount(),
        settings.getContractStateHandler());
  }

  PeerDiscoveryProcess(
      Supplier<KeyPair> ephemeralKeySupplier,
      WebClient webClient,
      KeyPair restServerSigningKey,
      PeerDatabase peerDatabase,
      BlockchainAddress bpOrchestrationContract,
      VersionInformation versionInformation,
      Address myEndpoint,
      BlockchainAddress clientPbcAddress,
      ContractStateHandler contractStateHandler) {
    this.ephemeralKeySupplier = ephemeralKeySupplier;
    this.webClient = webClient;
    this.restServerSigningKey = restServerSigningKey;
    this.peerDatabase = peerDatabase;
    this.bpOrchestrationContract = bpOrchestrationContract;
    this.versionInformation = versionInformation;
    this.myEndpoint = myEndpoint;
    this.clientPbcAddress = clientPbcAddress;
    this.justStarted = true;
    this.contractStateHandler = contractStateHandler;
    waiter = contractStateHandler.register(List.of(bpOrchestrationContract));
  }

  @Override
  public boolean run() {
    waiter.waitForChange();
    BpOrchestrationContractState bpOrchestrationContractState =
        contractStateHandler.getState(bpOrchestrationContract, BpOrchestrationContractState.class);
    if (bpOrchestrationContractState == null) {
      return false;
    }

    peerDatabase.setRegisteredBlockProducers(
        getRegisteredProducerKeys(
            bpOrchestrationContractState.committee(),
            bpOrchestrationContractState.getConfirmedBlockProducers()));
    if (justStarted) {
      handshakeWithAllKnownProducers(
          bpOrchestrationContractState.committee(),
          bpOrchestrationContractState.getConfirmedBlockProducers());
      justStarted = false;
    }
    if (isRegisteredProducer()) {
      discoverNewPeers(
          bpOrchestrationContractState.committee(),
          bpOrchestrationContractState.getConfirmedBlockProducers());
    }
    return false;
  }

  private void handshakeWithAllKnownProducers(
      FixedList<BlockProducer> committee, List<BlockProducer> confirmedBlockProducers) {
    for (BlockchainPublicKey address : peerDatabase.getBlockProducersWithEndpoint()) {
      Address endpoint = peerDatabase.getBlockProducerEndpoint(address);
      if (!endpoint.equals(myEndpoint)) {
        PeerDiscoveryClient client =
            createRestClient(
                address, endpoint, getIdentity(committee, confirmedBlockProducers, address));
        peerHandshake(address, endpoint, client);
      }
    }
  }

  static BlockchainAddress getIdentity(
      FixedList<BlockProducer> committee,
      List<BlockProducer> confirmedBlockProducers,
      BlockchainPublicKey key) {
    return includeCommitteeMembersInDiscovery(committee, confirmedBlockProducers).stream()
        .filter(p -> p.publicKey().equals(key))
        .findAny()
        .orElseThrow()
        .identity();
  }

  boolean isRegisteredProducer() {
    return peerDatabase.isRegisteredBlockProducer(restServerSigningKey.getPublic());
  }

  List<BlockchainPublicKey> determineUnknownNodes(
      FixedList<BlockProducer> committee, List<BlockProducer> confirmedBlockProducers) {
    List<BlockchainPublicKey> unknownNodes = new ArrayList<>();
    List<BlockchainPublicKey> registeredProducerKeys =
        getRegisteredProducerKeys(committee, confirmedBlockProducers);
    for (BlockchainPublicKey key : registeredProducerKeys) {
      if (!peerDatabase.containsBlockProducer(key)) {
        unknownNodes.add(key);
      }
    }
    return unknownNodes;
  }

  private boolean peerHandshake(
      BlockchainPublicKey producer, Address endpoint, PeerDiscoveryClient client) {
    if (handshake(client)) {
      peerDatabase.updateBlockProducerEndpoint(producer, endpoint);
      return true;
    } else {
      return false;
    }
  }

  static boolean handshake(PeerDiscoveryClient client) {
    try {
      return client.handshake();
    } catch (Exception exception) {
      logger.warn("Error during handshake: " + exception);
      return false;
    }
  }

  static List<PeerDiscoveryClient.DiscoveredPeer> discoverPeers(
      List<BlockchainPublicKey> unknownNodes, PeerDiscoveryClient client) {
    try {
      return client.discover(unknownNodes);
    } catch (Exception exception) {
      logger.warn("Error during discovery: " + exception);
      return List.of();
    }
  }

  void discoverNewPeers(
      FixedList<BlockProducer> committee, List<BlockProducer> confirmedBlockProducers) {
    List<BlockchainPublicKey> unknownNodes =
        determineUnknownNodes(committee, confirmedBlockProducers);
    for (BlockchainPublicKey key : getRegisteredBlockProducerEndpoints()) {
      if (unknownNodes.isEmpty()) {
        return;
      }
      if (!key.equals(restServerSigningKey.getPublic())) {
        Address blockProducerEndpoint = peerDatabase.getBlockProducerEndpoint(key);
        List<PeerDiscoveryClient.DiscoveredPeer> discover =
            discoverPeers(
                unknownNodes,
                createRestClient(
                    key,
                    blockProducerEndpoint,
                    getBlockProducerByKey(key, confirmedBlockProducers).identity()));
        for (PeerDiscoveryClient.DiscoveredPeer peer : discover) {
          // We can reasonably assume that the peer database always contain an entry with our own
          // data, so no need to specifically handle that case.
          BlockProducer producer = getBlockProducerByKey(peer.key, confirmedBlockProducers);
          if (producer != null) {
            BlockProducerWithEndpoint newProducer =
                new BlockProducerWithEndpoint(producer, peer.endpoint);
            PeerDiscoveryClient client =
                createRestClient(
                    newProducer.producer.publicKey(),
                    newProducer.endpoint,
                    newProducer.producer.identity());
            if (peerHandshake(newProducer.producer.publicKey(), newProducer.endpoint, client)) {
              unknownNodes.remove(producer.publicKey());
            }
          }
        }
      }
    }
  }

  private List<BlockchainPublicKey> getRegisteredBlockProducerEndpoints() {
    return peerDatabase.getBlockProducersWithEndpoint().stream()
        .filter(peerDatabase::isRegisteredBlockProducer)
        .toList();
  }

  private BlockProducer getBlockProducerByKey(
      BlockchainPublicKey key, List<BlockProducer> confirmedBlockProducers) {
    for (BlockProducer producer : confirmedBlockProducers) {
      if (producer.publicKey().equals(key)) {
        return producer;
      }
    }
    return null;
  }

  private PeerDiscoveryClient createRestClient(
      BlockchainPublicKey publicKey, Address endpoint, BlockchainAddress serverPbcAddress) {
    return createRestClient(
        versionInformation,
        endpoint,
        publicKey,
        serverPbcAddress,
        clientPbcAddress,
        restServerSigningKey,
        myEndpoint,
        webClient,
        ephemeralKeySupplier.get());
  }

  static PeerDiscoveryClient createRestClient(
      VersionInformation versionInformation,
      Address endpoint,
      BlockchainPublicKey publicKey,
      BlockchainAddress serverPbcAddress,
      BlockchainAddress clientPbcAddress,
      KeyPair signingKey,
      Address myEndpoint,
      WebClient webClient,
      KeyPair ephemeralKey) {
    return new PeerDiscoveryClient(
        versionInformation,
        endpoint,
        publicKey,
        signingKey,
        webClient,
        myEndpoint,
        ephemeralKey,
        serverPbcAddress,
        clientPbcAddress);
  }

  private List<BlockchainPublicKey> getRegisteredProducerKeys(
      FixedList<BlockProducer> committee, List<BlockProducer> confirmedBlockProducers) {
    FixedList<BlockProducer> allProducersInCommitteeAndConfirmed =
        includeCommitteeMembersInDiscovery(committee, confirmedBlockProducers);
    List<BlockchainPublicKey> keys = new ArrayList<>();
    for (BlockProducer producer : allProducersInCommitteeAndConfirmed) {
      keys.add(producer.publicKey());
    }
    return keys;
  }

  static FixedList<BlockProducer> includeCommitteeMembersInDiscovery(
      FixedList<BlockProducer> committee, List<BlockProducer> confirmedBlockProducers) {

    return committee.addElements(
        confirmedBlockProducers.stream().filter(Predicate.not(committee::contains)).toList());
  }

  @Override
  public String name() {
    return getClass().getSimpleName();
  }

  static final class BlockProducerWithEndpoint {
    public BlockProducer producer;
    public Address endpoint;

    public BlockProducerWithEndpoint(BlockProducer producer, Address endpoint) {
      this.producer = producer;
      this.endpoint = endpoint;
    }
  }
}
