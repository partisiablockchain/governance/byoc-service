package com.partisiablockchain.demo.byoc.monitor;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.AesHelper;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.KeyPair;
import com.partisiablockchain.crypto.Signature;
import com.partisiablockchain.demo.byoc.Address;
import com.partisiablockchain.demo.byoc.rest.dto.DiscoveryResponseDto;
import com.partisiablockchain.demo.byoc.rest.dto.EncryptedPayloadDto;
import com.partisiablockchain.demo.byoc.rest.dto.HandshakeDto;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import com.secata.tools.coverage.ExceptionConverter;
import jakarta.inject.Inject;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.WebApplicationException;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import java.nio.charset.StandardCharsets;
import java.util.List;
import javax.crypto.Cipher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** Resource used for peer discovery. */
@Path("/discover")
public final class PeerDiscoveryResource {

  private static final Logger logger = LoggerFactory.getLogger(PeerDiscoveryResource.class);
  private final PeerDatabase peerDatabase;
  private final VersionInformation versionInformation;
  private final KeyPair key;
  private final Address myEndpoint;
  private final PublicProvider publicProvider;

  /**
   * Create a new discovery REST resource.
   *
   * @param versionInformation a string describing what running version
   * @param peerDatabase a database with peers
   * @param key this servers encryption and signing key
   * @param publicProvider public provider
   */
  @Inject
  public PeerDiscoveryResource(
      VersionInformation versionInformation,
      PeerDatabase peerDatabase,
      KeyPair key,
      PublicProvider publicProvider) {
    this.versionInformation = versionInformation;
    this.peerDatabase = peerDatabase;
    this.key = key;
    this.myEndpoint = peerDatabase.getBlockProducerEndpoint(key.getPublic());
    this.publicProvider = publicProvider;
  }

  /**
   * Discover endpoints known by this peer.
   *
   * @param dto a DTO with a discovery request
   * @return a DTO with endpoints known by this peer.
   */
  @POST
  @Produces(MediaType.APPLICATION_JSON)
  @Consumes(MediaType.APPLICATION_JSON)
  public DiscoveryResponseDto discover(EncryptedPayloadDto dto) {
    BlockchainPublicKey pubKey = BlockchainPublicKey.fromEncodedEcPoint(dto.ephemeralKey);
    Signature signature = Signature.read(SafeDataInputStream.createFromBytes(dto.signature));
    byte[] payload = dto.payload;
    Hash message = message(key.getPublic(), pubKey, payload, versionInformation);
    BlockchainPublicKey publicKey = signature.recoverPublicKey(message);
    if (peerDatabase.isRegisteredBlockProducer(publicKey)) {
      DiscoveryRequestPayload decrypted = decryptDiscoveryRequest(pubKey, payload);
      if (!decrypted.serverEndPoint.equals(myEndpoint)) {
        logger.info("Discovery was not intended for us, but " + decrypted.serverEndPoint);
        throw new WebApplicationException(Response.Status.NOT_FOUND);
      }
      if (decrypted.publicKey.equals(publicKey)) {
        logger.debug(
            "Got valid discovery request from "
                + publicKey.createAddress()
                + " with endpoint "
                + decrypted.endpoint);
        peerDatabase.updateBlockProducerEndpoint(publicKey, decrypted.endpoint);
        byte[] discovery =
            encryptEndpoints(pubKey, message.getBytes(), peerDatabase, decrypted.neededPeers);
        return DiscoveryResponseDto.create(discovery);
      } else {
        logger.info(
            "Known node "
                + publicKey.createAddress()
                + " sent bogus payload: publicKey="
                + decrypted.publicKey.createAddress()
                + ", endpoint="
                + decrypted.endpoint);
        throw new WebApplicationException(Response.Status.NOT_FOUND);
      }
    } else {
      logger.info("Discovery request from unknown peer: " + publicKey.createAddress());
      throw new WebApplicationException(Response.Status.NOT_FOUND);
    }
  }

  /**
   * Perform a handshake to see if this node is alive. Allows the sender to also update information
   * about their endpoint.
   *
   * @param dto a dto
   * @return a handshake.
   */
  @POST
  @Path("/handshake")
  @Consumes({MediaType.APPLICATION_JSON})
  @Produces(MediaType.APPLICATION_JSON)
  public HandshakeDto handshake(EncryptedPayloadDto dto) {
    BlockchainPublicKey clientEphKey = BlockchainPublicKey.fromEncodedEcPoint(dto.ephemeralKey);
    Signature signature = Signature.read(SafeDataInputStream.createFromBytes(dto.signature));
    byte[] payload = dto.payload;
    Hash message = message(key.getPublic(), clientEphKey, payload, versionInformation);
    BlockchainPublicKey clientKey = signature.recoverPublicKey(message);
    if (peerDatabase.isRegisteredBlockProducer(clientKey)) {
      SafeDataInputStream decrypted = decryptHandshake(clientEphKey, payload);
      Address endPoint = Address.read(decrypted);
      BlockchainAddress clientPbcAddress = BlockchainAddress.read(decrypted);
      peerDatabase.updateBlockProducerEndpoint(clientKey, endPoint);
      return HandshakeDto.create(
          encryptHandshakeResponse(clientEphKey, clientPbcAddress, endPoint));
    } else {
      logger.info("Handshake from unknown peer: " + clientKey);
      throw new WebApplicationException(Response.Status.NOT_FOUND);
    }
  }

  private SafeDataInputStream decryptHandshake(BlockchainPublicKey publicKey, byte[] payload) {
    byte[] extraData = handshakeExtraDataDecrypt(versionInformation);
    Cipher aesForDecrypt = AesHelper.createAesForDecrypt(key.getPrivateKey(), publicKey, extraData);
    byte[] endpointBytes =
        ExceptionConverter.call(
            () -> aesForDecrypt.doFinal(payload), "Could not decrypt handshake");
    SafeDataInputStream fromBytes = SafeDataInputStream.createFromBytes(endpointBytes);
    Address serverEndpoint = Address.read(fromBytes);
    BlockchainAddress serverPbcAddress = BlockchainAddress.read(fromBytes);

    if (serverEndpoint.equals(myEndpoint)
        && serverPbcAddress.equals(this.publicProvider.getAccountBlockchainAddress())) {
      return fromBytes;
    } else {
      logger.info(
          "Handshake was not intended for us, but "
              + serverEndpoint
              + " with pbc address "
              + serverPbcAddress);
      throw new WebApplicationException(Response.Status.NOT_FOUND);
    }
  }

  static byte[] handshakeExtraDataDecrypt(VersionInformation versionInformation) {
    return SafeDataOutputStream.serialize(
        s -> {
          s.write(versionInformation.toString().getBytes(StandardCharsets.UTF_8));
          s.writeString("client -> server");
        });
  }

  static byte[] handshakeExtraDataEncrypt(VersionInformation versionInformation) {
    return SafeDataOutputStream.serialize(
        s -> {
          s.write(versionInformation.toString().getBytes(StandardCharsets.UTF_8));
          s.writeString("server -> client");
        });
  }

  private byte[] encryptHandshakeResponse(
      BlockchainPublicKey publicKey, BlockchainAddress clientPbcAddress, Address clientEndPoint) {
    byte[] extraData = handshakeExtraDataEncrypt(versionInformation);
    Cipher aesForEncrypt = AesHelper.createAesForEncrypt(key.getPrivateKey(), publicKey, extraData);
    byte[] serialize =
        SafeDataOutputStream.serialize(
            stream -> {
              clientEndPoint.write(stream);
              clientPbcAddress.write(stream);
            });
    return ExceptionConverter.call(
        () -> aesForEncrypt.doFinal(serialize), "Could encrypt handshake response");
  }

  private DiscoveryRequestPayload decryptDiscoveryRequest(
      BlockchainPublicKey pubKey, byte[] payload) {
    Cipher decrypt =
        AesHelper.createAesForDecrypt(
            key.getPrivateKey(),
            pubKey,
            versionInformation.toString().getBytes(StandardCharsets.UTF_8));
    return ExceptionConverter.call(
        () -> DiscoveryRequestPayload.read(decrypt.doFinal(payload)), "Could not decrypt payload");
  }

  private byte[] encryptEndpoints(
      BlockchainPublicKey publicKey,
      byte[] extraData,
      PeerDatabase peerDatabase,
      List<BlockchainPublicKey> neededPeers) {
    Cipher aesForEncrypt = AesHelper.createAesForEncrypt(key.getPrivateKey(), publicKey, extraData);
    return ExceptionConverter.call(
        () -> aesForEncrypt.doFinal(serializeEndpoints(peerDatabase, neededPeers)),
        "Could not encrypt discovery result");
  }

  static byte[] serializeEndpoints(
      PeerDatabase peerDatabase, List<BlockchainPublicKey> neededPeers) {
    List<BlockchainPublicKey> entries = peerDatabase.getBlockProducersWithEndpoint();
    int count = (int) entries.stream().filter(neededPeers::contains).count();
    return SafeDataOutputStream.serialize(
        stream -> {
          stream.writeInt(count);
          for (BlockchainPublicKey publicKey : entries) {
            if (neededPeers.contains(publicKey)) {
              publicKey.write(stream);
              peerDatabase.getBlockProducerEndpoint(publicKey).write(stream);
            }
          }
        });
  }

  static Hash message(
      BlockchainPublicKey serverPublicKey,
      BlockchainPublicKey senderEphemeralKey,
      byte[] payload,
      VersionInformation versionInformation) {
    return Hash.create(
        s -> {
          serverPublicKey.write(s);
          senderEphemeralKey.write(s);
          s.write(payload);
          s.writeString(versionInformation.toString());
        });
  }

  static final class DiscoveryRequestPayload {
    public BlockchainPublicKey publicKey;
    public Address endpoint;
    public Address serverEndPoint;
    public List<BlockchainPublicKey> neededPeers;

    static DiscoveryRequestPayload read(byte[] bytes) {
      SafeDataInputStream fromBytes = SafeDataInputStream.createFromBytes(bytes);
      DiscoveryRequestPayload discoveryRequestPayload = new DiscoveryRequestPayload();
      discoveryRequestPayload.publicKey = BlockchainPublicKey.read(fromBytes);
      discoveryRequestPayload.endpoint = Address.read(fromBytes);
      discoveryRequestPayload.serverEndPoint = Address.read(fromBytes);
      discoveryRequestPayload.neededPeers =
          BlockchainPublicKey.LIST_SERIALIZER.readDynamic(fromBytes);
      return discoveryRequestPayload;
    }
  }
}
