package com.partisiablockchain.demo.byoc.monitor;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.governance.ThresholdSessionId;
import java.util.List;

/** Accessing information for a given key generation session. */
public interface LargeOracleStatusInformation {

  /**
   * Returns the current round number for the threshold signing session.
   *
   * @param thresholdSessionId session id.
   * @return current round number
   */
  int getCurrentRoundNumber(ThresholdSessionId thresholdSessionId);

  /**
   * Returns the number of unused signature ids in the threshold signing session.
   *
   * @param thresholdSessionId session id.
   * @return number of unused signature ids.
   */
  int getSignatureIdsLeft(ThresholdSessionId thresholdSessionId);

  /**
   * Returns a list of parties that acted honest doing the protocol.
   *
   * @param thresholdSessionId session id.
   * @return a list of honest parties.
   */
  List<BlockchainPublicKey> getHonestParties(ThresholdSessionId thresholdSessionId);

  /**
   * Returns the list of producers that this producer has received unicast messages from in the
   * second round of the key generation protocol.
   *
   * @param thresholdSessionId session id
   * @return a list of parties who has sent unicast messages to this party.
   */
  List<BlockchainPublicKey> getUnicastResponders(ThresholdSessionId thresholdSessionId);
}
