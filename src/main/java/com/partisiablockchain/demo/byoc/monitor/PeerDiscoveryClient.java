package com.partisiablockchain.demo.byoc.monitor;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.client.web.WebClient;
import com.partisiablockchain.crypto.AesHelper;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.KeyPair;
import com.partisiablockchain.crypto.Signature;
import com.partisiablockchain.demo.byoc.Address;
import com.partisiablockchain.demo.byoc.rest.dto.DiscoveryResponseDto;
import com.partisiablockchain.demo.byoc.rest.dto.EncryptedPayloadDto;
import com.partisiablockchain.demo.byoc.rest.dto.HandshakeDto;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import com.secata.tools.coverage.ExceptionConverter;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import javax.crypto.Cipher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

final class PeerDiscoveryClient {

  private static final Logger logger = LoggerFactory.getLogger(PeerDiscoveryClient.class);

  private final VersionInformation versionInformation;
  private final Address endpoint;
  private final Address myEndpoint;
  private final BlockchainPublicKey remoteServer;
  private final WebClient client;
  private final KeyPair ephemeralKey;
  private final KeyPair signingKey;
  private final BlockchainAddress serverPbcAddress;
  private final BlockchainAddress myPbcAddress;

  PeerDiscoveryClient(
      VersionInformation versionInformation,
      Address endpoint,
      BlockchainPublicKey remoteServer,
      KeyPair signingKey,
      WebClient client,
      Address myEndpoint,
      KeyPair ephemeralKey,
      BlockchainAddress serverPbcAddress,
      BlockchainAddress myPbcAddress) {
    this.versionInformation = versionInformation;
    this.endpoint = endpoint;
    this.remoteServer = remoteServer;
    this.signingKey = signingKey;
    this.client = client;
    this.myEndpoint = myEndpoint;
    this.ephemeralKey = ephemeralKey;
    this.serverPbcAddress = serverPbcAddress;
    this.myPbcAddress = myPbcAddress;
  }

  KeyPair getEphemeralKey() {
    return ephemeralKey;
  }

  List<DiscoveredPeer> discover(List<BlockchainPublicKey> unknownNodes) {
    byte[] payload = getDiscoverPayload(unknownNodes);
    Hash message =
        PeerDiscoveryResource.message(
            remoteServer, ephemeralKey.getPublic(), payload, versionInformation);
    Signature sign = signingKey.sign(message);
    EncryptedPayloadDto dto = createDto(sign, payload);
    DiscoveryResponseDto post =
        client.post(endpoint.getEndpoint() + "/discover", dto, DiscoveryResponseDto.class);
    return decryptDiscoveredEndpoints(post.data, message.getBytes());
  }

  boolean handshake() {
    byte[] handshake = getHandshakePayload();
    Hash message =
        PeerDiscoveryResource.message(
            remoteServer, ephemeralKey.getPublic(), handshake, versionInformation);
    Signature sign = signingKey.sign(message);
    EncryptedPayloadDto dto = createDto(sign, handshake);
    String url = endpoint.getEndpoint() + "/discover/handshake";
    HandshakeDto post = client.post(url, dto, HandshakeDto.class);
    return decryptAndVerifyHandshake(post);
  }

  private boolean decryptAndVerifyHandshake(HandshakeDto post) {
    byte[] extraData = PeerDiscoveryResource.handshakeExtraDataEncrypt(versionInformation);
    Cipher aesForDecrypt =
        AesHelper.createAesForDecrypt(ephemeralKey.getPrivateKey(), remoteServer, extraData);
    return ExceptionConverter.call(
        () -> {
          byte[] bytes = aesForDecrypt.doFinal(post.data);
          SafeDataInputStream rpc = SafeDataInputStream.createFromBytes(bytes);
          Address endpoint = Address.read(rpc);
          BlockchainAddress pbcAddress = BlockchainAddress.read(rpc);
          if (!endpoint.equals(myEndpoint) || !pbcAddress.equals(myPbcAddress)) {
            logger.info(
                "Received endpoint and pbc address was: "
                    + "{} and {}, but expected endpoint and pbc address to be: {} and {}",
                endpoint,
                pbcAddress,
                myEndpoint,
                myPbcAddress);
            return false;
          } else {
            return true;
          }
        },
        "Could not decrypt handshake response");
  }

  byte[] getHandshakePayload() {
    byte[] payload = handshakeMessage();
    byte[] extraData = PeerDiscoveryResource.handshakeExtraDataDecrypt(versionInformation);
    Cipher aesForEncrypt =
        AesHelper.createAesForEncrypt(ephemeralKey.getPrivateKey(), remoteServer, extraData);
    return ExceptionConverter.call(
        () -> aesForEncrypt.doFinal(payload), "Could not encrypt handshake");
  }

  private byte[] handshakeMessage() {
    return SafeDataOutputStream.serialize(
        s -> {
          endpoint.write(s);
          serverPbcAddress.write(s);
          myEndpoint.write(s);
          myPbcAddress.write(s);
        });
  }

  private List<DiscoveredPeer> decryptDiscoveredEndpoints(byte[] data, byte[] extraData) {
    Cipher decrypt =
        AesHelper.createAesForDecrypt(ephemeralKey.getPrivateKey(), remoteServer, extraData);
    byte[] decrypted =
        ExceptionConverter.call(() -> decrypt.doFinal(data), "Could not decrypt discovery result");
    SafeDataInputStream fromBytes = SafeDataInputStream.createFromBytes(decrypted);
    int n = fromBytes.readInt();
    List<DiscoveredPeer> endpoints = new ArrayList<>();
    for (int i = 0; i < n; i++) {
      BlockchainPublicKey address = BlockchainPublicKey.read(fromBytes);
      Address endpoint = Address.read(fromBytes);
      endpoints.add(new DiscoveredPeer(address, endpoint));
    }
    return endpoints;
  }

  private EncryptedPayloadDto createDto(Signature sign, byte[] payload) {
    byte[] serializedSignature = SafeDataOutputStream.serialize(sign::write);
    return EncryptedPayloadDto.create(
        ephemeralKey.getPublic().asBytes(), serializedSignature, payload);
  }

  byte[] getDiscoverPayload(List<BlockchainPublicKey> unknownNodes) {
    byte[] payload =
        SafeDataOutputStream.serialize(
            s -> {
              signingKey.getPublic().write(s);
              myEndpoint.write(s);
              endpoint.write(s);
              BlockchainPublicKey.LIST_SERIALIZER.writeDynamic(s, unknownNodes);
            });
    Cipher encrypt =
        AesHelper.createAesForEncrypt(
            ephemeralKey.getPrivateKey(),
            remoteServer,
            versionInformation.toString().getBytes(StandardCharsets.UTF_8));
    return ExceptionConverter.call(() -> encrypt.doFinal(payload), "Could not encrypt payload");
  }

  static final class DiscoveredPeer {

    public BlockchainPublicKey key;
    public Address endpoint;

    public DiscoveredPeer(BlockchainPublicKey key, Address endpoint) {
      this.key = key;
      this.endpoint = endpoint;
    }
  }
}
