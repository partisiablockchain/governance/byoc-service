package com.partisiablockchain.demo.byoc.monitor;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.demo.byoc.governance.priceoracle.ChallengePeriod;
import com.partisiablockchain.demo.byoc.governance.priceoracle.PriceOracleContractState;
import com.partisiablockchain.demo.byoc.governance.priceoracle.PriceUpdates;
import com.partisiablockchain.demo.byoc.monitor.chainlink.ChainlinkClient;
import com.partisiablockchain.demo.byoc.settings.PriceOracleSettings;
import com.partisiablockchain.math.Unsigned256;
import com.partisiablockchain.tree.AvlTree;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** Handles challenge periods on price oracle contracts. */
public final class PriceOracleChallengePeriodProcess implements ByocProcess {

  private static final Logger LOGGER =
      LoggerFactory.getLogger(PriceOracleChallengePeriodProcess.class);

  private final SystemTimeProvider systemTime;
  private final BlockchainAddress nodeAddress;
  private final List<BlockchainAddress> priceOracleContractAddresses;
  private final long transactionCost;
  private final ByocTransactionSender transactionSender;
  private final ContractStateHandler contractStateHandler;
  private final Map<String, ChainlinkClient> chainlinkClients;
  private final Waiter priceOracleWaiter;

  PriceOracleChallengePeriodProcess(
      SystemTimeProvider systemTime,
      BlockchainAddress nodeAddress,
      List<BlockchainAddress> priceOracleContractAddresses,
      long transactionCost,
      ByocTransactionSender transactionSender,
      ContractStateHandler contractStateHandler,
      Map<String, ChainlinkClient> chainlinkClients) {
    this.systemTime = systemTime;
    this.nodeAddress = nodeAddress;
    this.priceOracleContractAddresses = priceOracleContractAddresses;
    this.transactionCost = transactionCost;
    this.transactionSender = transactionSender;
    this.contractStateHandler = contractStateHandler;
    this.chainlinkClients = chainlinkClients;

    this.priceOracleWaiter = contractStateHandler.register(priceOracleContractAddresses);
  }

  /**
   * Creates a new price oracle challenge period process.
   *
   * @param settings price oracle settings
   * @return new price oracle challenge period process
   */
  public static PriceOracleChallengePeriodProcess create(PriceOracleSettings settings) {
    return new PriceOracleChallengePeriodProcess(
        new SystemTimeProviderImpl(),
        settings.getPbcAccount(),
        settings.getPriceOracleContracts(),
        settings.getTransactionCost(),
        settings.getTransactionSender(),
        settings.getContractStateHandler(),
        settings.createChainlinkClients());
  }

  @Override
  public boolean run() {
    priceOracleWaiter.waitForChange();

    for (BlockchainAddress priceOracleContractAddress : priceOracleContractAddresses) {
      handleChallengePeriod(priceOracleContractAddress);
    }
    return false;
  }

  private void handleChallengePeriod(BlockchainAddress priceOracleContractAddress) {
    PriceOracleContractState pocState =
        contractStateHandler.getPriceOracleState(priceOracleContractAddress);
    if (pocState == null || pocState.challengePeriod() == null) {
      return;
    }

    if (!pocState.isRegisteredPriceOracleNode(nodeAddress)) {
      return;
    }

    ChainlinkClient chainlinkClient = chainlinkClients.get(pocState.chainId());
    if (chainlinkClient == null) {
      LOGGER.warn("Node is not set up with endpoint for EVM chain {}", pocState.chainId());
      return;
    }

    notifyPriceUpdateForChallengePeriod(
        priceOracleContractAddress,
        pocState.challengePeriod(),
        pocState.roundPriceUpdates().roundPriceUpdates(),
        pocState.referenceContractAddress(),
        chainlinkClient);
  }

  /**
   * Notifies round data for the round with an ongoing challenge period. If the challenge period is
   * over or the node has already notified a price for the round then do nothing.
   *
   * @param priceOracleContractAddress price oracle contract
   * @param challengePeriod challenge period
   * @param priceUpdates notified price updates
   * @param chainlinkContractAddress Chainlink data feed contract address
   * @param chainlinkClient Chainlink client for interacting with external chain
   */
  private void notifyPriceUpdateForChallengePeriod(
      BlockchainAddress priceOracleContractAddress,
      ChallengePeriod challengePeriod,
      AvlTree<Unsigned256, PriceUpdates> priceUpdates,
      String chainlinkContractAddress,
      ChainlinkClient chainlinkClient) {
    Unsigned256 challengePeriodRoundId = challengePeriod.roundId();
    Unsigned256 updatedAt = getUpdatedAt(priceUpdates, challengePeriodRoundId);
    if (!chainlinkClient.hasTimestampOfLatestValidBlockExceeded(updatedAt)) {
      return;
    }

    if (challengePeriod.hasEnded(systemTime.getSystemTime())) {
      return;
    }

    boolean hasNotifiedForChallengePeriod =
        priceUpdates.getValue(challengePeriodRoundId).oraclePriceUpdates().containsKey(nodeAddress);
    if (hasNotifiedForChallengePeriod) {
      return;
    }

    RoundData roundData =
        chainlinkClient.getRoundDataById(chainlinkContractAddress, challengePeriodRoundId);
    PriceOracleNotifyPriceUpdateSender.notifyPriceUpdate(
        priceOracleContractAddress, roundData, transactionSender, transactionCost);
  }

  /**
   * Gets the timestamp of when the round was updated by looking at the first notified price update
   * for the round with the specified round identifier. Since a challenge period has been started
   * the round is known to have at least two price updates.
   *
   * @param roundPriceUpdates price updates for the round
   * @param roundId identifier of the round
   * @return timestamp of when the round was updated
   */
  private static Unsigned256 getUpdatedAt(
      AvlTree<Unsigned256, PriceUpdates> roundPriceUpdates, Unsigned256 roundId) {
    return roundPriceUpdates.getValue(roundId).oraclePriceUpdates().values().get(0).updatedAt();
  }

  @Override
  public String name() {
    return PriceOracleChallengePeriodProcess.class.getSimpleName();
  }
}
