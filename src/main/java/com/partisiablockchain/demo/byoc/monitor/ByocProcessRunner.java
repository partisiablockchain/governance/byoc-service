package com.partisiablockchain.demo.byoc.monitor;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.secata.tools.coverage.ExceptionLogger;
import com.secata.tools.thread.ThreadedLoop;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** Driver class that handles logic for running a BYOC process. */
public final class ByocProcessRunner implements AutoCloseable {

  private static final Logger logger = LoggerFactory.getLogger(ByocProcessRunner.class);

  private final ThreadedLoop loop;
  private final ByocProcess process;
  private final int delayOnError;
  private final int requestInterval;

  /**
   * Default constructor for process runner.
   *
   * @param process the process to run
   * @param delayOnError time to wait if an error occurs before running process again
   * @param requestInterval time to wait between process calls
   */
  public ByocProcessRunner(ByocProcess process, int delayOnError, int requestInterval) {
    this.delayOnError = delayOnError;
    this.requestInterval = requestInterval;
    this.process = process;
    this.loop =
        ThreadedLoop.create(this::run, process.name())
            .logger(logger::warn, "Error in loop")
            .onException(this::onError)
            .start();
  }

  private void run() throws Exception {
    Thread.sleep(requestInterval);
    while (true) {
      if (!process.run()) {
        return;
      }
    }
  }

  /**
   * Get the process.
   *
   * @return the process
   */
  public ByocProcess getProcess() {
    return process;
  }

  void onError() {
    // Consider adding a back-off functionality
    ExceptionLogger.handle(logger::error, () -> Thread.sleep(delayOnError), "");
  }

  boolean isRunning() {
    return loop.isRunning();
  }

  @Override
  public void close() {
    loop.stop();
  }
}
