package com.partisiablockchain.demo.byoc.monitor;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import java.lang.management.ManagementFactory;
import java.lang.management.OperatingSystemMXBean;

/** CPU information provider implementation. */
public final class CpuInformationProviderImpl implements CpuInformationProvider {

  /**
   * Get the number of processors. See {@link Runtime#availableProcessors()}.
   *
   * @return number of processors
   */
  @Override
  public int getNumberOfProcessors() {
    return Runtime.getRuntime().availableProcessors();
  }

  /**
   * Get the average system load for the last minute. See {@link
   * OperatingSystemMXBean#getSystemLoadAverage()}.
   *
   * @return average system load
   */
  @Override
  public double getSystemLoad() {
    OperatingSystemMXBean osBean = ManagementFactory.getOperatingSystemMXBean();
    return osBean.getSystemLoadAverage();
  }
}
