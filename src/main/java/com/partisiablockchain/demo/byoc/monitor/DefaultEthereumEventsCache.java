package com.partisiablockchain.demo.byoc.monitor;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.demo.byoc.governance.byocincoming.DepositTransaction;
import com.partisiablockchain.demo.byoc.governance.byocoutgoing.WithdrawalTransaction;
import com.partisiablockchain.math.Unsigned256;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/** Default in-memory event cache. */
public final class DefaultEthereumEventsCache implements EthereumEventsCache {

  private final Map<Long, DepositTransaction> deposits;
  private final Map<DataSource.WithdrawalId, WithdrawalTransaction> withdrawals;
  private Unsigned256 latestCheckedBlock;

  /** Default constructor for the cache. */
  public DefaultEthereumEventsCache() {
    this.deposits = new ConcurrentHashMap<>();
    this.withdrawals = new ConcurrentHashMap<>();
    this.latestCheckedBlock = null;
  }

  @Override
  public DepositTransaction loadDeposit(long depositNonce) {
    return deposits.get(depositNonce);
  }

  @Override
  public List<DepositTransaction> loadAllDeposits() {
    return List.copyOf(deposits.values());
  }

  @Override
  public WithdrawalTransaction loadWithdrawal(DataSource.WithdrawalId withdrawalId) {
    return withdrawals.get(withdrawalId);
  }

  @Override
  public List<WithdrawalTransaction> loadAllWithdrawals() {
    return List.copyOf(withdrawals.values());
  }

  @Override
  public Unsigned256 loadLatestCheckedBlock() {
    return latestCheckedBlock;
  }

  @Override
  public void saveDeposits(List<DepositTransaction> deposits) {
    for (DepositTransaction deposit : deposits) {
      this.deposits.put(deposit.nonce(), deposit);
    }
  }

  @Override
  public void saveWithdrawals(List<WithdrawalTransaction> withdrawals) {
    for (WithdrawalTransaction withdrawal : withdrawals) {
      DataSource.WithdrawalId id =
          new DataSource.WithdrawalId(withdrawal.withdrawalNonce(), withdrawal.oracleNonce());
      this.withdrawals.put(id, withdrawal);
    }
  }

  @Override
  public void saveLatestCheckedBlock(Unsigned256 blockNumber) {
    this.latestCheckedBlock = blockNumber;
  }
}
