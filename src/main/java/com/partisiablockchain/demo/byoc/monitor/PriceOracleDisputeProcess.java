package com.partisiablockchain.demo.byoc.monitor;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.demo.byoc.governance.largeoracle.Dispute;
import com.partisiablockchain.demo.byoc.governance.largeoracle.LargeOracleContractState;
import com.partisiablockchain.demo.byoc.governance.largeoracle.OracleDisputeId;
import com.partisiablockchain.demo.byoc.governance.priceoracle.PriceOracleContractState;
import com.partisiablockchain.demo.byoc.governance.priceoracle.PriceOracleDispute;
import com.partisiablockchain.demo.byoc.governance.priceoracle.PriceOracleRoundData;
import com.partisiablockchain.demo.byoc.monitor.chainlink.ChainlinkClient;
import com.partisiablockchain.demo.byoc.settings.PriceOracleSettings;
import com.partisiablockchain.math.Unsigned256;
import com.partisiablockchain.tree.AvlTree;
import com.secata.stream.SafeDataOutputStream;
import com.secata.tools.immutable.FixedList;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** Handles disputes for price oracles on the large oracle contract. */
public final class PriceOracleDisputeProcess implements ByocProcess {

  private static final Logger LOGGER = LoggerFactory.getLogger(PriceOracleDisputeProcess.class);

  private final BlockchainAddress nodeAddress;
  private final List<BlockchainAddress> priceOracleContractAddresses;
  private final BlockchainAddress largeOracleContract;
  private final VotingUtil votingUtil;
  private final ContractStateHandler contractStateHandler;
  private final Map<String, ChainlinkClient> chainlinkClients;
  private final Waiter largeOracleWaiter;

  static final int NO_FRAUD_VOTE = -1;

  PriceOracleDisputeProcess(
      BlockchainAddress nodeAddress,
      List<BlockchainAddress> priceOracleContractAddresses,
      ByocTransactionSender transactionSender,
      BlockchainAddress largeOracleContract,
      ContractStateHandler contractStateHandler,
      Map<String, ChainlinkClient> chainlinkClients) {
    this.nodeAddress = nodeAddress;
    this.priceOracleContractAddresses = priceOracleContractAddresses;
    this.largeOracleContract = largeOracleContract;
    this.votingUtil = VotingUtil.create(transactionSender, largeOracleContract);
    this.contractStateHandler = contractStateHandler;
    this.chainlinkClients = chainlinkClients;

    this.largeOracleWaiter = contractStateHandler.register(List.of(largeOracleContract));
  }

  /**
   * Creates a new price oracle dispute process.
   *
   * @param settings configuration for the process
   * @return new price oracle dispute process
   */
  public static PriceOracleDisputeProcess create(PriceOracleSettings settings) {
    return new PriceOracleDisputeProcess(
        settings.getPbcAccount(),
        settings.getPriceOracleContracts(),
        settings.getTransactionSender(),
        settings.getLargeOracleContract(),
        settings.getContractStateHandler(),
        settings.createChainlinkClients());
  }

  /**
   * Manages disputes for all price oracle contract. Runs when the large oracle contract state
   * changes.
   *
   * @return false to never run eagerly
   */
  @Override
  public boolean run() {
    largeOracleWaiter.waitForChange();
    for (BlockchainAddress priceOracleContractAddress : priceOracleContractAddresses) {
      handlePriceOracleDisputes(priceOracleContractAddress);
    }
    return false;
  }

  private void handlePriceOracleDisputes(BlockchainAddress priceOracleContractAddress) {
    PriceOracleContractState pocState =
        contractStateHandler.getPriceOracleState(priceOracleContractAddress);
    LargeOracleContractState locState =
        contractStateHandler.getLargeOracleState(largeOracleContract);
    if (pocState == null || locState == null) {
      return;
    }

    int oracleMemberIndex = locState.getOracleMemberIndex(nodeAddress);
    if (oracleMemberIndex == -1) {
      return;
    }

    ChainlinkClient chainlinkClient = chainlinkClients.get(pocState.chainId());
    if (chainlinkClient == null) {
      LOGGER.warn("Node is not set up with endpoint for EVM chain {}", pocState.chainId());
      return;
    }

    AvlTree<OracleDisputeId, Dispute> activeDisputes = locState.activeDisputes();
    for (OracleDisputeId disputeId : activeDisputes.keySet()) {
      if (disputeId.smallOracle().equals(priceOracleContractAddress)) {
        Dispute activeDispute = activeDisputes.getValue(disputeId);
        if (!activeDispute.hasVoted(oracleMemberIndex)) {
          voteOnPriceOracleDispute(
              disputeId,
              pocState.referenceContractAddress(),
              pocState.disputeInfo().dispute(),
              chainlinkClient);
        }
      }
    }
  }

  private void voteOnPriceOracleDispute(
      OracleDisputeId id,
      String chainlinkContractAddress,
      PriceOracleDispute dispute,
      ChainlinkClient chainlinkClient) {
    Unsigned256 updatedAt = dispute.claims().get(0).updatedAt();
    if (!chainlinkClient.hasTimestampOfLatestValidBlockExceeded(updatedAt)) {
      return;
    }

    Unsigned256 roundId = RoundData.reconstructRoundIdFromOracleDisputeId(id);
    RoundData roundData = chainlinkClient.getRoundDataById(chainlinkContractAddress, roundId);
    if (roundData == null) {
      return;
    }

    Integer vote = determineVote(roundData, dispute.claims());
    if (vote != null) {
      votingUtil.castVote(id, vote);
    } else {
      votingUtil.addCounterClaim(id, SafeDataOutputStream.serialize(roundData::write));
    }
  }

  /**
   * Determine the correct vote by comparing the existing votes with the retrieved round data.
   *
   * <p>If a claim is found that is equal to the round data found on the external chain we vote for
   * that, unless this claim is the initial claim then we vote {@code -1} to indicate no fraud has
   * found place. If no claim was found, {@code null} is returned to indicate a counter-claim should
   * be added.
   *
   * @param roundData retrieved round data
   * @param claims claims of the dispute
   * @return index of claim matching the round data or -1 if the original claim was correct
   */
  private Integer determineVote(RoundData roundData, FixedList<PriceOracleRoundData> claims) {

    PriceOracleRoundData priceOracleRoundData = PriceOracleRoundData.convert(roundData);
    if (claims.get(0).equals(priceOracleRoundData)) {
      return NO_FRAUD_VOTE;
    } else if (claims.contains(priceOracleRoundData)) {
      return claims.indexOf(priceOracleRoundData);
    } else {
      return null;
    }
  }

  @Override
  public String name() {
    return PriceOracleDisputeProcess.class.getSimpleName();
  }
}
