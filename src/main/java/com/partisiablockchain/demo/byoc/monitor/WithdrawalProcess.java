package com.partisiablockchain.demo.byoc.monitor;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.client.transaction.Transaction;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.KeyPair;
import com.partisiablockchain.crypto.Signature;
import com.partisiablockchain.demo.byoc.governance.byocoutgoing.ByocOutgoingContractState;
import com.partisiablockchain.demo.byoc.governance.byocoutgoing.Epoch;
import com.partisiablockchain.demo.byoc.governance.byocoutgoing.EthereumAddress;
import com.partisiablockchain.demo.byoc.governance.byocoutgoing.Withdrawal;
import com.partisiablockchain.demo.byoc.settings.WithdrawalSettings;
import com.partisiablockchain.tree.AvlTree;
import com.secata.stream.SafeDataOutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Implementation of BYOC withdrawal requests.
 *
 * <p>Reads pending withdrawals requests on PBC and signs them.
 */
public final class WithdrawalProcess implements ByocProcess {

  private static final Logger logger = LoggerFactory.getLogger(WithdrawalProcess.class);

  private final BlockchainAddress withdrawalContract;
  private final KeyPair oracleSigningKey;
  private final BlockchainAddress pbcAccount;
  private final long transactionCost;
  private final ByocTransactionSender sender;
  private final ContractStateHandler contractStateHandler;
  private final Waiter waiter;

  /**
   * Default constructor setting up a withdrawal process using the settings.
   *
   * @param settings configuration object
   */
  public WithdrawalProcess(WithdrawalSettings settings) {
    this(
        settings.getWithdrawalContractAddress(),
        settings.getOracleSigningKey(),
        settings.getPbcAccount(),
        settings.getTransactionCost(),
        settings.getTransactionSender(),
        settings.getContractStateHandler());
  }

  WithdrawalProcess(
      BlockchainAddress withdrawalContract,
      KeyPair oracleSigningKey,
      BlockchainAddress pbcAccount,
      long transactionCost,
      ByocTransactionSender sender,
      ContractStateHandler contractStateHandler) {
    this.withdrawalContract = withdrawalContract;
    this.oracleSigningKey = oracleSigningKey;
    this.pbcAccount = pbcAccount;
    this.transactionCost = transactionCost;
    this.sender = sender;
    this.contractStateHandler = contractStateHandler;

    this.waiter = contractStateHandler.register(List.of(withdrawalContract));
  }

  @Override
  public boolean run() {
    waiter.waitForChange();
    ByocOutgoingContractState state =
        contractStateHandler.getState(withdrawalContract, ByocOutgoingContractState.class);
    if (state == null) {
      return false;
    }

    List<WithdrawMessage> pendingWithdrawals =
        pendingWithdrawals(state.oracleNonce(), state.epochs(), state.byocContract());
    for (WithdrawMessage withdrawal : pendingWithdrawals) {
      signAndUpdate(withdrawal);
    }
    return false;
  }

  private List<WithdrawMessage> pendingWithdrawals(
      long oracleNonce, AvlTree<Long, Epoch> epochs, EthereumAddress byocContract) {
    BlockchainAddress oracleAddress = pbcAccount;
    Epoch epoch = epochs.getValue(oracleNonce);

    int oracleIndex = epoch.getOracles().indexOf(oracleAddress);
    if (oracleIndex < 0) {
      logger.trace("Oracle node {} is unknown by contract", oracleAddress);
      return Collections.emptyList();
    }

    List<WithdrawMessage> result = new ArrayList<>();
    AvlTree<Long, Withdrawal> withdrawals = epoch.withdrawals();
    for (Long nonce : withdrawals.keySet()) {
      Withdrawal pendingWithdrawal = withdrawals.getValue(nonce);
      if (isMissingSignature(pendingWithdrawal, oracleIndex)) {
        Hash messageDigest = messageDigest(byocContract, oracleNonce, nonce, pendingWithdrawal);
        WithdrawMessage message = new WithdrawMessage(oracleNonce, nonce, messageDigest);
        result.add(message);
      }
    }
    return Collections.unmodifiableList(result);
  }

  @Override
  public String name() {
    return WithdrawalProcess.class.getSimpleName();
  }

  private boolean isMissingSignature(Withdrawal withdrawal, int oracleIndex) {
    return withdrawal.signatures().get(oracleIndex) == null;
  }

  private void signAndUpdate(WithdrawMessage withdrawal) {
    Signature signature = oracleSigningKey.sign(withdrawal.messageDigest);
    sender.sendTransactionAndWaitForEvents(
        createTransaction(withdrawal.oracleNonce, withdrawal.nonce, signature), transactionCost);
  }

  private Transaction createTransaction(long oracleNonce, long withdrawNonce, Signature signature) {
    return Transaction.create(
        withdrawalContract,
        SafeDataOutputStream.serialize(
            stream -> {
              stream.writeByte(1); // SIGN_PENDING_WITHDRAWAL
              stream.writeLong(oracleNonce);
              stream.writeLong(withdrawNonce);
              signature.write(stream);
            }));
  }

  static Hash messageDigest(
      EthereumAddress byocContract, long oracleNonce, long nonce, Withdrawal withdrawal) {
    return Hash.create(
        stream -> {
          byocContract.write(stream);
          stream.writeLong(oracleNonce);
          stream.writeLong(nonce);
          withdrawal.receiver().write(stream);
          withdrawal.amountWithoutTax().write(stream);
        });
  }

  private static final class WithdrawMessage {

    long oracleNonce;
    long nonce;
    Hash messageDigest;

    public WithdrawMessage(long oracleNonce, long nonce, Hash messageDigest) {
      this.oracleNonce = oracleNonce;
      this.nonce = nonce;
      this.messageDigest = messageDigest;
    }
  }
}
