package com.partisiablockchain.demo.byoc.monitor.chainlink;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.demo.byoc.monitor.RoundData;
import com.partisiablockchain.math.Unsigned256;
import com.secata.tools.coverage.ExceptionConverter;
import java.math.BigInteger;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.web3j.abi.FunctionEncoder;
import org.web3j.abi.FunctionReturnDecoder;
import org.web3j.abi.datatypes.Function;
import org.web3j.abi.datatypes.generated.Uint80;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.core.DefaultBlockParameter;
import org.web3j.protocol.core.Response;
import org.web3j.protocol.core.methods.request.Transaction;
import org.web3j.protocol.core.methods.response.EthCall;
import org.web3j.utils.Numeric;

/** Client for interacting with Chainlink data feed contracts on an external EVM chain. */
public final class ChainlinkClient {

  static final String DUMMY_ADDRESS = "0x0000000000000000000000000000000000000000";

  private static final Logger logger = LoggerFactory.getLogger(ChainlinkClient.class);

  private final Web3j client;
  private final BigInteger blockValidityDepth;
  private final ChainlinkRetryHandler retryHandler;

  static String errorString(Response.Error error) {
    return String.format(
        "[code=%d, message=%s, data=%s]", error.getCode(), error.getMessage(), error.getData());
  }

  /**
   * Default constructor.
   *
   * @param client client for interacting with an external EVM chain
   * @param blockValidityDepth block validity depth of external chain
   */
  public ChainlinkClient(Web3j client, BigInteger blockValidityDepth) {
    this.client = client;
    this.blockValidityDepth = blockValidityDepth;
    this.retryHandler = ChainlinkRetryHandler.create();
  }

  /**
   * Calls a Chainlink contract on the EVM chain to get round data for a specific round.
   *
   * <p>Because the round identifier is from a challenge period or a dispute, we cannot be sure that
   * round data actually exists for it, since it was reported by other nodes. Round data can be
   * missing in the following two cases:
   *
   * <ol>
   *   <li>The reader has not yet seen the round data for the round ID. In this case the client
   *       should retry, because the reader might then be up-to-date.
   *   <li>The round identifier was reported by a malicious node for a round without round data. In
   *       this case the node should report the round as malicious.
   * </ol>
   *
   * <p>When no round data was found - even after having retried - the round identifier is
   * considered malicious.
   *
   * @param chainlinkContractAddress address of the Chainlink contract on the external chain
   * @param roundId identifier of the round
   * @return round data if the call succeeded, {@code null} if the call failed with remaining retry
   *     attempts, or the special 'malicious' round data (see {@link
   *     ChainlinkClient#maliciousRoundData(Unsigned256)}) if the call failed with no remaining
   *     retry attempts.
   */
  public RoundData getRoundDataById(String chainlinkContractAddress, Unsigned256 roundId) {
    Uint80 roundIdUint80 = new Uint80(new BigInteger(roundId.serialize()));
    Function getRoundDataFunction =
        new Function("getRoundData", List.of(roundIdUint80), RoundData.OUTPUT_TYPES);
    EthCall response = call(chainlinkContractAddress, FunctionEncoder.encode(getRoundDataFunction));
    if (!response.hasError()) {
      RoundData roundData =
          RoundData.parseDecoded(
              FunctionReturnDecoder.decode(
                  response.getValue(), getRoundDataFunction.getOutputParameters()));
      if (roundData.isFinished()) {
        retryHandler.removeAttemptsForRoundsOlderThan(
            chainlinkContractAddress, roundData.roundId());
        return roundData;
      } else {
        logger.warn("EthCall 'getRoundData' for round id {} resulted in a future round", roundId);
        return handleFailedCall(chainlinkContractAddress, roundId);
      }
    } else {
      Response.Error error = response.getError();
      logger.warn(
          "EthCall 'getRoundData' for round id {} resulted in error: {}",
          roundId,
          errorString(error));
      String errorMessage = error.getMessage();
      // When the error message contains "execution reverted" the round identifier is for a future
      // round and might have been reported by a malicious node
      if (errorMessage.contains("execution reverted")) {
        return handleFailedCall(chainlinkContractAddress, roundId);
      } else {
        throw new RuntimeException("Unknown error for EthCall 'getRoundData': " + errorMessage);
      }
    }
  }

  /**
   * Handles a failed call to the external chain depending on the number of remaining retry
   * attempts.
   *
   * @param contractAddress address of the contract that was called
   * @param roundId identifier of the round to get round data for
   * @return {@code null} if there is retry attempts left, otherwise the special "malicious" round
   *     data object (see {@link ChainlinkClient#maliciousRoundData(Unsigned256)}).
   */
  private RoundData handleFailedCall(String contractAddress, Unsigned256 roundId) {
    if (retryHandler.getRemainingAttempts(contractAddress, roundId) > 0) {
      retryHandler.incrementAttempts(contractAddress, roundId);
      return null;
    } else {
      return maliciousRoundData(roundId);
    }
  }

  /**
   * Get the latest round data for a Chainlink data feed contract on the external EVM chain.
   *
   * @param chainlinkContractAddress address of Chainlink data feed contract
   * @return latest round data
   */
  public RoundData getLatestRoundData(String chainlinkContractAddress) {
    Function function = new Function("latestRoundData", List.of(), RoundData.OUTPUT_TYPES);
    EthCall response = call(chainlinkContractAddress, FunctionEncoder.encode(function));
    if (response.hasError()) {
      throw new RuntimeException(
          "Call function 'getLatestRoundData' resulted in error "
              + errorString(response.getError()));
    } else {
      return RoundData.parseDecoded(
          FunctionReturnDecoder.decode(response.getValue(), function.getOutputParameters()));
    }
  }

  /**
   * Creates the special round data object used to indicate that round data does not exist for the
   * round ID. The special round data object consists of all zeros except for the round identifier.
   *
   * @param roundId identifier of the round
   * @return special round data object to indicate that round data does not exist for the round ID
   */
  public static RoundData maliciousRoundData(Unsigned256 roundId) {
    return new RoundData(
        roundId, Unsigned256.ZERO, Unsigned256.ZERO, Unsigned256.ZERO, Unsigned256.ZERO);
  }

  /**
   * Checks if the block timestamp of the latest valid block has exceeded the provided timestamp.
   *
   * @param timestamp the timestamp to check
   * @return true if the timestamp of the latest valid block has exceeded the provided timestamp,
   *     otherwise false
   */
  public boolean hasTimestampOfLatestValidBlockExceeded(Unsigned256 timestamp) {
    BigInteger latestValidBlockTimestamp = getLatestValidBlockTimestamp();
    return Unsigned256.create(latestValidBlockTimestamp).compareTo(timestamp) >= 0;
  }

  /**
   * Calls a contract function on the external chain.
   *
   * @param contractAddress address of the contract
   * @param encodedFunction encoded function to call
   * @return call response
   */
  private EthCall call(String contractAddress, String encodedFunction) {
    return ExceptionConverter.call(
        () ->
            client
                .ethCall(
                    Transaction.createEthCallTransaction(
                        DUMMY_ADDRESS, Numeric.prependHexPrefix(contractAddress), encodedFunction),
                    getLatestValidBlock())
                .send());
  }

  /**
   * Gets the Unix timestamp in seconds of the latest valid block on the external chain.
   *
   * @return timestamp of the latest valid block
   */
  private BigInteger getLatestValidBlockTimestamp() {
    return ExceptionConverter.call(
        () ->
            client
                .ethGetBlockByNumber(getLatestValidBlock(), false)
                .send()
                .getBlock()
                .getTimestamp(),
        "Failed to get block by number");
  }

  /**
   * Gets the block number of the latest valid block on the external chain.
   *
   * @return the block number of the latest valid block
   */
  DefaultBlockParameter getLatestValidBlock() {
    BigInteger latestValidBlock =
        ExceptionConverter.call(
            () -> client.ethBlockNumber().send().getBlockNumber().subtract(blockValidityDepth),
            "Failed to get the number of the latest final block");
    return DefaultBlockParameter.valueOf(latestValidBlock);
  }
}
