package com.partisiablockchain.demo.byoc.monitor;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.client.transaction.Transaction;
import com.partisiablockchain.demo.byoc.governance.byocincoming.ByocIncomingContractState;
import com.partisiablockchain.demo.byoc.governance.byocincoming.Deposit;
import com.partisiablockchain.demo.byoc.governance.byocincoming.DepositTransaction;
import com.partisiablockchain.demo.byoc.governance.byocincoming.Dispute;
import com.partisiablockchain.demo.byoc.governance.byocincoming.Epoch;
import com.partisiablockchain.demo.byoc.governance.byocoutgoing.ByocOutgoingContractState;
import com.partisiablockchain.demo.byoc.governance.byocoutgoing.Withdrawal;
import com.partisiablockchain.demo.byoc.governance.byocoutgoing.WithdrawalTransaction;
import com.partisiablockchain.demo.byoc.governance.largeoracle.LargeOracleContractState;
import com.partisiablockchain.demo.byoc.governance.largeoracle.OracleDisputeId;
import com.partisiablockchain.demo.byoc.governance.largeoracle.StakedTokens;
import com.partisiablockchain.demo.byoc.rest.dto.DepositDto;
import com.partisiablockchain.demo.byoc.settings.DisputeSettings;
import com.partisiablockchain.math.Unsigned256;
import com.partisiablockchain.tree.AvlTree;
import com.secata.stream.SafeDataOutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.bouncycastle.util.encoders.Hex;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** Monitor PBC and Ethereum states to create and vote on BYOC disputes. */
public final class DisputeProcess implements ByocProcess {

  private static final Logger logger = LoggerFactory.getLogger(DisputeProcess.class);
  private static final byte DEPOSIT_DISPUTE_CREATE = 1;
  private static final byte WITHDRAWAL_DISPUTE_CREATE = 2;
  private static final long DISPUTE_TOKEN_COST = 2_500_0000L;
  private static final int NO_FRAUD_VOTE = -1;

  private final DataSource dataSource;
  private final BlockchainAddress identity;
  private final ByocTransactionSender sender;
  private final BlockchainAddress largeOracleContract;
  private final BlockchainAddress depositContract;
  private final BlockchainAddress withdrawContract;
  private final long transactionsCost;
  private final VotingUtil votingUtil;
  private final SystemTimeProvider systemTime;

  private long nextDepositNonce;
  private final Map<Long, Long> nextWithdrawalNonceForOracle;
  private long stakedTokens;

  private final ContractStateHandler contractStateHandler;

  /**
   * Default constructor.
   *
   * @param settings settings for this process
   */
  public DisputeProcess(DisputeSettings settings) {
    this(
        settings.getDataSource(),
        settings.getPbcAccount(),
        settings.getTransactionSender(),
        settings.getLargeOracleContract(),
        settings.getDepositContract(),
        settings.getWithdrawContract(),
        settings.getTransactionCost(),
        settings.getContractStateHandler(),
        new SystemTimeProviderImpl());
  }

  DisputeProcess(
      DataSource dataSource,
      BlockchainAddress identity,
      ByocTransactionSender sender,
      BlockchainAddress largeOracleContract,
      BlockchainAddress depositContract,
      BlockchainAddress withdrawContract,
      long transactionsCost,
      ContractStateHandler contractStateHandler,
      SystemTimeProvider systemTime) {
    this.dataSource = dataSource;
    this.identity = identity;
    this.sender = sender;
    this.largeOracleContract = largeOracleContract;
    this.depositContract = depositContract;
    this.withdrawContract = withdrawContract;
    this.transactionsCost = transactionsCost;
    this.nextDepositNonce = 0;
    this.nextWithdrawalNonceForOracle = new HashMap<>();
    this.votingUtil = VotingUtil.create(sender, largeOracleContract);
    this.contractStateHandler = contractStateHandler;
    this.systemTime = systemTime;
  }

  @Override
  public boolean run() {
    LargeOracleContractState locState =
        contractStateHandler.getLargeOracleState(largeOracleContract);
    ByocIncomingContractState byocIncomingState =
        contractStateHandler.getState(depositContract, ByocIncomingContractState.class);
    ByocOutgoingContractState byocOutgoingState =
        contractStateHandler.getState(withdrawContract, ByocOutgoingContractState.class);
    if (locState == null || byocIncomingState == null || byocOutgoingState == null) {
      return false;
    }

    int memberIdx = locState.getOracleMemberIndex(identity);
    if (memberIdx >= 0) {
      // Current account is a member of the large oracle and can vote on disputes
      voteOnCurrentDisputes(
          locState.activeDisputes(),
          memberIdx,
          byocIncomingState.depositNonce(),
          byocIncomingState.deposit(),
          byocIncomingState.deposits(),
          byocIncomingState.disputes(),
          byocOutgoingState.epochs());
    }
    StakedTokens tokensStakedBy = locState.getTokensStakedBy(identity);
    stakedTokens = tokensStakedBy.freeTokens();
    long availableTokens =
        Math.max(0, tokensStakedBy.freeTokens() - tokensStakedBy.reservedTokens());
    Long expirationTimestamp = tokensStakedBy.expirationTimestamp();
    boolean notExpired =
        expirationTimestamp == null || expirationTimestamp > systemTime.getSystemTime();
    updateNextDepositNonce(byocIncomingState.epochs());
    if (availableTokens >= DISPUTE_TOKEN_COST && notExpired) {
      auditTransactions(
          byocIncomingState.depositNonce(),
          byocIncomingState.deposits(),
          byocIncomingState.disputes(),
          byocOutgoingState.oracleNonce(),
          byocOutgoingState.epochs());
    }
    return false;
  }

  private void updateNextDepositNonce(AvlTree<Long, Epoch> epochs) {
    // We only want to check deposits for the current and the previous oracles.

    if (epochs.size() == 0) {
      // No epochs exist yet, meaning the current oracle is the first.
      nextDepositNonce = Math.max(nextDepositNonce, 0L);
    } else {
      long lastEpochId = epochs.size() - 1L;
      Epoch lastEpoch = epochs.getValue(lastEpochId);
      long startingNonce = lastEpoch.fromDepositNonce();
      nextDepositNonce = Math.max(nextDepositNonce, startingNonce);
    }
  }

  long getNextDepositNonce() {
    return nextDepositNonce;
  }

  Long getNextWithdrawalNonceForOracle(long oracle) {
    return nextWithdrawalNonceForOracle.get(oracle);
  }

  /**
   * Look for discrepancies between the transaction logged on Ethereum and the ones store on PBC
   * state. If any are found, report the error by creating a new dispute.
   */
  private void auditTransactions(
      long currentNonce,
      AvlTree<Long, Deposit> deposits,
      AvlTree<Long, Dispute> disputes,
      long oracleNonce,
      AvlTree<Long, com.partisiablockchain.demo.byoc.governance.byocoutgoing.Epoch> epochs) {
    auditDeposits(currentNonce, deposits, disputes);
    auditWithdrawals(oracleNonce, epochs);
  }

  private void auditDeposits(
      long currentNonce, AvlTree<Long, Deposit> deposits, AvlTree<Long, Dispute> disputes) {

    while (nextDepositNonce < currentNonce) {
      if (stakedTokens < DISPUTE_TOKEN_COST) {
        logger.warn("Not enough tokens remain to fund disputes. Stake more tokens to continue.");
        return;
      }

      DepositTransaction externalDeposit = dataSource.getDeposit(nextDepositNonce);
      if (externalDeposit == null) {
        throw new RuntimeException(
            "Ethereum deposit transaction with nonce " + nextDepositNonce + " does not exist!");
      }
      DepositTransaction pbcDeposit =
          DepositTransaction.create(nextDepositNonce, deposits.getValue(nextDepositNonce));
      if (!externalDeposit.equals(pbcDeposit) && !disputes.containsKey(nextDepositNonce)) {
        Transaction tx =
            Transaction.create(
                depositContract,
                SafeDataOutputStream.serialize(
                    stream -> {
                      stream.writeByte(DEPOSIT_DISPUTE_CREATE);
                      stream.writeLong(externalDeposit.nonce());
                      externalDeposit.receiver().write(stream);
                      externalDeposit.amount().write(stream);
                    }));
        sender.sendTransactionAndWaitForEvents(tx, transactionsCost);
        stakedTokens = stakedTokens - DISPUTE_TOKEN_COST;
      }
      nextDepositNonce++;
    }
  }

  private void auditWithdrawals(
      long oracleNonce,
      AvlTree<Long, com.partisiablockchain.demo.byoc.governance.byocoutgoing.Epoch> epochs) {
    // We only want to check withdrawals for the current and the previous oracles.
    long previousOracleNonce = Math.max(oracleNonce - 1, 0);
    if (previousOracleNonce != oracleNonce) {
      auditWithdrawalsFromPreviousOracle(epochs, previousOracleNonce);
    }
    auditWithdrawalsFromCurrentOracle(epochs, oracleNonce);
  }

  /**
   * Disputes a withdrawal if it is present on both Ethereum and PBC, and they do not match each
   * other, or if the withdrawal only exists on Ethereum. If the withdrawal has already been
   * disputed it is ignored.
   *
   * @param epochs epochs of outgoing state
   * @param oracleNonce nonce of the previous oracle
   */
  private void auditWithdrawalsFromPreviousOracle(
      AvlTree<Long, com.partisiablockchain.demo.byoc.governance.byocoutgoing.Epoch> epochs,
      long oracleNonce) {
    List<WithdrawalTransaction> withdrawals =
        dataSource.getAllWithdrawalsForOracleNonce(oracleNonce);

    for (WithdrawalTransaction withdrawal : withdrawals) {
      if (stakedTokens < DISPUTE_TOKEN_COST) {
        logger.warn("Not enough tokens staked to fund disputes. Stake more tokens to continue");
        return;
      }
      boolean disputeExists =
          epochs
                  .getValue(withdrawal.oracleNonce())
                  .disputes()
                  .getValue(withdrawal.withdrawalNonce())
              != null;
      WithdrawalTransaction pbcWithdrawal =
          getPbcWithdrawalTransaction(
              epochs, withdrawal.oracleNonce(), withdrawal.withdrawalNonce());
      if (pbcWithdrawal == null && !disputeExists) {
        byte[] receiver = Hex.decode(withdrawal.destination());
        sendWithdrawDispute(receiver, withdrawal);
      } else if (!withdrawal.equalsIgnoringBitmask(pbcWithdrawal) && !disputeExists) {
        byte[] receiver = Hex.decode(withdrawal.destination());
        sendWithdrawDispute(receiver, withdrawal);
      }

      nextWithdrawalNonceForOracle.put(oracleNonce, withdrawal.withdrawalNonce());
    }
  }

  /**
   * Disputes a withdrawal if it is present on both Ethereum and PBC, and they do not match each
   * other. If the withdrawal has already been disputed it is ignored.
   *
   * @param epochs epochs of outgoing state
   * @param oracleNonce nonce of the current oracle
   */
  private void auditWithdrawalsFromCurrentOracle(
      AvlTree<Long, com.partisiablockchain.demo.byoc.governance.byocoutgoing.Epoch> epochs,
      long oracleNonce) {
    long withdrawalNonce = 0;
    while (epochs.getValue(oracleNonce).withdrawals().getValue(withdrawalNonce) != null) {
      DataSource.WithdrawalId id = new DataSource.WithdrawalId(withdrawalNonce, oracleNonce);
      WithdrawalTransaction externalWithdrawal = dataSource.getWithdrawal(id);
      if (auditWithdrawal(epochs, externalWithdrawal)) {
        break;
      }
      withdrawalNonce++;
      nextWithdrawalNonceForOracle.put(oracleNonce, withdrawalNonce);
    }
  }

  private boolean auditWithdrawal(
      AvlTree<Long, com.partisiablockchain.demo.byoc.governance.byocoutgoing.Epoch> epochs,
      WithdrawalTransaction externalWithdrawal) {
    if (externalWithdrawal == null) {
      // Withdrawal not yet found on external chain.
      return true;
    }
    if (stakedTokens < DISPUTE_TOKEN_COST) {
      logger.warn("Not enough tokens remain to fund disputes. Stake more tokens to continue.");
      return true;
    }
    if (externalWithdrawal.bitmask() == 0b000) {
      // Withdrawal was created out-of-order after its epoch ended.
      // No oracle signatures were used so there is no one to dispute.
      return false;
    }

    WithdrawalTransaction pbcWithdrawal =
        getPbcWithdrawalTransaction(
            epochs, externalWithdrawal.oracleNonce(), externalWithdrawal.withdrawalNonce());

    boolean disputeExists =
        epochs
                .getValue(externalWithdrawal.oracleNonce())
                .disputes()
                .getValue(externalWithdrawal.withdrawalNonce())
            != null;
    if (!externalWithdrawal.equalsIgnoringBitmask(pbcWithdrawal) && !disputeExists) {
      byte[] receiver = Hex.decode(externalWithdrawal.destination());
      sendWithdrawDispute(receiver, externalWithdrawal);
    }
    return false;
  }

  private void sendWithdrawDispute(byte[] receiver, WithdrawalTransaction finalExternalWithdrawal) {
    Transaction tx =
        Transaction.create(
            withdrawContract,
            SafeDataOutputStream.serialize(
                stream -> {
                  stream.writeByte(WITHDRAWAL_DISPUTE_CREATE);
                  stream.writeLong(finalExternalWithdrawal.oracleNonce());
                  stream.writeLong(finalExternalWithdrawal.withdrawalNonce());
                  stream.write(receiver);
                  finalExternalWithdrawal.amount().write(stream);
                  stream.writeInt(finalExternalWithdrawal.bitmask());
                }));
    sender.sendTransactionAndWaitForEvents(tx, transactionsCost);
    stakedTokens = stakedTokens - DISPUTE_TOKEN_COST;
  }

  private WithdrawalTransaction getPbcWithdrawalTransaction(
      AvlTree<Long, com.partisiablockchain.demo.byoc.governance.byocoutgoing.Epoch> epochs,
      long oracleNonce,
      long withdrawalNonce) {
    Withdrawal withdrawal = epochs.getValue(oracleNonce).withdrawals().getValue(withdrawalNonce);
    if (withdrawal == null) {
      return null;
    } else {
      return WithdrawalTransaction.create(withdrawalNonce, oracleNonce, withdrawal);
    }
  }

  private void voteOnCurrentDisputes(
      AvlTree<OracleDisputeId, com.partisiablockchain.demo.byoc.governance.largeoracle.Dispute>
          activeDisputes,
      int memberIdx,
      long depositNonce,
      Deposit deposit,
      AvlTree<Long, Deposit> deposits,
      AvlTree<Long, Dispute> disputes,
      AvlTree<Long, com.partisiablockchain.demo.byoc.governance.byocoutgoing.Epoch> epochs) {
    for (OracleDisputeId id : activeDisputes.keySet()) {
      com.partisiablockchain.demo.byoc.governance.largeoracle.Dispute activeDispute =
          activeDisputes.getValue(id);
      Integer currentVote = activeDispute.votes().get(memberIdx);
      if (currentVote == null) {

        BlockchainAddress smallOracle = id.smallOracle();
        if (smallOracle.equals(depositContract)) {
          voteOnDepositDispute(id, depositNonce, deposit, deposits, disputes);
        } else if (smallOracle.equals(withdrawContract)) {
          voteOnWithdrawalDispute(id, epochs);
        } else {
          logger.warn(
              "Contract address {} does not match a known small oracle contract address",
              smallOracle);
        }
      }
    }
  }

  private void voteOnDepositDispute(
      OracleDisputeId id,
      long depositNonce,
      Deposit deposit,
      AvlTree<Long, Deposit> deposits,
      AvlTree<Long, Dispute> disputes) {
    long transactionNonce = id.disputeId();
    Deposit disputedDeposit;
    if (transactionNonce == depositNonce) {
      disputedDeposit = deposit;
    } else {
      disputedDeposit = deposits.getValue(transactionNonce);
    }
    // We can wait for the deposit to appear since we know it must exist on ETH.
    DepositDto depositDto = dataSource.waitForDeposit(transactionNonce);
    DepositTransaction actualTransaction =
        new DepositTransaction(
            transactionNonce,
            BlockchainAddress.fromString(depositDto.pbcAddress),
            Unsigned256.create(depositDto.amount));

    DepositTransaction disputedTransaction =
        DepositTransaction.create(transactionNonce, disputedDeposit);
    if (disputedTransaction.equals(actualTransaction)) {
      // The transaction logged on Ethereum matches the one in the PBC state. Vote no fraud.
      votingUtil.castVote(id, NO_FRAUD_VOTE);
    } else {
      Dispute detailedDispute = disputes.getValue(transactionNonce);
      int vote = findDepositTransactionCandidate(actualTransaction, detailedDispute);
      if (vote >= 0) {
        votingUtil.castVote(id, vote);
      } else {
        byte[] counterClaim = serializeCounterClaim(actualTransaction);
        votingUtil.addCounterClaim(id, counterClaim);
      }
    }
  }

  private void voteOnWithdrawalDispute(
      OracleDisputeId id,
      AvlTree<Long, com.partisiablockchain.demo.byoc.governance.byocoutgoing.Epoch> epochs) {
    long withdrawalNonce = id.disputeId();
    long oracleNonce = id.oracleId();
    WithdrawalTransaction pbcWithdrawal =
        getPbcWithdrawalTransaction(epochs, oracleNonce, withdrawalNonce);

    com.partisiablockchain.demo.byoc.governance.byocoutgoing.Dispute dispute =
        epochs.getValue(oracleNonce).disputes().getValue(withdrawalNonce);
    DataSource.WithdrawalId withdrawalId =
        new DataSource.WithdrawalId(withdrawalNonce, oracleNonce);
    WithdrawalTransaction externalWithdrawal = dataSource.getWithdrawal(withdrawalId);
    // If the disputed transaction was not found on Ethereum wait until it has been found, so we can
    // determine what to vote.
    if (externalWithdrawal != null) {
      if (externalWithdrawal.equalsIgnoringBitmask(pbcWithdrawal)) {
        // The transaction logged on Ethereum matches the one in the PBC state
        votingUtil.castVote(id, NO_FRAUD_VOTE);
      } else {
        int vote = findWithdrawalTransactionCandidate(externalWithdrawal, dispute);
        if (vote >= 0) {
          votingUtil.castVote(id, vote);
        } else {
          byte[] counterClaim = serializeCounterClaim(externalWithdrawal);
          votingUtil.addCounterClaim(id, counterClaim);
        }
      }
    }
  }

  private int findDepositTransactionCandidate(
      DepositTransaction loggedTransaction, Dispute detailedDispute) {
    List<DepositTransaction> claims = DepositTransaction.createList(detailedDispute.claims());
    for (int i = 0; i < claims.size(); i++) {
      DepositTransaction claim = claims.get(i);
      if (claim.equals(loggedTransaction)) {
        return i;
      }
    }
    return -1;
  }

  private int findWithdrawalTransactionCandidate(
      WithdrawalTransaction loggedTransaction,
      com.partisiablockchain.demo.byoc.governance.byocoutgoing.Dispute detailedDispute) {
    List<WithdrawalTransaction> claims =
        detailedDispute.claims().stream()
            .map(
                dispute ->
                    new WithdrawalTransaction(
                        loggedTransaction.withdrawalNonce(),
                        Hex.toHexString(dispute.receiver().identifier().getData()),
                        dispute.amount(),
                        loggedTransaction.oracleNonce(),
                        dispute.bitmask()))
            .toList();
    for (int i = 0; i < claims.size(); i++) {
      WithdrawalTransaction claim = claims.get(i);
      if (claim.equals(loggedTransaction)) {
        return i;
      }
    }
    return -1;
  }

  private byte[] serializeCounterClaim(DepositTransaction transaction) {
    return SafeDataOutputStream.serialize(
        stream -> {
          stream.writeLong(transaction.nonce());
          transaction.receiver().write(stream);
          transaction.amount().write(stream);
        });
  }

  private byte[] serializeCounterClaim(WithdrawalTransaction transaction) {
    return SafeDataOutputStream.serialize(
        stream -> {
          stream.writeLong(transaction.oracleNonce());
          stream.writeLong(transaction.withdrawalNonce());
          stream.write(Hex.decode(transaction.destination()));
          transaction.amount().write(stream);
          stream.writeInt(transaction.bitmask());
        });
  }

  @Override
  public String name() {
    return DisputeProcess.class.getSimpleName();
  }
}
