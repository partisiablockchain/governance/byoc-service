package com.partisiablockchain.demo.byoc.monitor;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.demo.byoc.governance.priceoracle.ChallengePeriod;
import com.partisiablockchain.demo.byoc.governance.priceoracle.PriceOracleContractState;
import com.partisiablockchain.demo.byoc.governance.priceoracle.PriceUpdates;
import com.partisiablockchain.demo.byoc.governance.priceoracle.RoundPriceUpdates;
import com.partisiablockchain.demo.byoc.monitor.chainlink.ChainlinkClient;
import com.partisiablockchain.demo.byoc.settings.PriceOracleSettings;
import com.partisiablockchain.math.Unsigned256;
import com.partisiablockchain.tree.AvlTree;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** Notifies price updates for Chainlink data feed contracts on external EVM chains. */
public final class PriceOracleNotifyProcess implements ByocProcess {

  private static final Logger LOGGER = LoggerFactory.getLogger(PriceOracleNotifyProcess.class);

  private final SystemTimeProvider systemTime;
  private final BlockchainAddress nodeAddress;
  private final List<BlockchainAddress> priceOracleContractAddresses;
  private final long transactionCost;
  private final ByocTransactionSender transactionSender;
  private final ContractStateHandler contractStateHandler;
  private final Map<String, ChainlinkClient> chainlinkClients;

  PriceOracleNotifyProcess(
      SystemTimeProvider systemTime,
      BlockchainAddress nodeAddress,
      List<BlockchainAddress> priceOracleContractAddresses,
      long transactionCost,
      ByocTransactionSender transactionSender,
      ContractStateHandler contractStateHandler,
      Map<String, ChainlinkClient> chainlinkClients) {
    this.systemTime = systemTime;
    this.nodeAddress = nodeAddress;
    this.priceOracleContractAddresses = priceOracleContractAddresses;
    this.transactionCost = transactionCost;
    this.transactionSender = transactionSender;
    this.contractStateHandler = contractStateHandler;
    this.chainlinkClients = chainlinkClients;
  }

  /**
   * Creates a new price oracle notify process.
   *
   * @param settings price oracle settings
   * @return new price oracle notify process
   */
  public static PriceOracleNotifyProcess create(PriceOracleSettings settings) {
    return new PriceOracleNotifyProcess(
        new SystemTimeProviderImpl(),
        settings.getPbcAccount(),
        settings.getPriceOracleContracts(),
        settings.getTransactionCost(),
        settings.getTransactionSender(),
        settings.getContractStateHandler(),
        settings.createChainlinkClients());
  }

  /**
   * Gets the identifier of the latest notified round data by a node. If no round data has been
   * notified by the node it defaults to zero.
   *
   * @param nodeAddress address of the node
   * @param roundPriceUpdates all notified price updates on the contract
   * @return identifier of the latest notified round by the node or zero if no rounds has been
   *     notified
   */
  static Unsigned256 getLatestNotifiedRoundIdByNode(
      BlockchainAddress nodeAddress, RoundPriceUpdates roundPriceUpdates) {
    AvlTree<Unsigned256, PriceUpdates> priceUpdates = roundPriceUpdates.roundPriceUpdates();
    return priceUpdates.keySet().stream()
        .filter(roundId -> hasPriceUpdateFromNode(priceUpdates.getValue(roundId), nodeAddress))
        .max(Unsigned256::compareTo)
        .orElse(Unsigned256.ZERO);
  }

  private static boolean hasPriceUpdateFromNode(
      PriceUpdates priceUpdates, BlockchainAddress nodeAddress) {
    return priceUpdates.oraclePriceUpdates().containsKey(nodeAddress);
  }

  @Override
  public boolean run() {
    for (BlockchainAddress priceOracleContractAddress : priceOracleContractAddresses) {
      runPriceOracleServices(priceOracleContractAddress);
    }
    return false;
  }

  private void runPriceOracleServices(BlockchainAddress priceOracleContractAddress) {
    PriceOracleContractState pocState =
        contractStateHandler.getPriceOracleState(priceOracleContractAddress);
    if (pocState == null) {
      return;
    }

    if (!pocState.isRegisteredPriceOracleNode(nodeAddress)) {
      return;
    }

    ChainlinkClient chainlinkClient = chainlinkClients.get(pocState.chainId());
    if (chainlinkClient == null) {
      LOGGER.warn("Node is not set up with endpoint for EVM chain {}", pocState.chainId());
      return;
    }

    ChallengePeriod challengePeriod = pocState.challengePeriod();
    if (challengePeriod != null && !challengePeriod.hasEnded(systemTime.getSystemTime())) {
      return;
    }
    RoundData latestRoundData =
        chainlinkClient.getLatestRoundData(pocState.referenceContractAddress());
    Unsigned256 latestRoundId = latestRoundData.roundId();
    if (latestRoundId.compareTo(pocState.latestPublishedRoundId()) <= 0) {
      return;
    }
    Unsigned256 latestNotifiedRoundIdByMe =
        getLatestNotifiedRoundIdByNode(nodeAddress, pocState.roundPriceUpdates());
    if (latestRoundId.compareTo(latestNotifiedRoundIdByMe) <= 0) {
      return;
    }
    PriceOracleNotifyPriceUpdateSender.notifyPriceUpdate(
        priceOracleContractAddress, latestRoundData, transactionSender, transactionCost);
  }

  @Override
  public String name() {
    return PriceOracleNotifyProcess.class.getSimpleName();
  }
}
