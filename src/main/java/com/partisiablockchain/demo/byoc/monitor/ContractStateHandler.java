package com.partisiablockchain.demo.byoc.monitor;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.blockchain.contract.CoreContractState;
import com.partisiablockchain.blockchain.contract.binder.BlockchainContract;
import com.partisiablockchain.demo.byoc.governance.largeoracle.LargeOracleContractState;
import com.partisiablockchain.demo.byoc.governance.largeoracle.OldLargeOracleContractState;
import com.partisiablockchain.demo.byoc.governance.priceoracle.PriceOracleContractState;
import com.partisiablockchain.serialization.StateAccessor;
import com.partisiablockchain.serialization.StateAccessorAvlLeafNode;
import com.partisiablockchain.serialization.StateSerializable;
import com.partisiablockchain.server.tcp.client.Client;
import com.partisiablockchain.util.StateAccessorUtil;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** A class for managing contract states for BYOC processes through listeners. */
public final class ContractStateHandler implements Client.CreateContractListener {
  private static final Logger logger = LoggerFactory.getLogger(ContractStateHandler.class);

  /** Map of contract addresses and the associated contract state. */
  public Map<BlockchainAddress, StateSerializable> contractStates;

  /** List of addresses of contracts that this object should keep track of. */
  private final List<BlockchainAddress> contractAddresses;

  private final List<Waiter> waiters;

  /**
   * Creates a ContractStateHandler object with a list of the specified contract addresses.
   *
   * @param contractAddresses the addresses of contracts to keep track of
   */
  public ContractStateHandler(List<BlockchainAddress> contractAddresses) {
    this.waiters = new ArrayList<>();
    this.contractAddresses = contractAddresses;

    this.contractStates = new ConcurrentHashMap<>();
  }

  @Override
  public Client.Listener createContract(
      BlockchainContract<?, ?> blockchainContract,
      Client.StateHistory stateHistory,
      BlockchainAddress blockchainAddress,
      CoreContractState coreContractState,
      StateSerializable stateSerializable) {
    if (contractAddresses.contains(blockchainAddress)) {
      contractStates.put(blockchainAddress, stateSerializable);
      for (Waiter waiter : waiters) {
        waiter.update(blockchainAddress);
      }
      return new ByocContractListener(blockchainAddress);
    } else {
      logger.debug("Ignoring contract with address {}", blockchainAddress);
      return null;
    }
  }

  /**
   * Registers a Waiter with the supplied list of contract addresses which is used to wait for state
   * changes on the specified contracts.
   *
   * @param contractAddresses the contracts the process should wait on
   * @return a Waiter object
   */
  public Waiter register(List<BlockchainAddress> contractAddresses) {
    Waiter waiter = new Waiter(contractAddresses);
    waiters.add(waiter);
    return waiter;
  }

  /**
   * Gets the state, in the current state environment, associated with the contract that is found at
   * the contractAddress, through a StateAccessor. Must be a Record type.
   *
   * @param contractAddress the address of the contract to look up
   * @param stateClass the type of the contract state
   * @param <T> the type of the contract state
   * @return a new contract state object of type stateClass
   */
  public <T> T getState(BlockchainAddress contractAddress, Class<T> stateClass) {
    StateSerializable contractState = contractStates.get(contractAddress);
    return StateAccessorUtil.createContractStateRecord(stateClass, contractState);
  }

  /**
   * Gets the state for the Large Oracle contract.
   *
   * @param contractAddress the address of the large oracle contract to look up
   * @return the state for the large oracle contract
   */
  public LargeOracleContractState getLargeOracleState(BlockchainAddress contractAddress) {
    StateSerializable contractState = contractStates.get(contractAddress);
    if (contractState == null) {
      return null;
    }
    StateAccessor loStateAccessor = StateAccessor.create(contractState);
    LargeOracleContractState locState;
    // Determine if current or old version of contract state for backwards compatibility
    Optional<StateAccessorAvlLeafNode> stakedTokensAccessor =
        loStateAccessor.get("stakedTokens").getTreeLeaves().stream().findAny();
    boolean isNewVersion =
        stakedTokensAccessor.isPresent()
            && stakedTokensAccessor.get().getValue().hasField("expirationTimestamp");
    if (isNewVersion) {
      locState = getState(contractAddress, LargeOracleContractState.class);
    } else {
      OldLargeOracleContractState oldVersionState =
          getState(contractAddress, OldLargeOracleContractState.class);
      locState = oldVersionState.upgrade();
    }
    return locState;
  }

  /**
   * Gets the state for a Price Oracle contract.
   *
   * @param blockchainAddress the address of the price oracle contract
   * @return the state for the price oracle contract
   */
  public PriceOracleContractState getPriceOracleState(BlockchainAddress blockchainAddress) {
    StateSerializable contractState = contractStates.get(blockchainAddress);
    if (contractState == null) {
      return null;
    }
    return getState(blockchainAddress, PriceOracleContractState.class);
  }

  private final class ByocContractListener implements Client.Listener {
    private final BlockchainAddress address;

    public ByocContractListener(BlockchainAddress blockchainAddress) {
      this.address = blockchainAddress;
    }

    @Override
    public synchronized void update(StateSerializable stateSerializable) {
      contractStates.put(address, stateSerializable);
      for (Waiter waiter : waiters) {
        waiter.update(address);
      }
    }

    @Override
    public synchronized void remove() {
      contractStates.remove(address);
      for (Waiter waiter : waiters) {
        waiter.update(address);
      }
    }
  }
}
