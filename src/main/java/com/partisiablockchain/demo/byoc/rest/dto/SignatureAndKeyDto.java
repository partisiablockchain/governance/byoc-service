package com.partisiablockchain.demo.byoc.rest.dto;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.Signature;
import com.secata.stream.SafeDataOutputStream;

/** A dto of a signature and an ephemeral key. */
public final class SignatureAndKeyDto {

  /** Signature. */
  public byte[] signature;

  /** Ephemeral key. */
  public byte[] ephemeralKey;

  /**
   * Create a new dto with the supplied signature and key.
   *
   * @param signature the signature to include
   * @param key a public key
   * @return the created dto
   */
  public static SignatureAndKeyDto create(Signature signature, BlockchainPublicKey key) {
    SignatureAndKeyDto dto = new SignatureAndKeyDto();
    dto.signature = SafeDataOutputStream.serialize(signature);
    dto.ephemeralKey = key.getEcPointBytes();
    return dto;
  }
}
