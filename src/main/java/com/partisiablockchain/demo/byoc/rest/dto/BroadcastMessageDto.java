package com.partisiablockchain.demo.byoc.rest.dto;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/** DTO containing a broadcast message. */
public final class BroadcastMessageDto {

  /** The broadcast message. */
  public byte[] message;

  /**
   * Create a new Broadcast message dto.
   *
   * @param message the message
   * @return a dto.
   */
  public static BroadcastMessageDto create(byte[] message) {
    BroadcastMessageDto dto = new BroadcastMessageDto();
    dto.message = message;
    return dto;
  }
}
