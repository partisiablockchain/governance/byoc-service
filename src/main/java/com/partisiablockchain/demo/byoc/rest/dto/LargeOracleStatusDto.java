package com.partisiablockchain.demo.byoc.rest.dto;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.crypto.BlockchainPublicKey;
import java.util.List;

/** DTO for the status of a large oracle node. */
public final class LargeOracleStatusDto {

  /** Current round for the key generation protocol, or -1 if it finished. */
  public int currentKeyGenRound;

  /** Number of pre-signatures that this party has left. */
  public int preSignaturesLeft;

  /** Parties that acted honest doing session. */
  public List<byte[]> honestParties;

  /** Parties that have sent unicast messages to this party. */
  public List<byte[]> unicastResponders;

  /**
   * Create a new dto with information about this node.
   *
   * @param currentKeyGenRound the current round of the key generation protocol
   * @param preSignaturesLeft the number of pre-signatures left
   * @param honestParties parties that acted honest doing the session
   * @param unicastResponders parties that sent a unicast message to this party
   * @return the status DTO
   */
  public static LargeOracleStatusDto create(
      int currentKeyGenRound,
      int preSignaturesLeft,
      List<BlockchainPublicKey> honestParties,
      List<BlockchainPublicKey> unicastResponders) {
    LargeOracleStatusDto dto = new LargeOracleStatusDto();
    dto.currentKeyGenRound = currentKeyGenRound;
    dto.preSignaturesLeft = preSignaturesLeft;
    dto.honestParties = honestParties.stream().map(BlockchainPublicKey::asBytes).toList();
    dto.unicastResponders = unicastResponders.stream().map(BlockchainPublicKey::asBytes).toList();
    return dto;
  }
}
