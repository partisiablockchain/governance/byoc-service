package com.partisiablockchain.demo.byoc.rest.dto;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import java.util.List;

/** DTO with information about a service. */
public final class ResourceStatusDto {

  /** The version of the service. */
  public String versionIdentifier;

  /** The uptime of the service. */
  public long uptime;

  /** The system time of the service. */
  public long systemTime;

  /** A list of the bytes for known peers network keys. */
  public List<byte[]> knownPeersNetworkKeys;

  /** The services blockchain public network key as bytes. */
  public byte[] networkKey;

  /** The services blockchain address as string. */
  public String blockchainAddress;

  /** The services BLS public key as bytes. */
  public byte[] finalizationKey;

  /** The number of processors of the system. */
  public int numberOfProcessors;

  /** The average system load for the last minute. */
  public double systemLoad;

  /** The amount of free memory in the JVM. */
  public long freeMemory;

  /** The total amount of memory in the JVM. */
  public long totalMemory;

  /** The maximum amount of memory the JVM will use. */
  public long maxMemory;

  /** List of status provider information. */
  public List<AdditionalStatusProviderDto> additionalStatus;
}
