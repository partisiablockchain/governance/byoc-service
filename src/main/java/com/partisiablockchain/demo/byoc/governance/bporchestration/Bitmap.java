package com.partisiablockchain.demo.byoc.governance.bporchestration;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.serialization.LargeByteArray;
import com.partisiablockchain.serialization.StateSerializable;
import java.util.Arrays;

/** A bitmap. */
@Immutable
public record Bitmap(LargeByteArray bits) implements StateSerializable {

  /**
   * Create a new bitmap with room for a certain amount of bits.
   *
   * @param size the size of the bitmap in bits
   * @return a bitmap.
   */
  public static Bitmap create(int size) {
    int bytesNeeded = size / 8 + 1;
    return new Bitmap(new LargeByteArray(new byte[bytesNeeded]));
  }

  /**
   * Create a bitmap of all 1s.
   *
   * @param size the number of 1 bits to include in the bitmap
   * @return a bitmap of all 1s.
   */
  static Bitmap allOnes(int size) {
    Bitmap bitmap = create(size);
    for (int i = 0; i < size; i++) {
      bitmap = bitmap.setBit(i);
    }
    return bitmap;
  }

  /**
   * Set a bit in the bitmap.
   *
   * @param index the index to set
   * @return the array with the index bit set.
   */
  public Bitmap setBit(int index) {
    byte[] data = bits.getData();
    setBit(index, data);
    return new Bitmap(new LargeByteArray(data));
  }

  private static void setBit(int bitIndex, byte[] bitArray) {
    int blockIndex = bitIndex / 8;
    int blockOffset = bitIndex % 8;
    bitArray[blockIndex] = (byte) (bitArray[blockIndex] | (1 << blockOffset));
  }

  /**
   * Test if a particular bit is set.
   *
   * @param index the index of the bit
   * @return true if the bit is set and false otherwise.
   */
  public boolean testBit(int index) {
    return testBit(bits.getData(), index);
  }

  private static boolean testBit(byte[] bits, int bitIndex) {
    byte block = bits[bitIndex >>> 3];
    int blockOffset = bitIndex % 8;
    return ((block >> blockOffset) & 1) == 1;
  }

  /**
   * Count the number of set bits in this bitmap.
   *
   * @return the population count.
   */
  public int popCount() {
    int count = 0;
    byte[] bitmap = bits.getData();
    for (byte block : bitmap) {
      // cast the block byte to an unsigned int before passing to bitCount to avoid incorrect
      // results that would arise when block is sign-extended.
      count += Integer.bitCount(((int) block) & 255);
    }
    return count;
  }

  Bitmap and(Bitmap other) {
    byte[] conj = new byte[bits.getLength()];
    byte[] left = asBytes();
    byte[] right = other.asBytes();
    for (int i = 0; i < bits.getLength(); i++) {
      conj[i] = (byte) (left[i] & right[i]);
    }
    return new Bitmap(new LargeByteArray(conj));
  }

  /**
   * Return this bitmap as a byte array.
   *
   * @return this bitmap as a byte array
   */
  public byte[] asBytes() {
    return bits.getData();
  }

  @Override
  public String toString() {
    return "Bytes " + Arrays.toString(bits.getData());
  }
}
