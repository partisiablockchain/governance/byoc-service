package com.partisiablockchain.demo.byoc.governance.bporchestration;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.contract.EventManager;
import com.partisiablockchain.crypto.Hash;
import java.util.ArrayList;
import java.util.List;

/** MerkleTree. */
final class MerkleTree {

  private MerkleTree() {}

  /**
   * Build a new merkle tree from the list of elements. The hashes of the tree will be pairwise
   * sorted before they are combined.
   *
   * @param leaves the leaf elements of the tree.
   * @return a new MerkleTree represented by the root.
   */
  public static Hash buildTreeFrom(List<Hash> leaves) {
    return recursivelyBuildTreeFrom(leaves);
  }

  private static Hash recursivelyBuildTreeFrom(List<Hash> leaves) {
    if (leaves.size() == 0) {
      return Hash.create(EventManager.EMPTY_RPC);
    }
    if (leaves.size() == 1) {
      return leaves.get(0);
    } else {
      List<Hash> nextLevel = new ArrayList<>();
      int i = 0;
      while (i + 1 < leaves.size()) {
        int idx = i;
        Hash leftLeaf = leaves.get(idx);
        Hash rightLeaf = leaves.get(idx + 1);
        Hash nextLeaf = combineLeaves(leftLeaf, rightLeaf);
        nextLevel.add(nextLeaf);
        i = i + 2;
        // If leaves have an odd number of hashes, include the last one in the next level.
        if (i == leaves.size() - 1) {
          nextLevel.add(leaves.get(i));
        }
      }
      return recursivelyBuildTreeFrom(nextLevel);
    }
  }

  private static Hash combineLeaves(Hash leftLeaf, Hash rightLeaf) {
    // Ensure that the hash pairs are sorted
    // (in the same manner that ethereum/solidity compares bytes32)
    int comparison = leftLeaf.toString().compareToIgnoreCase(rightLeaf.toString());
    if (comparison < 0) {
      return Hash.create(
          stream -> {
            leftLeaf.write(stream);
            rightLeaf.write(stream);
          });
    } else if (comparison > 0) {
      return Hash.create(
          stream -> {
            rightLeaf.write(stream);
            leftLeaf.write(stream);
          });
    } else {
      throw new RuntimeException("Two element's digests were the same");
    }
  }
}
