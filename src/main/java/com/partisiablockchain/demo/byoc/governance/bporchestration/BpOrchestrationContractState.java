package com.partisiablockchain.demo.byoc.governance.bporchestration;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.serialization.StateSerializable;
import com.partisiablockchain.tree.AvlTree;
import com.secata.tools.immutable.FixedList;
import java.util.List;

/** State for the BpOrchestrationContract. */
@Immutable
public record BpOrchestrationContractState(
    int sessionId,
    int retryNonce,
    ThresholdKey thresholdKey,
    LargeOracleUpdate oracleUpdate,
    AvlTree<BlockchainAddress, BlockProducer> blockProducers,
    FixedList<BlockProducer> committee,
    Hash domainSeparator)
    implements StateSerializable {

  /**
   * Returns all confirmed block producers in state.
   *
   * @return confirmed block producers
   */
  public List<BlockProducer> getConfirmedBlockProducers() {
    return blockProducers.values();
  }
}
