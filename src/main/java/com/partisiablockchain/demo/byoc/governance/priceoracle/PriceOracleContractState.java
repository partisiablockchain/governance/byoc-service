package com.partisiablockchain.demo.byoc.governance.priceoracle;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.math.Unsigned256;
import com.partisiablockchain.serialization.StateSerializable;
import com.partisiablockchain.tree.AvlTree;

/** State of the price oracle contract. */
@Immutable
public record PriceOracleContractState(
    BlockchainAddress largeOracleContractAddress,
    String referenceContractAddress,
    RoundPriceUpdates roundPriceUpdates,
    Unsigned256 latestPublishedRoundId,
    String symbol,
    PriceOracleNodes oracleNodes,
    ChallengePeriod challengePeriod,
    DisputeInfo disputeInfo,
    String chainId)
    implements StateSerializable {

  /**
   * Checks if the node address is registered as a price oracle node with status {@link
   * RegistrationStatus#REGISTERED}.
   *
   * @param nodeAddress the node address to check
   * @return true if the running node is {@link RegistrationStatus#REGISTERED} otherwise false
   */
  @SuppressWarnings("BooleanMethodIsAlwaysInverted")
  public boolean isRegisteredPriceOracleNode(BlockchainAddress nodeAddress) {
    AvlTree<BlockchainAddress, RegistrationStatus> nodes = oracleNodes().nodes();
    return nodes.containsKey(nodeAddress)
        && nodes.getValue(nodeAddress) == RegistrationStatus.REGISTERED;
  }
}
