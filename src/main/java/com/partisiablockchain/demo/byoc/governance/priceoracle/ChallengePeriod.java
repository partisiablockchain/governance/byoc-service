package com.partisiablockchain.demo.byoc.governance.priceoracle;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.math.Unsigned256;
import com.partisiablockchain.serialization.StateSerializable;
import java.time.Duration;
import java.time.temporal.ChronoUnit;

/**
 * A period started immediately after a price has been agreed upon for a round, that allows the
 * price to be challenged. Holds the round id of the round it was started for and the block
 * production time for when it was started at.
 */
@Immutable
public record ChallengePeriod(Unsigned256 roundId, long startedAt) implements StateSerializable {

  /** Copied from {@code PriceOracleContractState#CHALLENGE_PERIOD_DURATION_MILLIS}. */
  public static final long CHALLENGE_PERIOD_DURATION_MILLIS =
      Duration.of(1, ChronoUnit.HOURS).toMillis();

  /**
   * Checks if the challenge period has ended relative to the supplied system time.
   *
   * @param systemTime the system time
   * @return true if the system time is greater than the end timestamp of the challenge period
   */
  public boolean hasEnded(long systemTime) {
    long challengePeriodEnd = startedAt() + CHALLENGE_PERIOD_DURATION_MILLIS;
    return challengePeriodEnd < systemTime;
  }
}
