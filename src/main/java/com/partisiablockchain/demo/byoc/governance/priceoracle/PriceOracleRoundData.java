package com.partisiablockchain.demo.byoc.governance.priceoracle;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.demo.byoc.monitor.RoundData;
import com.partisiablockchain.math.Unsigned256;
import com.partisiablockchain.serialization.StateSerializable;

/** Round data in the price oracle contract state. */
@Immutable
public record PriceOracleRoundData(
    Unsigned256 roundId,
    Unsigned256 answer,
    Unsigned256 startedAt,
    Unsigned256 updatedAt,
    Unsigned256 answeredInRound)
    implements StateSerializable {

  /**
   * Converts a {@link RoundData} to a {@link PriceOracleRoundData}.
   *
   * @param roundData the {@link RoundData} to convert
   * @return the converted {@link PriceOracleRoundData}
   */
  public static PriceOracleRoundData convert(RoundData roundData) {
    return new PriceOracleRoundData(
        roundData.roundId(),
        roundData.answer(),
        roundData.startedAt(),
        roundData.updatedAt(),
        roundData.answeredInRound());
  }
}
