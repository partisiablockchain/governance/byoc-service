package com.partisiablockchain.demo.byoc.governance.largeoracle;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.serialization.StateSerializable;
import com.secata.tools.immutable.FixedList;

/** A dispute on the large oracle contract. */
@Immutable
public record Dispute(FixedList<Integer> votes) implements StateSerializable {

  /**
   * Checks if the committee member with the specified index has voted for the dispute.
   *
   * @param memberIdx committee member index
   * @return true if the member has voted, otherwise false
   */
  public boolean hasVoted(int memberIdx) {
    return votes().get(memberIdx) != null;
  }
}
