package com.partisiablockchain.demo.byoc.governance.byocoutgoing;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.math.Unsigned256;
import com.partisiablockchain.serialization.StateSerializable;
import com.secata.tools.immutable.FixedList;

/**
 * A claim to dispute that a certain withdrawal of BYOC twins is fraudulent. Consists of both the
 * claimant, the original claim and counter-claims if such have been presented.
 */
@Immutable
public record Dispute(FixedList<Transaction> claims) implements StateSerializable {

  /** A class representing a BYOC transaction. */
  @Immutable
  public record Transaction(EthereumAddress receiver, Unsigned256 amount, int bitmask)
      implements StateSerializable {}
}
