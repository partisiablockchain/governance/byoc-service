package com.partisiablockchain.demo.byoc.governance.byocincoming;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.math.Unsigned256;
import com.partisiablockchain.serialization.StateSerializable;

/** Class representing the state of an incoming transfer. */
@Immutable
public record Deposit(BlockchainAddress receiver, Unsigned256 amount, int signaturesRegistered)
    implements StateSerializable {

  /**
   * Check if node has signed.
   *
   * @param nodeIndex node to check
   * @return true if node has signed
   */
  public boolean hasSigned(int nodeIndex) {
    return (this.signaturesRegistered & 1L << nodeIndex) != 0L;
  }
}
