package com.partisiablockchain.demo.byoc.governance.largeoracle;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.serialization.StateSerializable;

/** Container for a message hash, a nonce and the transaction hash. */
@Immutable
public record SigningRequest(Integer nonce, Hash messageHash, Hash transactionHash)
    implements StateSerializable, Comparable<SigningRequest> {

  @Override
  public int compareTo(SigningRequest other) {
    int nonceCompare = other.nonce().compareTo(nonce);
    if (nonceCompare == 0) {
      int hashCompare = other.messageHash.compareTo(messageHash);
      if (hashCompare == 0) {
        if (transactionHash == null && other.transactionHash != null) {
          return -1;
        }
        if (transactionHash == null) {
          return 0;
        }
        return other.transactionHash.compareTo(transactionHash);
      }
      return hashCompare;
    }
    return nonceCompare;
  }
}
