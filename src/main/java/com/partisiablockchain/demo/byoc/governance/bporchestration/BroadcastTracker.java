package com.partisiablockchain.demo.byoc.governance.bporchestration;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.serialization.StateSerializable;
import com.partisiablockchain.tree.AvlTree;
import java.util.List;

/** Keeps track of broadcast messages with unique senders. */
@Immutable
public record BroadcastTracker(
    AvlTree<BlockchainAddress, Hash> messages,
    AvlTree<BlockchainAddress, Bitmap> bitmaps,
    Bitmap echos)
    implements StateSerializable {

  /**
   * Get the bitmap of the message sent by a producer.
   *
   * @param producer the producer
   * @return the bitmap or null if the producer did not send a message.
   */
  public Bitmap getMessageBitmap(BlockchainAddress producer) {
    return bitmaps.getValue(producer);
  }

  /**
   * Get the hash of a message from some sender.
   *
   * @param sender the sender
   * @return the message, or null if non were present.
   */
  public Hash getMessage(BlockchainAddress sender) {
    return messages.getValue(sender);
  }

  /**
   * Creates a bitmap indicating which producers have successfully broadcast a message. A party is
   * considered a broadcaster if (1) it has sent a message, and (2) at least t+1 other parties also
   * store this message. t is defined as (n - 1)/3 where n is the number of producers.
   *
   * @param producers a list of producers
   * @return a bitmap.
   */
  Bitmap getBroadcastersBitmap(List<BlockProducer> producers) {
    int threshold = (producers.size() - 1) / 3;
    Bitmap bitmap = Bitmap.create(producers.size());
    for (int i = 0; i < producers.size(); i++) {
      BlockchainAddress producer = producers.get(i).identity();
      Hash message = messages.getValue(producer);
      Bitmap messageBitmap = bitmaps.getValue(producer);
      if (message != null && messageBitmap.popCount() > threshold) {
        bitmap = bitmap.setBit(i);
      }
    }
    return bitmap;
  }
}
