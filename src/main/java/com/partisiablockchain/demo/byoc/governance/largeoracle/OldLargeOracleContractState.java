package com.partisiablockchain.demo.byoc.governance.largeoracle;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.Signature;
import com.partisiablockchain.serialization.StateSerializable;
import com.partisiablockchain.tree.AvlTree;
import com.secata.tools.immutable.FixedList;

/** Previous version of the large oracle contract state. Used for backwards compatibility. */
@Immutable
public record OldLargeOracleContractState(
    FixedList<OracleMember> oracleMembers,
    AvlTree<BlockchainAddress, OldStakedTokens> stakedTokens,
    FixedList<SigningRequest> pendingMessages,
    Integer currentMessageNonce,
    Integer initialMessageNonce,
    AvlTree<SigningRequest, Signature> signedMessages,
    AvlTree<OracleDisputeId, Dispute> activeDisputes)
    implements StateSerializable {

  /**
   * Upgrade to the current version of the large oracle contract state.
   *
   * <p>Converts {@link OldStakedTokens} to {@link StakedTokens} by introducing a
   * 'expirationTimestamp' field initialized to null (no expiration).
   *
   * @return an upgraded state
   */
  public LargeOracleContractState upgrade() {
    AvlTree<BlockchainAddress, StakedTokens> newStakedTokens = AvlTree.create();
    for (BlockchainAddress address : stakedTokens.keySet()) {
      long currentFreeTokens = stakedTokens.getValue(address).freeTokens();
      long currentReservedTokens = stakedTokens.getValue(address).reservedTokens();
      newStakedTokens =
          newStakedTokens.set(
              address, new StakedTokens(currentFreeTokens, currentReservedTokens, null));
    }

    return new LargeOracleContractState(
        oracleMembers(),
        newStakedTokens,
        pendingMessages(),
        currentMessageNonce(),
        initialMessageNonce(),
        signedMessages(),
        activeDisputes());
  }

  /** Previous version of the {@link StakedTokens} state. Used for backwards compatibility. */
  @Immutable
  public record OldStakedTokens(long freeTokens, long reservedTokens) {}
}
