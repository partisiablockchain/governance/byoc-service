package com.partisiablockchain.demo.byoc.governance.largeoracle;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.Signature;
import com.partisiablockchain.serialization.StateSerializable;
import com.partisiablockchain.tree.AvlTree;
import com.secata.tools.immutable.FixedList;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/** Large oracle contract state. */
@Immutable
public record LargeOracleContractState(
    FixedList<OracleMember> oracleMembers,
    AvlTree<BlockchainAddress, StakedTokens> stakedTokens,
    FixedList<SigningRequest> pendingMessages,
    Integer currentMessageNonce,
    Integer initialMessageNonce,
    AvlTree<SigningRequest, Signature> signedMessages,
    AvlTree<OracleDisputeId, Dispute> activeDisputes)
    implements StateSerializable {

  /**
   * Get the index of an oracle member.
   *
   * @param address the address of the oracle member
   * @return the index of the oracle member or -1 if not found.
   */
  public int getOracleMemberIndex(BlockchainAddress address) {
    int index = 0;
    for (OracleMember member : Objects.requireNonNull(oracleMembers)) {
      if (member.address().equals(address)) {
        return index;
      }
      index++;
    }
    return -1;
  }

  /**
   * Get tokens staked by account.
   *
   * @param account account address
   * @return tokens staked by account
   */
  public StakedTokens getTokensStakedBy(BlockchainAddress account) {
    return Objects.requireNonNullElse(
        stakedTokens.getValue(account), new StakedTokens(0L, 0L, null));
  }

  /**
   * Get signed messages.
   *
   * @return list of signed messages
   */
  public List<SigningRequest> getSignedMessages() {
    return new ArrayList<>(Objects.requireNonNull(signedMessages).keySet());
  }
}
