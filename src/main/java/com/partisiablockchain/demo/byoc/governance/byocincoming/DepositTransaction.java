package com.partisiablockchain.demo.byoc.governance.byocincoming;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.math.Unsigned256;
import com.secata.tools.immutable.FixedList;
import java.util.List;

/** Representation of a deposit transaction. */
public record DepositTransaction(long nonce, BlockchainAddress receiver, Unsigned256 amount) {

  /**
   * Create transaction from a deposit.
   *
   * @param nonce the nonce of the deposit
   * @param deposit the deposit
   * @return new transaction
   */
  public static DepositTransaction create(long nonce, Deposit deposit) {
    return new DepositTransaction(nonce, deposit.receiver(), deposit.amount());
  }

  private static DepositTransaction create(Dispute.Transaction transaction) {
    return new DepositTransaction(
        transaction.nonce(), transaction.receiver(), transaction.amount());
  }

  /**
   * Create a list of transactions from a list of dispute transactions.
   *
   * @param transactions the dispute transactions
   * @return new list of transactions
   */
  public static List<DepositTransaction> createList(FixedList<Dispute.Transaction> transactions) {
    return transactions.stream().map(DepositTransaction::create).toList();
  }
}
