package com.partisiablockchain.demo.byoc.governance.byocoutgoing;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.math.Unsigned256;
import java.util.Objects;
import org.bouncycastle.util.encoders.Hex;

/** Representation of a withdrawal transaction. */
public record WithdrawalTransaction(
    long withdrawalNonce, String destination, Unsigned256 amount, long oracleNonce, int bitmask) {

  /**
   * Create new withdrawal transaction.
   *
   * @param nonce withdrawal nonce
   * @param oracleNonce oracle nonce
   * @param withdrawal the withdrawal
   * @return new withdrawal transaction
   */
  public static WithdrawalTransaction create(long nonce, long oracleNonce, Withdrawal withdrawal) {
    String destination = Hex.toHexString(withdrawal.receiver().identifier().getData());
    return new WithdrawalTransaction(
        nonce, destination, withdrawal.amountWithoutTax(), oracleNonce, 0b000);
  }

  /**
   * Check if this withdrawal transaction is equals to another when ignoring the signer bitmask.
   * Needed for checking equality between a transaction on PBC vs one from an external source.
   *
   * @param other withdrawal to check against
   * @return true if other is equals to this when ignoring bitmask, false otherwise
   */
  public boolean equalsIgnoringBitmask(WithdrawalTransaction other) {
    if (other == null) {
      return false;
    }
    return withdrawalNonce == other.withdrawalNonce
        && oracleNonce == other.oracleNonce
        && Objects.equals(destination, other.destination)
        && Objects.equals(amount, other.amount);
  }
}
