package com.partisiablockchain.demo.byoc.governance.byocoutgoing;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.crypto.Signature;
import com.partisiablockchain.math.Unsigned256;
import com.partisiablockchain.serialization.StateSerializable;
import com.secata.tools.immutable.FixedList;

/** An outgoing transfer pending the oracles' signatures. */
@Immutable
public record Withdrawal(
    EthereumAddress receiver, Unsigned256 amount, FixedList<Signature> signatures)
    implements StateSerializable {

  /**
   * Returns the amount with tax subtracted.
   *
   * @return amount without tax.
   */
  public Unsigned256 amountWithoutTax() {
    return amount.subtract(getTax());
  }

  Unsigned256 getTax() {
    return amount
        .multiply(Unsigned256.create(1)) // BYOC_TAX_PERMIL in outgoing contract
        .divide(Unsigned256.create(1000));
  }
}
