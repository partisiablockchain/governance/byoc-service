package com.partisiablockchain.demo.byoc.governance.priceoracle;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.serialization.StateSerializable;

/**
 * Holds information about the dispute, whether it is pending or not, and if a dispute has been
 * queued in the meantime. A dispute is pending while waiting for the callback of the invocation to
 * the large oracle contract. If a dispute should is started while a dispute is currently pending
 * the new dispute is instead queued.
 */
@Immutable
public record DisputeInfo(PriceOracleDispute dispute, boolean pending, PriceOracleDispute queued)
    implements StateSerializable {}
