package com.partisiablockchain.demo.byoc.governance.bporchestration;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.BlockchainPublicKey;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.serialization.StateSerializable;
import com.partisiablockchain.tree.AvlTree;
import com.secata.tools.immutable.FixedList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/** Container for information about a large oracle update. */
@Immutable
public record LargeOracleUpdate(
    FixedList<BlockProducer> producers,
    ThresholdKey thresholdKey,
    FixedList<CandidateKey> candidates,
    Bitmap voters,
    AvlTree<BlockchainAddress, Bitmap> heartbeats,
    Bitmap activeProducers,
    int retryNonce,
    FixedList<BroadcastTracker> broadcastMessages,
    Hash signatureRandomization)
    implements StateSerializable {

  /**
   * Get the current broadcast round.
   *
   * @return the broadcast round.
   */
  public int getCurrentBroadcastRound() {
    return broadcastMessages.size();
  }

  /**
   * Get the broadcast tracker object for a particular round. Trackers are 1 indexed.
   *
   * @param roundNumber the round number
   * @return the round number.
   */
  public BroadcastTracker getBroadcastTracker(int roundNumber) {
    return broadcastMessages.get(roundNumber - 1);
  }

  /**
   * Get the list of producers determined to be active.
   *
   * @return a list of active producers.
   */
  public List<BlockProducer> getActiveProducers() {
    if (activeProducers == null) {
      return null;
    } else {
      return filterProducersWithBitmap(List.copyOf(producers), activeProducers);
    }
  }

  private List<BlockProducer> filterProducersWithBitmap(
      List<BlockProducer> producers, Bitmap bitmap) {
    return IntStream.range(0, producers.size())
        .filter(bitmap::testBit)
        .mapToObj(producers::get)
        .toList();
  }

  /**
   * Get the list of producers which successfully broadcast a message in some round.
   *
   * @param roundNumber the round number
   * @return a list of producers.
   */
  public List<BlockProducer> getBroadcasters(int roundNumber) {
    if (activeProducers == null) {
      return null;
    } else {
      List<BlockProducer> activeProducers = getActiveProducers();
      Bitmap current = getBroadcastTracker(roundNumber).getBroadcastersBitmap(activeProducers);
      Bitmap previous = Bitmap.allOnes(activeProducers.size());
      if (roundNumber > 1) {
        previous = getBroadcastTracker(roundNumber - 1).getBroadcastersBitmap(activeProducers);
      }
      return filterProducersWithBitmap(activeProducers, current.and(previous));
    }
  }

  /**
   * Create the hash that current oracle needs to sign in order to validate an incoming oracle.
   *
   * @param nextSessionId the identifier for the next session
   * @param domainSeparator a domain separator
   * @return a hash to be signed by the large oracle.
   */
  public Hash getMessageToBeSigned(int nextSessionId, Hash domainSeparator) {
    Hash merkleRootOfActiveProducers = getMerkleRootOfActiveProducers();
    return Hash.create(
        s -> {
          domainSeparator.write(s);
          s.writeLong(nextSessionId);
          s.writeInt(getActiveProducers().size());
          merkleRootOfActiveProducers.write(s);
          s.write(getEncodedWithoutHeader(thresholdKey.key()));
        });
  }

  Hash getMerkleRootOfActiveProducers() {
    List<Hash> activeProducersHashes =
        getActiveProducers().stream()
            .map(bp -> Hash.create(s -> s.write(getEncodedWithoutHeader(bp.publicKey()))))
            .collect(Collectors.toList());
    Collections.sort(
        activeProducersHashes, (h1, h2) -> h1.toString().compareToIgnoreCase(h2.toString()));
    return MerkleTree.buildTreeFrom(activeProducersHashes);
  }

  private static byte[] getEncodedWithoutHeader(BlockchainPublicKey publicKey) {
    byte[] encoded = publicKey.getEcPointBytes();
    byte[] headless = new byte[encoded.length - 1];
    System.arraycopy(encoded, 1, headless, 0, headless.length);
    return headless;
  }

  /**
   * Get the heartbeat published for a particular producer.
   *
   * @param address the address of the producer
   * @return the heartbeat published by a producer.
   */
  public Bitmap getHeartbeat(BlockchainAddress address) {
    return heartbeats.getValue(address);
  }
}
