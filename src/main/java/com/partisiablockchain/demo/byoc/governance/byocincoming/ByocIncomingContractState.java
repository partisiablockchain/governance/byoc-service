package com.partisiablockchain.demo.byoc.governance.byocincoming;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.serialization.StateSerializable;
import com.partisiablockchain.tree.AvlTree;
import com.secata.tools.immutable.FixedList;

/** State for incoming BYOC transfers. */
@Immutable
public record ByocIncomingContractState(
    long depositNonce,
    AvlTree<Long, Deposit> deposits,
    FixedList<BlockchainAddress> oracleNodes,
    Deposit deposit,
    AvlTree<Long, Dispute> disputes,
    long oracleNonce,
    AvlTree<Long, Epoch> epochs)
    implements StateSerializable {}
