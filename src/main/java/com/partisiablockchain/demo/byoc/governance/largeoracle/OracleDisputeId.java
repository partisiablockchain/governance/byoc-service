package com.partisiablockchain.demo.byoc.governance.largeoracle;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.serialization.StateSerializable;
import java.util.Objects;

/** Id to determine a dispute. */
@Immutable
public record OracleDisputeId(BlockchainAddress smallOracle, long oracleId, long disputeId)
    implements Comparable<OracleDisputeId>, StateSerializable {

  @Override
  public int compareTo(OracleDisputeId o) {
    int addressCompare = Objects.requireNonNull(smallOracle).compareTo(o.smallOracle());
    if (addressCompare != 0) {
      return addressCompare;
    }
    int oracleCompare = Long.compare(oracleId, o.oracleId());
    if (oracleCompare != 0) {
      return oracleCompare;
    }
    return Long.compare(disputeId, o.disputeId());
  }
}
