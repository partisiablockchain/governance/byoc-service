# BYOC Service

Implementations of vital node-based governance components for Partisia Blockchain.

Components implemented here:

- **Price Oracle**: Oracle tranferring price information from
  a [Chainlink](https://chain.link/) Data Feed to the Partisia Blockchain.
  Includes dispute processes to identify malicious oracles.
- **Bridging**: System for transferring coins from other chains to Partisia
  Blockchain. Includes deposit, withdraw and dispute processes.
- **Large Oracle**: Node-based components of the system that selects the
  Block Producer Node Committee.
- **Peer Discovery**: System for identifying nodes to connect to.
- **Status**: Status endpoints for node health.

Not all components are run by all nodes, due to various staking mechanisms.
